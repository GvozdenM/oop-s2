package ChatView;

import Utils.Swing.Image.ImagePanel;

import javax.swing.*;
import java.awt.*;
import java.net.MalformedURLException;
import java.net.URL;

public class SwingChatViewHeaderPanel extends JPanel {
    private ImagePanel partnerImg;
    private final JLabel partnerName;

    public SwingChatViewHeaderPanel(ImagePanel partnerImg, JLabel partnerName) {
        this.partnerImg = partnerImg;
        this.partnerName = partnerName;

        configurePanel();
    }

    public SwingChatViewHeaderPanel() {
        this.partnerImg = new ImagePanel();
        this.partnerName = new JLabel();

        configurePanel();
    }

    private void setupImgSize() {
        this.partnerImg.setImgSize(50, 50);
    }

    private void configurePanel() {
        int paddingSides = 20;
        int paddingTop = 10;

        this.setLayout(new FlowLayout(FlowLayout.LEADING, paddingSides, paddingTop));

        setupImgSize();

        this.add(this.partnerImg);
        this.add(this.partnerName);
    }

    public void setPartnerImgPanel(ImagePanel userImg) {
        this.partnerImg = userImg;
        setupImgSize();
        this.revalidate();
    }

    public void setPartnerImgUrl(String userImgUrl) {
        try {
            this.partnerImg.load(
                    new URL(userImgUrl)
            );
            setupImgSize();
        } catch (MalformedURLException ignore) {}
    }

    public void setPartnerName(String partnerName) {
        this.partnerName.setText(partnerName);
        this.revalidate();
    }
}
