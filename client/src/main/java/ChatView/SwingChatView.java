package ChatView;

import ChatController.AbstractChatControllerFactory;
import ChatController.ChatControllerI;
import ChatPresenter.ChatMessageViewModel;
import ChatPresenter.ChatView;
import UserSession.UserSessionManager;
import Utils.Swing.FrameManager.FrameManager;
import Utils.Swing.Image.ImagePanel;
import Utils.Views.ResourceFetcher;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;

/**
 * This class is the main ChatView view panel implementation as well as the implementation of given parent interface.
 * @see ChatView
 */
public class SwingChatView extends JPanel implements ChatView {
    private FrameManager chatFrameManager;

    private final ChatControllerI listenerI;

    private final SwingChatViewHeaderPanel headerPanel;
    private final SwingChatViewMsgPanel msgPanel;
    private final SwingChatViewInputPanel inputPanel;

    private final ImagePanel userImg;
    private final ImagePanel partnerImg;

    public SwingChatView(Long receiverID, String receiverName, String partnerImgUrl, AbstractChatControllerFactory controllerFactory) {
        this.listenerI = controllerFactory.create();
        listenerI.setPartnerID(receiverID);

        this.setSize(
                new Dimension(600, 600)
        );

        Dimension imgDim = new Dimension(45, 45);
        this.userImg = new ImagePanel();
        this.partnerImg = new ImagePanel();
        this.userImg.setImgSize(imgDim.width, imgDim.height);
        this.partnerImg.setImgSize(imgDim.width, imgDim.height);

        setImageByUrl(this.userImg, UserSessionManager.getUserProfileImgUrl());
        setImageByUrl(this.partnerImg, partnerImgUrl);

        this.headerPanel = new SwingChatViewHeaderPanel();
        this.headerPanel.setPartnerImgUrl(partnerImgUrl);
        this.headerPanel.setPartnerName(receiverName);
        this.msgPanel = new SwingChatViewMsgPanel(this.userImg, this.partnerImg);
        this.inputPanel = new SwingChatViewInputPanel();


        configurePanel();
        attachListeners();
    }

    private void configurePanel() {
        this.setLayout(new BorderLayout());

        this.add(this.headerPanel, BorderLayout.NORTH);
        this.add(this.msgPanel, BorderLayout.CENTER);
        this.add(this.inputPanel, BorderLayout.SOUTH);

        this.setVisible(true);

    }


    private void attachListeners() {
        this.inputPanel.attachSendBtnListener(
                l -> this.listenerI.onSend(this.inputPanel.getMsgText())
        );
    }

    @Override
    public void displayChat(Collection<ChatMessageViewModel> viewModels) {
        this.msgPanel.addMessages(viewModels);
    }

    @Override
    public void displayNewMessage(ChatMessageViewModel viewModel) {
        this.msgPanel.addMessage(viewModel);
    }

    @Override
    public void setMessageStateToSent(String hashId) {

    }

    @Override
    public void updateHeaderContent(String partnerName, String partnerImgUrl) {
        this.headerPanel.setPartnerName(partnerName);
        this.headerPanel.setPartnerImgUrl(partnerImgUrl);
    }

    @Override
    public void displayMsgDialog(String msg) {
        SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(
                new JFrame(),
                msg,
                "Warning",
                JOptionPane.WARNING_MESSAGE
        ));
    }

    private void setImageByUrl(ImagePanel img, String imgUrl) {
        try {
            img.load(new URL(imgUrl));
        } catch (MalformedURLException e) {
            e.printStackTrace();
            ResourceFetcher rf = new ResourceFetcher("defaultuserimg.png");
            try {
                img.load(rf.getResourceFile().getPath());
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }

    @Override
    public void setUserImgUrl(String imgUrl) {
        setImageByUrl(this.userImg, imgUrl);
        this.msgPanel.setUserImg(this.userImg);
    }

    @Override
    public void end() {
        this.chatFrameManager.closeFrame();
    }

    @Override
    public void clearMessageInput() {
        this.inputPanel.clearMsg();
    }

    @Override
    public void onTransitionTo() {
        ResourceFetcher resourceFetcher = new ResourceFetcher("favicon.jpg");

        this.chatFrameManager = ChatFrameManagerFactory.create();
        try {
            this.chatFrameManager
                    .configureFramePreferences("Chat", this, getWidth(), getHeight(), FrameManager.CENTER)
                    .setResizable(false)
                    .setCloseProcedure(this.listenerI::onWindowClose, false)
                    .setFavicon(resourceFetcher.getResourceFile().getPath());
            this.listenerI.onLoad();
        } catch (IOException ignore) {}
    }
}
