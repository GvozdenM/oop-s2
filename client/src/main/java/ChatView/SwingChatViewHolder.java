package ChatView;

import Utils.Swing.Image.ImagePanel;
import Utils.Swing.ListView.Interface.ListViewHolder;

import javax.swing.*;

public abstract class SwingChatViewHolder extends ListViewHolder {
    protected final int PADDING_VERTICALLY = 20;
    protected final int PADDING_HORIZONTALLY = 20;

    protected ImagePanel image;
    protected JTextArea message;

    public abstract void specifyPanel();

    @Override
    public void configurePanel() {
        if (this.image == null) {
            this.image = new ImagePanel();
            this.message = new JTextArea();

            setupMessageArea();
        }

        specifyPanel();
    }

    private void setupMessageArea() {
        this.message.setEditable(false);
        this.message.setCursor(null);
        this.message.setFocusable(false);
        this.message.setWrapStyleWord(true);
        this.message.setLineWrap(true);
        this.message.setColumns(20);


        short padding = 10;
        this.message.setBorder(BorderFactory.createCompoundBorder(
                this.message.getBorder(),
                BorderFactory.createEmptyBorder(padding, padding, padding, padding)
        ));
    }

    public ImagePanel getImage() {
        return this.image;
    }

    public void setImage(ImagePanel image) {
        this.image = image;
        this.image.revalidate();
        configurePanel();
        this.revalidate();
    }

    public String getMessage() {
        return this.message.getText();
    }

    public void setMessage(String message) {
        this.message.setText(message);
    }
}
