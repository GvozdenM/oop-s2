package ChatView;

import ChatPresenter.ChatMessageViewModel;
import Utils.Swing.Image.ImagePanel;
import Utils.Swing.ListView.CachedListViewBuilder;
import Utils.Swing.ListView.ListView;

import javax.swing.*;
import java.awt.*;
import java.util.Collection;

public class SwingChatViewMsgPanel extends JPanel {
    private final CachedListViewBuilder cachedListViewBuilder;

    private final SwingChatListViewAdapter listViewAdapter;

    private ListView chatMessages;

    public SwingChatViewMsgPanel(ImagePanel userImg, ImagePanel partnerImg) {
        final int IMG_DIM_W = 20;
        final int IMG_DIM_H = 20;

        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        userImg.setImgSize(IMG_DIM_W, IMG_DIM_H);
        partnerImg.setImgSize(IMG_DIM_W, IMG_DIM_H);



        this.cachedListViewBuilder = new CachedListViewBuilder();
        this.listViewAdapter = new SwingChatListViewAdapter(userImg, partnerImg);

        this.setLayout(new GridLayout(1, 1));
        this.setVisible(true);
    }

    public void setUserImg(ImagePanel userImg) {
        this.listViewAdapter.setUserImg(userImg);
    }

    public void addMessage(ChatMessageViewModel messageViewModel) {
        this.removeAll();

        this.listViewAdapter.addMessage(messageViewModel);
        this.chatMessages = this.cachedListViewBuilder
                .setListViewAdapter(this.listViewAdapter)
                .build();

        this.chatMessages.scrollToBottom();

        this.revalidate();
        this.add(this.chatMessages);
    }

    public void addMessages(Collection<ChatMessageViewModel> messageViewModels) {
        this.removeAll();

        this.listViewAdapter.addMessages(messageViewModels);
        this.chatMessages = this.cachedListViewBuilder
                .setListViewAdapter(this.listViewAdapter)
                .build();

        this.chatMessages.scrollToBottom();

        this.revalidate();
        this.add(this.chatMessages);
    }
}
