package ChatView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class SwingChatViewInputPanel extends JPanel {
    private final JButton sendBtn;
    private final JTextField messageField;


    public SwingChatViewInputPanel() {
        this.sendBtn = new JButton();
        this.messageField = new JTextField();

        configurePanel();
    }

    private void configurePanel() {
        this.setLayout(new BorderLayout());
        this.add(this.messageField, BorderLayout.CENTER);
        this.add(this.sendBtn, BorderLayout.EAST);
    }

    public void attachSendBtnListener(ActionListener listener) {
        this.sendBtn.addActionListener(listener);
    }

    public String getMsgText() {
        return this.messageField.getText();
    }

    public void clearMsg() {
        this.messageField.setText("");
    }
}
