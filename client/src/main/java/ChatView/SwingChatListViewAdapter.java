package ChatView;

import ChatPresenter.ChatMessageViewModel;
import Utils.Swing.Image.ImagePanel;
import Utils.Swing.ListView.Interface.ListViewAdapter;
import Utils.Swing.ListView.Interface.ListViewHolder;

import java.awt.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

/**
 * This class provides a link between the connection of an array of messages to a Swing view implemented in the
 * ChatView section.
 * @see ChatPresenter.ChatView
 */
public class SwingChatListViewAdapter extends ListViewAdapter {
    private ImagePanel userImg;
    private final ImagePanel partnerImg;
    private final ArrayList<String> messages;
    private final ArrayList<Boolean> sentByUser;

    public SwingChatListViewAdapter(ImagePanel userImg, ImagePanel partnerImg) {
        this.userImg = userImg;
        this.partnerImg = partnerImg;
        this.messages = new ArrayList<>();
        this.sentByUser = new ArrayList<>();
    }

    public void setUserImg(ImagePanel userImg) {
        this.userImg = userImg;
        this.userImg.revalidate();
    }

    public void addMessages(Collection<ChatMessageViewModel> messages) {
        for (ChatMessageViewModel chatMessage : messages) {
            this.sentByUser.add(chatMessage.isSentByThisUser());
            this.messages.add(chatMessage.getMessage());
        }
    }

    public void addMessage(ChatMessageViewModel message) {
        this.sentByUser.add(message.isSentByThisUser());
        this.messages.add(message.getMessage());
    }

    @Override
    public ListViewHolder onCreateViewHolder(int position) {
        SwingChatViewHolder holder;

        if (this.sentByUser.get(position)) {
            holder = new SwingChatUserViewHolder();
            holder.setImage(this.userImg);
        }
        else {
            holder = new SwingChatPartnerViewHolder();
            holder.setImage(this.partnerImg);
        }

        holder.setMessage(this.messages.get(position));

        return holder;
    }

    @Override
    public int getNumItems() {
        return this.messages.size();
    }

    static class SwingChatPartnerViewHolder extends SwingChatViewHolder {

        @Override
        public void specifyPanel() {
            this.setLayout(new FlowLayout(FlowLayout.LEADING, PADDING_HORIZONTALLY, PADDING_VERTICALLY));
            this.add(this.image);
            this.add(this.message);
        }
    }

    static class SwingChatUserViewHolder extends SwingChatViewHolder {

        @Override
        public void specifyPanel() {
            this.setLayout(new FlowLayout(FlowLayout.RIGHT, PADDING_HORIZONTALLY, PADDING_VERTICALLY));
            this.add(this.message);
            this.add(this.image);
        }
    }
}
