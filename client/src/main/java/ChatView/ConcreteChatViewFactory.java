package ChatView;

import ChatController.ChatPresenterI;
import ChatController.ConcreteChatControllerFactory;
import ChatPresenter.AbstractChatViewFactory;
import ChatPresenter.ChatView;


public class ConcreteChatViewFactory extends AbstractChatViewFactory {
    @Override
    public ChatView create(ChatPresenterI presenterI, Long receiverID, String receiverName, String imgUrl) {
        return new SwingChatView(
                receiverID,
                receiverName,
                imgUrl,
                new ConcreteChatControllerFactory(presenterI)
        );
    }
}
