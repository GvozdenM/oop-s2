package ChatView;

import Utils.Swing.FrameManager.FrameManager;

public interface ChatFrameManagerFactory {
    static FrameManager create() {
        return FrameManager.newInstance();
    }
}
