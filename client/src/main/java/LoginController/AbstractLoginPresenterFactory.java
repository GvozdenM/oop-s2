package LoginController;

public abstract class AbstractLoginPresenterFactory {

    public abstract LoginPresenter create();
}
