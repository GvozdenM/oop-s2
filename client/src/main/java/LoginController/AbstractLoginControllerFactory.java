package LoginController;

public abstract class AbstractLoginControllerFactory {

    public abstract LoginControllerI create();
}
