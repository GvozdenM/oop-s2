package LoginController;

import ClientMessagingEndpoint.ClientEndpoint;

public class ConcreteLoginControllerFactory extends AbstractLoginControllerFactory {
    private LoginPresenter loginPresenter;

    public ConcreteLoginControllerFactory(LoginPresenter loginPresenter) {
        this.loginPresenter = loginPresenter;
    }

    @Override
    public LoginControllerI create() {
        return new  LoginController(
                this.loginPresenter,
                ClientEndpoint.getInstance().createLoginRequester()
        );
    }
}
