package LoginController;

/**
 * This interface provides a map of methods and functions to be implemented by the LoginController
 */
public interface LoginControllerI {

    /**
     * This signifies the user provided data for a login call to the server side. It will take the data and will
     * generate a LoginRequest
     * @param username
     * @param password
     */
    void onLoginBtnClick(String username, String password);

    /**
     * This method calls for the View to allow user to initiate request to server side as information has been filled
     * in the view.
     */
    void onFormFilled();

    /**
     * This method calls for an implementation to transition to the Registration View and use case
     */
    void onRegisterBtnClicked();

    void close();
}
