package LoginController;

import ClientMessagingEndpoint.ClientEndpoint;
import FeedController.FeedPresenter;
import FeedController.FeedPresenterFactory;
import FeedPresenter.FeedScreenPresenterFactory;
import LoginInteractor.LoginRequest;
import LoginInteractor.LoginRequester;
import LoginInteractor.LoginResponse;
import UserSession.UserSessionManager;
import RegistrationController.RegistrationPresenterI;
import RegistrationPresenter.ConcreteRegistrationPresenterFactory;
import Utils.Callbacks.CallbackAdapter;

import java.io.IOException;

/**
 * This Class implements all methods and functions related to the link between the presenter and the requester of the
 * Login use case.
 * @see LoginControllerI list of methods implemented.
 */
public class LoginController implements LoginControllerI {
    private final LoginPresenter presenter;
    private final LoginRequester<LoginRequest, LoginResponse> requester;

    public LoginController(LoginPresenter loginPresenter, LoginRequester<LoginRequest, LoginResponse> requester) {
        this.presenter = loginPresenter;
        this.requester = requester;
    }

    /**
     * In the case that the login transaction has been called, this method will generate a new LoginRequest and assign
     * a new Callback with the Login Response to delegate to external methods depending on transaction success.
     * @param username
     * @param password
     */
    @Override
    public void onLoginBtnClick(String username, String password) {
        this.requester.login(
                new LoginRequest(
                        username,
                        password
                ),
                new CallbackAdapter<>(
                        (onSuccessResponse) -> {
                            if(onSuccessResponse.isAuthenticated()) {
                                UserSessionManager.setUserId(onSuccessResponse.getUserId());
                                this.presenter.end();
                                FeedPresenterFactory feedPresenterFactory = new FeedScreenPresenterFactory();
                                FeedPresenter feedPresenter = feedPresenterFactory.create();
                                feedPresenter.init();
                            }
                            else {
                                this.presenter.presentAuthenticationError();
                            }
                        },
                        (onFailResponse) -> {
                            presenter.presentError(onFailResponse.getMessage());
                        },
                        (onExceptionResponse) -> {
                            presenter.presentError(onExceptionResponse.getMessage());

                        }
                )
        );
    }

    @Override
    public void onFormFilled() {
        this.presenter.updatePresentingState(true);
    }

    @Override
    public void onRegisterBtnClicked() {
        ConcreteRegistrationPresenterFactory registrationPresenterFactory = new ConcreteRegistrationPresenterFactory();
        RegistrationPresenterI registrationPresenter = registrationPresenterFactory.create();
        registrationPresenter.init();
    }

    @Override
    public void close() {
        try {
            ClientEndpoint.getInstance().close();
        } catch (IOException e) {
            try {
                ClientEndpoint.getInstance().close();
            } catch (IOException ioException) {
                System.out.println("couldn't properly close client messaging endpoint");
            }
        }
    }
}
