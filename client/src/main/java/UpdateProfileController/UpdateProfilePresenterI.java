package UpdateProfileController;

import UpdateProfileInteractor.UserProfileResponse;

/**
 * This interface provides a list of methods and functions to be implemented by the UpdateProfilePresenter.
 * @see UpdateProfilePresenter.UpdateProfileScreenPresenter
 */
public interface UpdateProfilePresenterI {
    /**
     * Initialize method for the Update User Presenter, assigns concrete FeedView and initializes items and attaches
     * listeners with views.
     */
    void init();

    /**
     * Provides a data point to get all the user related information and moves it to the view,
     * @param profile
     */
    void presentProfile(UserProfileResponse profile);

    /**
     * Displays a notification when the user has changed something in their preferences and information correctly.
     * @param profile
     */
    void presentUpdatedProfileSuccess(UserProfileResponse profile);

    /**
     * This method is called for a display of an error in the current View whenever there is no connection as a result
     * from a callback.
     */
    void presentNoConnectionError();

    void presentIncorrectArguments(String msg);
}
