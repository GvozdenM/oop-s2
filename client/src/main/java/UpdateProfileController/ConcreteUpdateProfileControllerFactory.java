package UpdateProfileController;


import ClientMessagingEndpoint.ClientEndpoint;

public class ConcreteUpdateProfileControllerFactory extends AbstractUpdateProfileControllerFactory {

   private final UpdateProfilePresenterI profilePresenter;

    public ConcreteUpdateProfileControllerFactory(UpdateProfilePresenterI profilePresenter) {
        this.profilePresenter = profilePresenter;
    }

    @Override
    public UpdateProfileController create(){
        return new UpdateProfileController(
                this.profilePresenter,
                ClientEndpoint.getInstance()
        );
    }


}
