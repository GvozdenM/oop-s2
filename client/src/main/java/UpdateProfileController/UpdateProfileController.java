package UpdateProfileController;


import UpdateProfileInteractor.UpdateProfileRequest;
import UpdateProfileInteractor.UpdateProfileRequester;
import UpdateProfileInteractor.UpdateProfileRequesterFactory;
import UpdateProfileInteractor.UserProfileResponse;
import UserSession.UserSessionManager;
import Utils.Callbacks.CallbackAdapter;

import java.util.regex.Pattern;

/**
 * This Class implements all methods and functions related to the link between the presenter and the requester of the
 * User Update Profile scenario.
 * @see UpdateProfileControllerI list of methods implemented.
 */
public class UpdateProfileController implements UpdateProfileControllerI{
    private final UpdateProfilePresenterI presenter;
    private final UpdateProfileRequesterFactory<UpdateProfileRequest, UserProfileResponse> requesterFactory;

    private static final String REGEX_EMAIL_VALIDATION = "^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$";


    public UpdateProfileController(UpdateProfilePresenterI presenter,
                                   UpdateProfileRequesterFactory<UpdateProfileRequest, UserProfileResponse> requesterFactory) {
        this.presenter = presenter;
        this.requesterFactory = requesterFactory;
    }


    @Override
    public void onLoad(){
        UpdateProfileRequester<UpdateProfileRequest, UserProfileResponse> requester
                = this.requesterFactory.createUpdateProfileRequester();

        long userId = UserSessionManager.getUserId();

        requester.getUser(
                userId,
                new CallbackAdapter<>(presenter::presentProfile,
                        onFailResponse -> presenter.presentNoConnectionError(),
                        onExceptionResponse -> presenter.presentNoConnectionError()
                )

        );
    }


    @Override
    public void onSaveBtnClick(String username, String name, int age, String email, String password, String interest,
                               boolean isPremium, String imgUrl, String description) {
        if (verifyPassword(password)) {
            if (age > 17) {
                if (Pattern.matches(REGEX_EMAIL_VALIDATION, email)) {
                    UpdateProfileRequester<UpdateProfileRequest, UserProfileResponse> requester
                            = this.requesterFactory.createUpdateProfileRequester();

                    long userId = UserSessionManager.getUserId();

                    requester.updateUser(new UpdateProfileRequest(userId, username, name, age, email, password, interest,
                                    isPremium, imgUrl, description),

                            new CallbackAdapter<>(
                                    userProfileResponse -> {
                                        UserSessionManager.setUserProfileImgUrl(imgUrl);
                                        this.presenter.presentUpdatedProfileSuccess(userProfileResponse);
                                    },

                                    onFailResponse -> {
                                        presenter.presentNoConnectionError();
                                    },

                                    onExceptionResponse -> {
                                        presenter.presentNoConnectionError();
                                    }

                            )
                    );
                }
                else {
                    this.presenter.presentIncorrectArguments("email");
                }

            }
            else {
                this.presenter.presentIncorrectArguments("age");
            }
        }
        else {
            this.presenter.presentIncorrectArguments("password");
        }
    }

    private boolean verifyPassword(String password){
        return password.matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).+$");
    }

}
