package UpdateProfileController;

/**
 * This interface provides the functions and methods to be implemented by the UpdateProfile class
 * @see UpdateProfileController
 */
public interface UpdateProfileControllerI {
    /**
     * Provides initialization method for the controller, opens information after request to obtain current
     * saved data on user and assigns callback for exceptions.
     */
    void onLoad();

    /**
     * This method obtains all the data to be sent in a request to change the user's personal data generating
     * a UpdateProfileResponse and assigning the corresponding callback adapter.
     * @param username
     * @param name
     * @param age
     * @param email
     * @param password
     * @param interest
     * @param isPremium
     */
    void onSaveBtnClick( String username, String name,  int age, String email, String password, String interest,
                         boolean isPremium, String imgUrl, String description);
}
