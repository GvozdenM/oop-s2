import LoginController.LoginPresenter;
import LoginPresenter.LoginPresenterFactory;

public class MinderClientUI implements Runnable {
    private static MinderClientUI UI  = null;

    public static MinderClientUI getInstance() {
        if (null == UI) UI = new MinderClientUI();
        return UI;
    }

    private MinderClientUI() {
    }

    @Override
    public void run() {
        LoginPresenterFactory loginPresenterFactory = new LoginPresenterFactory();
        LoginPresenter loginPresenter = loginPresenterFactory.create();
        loginPresenter.init();
    }
}
