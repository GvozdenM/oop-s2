package FeedView;

import FeedController.FeedControllerFactory;
import FeedController.FeedPresenter;
import FeedPresenter.FeedViewFactory;
import FeedPresenter.FeedView;

public class SwingFeedViewFactory extends FeedViewFactory {
    @Override
    public FeedView create(FeedPresenter feedPresenter) {
        return new SwingFeedView(
                new FeedControllerFactory(feedPresenter)
        );
    }
}
