package FeedView;

import Utils.Swing.FrameManager.FrameManager;

public interface FeedFrameManagerFactory {
    FrameManager frameManager = FrameManager.newInstance();

    static FrameManager create() {
        return frameManager;
    }
}
