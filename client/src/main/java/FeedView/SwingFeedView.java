package FeedView;

import FeedController.FeedControllerFactory;
import FeedController.FeedControllerI;
import FeedPresenter.FeedUserViewModel;
import FeedPresenter.FeedView;
import Utils.Swing.FrameManager.FrameManager;
import Utils.Constants;
import Utils.Swing.Image.ImagePanel;
import Utils.Views.ResourceFetcher;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * This class is the main feed view panel implementation as well as the implementation of given parent interface.
 * @see FeedView
 */
public class SwingFeedView extends JPanel implements FeedView {
    private final FeedControllerI controller;

    private ImagePanel imagePanel;
    private final JLabel lUsername;
    private final JLabel lAge;
    private final JLabel lInterestedIn;
    private final JTextArea taShortDescription;

    private final JButton viewProfileBtn;
    private final JButton acceptBtn;
    private final JButton dismissBtn;

    private final JButton chatsBtn;
    private final JButton profileBtn;

    public SwingFeedView(FeedControllerFactory controllerFactory) {
        this.controller = controllerFactory.create();

        this.setSize(
                new Dimension(800, 600)
        );

        try {
            this.imagePanel = new ImagePanel(new ResourceFetcher("defaultuserimg.png").getResourceFile().getPath());
        } catch (IOException e) {
            this.imagePanel = new ImagePanel();
            e.printStackTrace();
        }
        this.lUsername = new JLabel();
        this.lAge = new JLabel();
        this.lInterestedIn = new JLabel();
        this.taShortDescription = new JTextArea();

        this.viewProfileBtn = new JButton(" View profile ");
        this.acceptBtn = new JButton("Like");
        this.dismissBtn = new JButton("Dislike");
        this.chatsBtn = new JButton("Chats");
        this.profileBtn = new JButton("Update Profile");

        attachListeners();
    }

    private void configureUserViewPanel() {
        cleanPanel();

        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        this.add(this.imagePanel);

        int wrapperDetailsTextPaddingV = 20;
        int wrapperDetailsTextPaddingH = 10;
        JPanel wrapperDetailsTextPadded = new JPanel(
                new FlowLayout(FlowLayout.CENTER, wrapperDetailsTextPaddingH, wrapperDetailsTextPaddingV)
        );
        JPanel wrapperDetailsText = new JPanel();
        wrapperDetailsText.setLayout(new BoxLayout(wrapperDetailsText, BoxLayout.PAGE_AXIS));

        wrapperDetailsText.add(this.lUsername);
        wrapperDetailsText.add(this.lAge);
        wrapperDetailsText.add(this.lInterestedIn);

        JPanel viewProfileBtnPaddingWrapper = new JPanel(
                new FlowLayout(FlowLayout.CENTER, 0, 15)
        );
        viewProfileBtnPaddingWrapper.add(this.viewProfileBtn);
        wrapperDetailsText.add(viewProfileBtnPaddingWrapper);

        wrapperDetailsTextPadded.add(wrapperDetailsText);
        this.add(wrapperDetailsTextPadded);


        this.taShortDescription.setEditable(false);
        //this.add(this.taShortDescription);

        JPanel wrapperDecisionBtns = setupManagementBtnsPanel(this.acceptBtn, this.dismissBtn);

        this.add(wrapperDecisionBtns);

        JPanel wrapperManagementBtns = setupManagementBtnsPanel(this.chatsBtn, this.profileBtn);

        this.add(wrapperManagementBtns);

        this.setVisible(true);
    }

    private void attachListeners() {
        this.viewProfileBtn.addActionListener(l -> controller.onViewProfile());
        this.acceptBtn.addActionListener(l -> controller.onLike());
        this.dismissBtn.addActionListener(l -> controller.onDislike());
        this.chatsBtn.addActionListener(l -> controller.onChatBtnClicked());
        this.profileBtn.addActionListener(l -> controller.onProfileBtnClicked());
    }

    @Override
    public void displayUser(FeedUserViewModel user, boolean like) {
        configureUserViewPanel();

        try {
            if (!(user.getImageUrl() != null && user.getImageUrl().length() > 5)) {
                user.setImageUrl(Constants.COSMETICS.DEFAULT_USER_IMG_URL);
            }

            this.imagePanel.setImgSize(270, 270);
            this.imagePanel.load(new URL(user.getImage()));
            this.lUsername.setText("Username: " + user.getUsername());
            this.lAge.setText("Age: " + user.getAge());
            this.lInterestedIn.setText("Interested in: " + user.getInterestedIn());
            this.taShortDescription.setText(user.getShortDescription());
            this.revalidate();
        } catch (MalformedURLException ignored) {}
    }

    @Override
    public void displayErrorMsg(String msg) {
        JOptionPane.showMessageDialog(
                new JFrame(),
                msg,
                "Warning",
                JOptionPane.WARNING_MESSAGE
        );
    }

    @Override
    public void displayNoUsers() {
        cleanPanel();

        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        ResourceFetcher resourceFetcher = new ResourceFetcher("FeedRepeatButton.png");

        ImagePanel refreshButton = new ImagePanel();
        try {
            refreshButton.load(resourceFetcher.getResourceFile().getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        refreshButton.setOnClickListener(this.controller::onFeedUpdateBtnClicked);

        this.add(refreshButton);

        JPanel wrapperManagementBtns = setupManagementBtnsPanel(this.chatsBtn, this.profileBtn);

        this.add(wrapperManagementBtns);

        this.repaint();
        this.revalidate();

        this.setVisible(true);
    }

    private void cleanPanel() {
        Component[] components = this.getComponents();

        for (Component component : components) {
            this.remove(component);
        }

        this.revalidate();
        this.repaint();
    }

    @NotNull
    private JPanel setupManagementBtnsPanel(JButton chatsBtn, JButton profileBtn) {
        JPanel wrapperManagementBtns = new JPanel(new FlowLayout());
        wrapperManagementBtns.add(chatsBtn);
        wrapperManagementBtns.add(profileBtn);
        return wrapperManagementBtns;
    }

    @Override
    public void onTransitionTo() {
        ResourceFetcher resourceFetcher = new ResourceFetcher("favicon.jpg");

        FrameManager frameManager = FeedFrameManagerFactory.create();
        try {
            frameManager
                    .setCloseProcedure(controller::onClose, false)
                    .configureFramePreferences("Minder", this, getWidth(), getHeight(), FrameManager.CENTER)
                    .setFavicon(resourceFetcher.getResourceFile().getPath());
        } catch (IOException e) {
            frameManager
                    .setCloseProcedure(controller::onClose, false)
                    .configureFramePreferences("Minder", this, getWidth(), getHeight(), FrameManager.CENTER);
        }

        this.controller.onLoad();
    }
}
