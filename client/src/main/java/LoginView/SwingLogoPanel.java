package LoginView;

import Utils.Views.ResourceFetcher;
import Utils.Swing.Image.ImagePanel;

import javax.swing.*;
import java.io.IOException;

public class SwingLogoPanel extends JPanel {

    public SwingLogoPanel() {
        this.setVisible(true);
        final ResourceFetcher resourceFetcher = new ResourceFetcher("favicon.jpg");
        try {
            ImagePanel imagePanel = new ImagePanel(resourceFetcher.getResourceFile().getPath());
            this.add(imagePanel);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.setSize(50, 50);
        this.setVisible(true);
    }
}
