package LoginView;

import LoginController.ConcreteLoginControllerFactory;
import LoginPresenter.AbstractLoginViewFactory;
import LoginController.LoginPresenter;
import LoginPresenter.LoginView;

public class ConcreteLoginViewFactory extends AbstractLoginViewFactory {
    @Override
    public LoginView create(LoginPresenter presenter) {
        return new SwingLoginView(
            new ConcreteLoginControllerFactory(presenter)
        );
    }
}
