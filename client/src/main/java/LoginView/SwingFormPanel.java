package LoginView;

import javax.swing.*;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * This class contains the form Panel in the LoginView and is used to centralize the data fields to be controlled
 * as well as implementing the functions to attach respective listeners as well as getters and setters.
 */
public class SwingFormPanel extends JPanel {
    private final int GRID_LAYOUT_ROWS = 5;
    private final int GRID_LAYOUT_COLS = 1;
    private final int INPUT_FIELD_SIDES_PADDING = 20;

    private JTextField tfUsername;
    private JPasswordField pfPassword;
    private FormPanelButtonGroup buttonGroup;

    public SwingFormPanel() {
        this.setLayout(new GridLayout(GRID_LAYOUT_ROWS, GRID_LAYOUT_COLS));

        JLabel lUsername = new JLabel("Username:");
        lUsername.setHorizontalAlignment(JLabel.CENTER);

        JLabel lPassword = new JLabel("Password:");
        lPassword.setHorizontalAlignment(JLabel.CENTER);

        GridBagConstraints cFormPanel = new GridBagConstraints();
        cFormPanel.insets = new Insets(0,
                INPUT_FIELD_SIDES_PADDING,
                0,
                INPUT_FIELD_SIDES_PADDING
        );

        this.tfUsername = new JTextField(20);
        this.tfUsername.setHorizontalAlignment(JTextField.CENTER);
        this.tfUsername.requestFocus(true);

        JPanel pfPasswordWrapper = new JPanel(new GridBagLayout());
        this.pfPassword = new JPasswordField(20);
        this.pfPassword.setHorizontalAlignment(JPasswordField.CENTER);

        this.buttonGroup = new FormPanelButtonGroup();
        GridBagConstraints c = new GridBagConstraints();

        this.add(lUsername);
        this.add(this.tfUsername);
        this.add(lPassword);
        this.add(this.pfPassword);

        this.buttonGroup.configureLayoutConstraints(c);
        this.add(this.buttonGroup, c);

        this.setVisible(true);
    }

    public void setLoginBtnState(boolean clickable) {
        this.buttonGroup.setLoginBtnState(clickable);
    }

    public void attachLoginBtnListener(ActionListener listener) {
        this.buttonGroup.attachLoginBtnListener(listener);
    }

    public void attachRegisterBtnListener(ActionListener listener) {
        this.buttonGroup.attachRegisterBtnListener(listener);
    }

    public void attachUsernameChangeListener(DocumentListener listener) {
        this.tfUsername.getDocument().addDocumentListener(listener);
    }

    public void attachPasswordChangeListener(DocumentListener listener) {
        this.pfPassword.getDocument().addDocumentListener(listener);
    }

    public String getUsername() {
        return this.tfUsername.getText();
    }

    public String getPassword() {
        return new String(this.pfPassword.getPassword());
    }

    public void setUsername(String input) {
        this.tfUsername.setText(input);
    }

    public void setPassword(String input) {
        this.pfPassword.setText(input);
    }
}
