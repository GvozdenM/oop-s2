package LoginView;


import Utils.Swing.FrameManager.FrameManager;
import LoginController.AbstractLoginControllerFactory;
import LoginController.LoginControllerI;
import LoginPresenter.LoginView;
import Utils.Adapters.TextChangeListenerSwingAdapter;
import Utils.Views.ResourceFetcher;
import Utils.listeners.interfaces.TextChangedListener;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

/**
 * This class provides the main JPanel in Login View as well as attachers of parts like listeners and different
 * methods and functions for the Login Use Case
 */
public class SwingLoginView extends JPanel implements LoginView {
    private final int FORM_PANEL_SIDE_PADDING = 15;

    private LoginControllerI controller;

    private final SwingLogoPanel swingLogoPanel;
    private final SwingFormPanel swingFormPanel;

    public SwingLoginView(AbstractLoginControllerFactory controllerFactory) throws HeadlessException {
        this.controller = controllerFactory.create();

        setSize(new Dimension(600,600));
        this.setLayout(new BorderLayout());

        JPanel formWrapper = new JPanel();
        formWrapper.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        this.swingLogoPanel = new SwingLogoPanel();
        this.swingFormPanel = new SwingFormPanel();

        this.add(this.swingLogoPanel, BorderLayout.PAGE_START);

        setFormPanelConstraints(c);
        formWrapper.add(this.swingFormPanel, c);
        this.add(formWrapper, BorderLayout.CENTER);

        this.setVisible(true);

        attachListeners();
    }

    /**
     * Sets padding for local Swing JPanel Layouts
     * @param c an object to retain constraints info.
     */
    private void setFormPanelConstraints(GridBagConstraints c) {
        c.insets = new Insets(
                0,
                FORM_PANEL_SIDE_PADDING,
                0,
                FORM_PANEL_SIDE_PADDING);
    }

    private void attachListeners() {
        this.swingFormPanel.attachLoginBtnListener(
                actionEvent -> controller.onLoginBtnClick(
                        getUsername(),
                        getPassword()
                )
        );

        this.swingFormPanel.attachRegisterBtnListener(
                actionEvent -> controller.onRegisterBtnClicked()
        );
    }

    /**
     * This method provides an initialization of the frame, it sets main properties and defaults values.
     * @see Utils.Views.TransitionView
     */
    @Override
    public void onTransitionTo() {
        this.swingFormPanel.setLoginBtnState(false);

        ResourceFetcher resourceFetcher = new ResourceFetcher("favicon.jpg");

        FrameManager frameManager = AuthenticationFrameManagerFactory.create();
        try {
            frameManager
                    .configureFramePreferences("Login", this, getWidth(), getHeight(), FrameManager.CENTER)
                    .setCloseProcedure(controller::close, true)
                    .setResizable(false)
                    .setFavicon(resourceFetcher.getResourceFile().getPath());

        } catch (IOException ignore) {}
    }

    @Override
    public void attachFormStateListener(TextChangedListener listener) {
        this.swingFormPanel.attachUsernameChangeListener(new TextChangeListenerSwingAdapter(listener));
        this.swingFormPanel.attachPasswordChangeListener(new TextChangeListenerSwingAdapter(listener));
    }

    @Override
    public void displayErrorMsg(String errorMsg) {
        SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(
                new JFrame(),
                "ERROR: " + errorMsg,
                "Warning",
                JOptionPane.WARNING_MESSAGE
        ));
    }

    @Override
    public void onCloseSuccessfully() {
        FrameManager frameManager = AuthenticationFrameManagerFactory.create();
        frameManager
                .setCloseProcedure(() -> {}, false)
                .closeFrame();
    }

    @Override
    public String getUsername() {
        return this.swingFormPanel.getUsername();
    }

    @Override
    public String getPassword() {
        return this.swingFormPanel.getPassword();
    }

    @Override
    public void setUsername(String input) {
        this.swingFormPanel.setUsername(input);
    }

    @Override
    public void setPassword(String input) {
        this.swingFormPanel.setPassword(input);
    }

    @Override
    public void setLoginBtnState(boolean clickable) {
        this.swingFormPanel.setLoginBtnState(clickable);
    }
}
