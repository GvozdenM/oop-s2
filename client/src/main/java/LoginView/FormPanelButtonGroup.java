package LoginView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class FormPanelButtonGroup extends JPanel {
    private final String LOGIN_BUTTON_TEXT = "Login";
    private final String REGISTER_BUTTON_TEXT = "Register";

    private JButton loginButton;
    private JButton registerButton;

    public FormPanelButtonGroup() {
        this.setLayout(new GridBagLayout());

        this.loginButton = new JButton(LOGIN_BUTTON_TEXT);
        this.registerButton = new JButton(REGISTER_BUTTON_TEXT);

        GridBagConstraints c = new GridBagConstraints();

        JPanel btnWrapper = new JPanel(new GridLayout());
        btnWrapper.add(this.loginButton);
        btnWrapper.add(this.registerButton);

       this.add(btnWrapper, c);

        this.setVisible(true);
    }

    public void configureLayoutConstraints(GridBagConstraints c) {
        c.insets = new Insets(20, 0, 0, 0);
    }

    public void setLoginBtnState(boolean clickable) {
        this.loginButton.setEnabled(clickable);
        this.loginButton.setBorderPainted(clickable);
        this.loginButton.setFocusPainted(clickable);
    }

    public void attachLoginBtnListener(ActionListener listener) {
        this.loginButton.addActionListener(listener);
    }

    public void attachRegisterBtnListener(ActionListener listener) {
        this.registerButton.addActionListener(listener);
    }
}
