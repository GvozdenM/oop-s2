package LoginView;

import Utils.Swing.FrameManager.FrameManager;

public interface AuthenticationFrameManagerFactory {
    FrameManager frameManager = FrameManager.newInstance();

    static FrameManager create() {
        return frameManager;
    }
}
