package MatchedUsersPresenter;

import MatchedUsersController.AbstractMatchedUsersPresenterFactory;
import MatchedUsersController.MatchedUsersPresenter;
import MatchedUsersView.SwingMatchedUsersViewFactory;

public class MatchedUsersPresenterFactory extends AbstractMatchedUsersPresenterFactory {
    @Override
    public MatchedUsersPresenter create() {
        return new MatchedUsersScreenPresenter(
                new SwingMatchedUsersViewFactory()
        );
    }
}
