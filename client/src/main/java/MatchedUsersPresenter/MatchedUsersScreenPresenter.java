package MatchedUsersPresenter;

import MatchedUsersController.MatchedUsersPresenter;
import MatchedUsersInteractor.MatchedUsersResponse;

/**
 * The MatchedUsersScreenPresenter is in charge of delegating requests from the attached MatchUsersController
 * in order to operate the view externally.
 * This class implements the methods and functions of its respective interface.
 * @see MatchedUsersPresenter
 */
public class MatchedUsersScreenPresenter implements MatchedUsersPresenter {
    private final MatchedUsersView view;

    private MatchedUsersViewModel vm;

    public MatchedUsersScreenPresenter(AbstractMatchedUsersViewFactory viewFactory) {
        this.view = viewFactory.create(this);
    }

    @Override
    public void init() {
        this.view.onTransitionTo();
    }

    @Override
    public void presentUsersList(MatchedUsersResponse response) {
        this.vm = new MatchedUsersViewModel();
        for (MatchedUsersResponse.MatchedUser u : response.getMatchedUsers()) {
            this.vm.addUser(
                    this.vm.createUser(u.userID, u.userLogin, u.userImgUrl)
            );
        }
        if (this.vm.getUsers().size() > 0) {
            this.view.displayUsersList(vm);
        }
        else {
            presentNoMatchesError();
        }
    }

    @Override
    public void removeUser(long userID) {
        if (this.vm != null) {
            this.vm.removeUser(userID);
            this.view.displayUsersList(this.vm);
            if (this.vm.getUsers().size() <= 0) {
                this.view.end();
                presentNoMatchesError();
            }
        }
    }

    private void presentNoMatchesError() {
        this.view.displayErrorMsg("Sorry you have any matches! :( ");
    }

    @Override
    public void presentNoConnectionError() {
        this.view.displayErrorMsg("Connection problems");
    }
}
