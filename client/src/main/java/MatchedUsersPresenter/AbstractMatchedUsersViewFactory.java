package MatchedUsersPresenter;

import MatchedUsersController.MatchedUsersPresenter;

public abstract class AbstractMatchedUsersViewFactory {
    public abstract MatchedUsersView create(MatchedUsersPresenter presenter);
}
