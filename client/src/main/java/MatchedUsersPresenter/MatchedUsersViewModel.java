package MatchedUsersPresenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * This class provides a data model to contain all of a user's matches and manipulate them externally. Implemented with
 * an ArrayList
 */
public class MatchedUsersViewModel {
    private final ArrayList<User> users;
    private final Map<Long, Integer> userIDtoIndexMap;

    public MatchedUsersViewModel() {
        this.users = new ArrayList<>();
        this.userIDtoIndexMap = new HashMap<>();
    }

    public void addUser(User user) {
        this.userIDtoIndexMap.put(user.userID, users.size());
        users.add(user);
    }

    void removeUser(long userID) {
        this.users.remove(this.userIDtoIndexMap.get(userID).intValue());
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public User createUser(Long userID, String userLogin, String userImgUrl) {
        return new User(userID, userLogin, userImgUrl);
    }

    /**
     * Model definition of a user
     */
    public static class User {
        public final Long userID;
        public final String userLogin;
        public final String userImgUrl;

        User(Long userID, String userLogin, String userImgUrl) {
            this.userID = userID;
            this.userLogin = userLogin;
            this.userImgUrl = userImgUrl;
        }
    }
}
