package MatchedUsersPresenter;

import Utils.Views.TransitionView;

/**
 * This interface provides the functions to be implemented by the MatchedUsersView Class
 */
public interface MatchedUsersView extends TransitionView {
    /**
     * This method opens a new thread and adds all current matches to a list view implementation.
     * @param vm match users list
     */
    void displayUsersList(MatchedUsersViewModel vm);

    /**
     * This method is called for a display of an error in the current View
     * @param errMsg Error message to be displayed.
     */
    void displayErrorMsg(String errMsg);

    /**
     * Method triggered to end the presentation of the matched users.
     */
    void end();
}
