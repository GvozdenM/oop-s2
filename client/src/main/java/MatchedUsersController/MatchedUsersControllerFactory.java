package MatchedUsersController;

import ChatPresenter.ConcreteChatPresenterFactory;
import ClientMessagingEndpoint.ClientEndpoint;
import MatchedUsersView.AbstractMatchedUsersControllerFactory;

public class MatchedUsersControllerFactory extends AbstractMatchedUsersControllerFactory {
    private final MatchedUsersPresenter presenter;

    public MatchedUsersControllerFactory(MatchedUsersPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public MatchedUsersControllerI create() {
        return new MatchedUsersController(
                this.presenter,
                ClientEndpoint.getInstance(),
                new ConcreteChatPresenterFactory()
        );
    }
}
