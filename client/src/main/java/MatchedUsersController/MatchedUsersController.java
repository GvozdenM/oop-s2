package MatchedUsersController;

import ChatController.ChatPresenterI;
import ChatPresenter.AbstractChatPresenterFactory;
import MatchedUsersInteractor.*;
import UserSession.UserSessionManager;
import Utils.Callbacks.CallbackAdapter;
import Utils.Logger.Log;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This Class implements all methods and functions related to the link between the presenter and the requester of the
 * MatchedUsers scenario.
 * @see MatchedUsersControllerI list of methods implemented.
 */
public class MatchedUsersController implements MatchedUsersControllerI {
    private final MatchedUsersPresenter presenter;
    private final MatchedUsersRequester<MatchedUsersRequest, MatchedUsersUnmatchRequest, MatchedUsersResponse> requester;
    private final AbstractChatPresenterFactory chatPresenterFactory;

    private final Map<Long, ChatPresenterI> chatPresenterMap = new ConcurrentHashMap<>();

    public MatchedUsersController(MatchedUsersPresenter presenter,
                                  MatchedUsersRequesterFactory<MatchedUsersRequest, MatchedUsersUnmatchRequest, MatchedUsersResponse> requesterFactory,
                                  AbstractChatPresenterFactory chatPresenterFactory) {
        this.presenter = presenter;
        this.requester = requesterFactory.createMatchedUsersRequester();
        this.chatPresenterFactory = chatPresenterFactory;
    }

    private void handleChatWindowClosed(long userID) {
        this.chatPresenterMap.remove(userID);
    }

    @Override
    public void onLoad() {
        this.requester.getMatchedUsers(
                new MatchedUsersRequest(UserSessionManager.getUserId()),
                new CallbackAdapter<>(
                        this.presenter::presentUsersList,
                        (failureResponse) -> this.presenter.presentNoConnectionError(),
                        (exceptionResponse) -> this.presenter.presentNoConnectionError()
                )
        );
    }

    @Override
    public void onChatBtnClicked(long userID, String login, String imgUrl) {
        ChatPresenterI chatPresenter;
        if (this.chatPresenterMap.get(userID) != null) {
            chatPresenter = this.chatPresenterMap.get(userID);
        }
        else {
            chatPresenter = this.chatPresenterFactory.create();
            this.chatPresenterMap.put(userID, chatPresenter);
        }
        chatPresenter.init(userID, login, imgUrl, () -> handleChatWindowClosed(userID));
        Log.s("Chat btn for user: " + userID + " clicked");
    }

    @Override
    public void onUnmatchUserBtnClicked(long userID) {
        requester.unmatchUser(
                new MatchedUsersUnmatchRequest(UserSessionManager.getUserId(), userID),
                new CallbackAdapter<>(
                        l -> {

                        },
                        null,
                        null
                )
        );
        if (this.chatPresenterMap.get(userID) != null) {
            ChatPresenterI chatPresenter = this.chatPresenterMap.get(userID);
            chatPresenter.onUnmatch();
        }
        this.presenter.removeUser(userID);
    }
}
