package MatchedUsersController;

public abstract class AbstractMatchedUsersPresenterFactory {
    public abstract MatchedUsersPresenter create();
}
