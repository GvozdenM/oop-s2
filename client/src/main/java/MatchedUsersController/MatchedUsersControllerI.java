package MatchedUsersController;

/**
 * This interface provides the functions and methods to be implemented by the MatchedUsersController class
 * @see MatchedUsersController
 */
public interface MatchedUsersControllerI {
    /**
     * Provides initialization method for the controller, links the current user to the server side and gets a list of
     * people matched as well as handling callback responses
     */
    void onLoad();

    /**
     * Transitions to Chat with the selected user in the list view.
     * @param userID user to chat
     * @param s
     * @param s1
     */
    void onChatBtnClicked(long userID, String login, String imgUrl);

    /**
     * Provides a request to the server side in order to notify that the user unmatched the user selected and
     * handles responses.
     * @param userID user to unmatch
     */
    void onUnmatchUserBtnClicked(long userID);
}
