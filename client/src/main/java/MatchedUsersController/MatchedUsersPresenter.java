package MatchedUsersController;

import MatchedUsersInteractor.MatchedUsersResponse;

/**
 * This interface provides a list of methods and functions to be implemented by the MatchedUsersPresenter.
 */
public interface MatchedUsersPresenter {
    /**
     * Ininialize method for the MatchedUsersPresenter, assigns concrete FeedView and initializes items and attaches
     * listeners with views.
     */
    void init();

    /**
     * This method carries out the presenting of the acquired users list of matches
     * @param response responseObj
     */
    void presentUsersList(MatchedUsersResponse response);

    /**
     * This method is called to remove a user from the matched users list.
     * @param userID the id of the user that is desired to remove
     */
    void removeUser(long userID);

    /**
     * This method is called for a display of an error in the current View whenever there is no connection as a result
     * from a callback.
     */
    void presentNoConnectionError();
}
