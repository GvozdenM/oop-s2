package ChatController;


import ClientMessagingEndpoint.ClientEndpoint;

public class ConcreteChatControllerFactory extends AbstractChatControllerFactory{
    private ChatPresenterI chatPresenter;

    public ConcreteChatControllerFactory(ChatPresenterI chatPresenter) {
        this.chatPresenter = chatPresenter;
    }

    @Override
    public ChatControllerI create() {
        return new ChatController(
                this.chatPresenter,
                ClientEndpoint.getInstance().createChatRequester()
        );
    }
}
