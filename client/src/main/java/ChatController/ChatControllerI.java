package ChatController;


/**
 * This interface provides the functions and methods to be implemented by the ChatController class
 * @see ChatController
 */
public interface ChatControllerI {
    /**
     * Provides initialization method for the controller, links the current user to the server side and sets the
     * partner user to chat with in the form of a request via user ids retrieving the past chat log of the relation.
     */
    void onLoad();

    /**
     * Provides for the transactional operation whenever the local user wants to send a message going to server
     * side and ultimately the partner user
     * @param message message to be sent
     */
    void onSend(String message);

    /**
     * Provides functionality to unmatch the current user chatting with.
     * @param userToUnmatchID unmatching user Id
     */
    void onUnmatch(Long userToUnmatchID);

    /**
     *  Triggered when window is closing.
     */
    void onWindowClose();

    //SETTER
    void setPartnerID(Long partnerID);


}
