package ChatController;


import ChatInteractor.ChatLogResponse;
import ChatInteractor.IncomingChatMessage;
import Utils.listeners.interfaces.SimpleOnClickListener;

/**
 * This interface provides a list of methods and functions to be implemented by the ChatPresenter.
 */
public interface ChatPresenterI {

    /**
     * Ininialize method for the Chat Presenter, assigns concrete ChatView and initializes items and attaches
     * listeners with views respective to chatting user and partner.
     * @param receiverID id of chatting user
     * @param receiverName
     */
    void init(Long receiverID, String receiverName);

    /**
     * Ininialize method for the Chat Presenter, assigns concrete ChatView and initializes items and attaches
     * listeners with views respective to chatting user and partner.
     * @param receiverID
     * @param receiverName
     * @param imgUrl
     */
    void init(Long receiverID, String receiverName, String imgUrl, SimpleOnClickListener onExit);

    /**
     * Calls for the view to display and add the previous chat log to the view.
     * @param chatLogResponse collection of previous messages
     */
    void presentChat(ChatLogResponse chatLogResponse);

    /**
     * This method is called for a display of an error in the current View whenever there is no connection as a result
     * from a callback.
     */
    void presentNoConnection();

    /**
     * This method delegates to view to add the display of a new message sent from user to the view and previous
     * chatlog
     * @param message
     */
    void presentNewSentMessage(String message);

    /**
     * This method delegates to view to add the display of a new message received from partner user to the view and
     * previous chatlog
     * @param message
     */
    void presentNewReceivedMessage(IncomingChatMessage message);

    /**
     *  This method is called when the user exits the chat window
     */
    void onExit();

    /**
     *  This method is triggered when a user attempts to unmatch the open chat window,
     *  therefore it should close the Chat window.
     */
    void onUnmatch();
}
