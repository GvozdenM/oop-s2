package ChatController;

public abstract class AbstractChatControllerFactory {
    public abstract ChatControllerI create();

}
