package ChatController;

import ChatInteractor.*;
import UserSession.UserSessionManager;
import Utils.Callbacks.CallbackAdapter;

import java.sql.Date;
import java.time.*;

/**
 * This Class implements all methods and functions related to the link between the presenter and the requester of the
 * Chat scenario.
 * @see ChatControllerI
 */
public class ChatController implements ChatControllerI{
    private final ChatPresenterI chatPresenter;
    private final ChatRequester<ChatLogRequest, OutgoingChatMessage, IncomingChatMessage, ChatLogResponse> requester;
    private Long partnerID;

    public ChatController(
            ChatPresenterI chatPresenter,
            ChatRequester<ChatLogRequest, OutgoingChatMessage, IncomingChatMessage, ChatLogResponse> requester) {
        this.chatPresenter = chatPresenter;
        this.requester = requester;
    }

    @Override
    public void onLoad() {
        if(this.partnerID != null) {
            this.requester.getChatLog(
                    new ChatLogRequest(
                            UserSessionManager.getUserId(),
                            this.partnerID
                    ),
                    new CallbackAdapter<>(
                            this.chatPresenter::presentChat,
                            throwable -> this.chatPresenter.presentNoConnection(),
                            throwable -> this.chatPresenter.presentNoConnection()
                    )
            );
            this.requester.setOnMessageReceived(new CallbackAdapter<>(
                    chatPresenter::presentNewReceivedMessage,
                    null,
                    null
            ));
        }
    }

    @Override
    public void setPartnerID(Long partnerID) {
        this.partnerID = partnerID;
    }

    @Override
    public void onSend(String message) {
        LocalTime localTime = LocalTime.now();
        Instant instant = localTime
                .atDate(LocalDate.now())
                .atZone(ZoneId.systemDefault()).toInstant();

        this.requester.send(
                new OutgoingChatMessage(message, Date.from(instant), UserSessionManager.getUserId(), this.partnerID),
                new CallbackAdapter<>(
                        (incomingMsg) -> {},
                        null,
                        null
                )
        );

        this.chatPresenter.presentNewSentMessage(message);
    }

    @Override
    public void onUnmatch(Long userToUnmatchID) {
        //TODO: unmatch implem.
    }

    @Override
    public void onWindowClose() {
        this.chatPresenter.onExit();
    }
}