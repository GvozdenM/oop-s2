package MatchAlertPresenter;

import Utils.Swing.ListView.DefaultListView.DefaultListViewModel;

public class MatchViewModel extends DefaultListViewModel {
    private String username;
    private String imageUrl;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImage() {
        return imageUrl;
    }

    public void setImage(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
