package MatchAlertPresenter;

import FeedController.MatchAlertPresenter;
import FeedInteractor.FeedActionUserResponse;

/**
 * This class provides a way to alert the user with a notification whenever a match has been done.
 * @see MatchAlertPresenter list of methods implemented
 */
public class MatchAlertScreenPresenter implements MatchAlertPresenter {
    private final MatchAlertView view;

    public MatchAlertScreenPresenter(MatchAlertViewFactory viewFactory) {
        this.view = viewFactory.create();
    }

    @Override
    public void presentMatch(FeedActionUserResponse match) {
        MatchViewModel vm = new MatchViewModel();
        vm.setImage(match.getProfileImageUrl());
        vm.setUsername(match.getUsername());
        this.view.displayMatch(vm);
    }
}
