package MatchAlertPresenter;

public abstract class MatchAlertViewFactory {
    public abstract MatchAlertView create();
}
