package MatchAlertPresenter;

import FeedController.MatchAlertPresenter;
import FeedController.MatchAlertPresenterFactory;
import MatchAlertView.SwingMatchAlertViewFactory;

public class MatchAlertScreenPresenterFactory extends MatchAlertPresenterFactory {

    @Override
    public MatchAlertPresenter create() {
        return new MatchAlertScreenPresenter(
                new SwingMatchAlertViewFactory()
        );
    }
}
