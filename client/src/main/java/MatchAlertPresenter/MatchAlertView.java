package MatchAlertPresenter;

public interface MatchAlertView {
    void displayMatch(MatchViewModel match);
}
