package ClientMessagingEndpoint;

import MatchedUsersInteractor.MatchedUsersRequest;
import MatchedUsersInteractor.MatchedUsersRequester;
import MatchedUsersInteractor.MatchedUsersResponse;
import MatchedUsersInteractor.MatchedUsersUnmatchRequest;
import Utils.Callbacks.CallbackAdapter;
import Utils.MessageRequester;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

public class MatchedUsersMessageRequester
        extends MessageRequester
        implements MatchedUsersRequester<MatchedUsersRequest, MatchedUsersUnmatchRequest, MatchedUsersResponse> {
    /**
     * Default constructor
     *
     * @param connection JMS Connection object created via {@link ConnectionFactory}
     */
    public MatchedUsersMessageRequester(Connection connection) {
        super(connection);
    }

    @Override
    public void getMatchedUsers(MatchedUsersRequest request, CallbackAdapter<MatchedUsersResponse> callback) {
        try {
            this.listenForMessagesOn("queue/matchedUsersResponses", callback);

            this.makeRequestViaMessage(
                    request,
                    "queue/matchedUsersRequests",
                    "queue/matchedUsersResponses"
            );

        } catch (JMSException e) {
            callback.onException(e);
            e.printStackTrace();
        }
    }

    @Override
    public void unmatchUser(MatchedUsersUnmatchRequest request, CallbackAdapter<Class<Void>> callback) {
        try {
            this.makeRequestViaMessage(
                    request,
                    "queue/unmatchUserRequests",
                    null
            );

            callback.onSuccess(Void.class);
        } catch (JMSException e) {
            callback.onException(e);
            e.printStackTrace();
        }
    }
}
