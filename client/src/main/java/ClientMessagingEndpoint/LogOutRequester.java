package ClientMessagingEndpoint;

import Utils.MessageRequester;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

public class LogOutRequester extends MessageRequester {
    /**
     * Default constructor
     *
     * @param connection JMS Connection object created via {@link ConnectionFactory}
     */
    public LogOutRequester(Connection connection) {
        super(connection);
    }

    public void logOut(Long id) {
        try {
            //send request
            this.makeRequestViaMessage(
                    id,
                    "queue/logOutRequests",
                    null
            );

        } catch (JMSException e) {
            e.printStackTrace();
            System.out.println("Failed to log out :(((((((");
        };
    }
}
