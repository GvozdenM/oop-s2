package ClientMessagingEndpoint;

import RegistrationInteractor.RegistrationRequest;
import RegistrationInteractor.RegistrationRequester;
import RegistrationInteractor.RegistrationResponse;
import Utils.Callbacks.CallbackAdapter;
import Utils.MessageRequester;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

public class RegistrationMessageRequester
        extends MessageRequester
        implements RegistrationRequester<RegistrationRequest, RegistrationResponse> {
    /**
     * Default constructor
     *
     * @param connection JMS Connection object created via {@link ConnectionFactory}
     */
    public RegistrationMessageRequester(Connection connection) {
        super(connection);
    }

    @Override
    public void register(RegistrationRequest request, CallbackAdapter<RegistrationResponse> callback) {
        try {
            this.listenForMessagesOn("queue/registrationResponses", callback);

            this.makeRequestViaMessage(
                    request,
                    "queue/registrationRequests",
                    "queue/registrationResponses"
            );

        } catch (JMSException e) {
            callback.onException(e);
            e.printStackTrace();
        }
    }
}
