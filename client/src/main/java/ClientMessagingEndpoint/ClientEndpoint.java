package ClientMessagingEndpoint;

import ChatInteractor.*;
import FeedController.MatchAlertPresenter;
import FeedController.MatchAlertPresenterFactory;
import FeedInteractor.*;
import LoginInteractor.LoginRequest;
import LoginInteractor.LoginRequester;
import LoginInteractor.LoginRequesterFactory;
import LoginInteractor.LoginResponse;
import MatchedUsersInteractor.*;
import RegistrationInteractor.RegistrationRequest;
import RegistrationInteractor.RegistrationRequester;
import RegistrationInteractor.RegistrationRequesterFactory;
import RegistrationInteractor.RegistrationResponse;
import UpdateProfileInteractor.UpdateProfileRequest;
import UpdateProfileInteractor.UpdateProfileRequester;
import UpdateProfileInteractor.UpdateProfileRequesterFactory;
import UpdateProfileInteractor.UserProfileResponse;
import Utils.JMSlim;
import MatchAlertPresenter.*;

import javax.jms.*;
import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Supplier;

public class ClientEndpoint
        implements Runnable,
        Closeable,
        FeedRequesterFactory<FeedActionRequest, FeedActionResponse, FeedActionUserResponse>,
        LoginRequesterFactory<LoginRequest, LoginResponse>,
        MatchedUsersRequesterFactory<MatchedUsersRequest, MatchedUsersUnmatchRequest, MatchedUsersResponse>,
        RegistrationRequesterFactory<RegistrationRequest, RegistrationResponse>,
        UpdateProfileRequesterFactory<UpdateProfileRequest, UserProfileResponse>,
        AbstractChatRequesterFactory<ChatLogRequest, OutgoingChatMessage, IncomingChatMessage, ChatLogResponse> {
    private static ClientEndpoint ENDPOINT = null;

    private final JMSlim jmSlim = JMSlim.getJMSlim();
    private final ReentrantLock lock = new ReentrantLock(true);

    private Connection connection;
    private boolean started = false;
    private final MatchAlertPresenterFactory matchAlertPresenterFactory = new MatchAlertScreenPresenterFactory();

    public static ClientEndpoint getInstance() {
        if(null == ENDPOINT) {
            ENDPOINT = new ClientEndpoint();
        }

        return ENDPOINT;
    }

    /**
     * Default constructor gets connection instance but does not start it
     */
    public ClientEndpoint() {
        try {
            this.connection = ((ConnectionFactory) this.jmSlim.lookup("ConnectionFactory")).createConnection();
        } catch (JMSException ignored) {}
    }

    /**
     * Initiates the queues exposed on the end-point and starts
     * the connection to the messaging server
     *
     * {@inheritDoc}
     */
    @Override
    public void run() {
        this.lock.lock();
        try {
            this.initIncomingQueues(this.connection);
            this.initOutgoingQueues();
            this.connection.start();
            this.started = true;
        } catch (JMSException e) {
            e.printStackTrace();
        }
        finally {
            this.lock.unlock();
        }
    }

    private void initOutgoingQueues() {
        this.lock.lock();
        try {
            this.jmSlim.createQueue("loginRequests");
            this.jmSlim.createQueue("feedActionRequests");
            this.jmSlim.createQueue("matchedUsersRequests");
            this.jmSlim.createQueue("unmatchUserRequests");
            this.jmSlim.createQueue("registrationRequests");
            this.jmSlim.createQueue("updateProfileRequests");
            this.jmSlim.createQueue("chatMessageSend");
            this.jmSlim.createQueue("userByIdRequests");
            this.jmSlim.createQueue("feedBufferRequests");
            this.jmSlim.createQueue("chatLogRequests");
            this.jmSlim.createQueue("feedUserByIdRequests");
            this.jmSlim.createQueue("logOutRequests");
        }
        finally {
            this.lock.unlock();
        }
    }

    private void initIncomingQueues(Connection connection) throws JMSException {
        this.lock.lock();
        try {
            Destination feedActionResponses = this.jmSlim.createQueue("feedActionResponses");
            this.jmSlim.createQueue("loginResponses");
            this.jmSlim.createQueue("matchedUsersResponses");
            this.jmSlim.createQueue("registrationResponses");
            this.jmSlim.createQueue("chatMessageReceive");
            this.jmSlim.createQueue("userByIdResponses");
            this.jmSlim.createQueue("feedBufferResponses");
            this.jmSlim.createQueue("updateProfileResponse");
            this.jmSlim.createQueue("chatLogResponses");
            this.jmSlim.createQueue("feedUserByIdResponses");

            startListeningForMatchAlerts(connection.createSession(), feedActionResponses);
        }
        finally {
            this.lock.unlock();
        }
    }

    private void startListeningForMatchAlerts(Session session, Destination feedActionResponses) throws JMSException {
        session
                .createConsumer(feedActionResponses)
                .setMessageListener((message) -> {
                    FeedActionResponse onSuccessResponse;
                    try {
                        onSuccessResponse = (FeedActionResponse) ((ObjectMessage) message).getObject();

                        if (onSuccessResponse.isMatched()) {
                            MatchAlertPresenter matchAlertPresenter = this.matchAlertPresenterFactory.create();
                            matchAlertPresenter.presentMatch(
                                    onSuccessResponse.getMatchedWith()
                            );
                        }
                    } catch (JMSException ignored) {}

                    }
                );
    }

    private <T> T lockedStartProtectedFactoryMethod(Supplier<T> factoryMethod) {
        this.lock.lock();
        try {
            if(!this.started) {
                this.run();
            }

            return factoryMethod.get();
        }
        finally {
            this.lock.unlock();
        }
    }

    @Override
    public FeedRequester<FeedActionRequest, FeedActionResponse, FeedActionUserResponse> createFeedRequester() {
        return this.lockedStartProtectedFactoryMethod(() ->  new FeedMessageRequester(this.connection));
    }
    
    public LoginRequester<LoginRequest, LoginResponse> createLoginRequester() {
        return this.lockedStartProtectedFactoryMethod(() -> new LoginMessageRequester(this.connection));
    }

    @Override
    public MatchedUsersRequester<MatchedUsersRequest,
                                MatchedUsersUnmatchRequest,
                                MatchedUsersResponse> createMatchedUsersRequester() {
        return this.lockedStartProtectedFactoryMethod(() -> new MatchedUsersMessageRequester(this.connection));
    }

    @Override
    public RegistrationRequester<RegistrationRequest, RegistrationResponse> createRegistrationRequester() {
        return this.lockedStartProtectedFactoryMethod(() -> new RegistrationMessageRequester(this.connection));
    }

    @Override
    public UpdateProfileRequester<UpdateProfileRequest, UserProfileResponse> createUpdateProfileRequester() {
        return this.lockedStartProtectedFactoryMethod(() -> new UpdateProfileMessageRequester(this.connection));
    }

    @Override
    public ChatRequester<ChatLogRequest, OutgoingChatMessage, IncomingChatMessage, ChatLogResponse> createChatRequester() {
        return this.lockedStartProtectedFactoryMethod(() -> new ChatMessageRequester(this.connection));
    }

    public LogOutRequester createLogOutRequester() {
        return new LogOutRequester(this.connection);
    }

    @Override
    public void close() throws IOException {
        try {
            this.connection.close();
        } catch (JMSException e) {
            throw new IOException(e.getMessage());
        }
    }
}
