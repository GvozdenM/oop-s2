package ClientMessagingEndpoint;

import ChatInteractor.*;
import Utils.Callbacks.CallbackAdapter;
import Utils.MessageRequester;
import org.jetbrains.annotations.NotNull;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

public class ChatMessageRequester
        extends MessageRequester
        implements ChatRequester<ChatLogRequest, OutgoingChatMessage, IncomingChatMessage, ChatLogResponse> {
    /**
     * Default constructor
     *
     * @param connection JMS Connection object created via {@link ConnectionFactory}
     */
    public ChatMessageRequester(Connection connection) {
        super(connection);
    }

    @Override
    public void send(@NotNull OutgoingChatMessage message, @NotNull CallbackAdapter<IncomingChatMessage> callback) {
        try {
            this.makeRequestViaMessage(message, "queue/chatMessageSend", null);
        } catch (JMSException e) {
            callback.onException(e);
        }
    }

    @Override
    public void setOnMessageReceived(@NotNull CallbackAdapter<IncomingChatMessage> callback) {
        try {
            this.listenForMessagesOn("queue/chatMessageReceive", callback);
        } catch (JMSException e) {
            callback.onFail(e);
        }
    }

    @Override
    public void getChatLog(@NotNull ChatLogRequest request, @NotNull CallbackAdapter<ChatLogResponse> callback) {
        try {
            //start listening for responses
            this.listenForMessagesOn("queue/chatLogResponses", callback);

            //send request
            this.makeRequestViaMessage(
                    request,
                    "queue/chatLogRequests",
                    "queue/chatLogResponses"
            );

        } catch (JMSException e) {
            callback.onException(e);
        }
    }
}
