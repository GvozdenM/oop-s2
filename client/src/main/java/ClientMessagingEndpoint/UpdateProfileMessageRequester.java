package ClientMessagingEndpoint;

import UpdateProfileInteractor.UpdateProfileRequest;
import UpdateProfileInteractor.UpdateProfileRequester;
import UpdateProfileInteractor.UserProfileResponse;
import Utils.Callbacks.CallbackAdapter;
import Utils.MessageRequester;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

public class UpdateProfileMessageRequester
        extends MessageRequester
        implements UpdateProfileRequester<UpdateProfileRequest, UserProfileResponse> {

    /**
     * Default constructor
     *
     * @param connection JMS Connection object created via {@link ConnectionFactory}
     */
    public UpdateProfileMessageRequester(Connection connection) {
        super(connection);
    }

    @Override
    public void updateUser(UpdateProfileRequest request, CallbackAdapter<UserProfileResponse> callback) {
        try {
            this.listenForMessagesOn("queue/updateProfileResponse", callback);

            this.makeRequestViaMessage(
                    request,
                    "queue/updateProfileRequests",
                    "queue/updateProfileResponse"
            );

        } catch (JMSException e) {
            callback.onException(e);
            e.printStackTrace();
        }
    }

    @Override
    public void getUser(long id, CallbackAdapter<UserProfileResponse> callback) {
        try {
            this.listenForMessagesOn("queue/userByIdResponses", callback);

            this.makeRequestViaMessage(
                    id,
                    "queue/userByIdRequests",
                    "queue/userByIdResponses"
            );

        } catch (JMSException e) {
            callback.onException(e);
            e.printStackTrace();
        }
    }
}
