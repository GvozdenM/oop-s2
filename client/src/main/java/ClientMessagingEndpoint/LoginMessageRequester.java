package ClientMessagingEndpoint;

import LoginInteractor.LoginRequest;
import LoginInteractor.LoginRequester;
import LoginInteractor.LoginResponse;
import Utils.Callbacks.CallbackAdapter;
import Utils.MessageRequester;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

public class LoginMessageRequester extends MessageRequester implements LoginRequester<LoginRequest, LoginResponse> {
    /**
     * Default constructor
     *
     * @param connection JMS Connection object created via {@link ConnectionFactory}
     */
    public LoginMessageRequester(Connection connection) {
        super(connection);
    }

    @Override
    public void login(LoginRequest request, CallbackAdapter<LoginResponse> callback) {
        try {
            //start listening for responses
            this.listenForMessagesOn("queue/loginResponses", callback);

            //send request
            this.makeRequestViaMessage(
                    request,
                    "queue/loginRequests",
                    "queue/loginResponses"
            );

        } catch (JMSException e) {
            callback.onException(e);
        }
    }
}
