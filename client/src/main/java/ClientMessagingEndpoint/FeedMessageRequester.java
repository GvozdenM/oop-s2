package ClientMessagingEndpoint;

import FeedInteractor.FeedActionRequest;
import FeedInteractor.FeedActionResponse;
import FeedInteractor.FeedActionUserResponse;
import FeedInteractor.FeedRequester;
import Utils.Callbacks.CallbackAdapter;
import Utils.MessageRequester;

import javax.jms.Connection;
import javax.jms.JMSException;
import java.util.List;

public class FeedMessageRequester
        extends MessageRequester
        implements FeedRequester<FeedActionRequest, FeedActionResponse, FeedActionUserResponse> {

    public FeedMessageRequester(Connection connection) {
        super(connection);
    }

    @Override
    public void getUserFeedBuffer(long id, CallbackAdapter<List<FeedActionUserResponse>> callback) {
        try {
            //start listening for responses
            this.listenForMessagesOn("queue/feedBufferResponses", callback);

            //send request
            this.makeRequestViaMessage(
                    id,
                    "queue/feedBufferRequests",
                    "queue/feedBufferResponses"
            );

        } catch (JMSException e) {
            callback.onException(e);
        }
    }

    @Override
    public void interact(FeedActionRequest interaction, CallbackAdapter<FeedActionResponse> callback) {
        try {
            //start listening for responses
//            this.listenForMessagesOn("queue/feedActionResponses", callbackAdapter);

            //send request
            this.makeRequestViaMessage(
                    interaction,
                    "queue/feedActionRequests",
                    "queue/feedActionResponses"
            );
        } catch (JMSException e) {
            callback.onException(e);
        }
    }

    @Override
    public void getUserById(long id, CallbackAdapter<FeedActionUserResponse> callback) {
        try {
            //start listening for responses
            this.listenForMessagesOn("queue/feedUserByIdResponses", callback);

            //send request
            this.makeRequestViaMessage(
                    id,
                    "queue/feedUserByIdRequests",
                    "queue/feedUserByIdResponses"
            );

        } catch (JMSException e) {
            callback.onException(e);
        }
    }
}
