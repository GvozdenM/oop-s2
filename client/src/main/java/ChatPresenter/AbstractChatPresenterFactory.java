package ChatPresenter;

import ChatController.ChatPresenterI;

public abstract class AbstractChatPresenterFactory {
    public abstract ChatPresenterI create();
}
