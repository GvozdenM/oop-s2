package ChatPresenter;

import ChatController.ChatPresenterI;

public abstract class AbstractChatViewFactory {
    public abstract ChatView create(ChatPresenterI presenterI, Long receiverID, String receiverName, String imgUrl);
}
