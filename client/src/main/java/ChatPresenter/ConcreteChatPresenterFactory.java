package ChatPresenter;

import ChatController.ChatPresenterI;
import ChatView.ConcreteChatViewFactory;

public class ConcreteChatPresenterFactory extends AbstractChatPresenterFactory{

    @Override
    public ChatPresenterI create() {
        return new ChatPresenter(
                new ConcreteChatViewFactory()
        );
    }
}
