package ChatPresenter;

import ChatController.ChatPresenterI;
import ChatInteractor.ChatLogResponse;
import ChatInteractor.IncomingChatMessage;
import ChatInteractor.LoggedChatMessage;
import UserSession.UserSessionManager;
import Utils.listeners.interfaces.SimpleOnClickListener;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * The ChatPresenter is in charge of delegating requests from the attached ChatController
 * in order to operate the view externally.
 * This class implements the methods and functions of its respective interface.
 * @see ChatPresenterI
 */
public class ChatPresenter implements ChatPresenterI {
    private final AbstractChatViewFactory viewFactory;

    private ChatView chatView;

    private SimpleOnClickListener onExitListener;

    public ChatPresenter(AbstractChatViewFactory factory) {
        this.viewFactory = factory;
    }

    @Override
    public void presentChat(ChatLogResponse chatLogResponse) {
        this.chatView.updateHeaderContent(chatLogResponse.getPartner().getUsername(), chatLogResponse.getPartner().getProfileImageUrl());

        Collection<ChatMessageViewModel> chatMessageCollection = new ArrayList<ChatMessageViewModel>();

        for (LoggedChatMessage chatMessage : chatLogResponse.getMessagesSortedByTimestamp()) {
            ChatMessageViewModel vm = new ChatMessageViewModel();

            vm.setMessage(chatMessage.getMessage());
            vm.setTimestamp(chatMessage.getTimestamp());
            vm.setSentByThisUser(
                    UserSessionManager.getUserId() == chatMessage.getSenderId()
            );

            chatMessageCollection.add(vm);
        }

        this.chatView.displayChat(chatMessageCollection);
    }

    @Override
    public void init(Long receiverID, String receiverName) {
        this.chatView = viewFactory.create(this, receiverID, receiverName, null);
        this.chatView.onTransitionTo();
    }

    @Override
    public void init(Long receiverID, String receiverName, String imgUrl, SimpleOnClickListener onExitListener) {
        this.chatView = viewFactory.create(this, receiverID, receiverName, imgUrl);
        this.onExitListener = onExitListener;
        this.chatView.onTransitionTo();
    }

    @Override
    public void presentNoConnection() {
        this.chatView.displayMsgDialog("ERROR: Could not fetch chat log from the server..");
        this.chatView.end();
    }

    @Override
    public void presentNewSentMessage(String message) {
        LocalTime localTime = LocalTime.now();
        Instant instant = localTime
                .atDate(LocalDate.now())
                .atZone(ZoneId.systemDefault()).toInstant();

        ChatMessageViewModel chatMessage = new ChatMessageViewModel();

        chatMessage.setSentByThisUser(true);
        chatMessage.setTimestamp(Date.from(instant));
        chatMessage.setMessage(message);
        chatMessage.setSentSuccessfully(true);

        this.chatView.displayNewMessage(chatMessage);
        this.chatView.clearMessageInput();
    }

    @Override
    public void presentNewReceivedMessage(IncomingChatMessage message) {
        ChatMessageViewModel chatMessage = new ChatMessageViewModel();

        chatMessage.setSentByThisUser(false);
        chatMessage.setTimestamp(message.getTimestamp());
        chatMessage.setMessage(message.getMessage());

        this.chatView.displayNewMessage(chatMessage);
    }

    @Override
    public void onExit() {
        if (this.onExitListener != null) this.onExitListener.onClick();
    }

    @Override
    public void onUnmatch() {
        this.chatView.end();
    }
}
