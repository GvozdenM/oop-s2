package ChatPresenter;

import java.util.Date;

public class ChatMessageViewModel {
    private String message;
    private Date timestamp;
    private boolean sentByThisUser;
    private boolean sentSuccessfully;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isSentByThisUser() {
        return sentByThisUser;
    }

    public void setSentByThisUser(boolean sentByThisUser) {
        this.sentByThisUser = sentByThisUser;
    }

    public boolean isSentSuccessfully() {
        return sentSuccessfully;
    }

    public void setSentSuccessfully(boolean sentSuccessfully) {
        this.sentSuccessfully = sentSuccessfully;
    }
}

