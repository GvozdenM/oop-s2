package ChatPresenter;


import Utils.Views.TransitionView;

import java.util.Collection;

/**
 * This interface provides the functions to be implemented by the ChatView Class
 */
public interface ChatView extends TransitionView {
    String EMPTY_TEXT_FIELD = "";

    /**
     * Provides for the functionality to display a list of past messages, sorted by timestamp.
     * @param viewModels collection of past messages
     */
    void displayChat(Collection<ChatMessageViewModel> viewModels);

    /**
     * This method provides an implementation to display a newly arrived message as obtained by the requester to be
     * added to the current view and chat log.
     * @param viewModel
     */
    void displayNewMessage(ChatMessageViewModel viewModel);

    /**
     * This method is called whenever the existing frame is successfully exited or closed.
     */
    void end();

    /**
     * This method provides an implemenmtation to change the header information on the user to chat with.
     * @param partnerName
     * @param partnerImgUrl
     */
    void updateHeaderContent(String partnerName, String partnerImgUrl);

    /**
     * This method provides an implementation for a notification of a warning in the current view
     * @param msg Warning message to be displayed.
     */
    void displayMsgDialog(String msg);

    /**
     * This method clears the Text Edit section in the chat view.
     */
    void clearMessageInput();

    //SETTERS
    void setMessageStateToSent(String hashId);

    void setUserImgUrl(String imgUrl);

}
