package UpdateProfilePresenter;

import UpdateProfileController.UpdateProfilePresenterI;
import UpdateProfileInteractor.UserProfileResponse;

/**
 * The UpdateProfilePresenter is in charge of delegating requests from the attached UpdateProfileController
 * in order to operate the view externally.
 * This class implements the methods and functions of its respective interface.
 * @see UpdateProfilePresenterI
 */
public class UpdateProfileScreenPresenter implements UpdateProfilePresenterI {
    private UpdateProfileView view;

    public UpdateProfileScreenPresenter(AbstractUpdateProfileViewFactory profileViewFactory) {
        this.view = profileViewFactory.create(this);

    }

    @Override
    public void init() {
        this.view.onTransitionTo();
    }

    @Override
    public void presentProfile(UserProfileResponse profile) {
        UpdateProfileViewModel vm = new UpdateProfileViewModel(
                profile.getId(),
                profile.getUsername(),
                profile.getName(),
                profile.getAge(),
                profile.getEmail(),
                profile.getPassword(),
                profile.getInterest(),
                profile.isPremium(),
                profile.getProfileImgUrl(),
                profile.getImgUrls(),
                profile.getShortDescription(),
                profile.getDescription()
        );

        this.view.displayProfile(vm);
    }

    @Override
    public void presentUpdatedProfileSuccess(UserProfileResponse profile) {
        presentProfile(profile);
        this.view.displayMsgDialog("Profile updated!");
    }

    @Override
    public void presentNoConnectionError() {
        this.view.displayErrorMsg("Connection could not be established");
    }

    @Override
    public void presentIncorrectArguments(String msg) {
        this.view.displayErrorMsg("Invalid input parameters.\n" + msg +" incorrect");
    }
}
