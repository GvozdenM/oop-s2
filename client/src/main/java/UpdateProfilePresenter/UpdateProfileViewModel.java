package UpdateProfilePresenter;

import java.awt.image.BufferedImage;
import java.util.List;

/**
 * This class provides a general data model to refer to a user and its available information.
 */
public class UpdateProfileViewModel {

    private final long id;
    private final String username;
    private final String name;
    private final int age;
    private final String email;
    private final String password;
    private final String interest;
    private final boolean isPremium;
    private final String profileImgUrl;
    private final List<String> imgUrls;
    private final String shortDescription;
    private final String description;

    public UpdateProfileViewModel(long id, String username, String name, int age, String email, String password,
                                  String interest, boolean isPremium, String profileImgUrl, List<String> imgUrls,
                                  String shortDescription, String description) {
        this.id = id;
        this.username = username;
        this.name = name;
        this.age = age;
        this.email = email;
        this.password = password;
        this.interest = interest;
        this.isPremium = isPremium;
        this.profileImgUrl = profileImgUrl;
        this.imgUrls = imgUrls;
        this.shortDescription = shortDescription;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getInterest() {
        return interest;
    }

    public boolean isPremium() {
        return isPremium;
    }

    public String getProfileImgUrl() {
        return profileImgUrl;
    }

    public List<String> getImgUrls() {
        return imgUrls;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public String getDescription() {
        return description;
    }
}
