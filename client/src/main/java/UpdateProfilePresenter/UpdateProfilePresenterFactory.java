package UpdateProfilePresenter;

import UpdateProfileController.AbstractUpdateProfilePresenterFactory;
import UpdateProfileController.UpdateProfilePresenterI;
import UpdateProfileView.UpdateProfileViewFactory;

public class UpdateProfilePresenterFactory extends AbstractUpdateProfilePresenterFactory {

    @Override
    public UpdateProfilePresenterI create() {
        return new UpdateProfileScreenPresenter(
                new UpdateProfileViewFactory()
        );
    }
}
