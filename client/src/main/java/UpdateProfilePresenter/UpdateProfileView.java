package UpdateProfilePresenter;

import Utils.Views.TransitionView;

/**
 * This interface provides the functions to be implemented by the UpdateProfileView Class
 */
public interface UpdateProfileView extends TransitionView {

    /**
     * This method is called for a display of a message in the current View
     * @param msg Text message to be displayed.
     */
    void displayMsgDialog(String msg);

    /**
     * This method is called for a display of an error in the current View
     * @param msg Error message to be displayed.
     */
    void displayErrorMsg(String msg);

    /**
     * Updates information of given user with the corresponding areas in the view.
     * @param vm An object containing all of the relevant data with the user.
     */
    void displayProfile(UpdateProfileViewModel vm);

}
