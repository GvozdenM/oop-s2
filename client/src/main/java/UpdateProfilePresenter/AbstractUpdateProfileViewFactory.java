package UpdateProfilePresenter;


import UpdateProfileController.UpdateProfilePresenterI;

public abstract class AbstractUpdateProfileViewFactory {

    public abstract UpdateProfileView create(UpdateProfilePresenterI presenter);
}
