package MatchedUsersView;

import MatchedUsersController.MatchedUsersControllerI;
import MatchedUsersPresenter.MatchedUsersViewModel;
import Utils.Constants;
import Utils.Swing.Image.ImagePanel;
import Utils.Swing.ListView.Interface.ListViewAdapter;
import Utils.Swing.ListView.Interface.ListViewHolder;
import Utils.Views.ResourceFetcher;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * This class serves as the translation of the initial ArrayList of matched users and converts it into a listview
 * with its respective listeners.
 */
public class SwingMatchedUsersViewAdapter extends ListViewAdapter {

    private final MatchedUsersControllerI listenerI;

    private final ArrayList<Long> userIDs;
    private final ArrayList<String> userLogins;
    private final ArrayList<String> imgUrls;

    public SwingMatchedUsersViewAdapter(MatchedUsersViewModel viewModel,
                                        MatchedUsersControllerI listenerI) {
        this.listenerI = listenerI;

        this.userIDs = new ArrayList<>();
        this.userLogins = new ArrayList<>();
        this.imgUrls = new ArrayList<>();

        for (MatchedUsersViewModel.User u : viewModel.getUsers()) {
            this.userIDs.add(u.userID);
            this.userLogins.add(u.userLogin);
            this.imgUrls.add(u.userImgUrl);
        }
    }

    @Override
    public ListViewHolder onCreateViewHolder(int position) {
        SwingMatchedUsersViewHolder holder = new SwingMatchedUsersViewHolder();

        holder.setBtnChatListener(l -> listenerI.onChatBtnClicked(
                this.userIDs.get(position),
                this.userLogins.get(position),
                this.imgUrls.get(position)
        ));
        holder.setBtnUnmatchListener(l -> listenerI.onUnmatchUserBtnClicked(this.userIDs.get(position)));

        holder.setTitle(this.userLogins.get(position));

        try {
            holder.setImgUrl(new URL(this.imgUrls.get(position)));
        } catch (MalformedURLException e) {
            e.printStackTrace();
            //Could not load img url, Load default img
            ResourceFetcher rf = new ResourceFetcher("defaultuserimg.png");
            try {
                holder.setImgUrl(rf.getResourceFile().getPath());
            } catch (IOException ioException) {
                ioException.printStackTrace();
                holder.setImgUrl(Constants.COSMETICS.DEFAULT_USER_IMG_URL);
            }
        }

        return holder;
    }

    @Override
    public int getNumItems() {
        return this.userIDs.size();
    }

    /**
     * This class provides the model for the implementation of one single item of the list in our array of user matches.
     */
    static class SwingMatchedUsersViewHolder extends ListViewHolder {

        private ImagePanel image;
        private JLabel lTitle;

        private JButton btnChat;
        private JButton btnUnmatch;

        /**
         * This method provides the JPanel final implementation of the list view.
         */
        @Override
        public void configurePanel() {
            int elementPadding = 40;
            this.setLayout(new FlowLayout());

            JPanel wrapperData = new JPanel();
            wrapperData.setLayout(new FlowLayout(FlowLayout.LEADING, elementPadding, elementPadding));

            this.image = new ImagePanel();
            this.lTitle = new JLabel();

            this.image.setImgSize(120, 120);
            this.add(this.image);
            JPanel wrapperCenter = new JPanel(new GridLayout(2, 1));
            wrapperCenter.add(this.lTitle);
            wrapperData.add(wrapperCenter);
            this.btnChat = new JButton("Chat");
            this.btnUnmatch = new JButton("Unmatch");
            JPanel wrapperButtons = new JPanel(new GridLayout(2,1));
            wrapperButtons.add(this.btnChat);
            wrapperButtons.add(this.btnUnmatch);
            wrapperData.add(wrapperButtons);
            this.add(wrapperData);
        }

        public void setImgUrl(URL imgUrl) {
            this.image.load(imgUrl);
            this.revalidate();
        }

        public void setImgUrl(String imgUrl) {
            this.image.load(imgUrl);
            this.revalidate();
        }

        public void setTitle(String title) {
            this.lTitle.setText(title);
        }

        public void setBtnChatListener(ActionListener listener) {
            this.btnChat.addActionListener(listener);
        }

        public void setBtnUnmatchListener(ActionListener listener) {
            this.btnUnmatch.addActionListener(listener);
        }

    }
}
