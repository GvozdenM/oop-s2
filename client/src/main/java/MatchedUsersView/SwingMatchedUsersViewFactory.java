package MatchedUsersView;

import MatchedUsersController.MatchedUsersControllerFactory;
import MatchedUsersController.MatchedUsersPresenter;
import MatchedUsersPresenter.AbstractMatchedUsersViewFactory;
import MatchedUsersPresenter.MatchedUsersView;

public class SwingMatchedUsersViewFactory extends AbstractMatchedUsersViewFactory {
    @Override
    public MatchedUsersView create(MatchedUsersPresenter presenter) {
        return new SwingMatchedUsersView(
                new MatchedUsersControllerFactory(presenter)
        );
    }
}
