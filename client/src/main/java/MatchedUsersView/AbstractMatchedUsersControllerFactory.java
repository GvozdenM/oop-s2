package MatchedUsersView;

import MatchedUsersController.MatchedUsersControllerI;
import MatchedUsersController.MatchedUsersPresenter;

public abstract class AbstractMatchedUsersControllerFactory {
    public abstract MatchedUsersControllerI create();
}
