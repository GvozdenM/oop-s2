package MatchedUsersView;

import FeedPresenter.FeedView;
import MatchedUsersController.MatchedUsersControllerI;
import MatchedUsersPresenter.MatchedUsersView;
import MatchedUsersPresenter.MatchedUsersViewModel;
import Utils.Swing.FrameManager.FrameManager;
import Utils.Swing.ListView.ListView;
import Utils.Views.ResourceFetcher;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

/**
 * This class is the main MatchedUsers view panel implementation as well as the implementation of given parent interface.
 * @see MatchedUsersView
 */
public class SwingMatchedUsersView extends JPanel implements MatchedUsersView {
    private final MatchedUsersControllerI controllerI;

    public SwingMatchedUsersView(AbstractMatchedUsersControllerFactory controllerFactory) {
        this.controllerI = controllerFactory.create();
        setSize(new Dimension(400, 660));
    }


    @Override
    public void displayUsersList(MatchedUsersViewModel vm) {
        SwingMatchedUsersViewAdapter viewAdapter = new SwingMatchedUsersViewAdapter(vm, this.controllerI);

        this.removeAll();

        ListView matchedUsersListView = ListView.builder()
                .setListViewAdapter(viewAdapter)
                .build();

        FrameManager frameManager = MatchedUsersFrameManagerFactory.create();
        frameManager.setContentPanel(matchedUsersListView);

        this.revalidate();
    }

    @Override
    public void displayErrorMsg(String errMsg) {
        SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(
                new JFrame(),
                "ERROR: " + errMsg,
                "Warning",
                JOptionPane.WARNING_MESSAGE
        ));
    }

    @Override
    public void end() {
        FrameManager frameManager = MatchedUsersFrameManagerFactory.create();
        frameManager.closeFrame();
    }

    @Override
    public void onTransitionTo() {
        ResourceFetcher resourceFetcher = new ResourceFetcher("favicon.jpg");

        FrameManager frameManager = MatchedUsersFrameManagerFactory.create();
        try {
            frameManager
                    .configureFramePreferences("Matched Users", getWidth(), getHeight())
                    .setResizable(false)
                    .setFavicon(resourceFetcher.getResourceFile().getPath());
            this.controllerI.onLoad();
        } catch (IOException e) {
            //favicon could not be loaded
            e.printStackTrace();
        }
    }
}
