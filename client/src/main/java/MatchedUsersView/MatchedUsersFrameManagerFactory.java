package MatchedUsersView;

import Utils.Swing.FrameManager.FrameManager;

public interface MatchedUsersFrameManagerFactory {
    FrameManager frameManager = FrameManager.newInstance();

    static FrameManager create() {
        return frameManager;
    }
}
