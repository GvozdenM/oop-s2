package RegistrationController;

import ClientMessagingEndpoint.ClientEndpoint;
import LoginPresenter.LoginPresenterFactory;

public class ConcreteRegistrationControllerFactory extends AbstractRegistrationControllerFactory {
    private final RegistrationPresenterI registrationPresenter;

    public ConcreteRegistrationControllerFactory(RegistrationPresenterI registrationPresenter) {
        this.registrationPresenter = registrationPresenter;
    }

    @Override
    public RegistrationControllerI create() {
        return new RegistrationController(
                this.registrationPresenter,
                new LoginPresenterFactory(),
                ClientEndpoint.getInstance().createRegistrationRequester()
        );
    }
}
