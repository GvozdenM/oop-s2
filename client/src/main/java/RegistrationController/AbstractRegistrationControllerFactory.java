package RegistrationController;

public abstract class AbstractRegistrationControllerFactory {

    public abstract RegistrationControllerI create();

}
