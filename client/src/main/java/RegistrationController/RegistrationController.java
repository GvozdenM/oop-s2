package RegistrationController;

import FeedController.FeedPresenter;
import FeedController.FeedPresenterFactory;
import FeedPresenter.FeedScreenPresenterFactory;
import LoginController.LoginPresenter;
import LoginController.AbstractLoginPresenterFactory;
import RegistrationInteractor.RegistrationRequest;
import RegistrationInteractor.RegistrationRequester;
import RegistrationInteractor.RegistrationResponse;
import UpdateProfileController.UpdateProfilePresenterI;
import UpdateProfilePresenter.UpdateProfilePresenterFactory;
import UserSession.UserSessionManager;
import Utils.Callbacks.CallbackAdapter;

/**
 * This Class implements all methods and functions related to the link between the presenter and the requester of the
 * Registration use case.
 * @see RegistrationControllerI list of methods implemented.
 */
public class RegistrationController implements RegistrationControllerI{
    private final RegistrationPresenterI presenter;
    private final AbstractLoginPresenterFactory loginPresenterFactory;
    private final RegistrationRequester<RegistrationRequest, RegistrationResponse> requester;

    public RegistrationController(RegistrationPresenterI presenter,
                                  AbstractLoginPresenterFactory loginPresenterFactory,
                                  RegistrationRequester<RegistrationRequest, RegistrationResponse> requester) {
        this.presenter = presenter;
        this.loginPresenterFactory = loginPresenterFactory;
        this.requester = requester;
    }

    /**
     * The Registration Controller will get the information and verify the password information, if true it will
     * generate a RegistrationRequest and call the requester associated method.
     * @param username
     * @param age
     * @param isPremium boolean indicating if the user is premium (true)
     * @param email
     * @param password
     * @param passwordRepeat
     * @param imgUrl
     */
    @Override
    public void onRegisterClick(String username, int age, boolean isPremium, String email, String password,
                                String interest, String passwordRepeat, String imgUrl) {

        if (verifyPassword(password, passwordRepeat)){
            if (verifyEmail(email)) {
                RegistrationRequest request = new RegistrationRequest(
                        username,
                        age,
                        isPremium,
                        password, email,
                        interest, imgUrl
                );
                this.requester.register(
                        request,
                        new CallbackAdapter<>(
                                (onSuccessResponse) -> {
                                    if(onSuccessResponse.isAuthenticated()) {
                                        UserSessionManager.setUserId(onSuccessResponse.getId());
                                        UserSessionManager.setUserProfileImgUrl(imgUrl);
                                        FeedPresenterFactory feedPresenterFactory = new FeedScreenPresenterFactory();
                                        FeedPresenter feedPresenter = feedPresenterFactory.create();
                                        feedPresenter.init();

                                        UpdateProfilePresenterFactory updateProfilePresenterFactory =
                                                new UpdateProfilePresenterFactory();
                                        UpdateProfilePresenterI updateProfilePresenter = updateProfilePresenterFactory.create();
                                        updateProfilePresenter.init();

                                        this.presenter.end();
                                    }
                                    else {
                                        this.presenter.presentAuthenticationError(onSuccessResponse);
                                    }
                                },
                                (onFailResponse) -> {
                                    presenter.presentError(onFailResponse.getMessage());
                                },
                                (onExceptionResponse) -> {
                                    presenter.presentError(onExceptionResponse.getMessage());
                                }
                        )
                );
            }
            else {
                this.presenter.presentAuthenticationError(
                        new RegistrationResponse(false, "\nEmail incorrect")
                );
            }
        } else {
            this.presenter.presentAuthenticationError(
                    new RegistrationResponse(false, "\nPassword incorrect\nMin. 4 chars, 1 Number and one uppercase")
            );
        }
    }

    private boolean verifyEmail(String email) {
        return email.matches("^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$");
    }


    @Override
    public void onFormFilled() {
        this.presenter.updatePresentingState(true);
    }

    /**
     * Creates a new Login View presenter and initializes it.
     */
    @Override
    public void onBackBtnClicked() {
        LoginPresenter presenterI = this.loginPresenterFactory.create();
        presenterI.init();
    }

    /**
     * This function verifies the password validity following that password is equal to passwordRepeat and it follows
     * that it has Upper and Lower case values as well as symbols and numbers.
     * @param password
     * @param passwordRepeat
     * @return true if valid
     */
    private boolean verifyPassword(String password, String passwordRepeat){
        return password.equals(passwordRepeat) && password.matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).+$");
    }
}
