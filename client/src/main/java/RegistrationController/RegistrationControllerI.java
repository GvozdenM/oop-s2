package RegistrationController;

/**
 * This interface provides a map of methods and functions to be implemented by the RegistrationController
 */
public interface RegistrationControllerI {
    /**
     * This signifies the user provided data for a registration call to the server side. It will take the data to
     * first verify its validity and will generate a RegistrationRequest
     * @param username
     * @param age
     * @param isPremium boolean indicating if the user is premium (true)
     * @param email
     * @param password
     * @param interest: the user's specified interest upon register
     * @param passwordRepeat
     * @param imgUrl
     */
    void onRegisterClick(String username, int age, boolean isPremium, String email, String password,
                         String interest, String passwordRepeat, String imgUrl);

    /**
     * This method calls for the View to allow user to initiate request to server side as information has been filled
     * in the view.
     */
    void onFormFilled();

    /**
     * Provides an implementation of going back to the LoginView
     */
    void onBackBtnClicked();
}
