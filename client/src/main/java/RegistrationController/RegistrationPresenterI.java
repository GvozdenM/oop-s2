package RegistrationController;

import RegistrationInteractor.RegistrationResponse;

/**
 * This interface provides a map of methods and functions to be implemented by the RegistrationPresenter
 */
public interface RegistrationPresenterI {

    /**
     * This method signifies the initialization of the Registration Presenter which needs to assign its working
     * parts, listeners and a RegistrationView.
     */
    void init();

    /**
     * This method is called to reset and clear all text fields in the view in order to reset it to its initial look.
     */
    void resetAndClear();

    /**
     * This method is called for a display of an error in the current View
     * @param errorMsg Error message to be displayed.
     */
    void presentError(String errorMsg);

    /**
     * This method is called for a display of an error in the current View when the authentication has failed.
     * @param onSuccessResponse
     */
    void presentAuthenticationError(RegistrationResponse onSuccessResponse);

    /**
     * This method calls for the View to allow the main interaction button to be pressed, in this case, whenever the
     * user has filled the provided text fields, the registration button will appear clickable.
     * @param formFilled true for registration form filled.
     */
    void updatePresentingState(boolean formFilled);

    /**
     * This method is called whenever the displayed Swing JPanel is closed or exited.
     */
    void end();
}
