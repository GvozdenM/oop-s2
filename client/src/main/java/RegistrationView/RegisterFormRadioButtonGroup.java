package RegistrationView;

import javax.swing.*;
import java.awt.*;

public class RegisterFormRadioButtonGroup extends JPanel {
    private final String REGULAR_BUTTON_TEXT = "Regular";
    private final String PREMIUM_BUTTON_TEXT = "Premium";

    private final JRadioButton regularButton;
    private final JRadioButton premiumButton;
    private final ButtonGroup buttonGroup;

    public RegisterFormRadioButtonGroup() {
        this.setLayout(new GridBagLayout());

        this.buttonGroup = new ButtonGroup();

        this.regularButton = new JRadioButton(REGULAR_BUTTON_TEXT);
        this.premiumButton = new JRadioButton(PREMIUM_BUTTON_TEXT);

        buttonGroup.add(this.regularButton);
        buttonGroup.add(this.premiumButton);

        regularButton.setSelected(true);

        GridBagConstraints c = new GridBagConstraints();

        JPanel btnWrapper = new JPanel(new GridLayout());
        btnWrapper.add(this.regularButton);
        btnWrapper.add(this.premiumButton);

        this.add(btnWrapper, c);

        this.setVisible(true);
    }

    public void configureLayoutConstraints(GridBagConstraints c) {
        c.insets = new Insets(20, 0, 0, 0);
    }

    public boolean getIsPremium() {
        return premiumButton.isSelected();
    }

    public void setPremiumButton(boolean isPremium) {
        if (isPremium) {
            this.regularButton.setSelected(false);
            this.premiumButton.setSelected(true);
        }
        else {
            this.regularButton.setSelected(true);
            this.premiumButton.setSelected(false);
        }
    }
}
