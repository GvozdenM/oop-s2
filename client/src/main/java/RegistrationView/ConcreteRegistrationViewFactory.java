package RegistrationView;

import RegistrationController.ConcreteRegistrationControllerFactory;
import RegistrationController.RegistrationPresenterI;
import RegistrationPresenter.AbstractRegistrationViewFactory;
import RegistrationPresenter.RegistrationView;

public class ConcreteRegistrationViewFactory extends AbstractRegistrationViewFactory {

    @Override
    public RegistrationView create(RegistrationPresenterI presenter) {
        return new SwingRegistrationView(
                new ConcreteRegistrationControllerFactory(presenter)
        );
    }
}
