package RegistrationView;

import LoginView.AuthenticationFrameManagerFactory;
import Utils.Constants;
import Utils.Logger.Log;
import Utils.Swing.FrameManager.FrameManager;
import RegistrationController.AbstractRegistrationControllerFactory;
import RegistrationController.RegistrationControllerI;
import RegistrationPresenter.RegistrationView;
import Utils.Adapters.TextChangeListenerSwingAdapter;
import Utils.Swing.Image.ImageFileFilter;
import Utils.Swing.Image.ImagePanel;
import Utils.Views.ResourceFetcher;
import Utils.listeners.interfaces.TextChangedListener;
import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

/**
 * This class provides the main JPanel in Registration View as well as attachers of parts like listeners and different
 * methods and functions for the Registration Use Case
 */
public class SwingRegistrationView extends JPanel implements RegistrationView {
    private static final String TAG = "SwingRegistrationView";


    private final int FORM_PANEL_SIDE_PADDING = 15;

    private final RegistrationControllerI controller;

    private final ImagePanel userImg;

    private final SwingRegistrationFormPanel formPanel;

    private final RegisterFormPanelButtonGroup buttonGroup;

    private String imgUrl;

    public SwingRegistrationView(AbstractRegistrationControllerFactory controllerFactory) {
        this.controller = controllerFactory.create();

        this.userImg = new ImagePanel();
        this.formPanel = new SwingRegistrationFormPanel();
        this.imgUrl = "";
        this.buttonGroup = new RegisterFormPanelButtonGroup();


        configurePanel();

        attachListeners();
    }

    private void configurePanel() {
        setLayout(new BorderLayout());
        JPanel formWrapper = new JPanel();
        formWrapper.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        ResourceFetcher resourceFetcher = new ResourceFetcher("defaultuserimg.png");
        this.userImg.setImgSize(280, 280);

        try {
            this.userImg.load(resourceFetcher.getResourceFile().getPath());
        } catch (IOException e) {
            Log.d(TAG, e.getMessage());
            loadUserImgUrl(Constants.COSMETICS.DEFAULT_USER_IMG_URL);
        }

        setFormPanelConstraints(c);
        this.add(this.userImg, BorderLayout.NORTH);
        formWrapper.add(this.formPanel, c);
        this.add(formWrapper, BorderLayout.CENTER);

        this.add(this.buttonGroup, BorderLayout.SOUTH);

        this.setVisible(true);
    }

    private void attachListeners() {
        this.userImg.setOnClickListener(this::onImageClicked);

        this.buttonGroup.attachRegisterBtnListener(
                actionEvent -> controller.onRegisterClick(
                        getUsername(),
                        getAge(),
                        getIsPremium(),
                        getEmail(),
                        getPassword(),
                        this.formPanel.getInterest(),
                        getRepeatPassword(),
                        getImgUrl()
                )
        );

        this.buttonGroup.attachBackBtnListener(
                actionEvent -> controller.onBackBtnClicked()
        );
    }

    private void setFormPanelConstraints(GridBagConstraints c) {
        c.insets = new Insets(
                15,
                FORM_PANEL_SIDE_PADDING,
                0,
                FORM_PANEL_SIDE_PADDING);
    }

    @Override
    public void onTransitionTo() {
        setRegisterBtnState(false);

        FrameManager frameManager = AuthenticationFrameManagerFactory.create();
        frameManager.setContentPanel(this);
        frameManager.configureFramePreferences("Registration");
    }

    @Override
    public void attachFormStateListener(TextChangedListener listener) {
        this.formPanel.attachUsernameChangeListener(new TextChangeListenerSwingAdapter(listener));
        this.formPanel.attachEmailChangeListener(new TextChangeListenerSwingAdapter(listener));
        this.formPanel.attachPasswordChangeListener(new TextChangeListenerSwingAdapter(listener));
        this.formPanel.attachRepeatPasswordChangeListener(new TextChangeListenerSwingAdapter(listener));
    }

    @Override
    public void displayErrorMsg(String errorMsg) {
        SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(
                new JFrame(),
                "ERROR: " + errorMsg,
                "Warning",
                JOptionPane.WARNING_MESSAGE
        ));
    }

    @Override
    public String getUsername() {
        return this.formPanel.getUsername();
    }

    @Override
    public int getAge() {
        return this.formPanel.getAge();
    }

    @Override
    public boolean getIsPremium() {
        return this.formPanel.isPremium();
    }

    @Override
    public String getEmail() {
        return this.formPanel.getEmail();
    }

    @Override
    public String getPassword() {
        return this.formPanel.getPassword();
    }

    @Override
    public String getRepeatPassword() {
        return this.formPanel.getRepeatPassword();
    }

    @Override
    public void setUsername(String input) {
        this.formPanel.setUsername(input);
    }

    @Override
    public void setAgeBoxByIndex(int index) {
        this.formPanel.setAgeByIndex(index);
    }

    @Override
    public void setIsPremium(boolean isPremium) {
        this.formPanel.setPremiumField(isPremium);
    }

    private void onImageClicked() {
        JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        fc.addChoosableFileFilter(new ImageFileFilter());
        fc.setAcceptAllFileFilterUsed(true);

        int ret = fc.showOpenDialog(this);

        if (ret == JFileChooser.APPROVE_OPTION) {
            File imgFile = fc.getSelectedFile();
            Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
                    "cloud_name", Constants.CLOUDINARY.CONFIGURATION_CLOUD_NAME,
                    "api_key", Constants.CLOUDINARY.CONFIGURATION_API_KEY,
                    "api_secret", Constants.CLOUDINARY.CONFIGURATION_SECRET
            ));
            Map uploadRes = null;
            try {
                uploadRes = cloudinary.uploader().upload(imgFile, ObjectUtils.asMap("folder", "Minder"));
                loadUserImgUrl(uploadRes.get("url").toString());
            } catch (IOException e) {
                Log.s("Could not upload img");
                e.printStackTrace();
            }
        }
    }


    private void loadUserImgUrl(String imgUrl) {
        try {
            this.imgUrl = imgUrl;
            this.userImg.setImgSize(280, 280);
            this.userImg.load(new URL(imgUrl));
            this.userImg.setOnClickListener(this::onImageClicked);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        this.revalidate();
    }

    public String getImgUrl() {
        if (imgUrl != null) return imgUrl;
        else return "";
    }

    @Override
    public void setEmail(String input) {
        this.formPanel.setEmail(input);
    }

    @Override
    public void setPassword(String input) {
        this.formPanel.setPassword(input);
    }

    @Override
    public void setRepeatPassword(String input) {
        this.formPanel.setRepeatPassword(input);
    }

    @Override
    public void setRegisterBtnState(boolean clickable) {
        this.buttonGroup.setLoginBtnState(clickable);;
    }

    @Override
    public void onClose() {
        FrameManager frameManager = AuthenticationFrameManagerFactory.create();
        frameManager
                .setCloseProcedure(false)
                .closeFrame();
    }
}
