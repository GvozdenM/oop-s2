package RegistrationView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class RegisterFormPanelButtonGroup extends JPanel {
    private final String REGISTER_BUTTON_TEXT = "Register";

    private final JButton registerButton;
    private final JButton jbBack;


    public RegisterFormPanelButtonGroup() {
        this.setLayout(new GridBagLayout());

        this.registerButton = new JButton(REGISTER_BUTTON_TEXT);
        this.jbBack = new JButton("Back");


        GridBagConstraints c = new GridBagConstraints();


        configureLayoutConstraints(c);

        this.add(this.jbBack, c);
        this.add(this.registerButton, c);

        this.setVisible(true);
    }

    public void configureLayoutConstraints(GridBagConstraints c) {
        c.insets = new Insets(20, 0, 5, 0);
    }

    public void setLoginBtnState(boolean clickable) {
        this.registerButton.setEnabled(clickable);
        this.registerButton.setBorderPainted(clickable);
        this.registerButton.setFocusPainted(clickable);
    }

    public void attachRegisterBtnListener(ActionListener listener) {
        this.registerButton.addActionListener(listener);
    }

    public void attachBackBtnListener(ActionListener listener) {
        this.jbBack.addActionListener(listener);
    }
}
