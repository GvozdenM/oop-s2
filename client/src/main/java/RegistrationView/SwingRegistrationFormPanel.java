package RegistrationView;

import Utils.Constants;

import javax.swing.*;
import javax.swing.event.DocumentListener;
import java.awt.*;

public class SwingRegistrationFormPanel extends JPanel {
    private final int GRID_LAYOUT_ROWS = 13;
    private final int GRID_LAYOUT_COLS = 2;
    private final int INPUT_FIELD_SIDES_PADDING = 20;

    private final int INTEREST_INDEX_JAVA = 0;
    private final int INTEREST_INDEX_C = 1;
    private final int INTEREST_INDEX_C_JAVA = 2;

    private final JTextField tfUsername;
    private final JComboBox cbAge;
    private final JTextField tfEmail;
    private final JPasswordField pfPassword;
    private final JPasswordField pfRepeatPassword;
    private final RegisterFormRadioButtonGroup bgIsPremium;
    private final JComboBox cbInterest;

    public SwingRegistrationFormPanel() {
        this.setLayout(new GridLayout(GRID_LAYOUT_ROWS, GRID_LAYOUT_COLS));

        JLabel lUsername = new JLabel("Username:");
        lUsername.setHorizontalAlignment(JLabel.CENTER);

        JLabel lAge = new JLabel("Age:");
        lAge.setHorizontalAlignment(JLabel.CENTER);

        JLabel lIsPremium = new JLabel("Account Type:");
        lIsPremium.setHorizontalAlignment(JLabel.CENTER);

        JLabel lEmail = new JLabel("Email:");
        lEmail.setHorizontalAlignment(JLabel.CENTER);

        JLabel lPassword = new JLabel("Password:");
        lPassword.setHorizontalAlignment(JLabel.CENTER);

        JLabel lRepeatPassword = new JLabel("Repeat Password:");
        lRepeatPassword.setHorizontalAlignment(JLabel.CENTER);

        GridBagConstraints cFormPanel = new GridBagConstraints();
        cFormPanel.insets = new Insets(0,
                INPUT_FIELD_SIDES_PADDING,
                0,
                INPUT_FIELD_SIDES_PADDING
        );

        this.tfUsername = new JTextField(20);
        this.tfUsername.setHorizontalAlignment(JTextField.CENTER);
        this.tfUsername.requestFocus(true);

        String[] ages = new String[82];
        int age;
        for (int i = 0; i  <82; i++){
            age = i + 18;
            ages[i] = String.valueOf(age);
        }

        this.cbAge = new JComboBox(ages);

        this.bgIsPremium = new RegisterFormRadioButtonGroup();

        this.tfEmail = new JTextField(20);
        this.tfEmail.setHorizontalAlignment(JTextField.CENTER);
        this.tfEmail.requestFocus(true);

        this.pfPassword = new JPasswordField(20);
        this.pfPassword.setHorizontalAlignment(JPasswordField.CENTER);

        this.pfRepeatPassword = new JPasswordField(20);
        this.pfRepeatPassword.setHorizontalAlignment(JPasswordField.CENTER);

        JLabel lInterest = new JLabel("Interest:");
        lInterest.setHorizontalAlignment(JLabel.CENTER);

        this.cbInterest = new JComboBox(new String[] {
                Constants.INTEREST.NAME_JAVA, Constants.INTEREST.NAME_C, Constants.INTEREST.NAME_C_JAVA
        });

        GridBagConstraints c = new GridBagConstraints();

        this.add(lUsername);
        this.add(this.tfUsername);
        this.add(lEmail);
        this.add(this.tfEmail);
        this.add(lPassword);
        this.add(this.pfPassword);
        this.add(lRepeatPassword);
        this.add(this.pfRepeatPassword);
        this.add(lAge);
        this.add(this.cbAge);
        this.bgIsPremium.configureLayoutConstraints(c);
        this.add(lInterest);
        this.add(this.cbInterest);
        this.add(lIsPremium);
        this.add(this.bgIsPremium, c);

        this.setVisible(true);
    }

    //Listener Attachments
    public void attachUsernameChangeListener(DocumentListener listener) {
        this.tfUsername.getDocument().addDocumentListener(listener);
    }

    public void attachEmailChangeListener(DocumentListener listener) {
        this.tfEmail.getDocument().addDocumentListener(listener);
    }

    public void attachPasswordChangeListener(DocumentListener listener) {
        this.pfPassword.getDocument().addDocumentListener(listener);
    }

    public void attachRepeatPasswordChangeListener(DocumentListener listener) {
        this.pfRepeatPassword.getDocument().addDocumentListener(listener);
    }

    //Getters and Setters
    public String getUsername() {
        return this.tfUsername.getText();
    }

    public void setUsername(String input) {
        this.tfUsername.setText(input);
    }

    public int getAge() {
        return Integer.parseInt(String.valueOf(cbAge.getSelectedItem()));
    }

    public void setAgeByIndex(int index) {
        this.cbAge.setSelectedIndex(index);
    }

    public String getEmail() {
        return tfEmail.getText();
    }

    public void setEmail(String input) {
        this.tfEmail.setText(input);
    }

    public String getPassword() {
        return String.valueOf(this.pfPassword.getPassword());
    }

    public void setPassword(String input) {
        this.pfPassword.setText(input);
    }

    public String getRepeatPassword() {
        return String.valueOf(pfRepeatPassword.getPassword());
    }

    public void setRepeatPassword(String input) {
        this.pfRepeatPassword.setText(input);
    }

    public boolean isPremium() {
        return bgIsPremium.getIsPremium();
    }

    public void setPremiumField(boolean isPremium) {
        this.bgIsPremium.setPremiumButton(isPremium);
    }

    public String getInterest() {
        switch (this.cbInterest.getSelectedIndex()) {
            case INTEREST_INDEX_C:
                return Constants.INTEREST.NAME_C;

            case INTEREST_INDEX_JAVA:
                return Constants.INTEREST.NAME_JAVA;

            case INTEREST_INDEX_C_JAVA:
                return Constants.INTEREST.NAME_C_JAVA;
        }
        return "";
    }
}
