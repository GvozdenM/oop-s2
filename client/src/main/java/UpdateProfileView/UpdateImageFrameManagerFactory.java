package UpdateProfileView;

import Utils.Swing.FrameManager.FrameManager;

public interface UpdateImageFrameManagerFactory {
    FrameManager frameManager = FrameManager.newInstance();

    static FrameManager create() {
        return frameManager;
    }
}
