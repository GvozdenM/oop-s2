package UpdateProfileView;

import Utils.Constants;

import javax.swing.*;
import javax.swing.event.DocumentListener;
import java.awt.*;

public class UpdateProfileViewFormPanel extends JPanel {
    private final int INTEREST_INDEX_JAVA = 0;
    private final int INTEREST_INDEX_C = 1;
    private final int INTEREST_INDEX_C_JAVA = 2;

    private final int MEMBERSHIP_INDEX_NORMAL = 0;
    private final int MEMBERSHIP_INDEX_PREMIUM = 1;

    private final int GRIDLAYOUT_ROWS = 7;
    private final int GRIDLAYOUT_COLS = 2;
    private final int INPUT_FIELD_SIDES_PADDING = 20;

    private final JTextField tfUsername;
    private final JTextField tfName;
    private final JTextField tfEmail;
    private final JTextField tfAge;
    private final JPasswordField pfPassword;
    private final JComboBox cbInterest;
    private final JComboBox cbSubscription;

    public UpdateProfileViewFormPanel() {
        int txtBxPadding = 5;
        int txtBxPaddingSides = 20;

        this.setLayout(new GridLayout(GRIDLAYOUT_ROWS, GRIDLAYOUT_COLS));

        JPanel wrapperUsername = new JPanel(new FlowLayout(FlowLayout.LEADING, txtBxPaddingSides, txtBxPadding));
        JPanel wrapperName = new JPanel(new FlowLayout(FlowLayout.LEADING, txtBxPaddingSides, txtBxPadding));
        JPanel wrapperEmail = new JPanel(new FlowLayout(FlowLayout.LEADING, txtBxPaddingSides, txtBxPadding));
        JPanel wrapperAge = new JPanel(new FlowLayout(FlowLayout.LEADING, txtBxPaddingSides, txtBxPadding));
        JPanel wrapperPassword = new JPanel(new FlowLayout(FlowLayout.LEADING, txtBxPaddingSides, txtBxPadding));
        JPanel wrapperInterest = new JPanel(new FlowLayout(FlowLayout.LEADING, txtBxPaddingSides, txtBxPadding));
        JPanel wrapperSubscription = new JPanel(new FlowLayout(FlowLayout.LEADING, txtBxPaddingSides, txtBxPadding));


        JLabel lUsername = new JLabel("Username:");
        lUsername.setHorizontalAlignment(JLabel.RIGHT);

        this.tfUsername = new JTextField(20);
        this.tfUsername.setHorizontalAlignment(JLabel.CENTER);
        this.tfUsername.setEditable(false);

        JLabel lName = new JLabel("Name:");
        lName.setHorizontalAlignment(JLabel.RIGHT);
        lName.setVerticalAlignment(JLabel.CENTER);

        this.tfName = new JTextField(20);
        this.tfName.setHorizontalAlignment(JLabel.CENTER);
        this.tfName.requestFocus(true);

        JLabel lAge = new JLabel("Age:");
        lAge.setHorizontalAlignment(JLabel.RIGHT);

        this.tfAge = new JTextField(20);
        this.tfAge.setHorizontalAlignment(JLabel.CENTER);

        JLabel lEmail = new JLabel("Email:");
        lEmail.setHorizontalAlignment(JLabel.RIGHT);

        this.tfEmail = new JTextField(20);
        this.tfEmail.setHorizontalAlignment(JLabel.CENTER);

        JLabel lPassword = new JLabel("Password:");
        lPassword.setHorizontalAlignment(JLabel.RIGHT);

        this.pfPassword = new JPasswordField(20);
        this.pfPassword.setHorizontalAlignment(JLabel.CENTER);

        JLabel lInterest = new JLabel("Interest:");
        lInterest.setHorizontalAlignment(JLabel.RIGHT);

        this.cbInterest = new JComboBox(new String[] {
                Constants.INTEREST.NAME_JAVA, Constants.INTEREST.NAME_C, Constants.INTEREST.NAME_C_JAVA
        });

        JLabel lSubscription = new JLabel("Subscription:");
        lSubscription.setHorizontalAlignment(JLabel.RIGHT);

        this.cbSubscription = new JComboBox(new String[] {"Normal", "Premium"});

        this.add(lUsername);
        wrapperUsername.add(this.tfUsername);
        this.add(wrapperUsername);

        this.add(lName);
        wrapperName.add(this.tfName);
        this.add(wrapperName);

        this.add(lAge);
        wrapperAge.add(this.tfAge);
        this.add(wrapperAge);

        this.add(lEmail);
        wrapperEmail.add(this.tfEmail);
        this.add(wrapperEmail);

        this.add(lPassword);
        wrapperPassword.add(this.pfPassword);
        this.add(wrapperPassword);

        wrapperInterest.add(this.cbInterest);
        this.add(lInterest);
        this.add(wrapperInterest);

        wrapperSubscription.add(this.cbSubscription);
        this.add(lSubscription);
        this.add(wrapperSubscription);
    }




    public String getUsername() {
        return tfUsername.getText();
    }

    public String getName() {
        return tfName.getText();
    }

    public String getEmail() {
        return tfEmail.getText();
    }

    public String getAge() {
        return tfAge.getText();
    }

    public String getPassword() {

        return new String(pfPassword.getPassword());
    }

    public String getInterest() {
        switch (this.cbInterest.getSelectedIndex()) {
            case INTEREST_INDEX_C:
                return Constants.INTEREST.NAME_C;

            case INTEREST_INDEX_JAVA:
                return Constants.INTEREST.NAME_JAVA;

            case INTEREST_INDEX_C_JAVA:
                return Constants.INTEREST.NAME_C_JAVA;
        }
        return "";
    }

    public boolean getSubscription() {
        switch (this.cbSubscription.getSelectedIndex()) {
            case MEMBERSHIP_INDEX_NORMAL:
                return false;

            case MEMBERSHIP_INDEX_PREMIUM:
                return true;
        }
        return false;
    }

    public void attachTfUsernameListener(DocumentListener listener) {
        this.tfUsername.getDocument().addDocumentListener(listener);
    }

    public void attachTfNameListener(DocumentListener listener) {
        this.tfName.getDocument().addDocumentListener(listener);
    }

    public void attachTfAgeListener(DocumentListener listener) {
        this.tfAge.getDocument().addDocumentListener(listener);
    }

    public void attachTfEmailListener(DocumentListener listener) {
        this.tfEmail.getDocument().addDocumentListener(listener);
    }

    public void attachPfPasswordListener(DocumentListener listener) {
        this.pfPassword.getDocument().addDocumentListener(listener);
    }

    public void setTfUsername(String tfUsername) {
        this.tfUsername.setText(tfUsername);
    }

    public void setTfName(String tfName) {
        this.tfName.setText(tfName);
    }

    public void setTfEmail(String tfEmail) {
        this.tfEmail.setText(tfEmail);
    }

    public void setTfAge(String tfAge) {
        this.tfAge.setText(tfAge);
    }

    public void setPfPassword(String pfPassword) {
        this.pfPassword.setText(pfPassword);
    }

    public void setCbInterest(String interest) {
        int index = 0;

        if (interest != null)
        switch (interest) {
            case Constants.INTEREST.NAME_JAVA:
                index = INTEREST_INDEX_JAVA;
                break;
            case Constants.INTEREST.NAME_C:
                index = INTEREST_INDEX_C;
                break;
            case Constants.INTEREST.NAME_C_JAVA:
                index = INTEREST_INDEX_C_JAVA;
                break;
        }

        this.cbInterest.setSelectedIndex(index);
    }

    public void setCbSubscription(boolean isPremium) {
        if (isPremium) {
            this.cbSubscription.setSelectedIndex(MEMBERSHIP_INDEX_PREMIUM);
        }
        else {
            this.cbSubscription.setSelectedIndex(MEMBERSHIP_INDEX_NORMAL);
        }
    }
}
