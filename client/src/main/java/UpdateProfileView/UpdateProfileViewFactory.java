package UpdateProfileView;


import UpdateProfileController.ConcreteUpdateProfileControllerFactory;
import UpdateProfileController.UpdateProfilePresenterI;
import UpdateProfilePresenter.AbstractUpdateProfileViewFactory;
import UpdateProfilePresenter.UpdateProfileView;

public class UpdateProfileViewFactory extends AbstractUpdateProfileViewFactory {
    @Override
    public UpdateProfileView create(UpdateProfilePresenterI presenter) {
        return new SwingUpdateProfileView(new ConcreteUpdateProfileControllerFactory(presenter));
    }
}
