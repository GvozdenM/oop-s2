package UpdateProfileView;

import Utils.Swing.FrameManager.FrameManager;

public interface UpdateProfileFrameManagerFactory {
    FrameManager frameManager = FrameManager.newInstance();

    static FrameManager create() {
        return frameManager;
    }
}
