package UpdateProfileView;

import Utils.Adapters.TextChangeListenerSwingAdapter;
import Utils.Constants;
import Utils.listeners.interfaces.TextChangedListener;
import com.cloudinary.Cloudinary;
import Utils.Swing.FrameManager.FrameManager;

import LoginView.SwingLogoPanel;
import UpdateProfileController.AbstractUpdateProfileControllerFactory;
import UpdateProfileController.UpdateProfileControllerI;
import UpdateProfilePresenter.UpdateProfileView;
import UpdateProfilePresenter.UpdateProfileViewModel;
import Utils.Swing.Image.ImageFileFilter;
import Utils.Swing.Image.ImagePanel;
import Utils.Views.ResourceFetcher;
import com.cloudinary.utils.ObjectUtils;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

/**
 * This class is the main update profile view panel implementation as well as the implementation of given parent
 * interface.
 * @see UpdateProfileView
 */
public class SwingUpdateProfileView extends JPanel implements UpdateProfileView {
    private UpdateProfileControllerI controller;
    private final String SAVE_BUTTON_TEXT = "SAVE";
    private SwingLogoPanel logoPanel;
    private ImagePanel userImg;
    private String userProfileImgUrl;
    private final UpdateProfileViewFormPanel formPanel;
    private JButton saveBtn;
    private JTextArea taDescription;

    public SwingUpdateProfileView(AbstractUpdateProfileControllerFactory controllerFactory) throws HeadlessException {
        this.controller = controllerFactory.create();


        this.setSize(new Dimension(600,760));
        this.setLayout(new BorderLayout());

        JPanel centerWrapper = new JPanel();
        centerWrapper.setLayout(new BoxLayout(centerWrapper,BoxLayout.Y_AXIS));

        //this.logoPanel = new SwingLogoPanel();
        //this.add(this.logoPanel, BorderLayout.PAGE_START);

        ResourceFetcher resourceFetcher = new ResourceFetcher("defaultuserimg.png");

        this.userImg = new ImagePanel();
        try {
            this.userImg.load(resourceFetcher.getResourceFile().getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        //this.userImg.setImgSize(50, 50);
        //loadUserImgUrl(Constants.COSMETICS.DEFAULT_USER_IMG_URL);

        this.userImg.setOnClickListener(this::onImgClicked);

        this.add(this.userImg, BorderLayout.PAGE_START);

        this.formPanel = new UpdateProfileViewFormPanel();
        centerWrapper.add(this.formPanel);

        this.taDescription = new JTextArea();
        this.taDescription.setColumns(82);
        this.taDescription.setLineWrap(true);
        JScrollPane taDescriptionWrapper = new JScrollPane(
                this.taDescription,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
        );
        centerWrapper.add(taDescriptionWrapper);

        this.saveBtn = new JButton(SAVE_BUTTON_TEXT);
        JPanel btnWrapper = new JPanel(new GridLayout());
        btnWrapper.add(this.saveBtn);
        this.add(btnWrapper, BorderLayout.SOUTH);

        this.add(centerWrapper, BorderLayout.CENTER);
        this.setVisible(true);
        setSaveBtnState(false);

        attachListeners();
    }

    private void onImgClicked() {
        JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        fc.addChoosableFileFilter(new ImageFileFilter());
        fc.setAcceptAllFileFilterUsed(true);

        int ret = fc.showOpenDialog(this);

        if (ret == JFileChooser.APPROVE_OPTION) {
            File imgFile = fc.getSelectedFile();
            Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
                    "cloud_name", Constants.CLOUDINARY.CONFIGURATION_CLOUD_NAME,
                    "api_key", Constants.CLOUDINARY.CONFIGURATION_API_KEY,
                    "api_secret", Constants.CLOUDINARY.CONFIGURATION_SECRET
            ));
            Map uploadRes = null;
            try {
                uploadRes = cloudinary.uploader().upload(imgFile, ObjectUtils.asMap("folder", "Minder"));
                loadUserImgUrl(uploadRes.get("url").toString());
            } catch (IOException e) {
                displayErrorMsg("Could not upload img");
                e.printStackTrace();
            }
        }
    }

    private void loadUserImgUrl(String imgUrl) {
        try {
            this.userImg.setImgSize(280, 280);
            this.userImg.load(new URL(imgUrl));
            this.userProfileImgUrl = imgUrl;
            this.userImg.setOnClickListener(this::onImgClicked);
        } catch (MalformedURLException ignore) {}
        this.revalidate();
    }

    private void attachListeners() {

        this.saveBtn.addActionListener(
                l->
                        this.controller.onSaveBtnClick(
                                this.formPanel.getUsername(),
                                this.formPanel.getName(),
                                Integer.parseInt(this.formPanel.getAge()),
                                this.formPanel.getEmail(),
                                this.formPanel.getPassword(),
                                this.formPanel.getInterest(),
                                this.formPanel.getSubscription(),
                                this.userProfileImgUrl,
                                this.taDescription.getText()
                        )
        );
        this.formPanel.attachTfUsernameListener(new TextChangeListenerSwingAdapter(new TextChangedListener(l->formPanelChanged())));
        this.formPanel.attachTfNameListener(new TextChangeListenerSwingAdapter(new TextChangedListener(l->formPanelChanged())));
        this.formPanel.attachTfAgeListener(new TextChangeListenerSwingAdapter(new TextChangedListener(l->formPanelChanged())));
        this.formPanel.attachTfEmailListener(new TextChangeListenerSwingAdapter(new TextChangedListener(l->formPanelChanged())));
        this.formPanel.attachPfPasswordListener(new TextChangeListenerSwingAdapter(new TextChangedListener(l->formPanelChanged())));

    }

    /**
     * This method contemplates state of form info to allow the save transactional button to be activated
     */
    private void formPanelChanged() {
        setSaveBtnState(!this.formPanel.getUsername().equals("") && !this.formPanel.getName().equals("") &&
                !this.formPanel.getAge().equals("") && !this.formPanel.getEmail().equals("") &&
                !this.formPanel.getPassword().equals(""));
    }

    public void setSaveBtnState(boolean clickable){
        this.saveBtn.setEnabled(clickable);
        this.saveBtn.setBorderPainted(clickable);
        this.saveBtn.setFocusPainted(clickable);
    }

    @Override
    public void displayMsgDialog(String msg) {
        SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(
                new JFrame(),
                msg,
                "Warning",
                JOptionPane.WARNING_MESSAGE
        ));
    }

    @Override
    public void displayErrorMsg(String msg) {
        displayMsgDialog("ERROR: " + msg);
    }

    @Override
    public void displayProfile(UpdateProfileViewModel vm) {
        loadUserImgUrl(vm.getProfileImgUrl());
        this.formPanel.setTfUsername(vm.getUsername());
        this.formPanel.setTfName(vm.getName());
        this.formPanel.setTfAge(String.valueOf(vm.getAge()));
        this.formPanel.setTfEmail(vm.getEmail());
        this.formPanel.setCbInterest(vm.getInterest());
        this.formPanel.setCbSubscription(vm.isPremium());
        this.taDescription.setText(vm.getDescription());
    }


    @Override
    public void onTransitionTo() {
        ResourceFetcher resourceFetcher = new ResourceFetcher("favicon.jpg");

        FrameManager frameManager = UpdateProfileFrameManagerFactory.create();
        frameManager
                .configureFramePreferences("EditProfile", this, getWidth(), getHeight(), FrameManager.CENTER)
                .setResizable(false);

        try {
            frameManager.setFavicon(resourceFetcher.getResourceFile().getPath());
        } catch (IOException ignore) {}


        this.controller.onLoad();
    }
}
