package FeedPresenter;

import FeedController.FeedPresenter;
import FeedController.FeedPresenterFactory;
import FeedView.SwingFeedViewFactory;

public class FeedScreenPresenterFactory extends FeedPresenterFactory {
    @Override
    public FeedPresenter create() {
        return new FeedScreenPresenter(
                new SwingFeedViewFactory()
        );
    }
}
