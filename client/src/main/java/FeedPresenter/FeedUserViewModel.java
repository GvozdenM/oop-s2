package FeedPresenter;

import java.awt.image.BufferedImage;

public class FeedUserViewModel {
    private String username;
    private int age;
    private String imageUrl;
    private String interestedIn;
    private String shortDescription;

    public String getImageUrl() {
        return imageUrl;
    }

    public FeedUserViewModel setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    public String getInterestedIn() {
        return interestedIn;
    }

    public FeedUserViewModel setInterestedIn(String interestedIn) {
        this.interestedIn = interestedIn;
        return this;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public FeedUserViewModel setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public FeedUserViewModel setUsername(String username) {
        this.username = username;
        return this;
    }

    public int getAge() {
        return age;
    }

    public FeedUserViewModel setAge(int age) {
        this.age = age;
        return this;
    }

    public String getImage() {
        return imageUrl;
    }

    public FeedUserViewModel setImage(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }
}
