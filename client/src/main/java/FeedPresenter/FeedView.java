package FeedPresenter;

import Utils.Views.TransitionView;

/**
 * This interface provides the functions to be implemented by the FeedView Class
 */
public interface FeedView extends TransitionView {

    /**
     * Provides for the implementation of adding a new user to the view and replacing the previous one as well as
     * resetting the values of like status to given
     * @param user to be added
     * @param like
     */
    void displayUser(FeedUserViewModel user, boolean like);

    /**
     * This method is called for a display of an error in the current View
     * @param msg Error message to be displayed.
     */
    void displayErrorMsg(String msg);


    /**
     * Shows that no users could not be found to match with
     */
    void displayNoUsers();
}
