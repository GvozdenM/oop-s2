package FeedPresenter;

import FeedController.FeedPresenter;
import FeedInteractor.FeedActionUserResponse;
import RegistrationController.RegistrationPresenterI;

/**
 * The FeedScreenPresenter is in charge of delegating requests from the attached FeedController
 * in order to operate the view externally.
 * This class implements the methods and functions of its respective interface.
 * @see FeedPresenter
 */
public class FeedScreenPresenter implements FeedPresenter {
    private final FeedView view;

    public FeedScreenPresenter(FeedViewFactory feedViewFactory) {
        this.view = feedViewFactory.create(this);
    }

    @Override
    public void init() {
        this.view.onTransitionTo();
    }

    @Override
    public void goToNext(FeedActionUserResponse user, boolean like) {
        FeedUserViewModel feedUserViewModel = new FeedUserViewModel();
        feedUserViewModel
                .setImage(user.getProfileImageUrl())
                .setUsername(user.getUsername())
                .setAge(user.getAge())
                .setInterestedIn(user.getInterestedIn())
                .setShortDescription(user.getShortDescription());

        this.view.displayUser(feedUserViewModel, like);
    }

    @Override
    public void presentNoConnectionError() {
        this.view.displayErrorMsg("Oops something went wrong..");
    }

    @Override
    public void presentFeedEmpty() {
        this.view.displayNoUsers();
    }
}
