package FeedPresenter;

import FeedController.FeedPresenter;

public abstract class FeedViewFactory {
    public abstract FeedView create(FeedPresenter feedPresenter);
}
