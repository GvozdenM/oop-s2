import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MinderClient {
    private static final ExecutorService CLIENT_EXECUTOR = Executors.newSingleThreadExecutor();

    public static void main(String[] args) {
        CLIENT_EXECUTOR.execute(MinderClientUI.getInstance());
    }
}
