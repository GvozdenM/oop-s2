package UserProfileController;


/**
 * This interface provides the functions and methods to be implemented by the UserProfileController class
 * @see UserProfileController
 */
public interface UserProfileControllerI {
    /**
     * Provides initialization method for the controller, assigns the different response body to the presenter
     * to update the view with the user info
     */
    void onLoad();

    /**
     * This method is called whenever the displayed Swing JPanel is closed or exited.
     */
    void onClose();
}
