package UserProfileController;

import FeedController.UserProfilePresenterI;

public class ConcreteUserProfileControllerFactory extends AbstractUserProfileControllerFactory {
    private final UserProfilePresenterI userProfilePresenter;

    public ConcreteUserProfileControllerFactory(UserProfilePresenterI userProfilePresenter) {
        this.userProfilePresenter = userProfilePresenter;
    }

    @Override
    public UserProfileControllerI create() {
        return new UserProfileController(
                userProfilePresenter
        );
    }
}
