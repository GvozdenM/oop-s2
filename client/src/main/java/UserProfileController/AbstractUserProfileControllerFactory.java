package UserProfileController;

public abstract class AbstractUserProfileControllerFactory {

    public abstract UserProfileControllerI create();
}
