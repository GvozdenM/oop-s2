package UserProfileController;

import FeedController.UserProfilePresenterI;

/**
 * This Class implements all methods and functions related to the link between the presenter and the requester of the
 * User Profile scenario.
 * @see UserProfileControllerI
 */
public class UserProfileController implements UserProfileControllerI {
    private final UserProfilePresenterI presenter;

    public UserProfileController(UserProfilePresenterI presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onLoad() {

    }

    @Override
    public void onClose() {

    }
}
