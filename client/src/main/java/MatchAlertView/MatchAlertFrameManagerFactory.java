package MatchAlertView;

import Utils.Swing.FrameManager.FrameManager;

public interface MatchAlertFrameManagerFactory {
    static FrameManager create() {
        return FrameManager.newInstance();
    }
}
