package MatchAlertView;

import MatchAlertPresenter.MatchAlertView;
import MatchAlertPresenter.MatchAlertViewFactory;

public class SwingMatchAlertViewFactory extends MatchAlertViewFactory {
    @Override
    public MatchAlertView create() {
        return new SwingMatchAlertView();
    }
}
