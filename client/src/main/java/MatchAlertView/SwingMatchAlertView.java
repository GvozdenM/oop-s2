package MatchAlertView;

import Utils.Swing.FrameManager.FrameManager;
import Utils.Swing.ListView.ListView;
import Utils.Swing.ListView.ListViewBuilder;
import Utils.Swing.ListView.ListViewBuilderFactory;
import MatchAlertPresenter.MatchAlertView;
import MatchAlertPresenter.MatchViewModel;

import javax.swing.*;

public class SwingMatchAlertView extends JPanel implements MatchAlertView {
    @Override
    public void displayMatch(MatchViewModel match) {
        FrameManager matchFrameManager = MatchAlertFrameManagerFactory.create();
        ListViewBuilder listViewBuilder = ListViewBuilderFactory.create();

        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        this.add(
                new JLabel("New match!")
        );

        match.addItem(
                match.createItem(
                        match.getUsername(),
                        "",
                        match.getImage()
                )
        );

        ListView matchView = listViewBuilder
                .setDefaultViewModel(match)
                .build();
        this.add(matchView);

        this.setVisible(true);

        matchFrameManager
                .configureFramePreferences(
                        "Matched with " + match.getUsername(),
                        this,
                        600,
                        300
                )
                .setSpawnRelativeTo(FrameManager.CENTER)
                .setResizable(false);
    }
}
