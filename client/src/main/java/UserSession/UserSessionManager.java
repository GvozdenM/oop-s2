package UserSession;

import ClientMessagingEndpoint.ClientEndpoint;

public class UserSessionManager {
    private static final UserSession userSession = new UserSession();

    public static long getUserId() {
        return userSession.getUserId();
    }

    public static void setUserId(long userId) {
        userSession.setUserId(userId);
    }

    public static String getUserUsername() {
        return userSession.getUsername();
    }

    public static void setUserUsername(String username) {
        userSession.setUsername(username);
    }

    public static void setUserProfileImgUrl(String imgUrl) {
        userSession.setImgUrl(imgUrl);
    }

    public static String getUserProfileImgUrl() {
        return userSession.getImgUrl();
    }

    public static void logOut() {
        ClientEndpoint.getInstance().createLogOutRequester().logOut(getUserId());
    }
}
