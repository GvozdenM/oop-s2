package UserSession;

class UserSession {
    private final User user;

    UserSession() {
        this.user = new User();
    }

    long getUserId() {
        return this.user.id;
    }

    void setUserId(long userId) {
        this.user.id = userId;
    }

    public String getUsername() {
        return user.username;
    }

    public void setUsername(String username) {
        this.user.username = username;
    }

    public void setImgUrl(String imgUrl) {
        this.user.imgUrl = imgUrl;
    }

    public String getImgUrl() {
        return this.user.imgUrl;
    }


    static class User {
        private long id;
        private String username;
        private String imgUrl;
        User() {}
    }
}
