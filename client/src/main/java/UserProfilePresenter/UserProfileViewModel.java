package UserProfilePresenter;


import java.awt.image.BufferedImage;
import java.util.List;

/**
 * This class provides a general data model to refer to a user and its available information.
 */
public class UserProfileViewModel {
    private final long id;
    private final String username;
    private final String name;
    private final int age;
    private final String email;
    private final String password;
    private final String interest;
    private final String profileImgUrl;
    private final List<BufferedImage> images;
    private final String shortDescription;
    private final String description;

    public UserProfileViewModel(long id, String username, String name, int age, String email, String password,
                                  String interest,  String profileImgUrl, List<BufferedImage> images,
                                  String shortDescription, String description) {
        this.id = id;
        this.username = username;
        this.name = name;
        this.age = age;
        this.email = email;
        this.password = password;
        this.interest = interest;
        this.profileImgUrl = profileImgUrl;
        this.images = images;
        this.shortDescription = shortDescription;
        this.description = description;
    }

    public UserProfileViewModel(long id, String username, String name, int age, String interestedIn,
                                String profileImageUrl, String shortDescription, String description) {
        this.id = id;
        this.username = username;
        this.name = name;
        this.age = age;
        this.email = null;
        this.password = null;
        this.interest = interestedIn;
        this.profileImgUrl = profileImageUrl;
        this.images = null;
        this.shortDescription = shortDescription;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getInterest() {
        return interest;
    }

    public String getProfileImgUrl() {
        return profileImgUrl;
    }

    public List<BufferedImage> getImages() {
        return images;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public String getDescription() {
        return description;
    }

}