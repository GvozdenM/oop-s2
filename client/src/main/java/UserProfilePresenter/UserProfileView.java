package UserProfilePresenter;

import Utils.Views.TransitionView;

/**
 * This interface provides the functions and methods to be implemented by the UserProfile View
 */
public interface UserProfileView extends TransitionView {
    String  VOID_TEXT_FIELD = "-";

    /**
     * This method is called for a display of a message in the current View
     * @param msg Text message to be displayed.
     */
    void displayMsgDialog(String msg);

    /**
     * This method is called for a display of an error in the current View
     * @param msg Error message to be displayed.
     */
    void displayErrorMsg(String msg);

    /**
     * Updates information of given user with the corresponding areas in the view.
     * @param vm An object containing all of the relevant data with the user.
     */
    void displayProfile(UserProfileViewModel vm);
}
