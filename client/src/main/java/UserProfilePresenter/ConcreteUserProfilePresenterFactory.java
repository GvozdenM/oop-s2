package UserProfilePresenter;

import UserProfileView.ConcreteUserProfileViewFactory;
import FeedController.UserProfilePresenterI;

public class ConcreteUserProfilePresenterFactory extends AbstractUserProfilePresenterFactory{

    @Override
    public UserProfilePresenterI create() {
        return new UserProfilePresenter(
                new ConcreteUserProfileViewFactory()
        );
    }
}
