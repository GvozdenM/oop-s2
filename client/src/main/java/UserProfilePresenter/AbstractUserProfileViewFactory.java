package UserProfilePresenter;

import FeedController.UserProfilePresenterI;

public abstract class AbstractUserProfileViewFactory {
    public abstract UserProfileView create(UserProfilePresenterI userProfilePresenterI);

}
