package UserProfilePresenter;

import FeedInteractor.FeedActionUserResponse;
import FeedController.UserProfilePresenterI;

/**
 * The UpdateProfilePresenter is in charge of delegating requests from the attached UpdateProfileController
 * in order to operate the view externally.
 * This class implements the methods and functions of its respective interface.
 * @see UserProfilePresenterI
 */
public class UserProfilePresenter implements UserProfilePresenterI {
    private UserProfileView view;

    public UserProfilePresenter(AbstractUserProfileViewFactory factory) {
        this.view = factory.create( this);
    }

    @Override
    public void init() {
        this.view.onTransitionTo();
    }

    @Override
    public void presentProfile(FeedActionUserResponse profile) {
        UserProfileViewModel vm = new UserProfileViewModel(
                profile.getId(),
                profile.getUsername(),
                profile.getName(),
                profile.getAge(),
                profile.getInterestedIn(),
                profile.getProfileImageUrl(),
                profile.getShortDescription(),
                profile.getDescription()
        );

        this.view.displayProfile(vm);
    }

    @Override
    public void presentNoConnectionError() {
        this.view.displayErrorMsg("Connection could not be established");
    }
}
