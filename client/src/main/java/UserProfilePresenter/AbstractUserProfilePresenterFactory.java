package UserProfilePresenter;

import FeedController.UserProfilePresenterI;

public abstract class AbstractUserProfilePresenterFactory {
    public abstract UserProfilePresenterI create();

}
