package RegistrationPresenter;

import RegistrationController.RegistrationPresenterI;
import RegistrationInteractor.RegistrationResponse;
import Utils.listeners.interfaces.TextChangedListener;

/**
 * The Registration Presenter is in charge of delegating requests from the attached RegistrationController
 * in order to operate the view externally.
 * This class implements the methods and functions of its respective interface.
 * @see RegistrationPresenterI
 */
public class RegistrationPresenter implements RegistrationPresenterI {
    private final RegistrationView registrationView;

    public RegistrationPresenter(AbstractRegistrationViewFactory registrationViewFactory) { this.registrationView =
           registrationViewFactory.create(this);
    }

    /**
     * This method initializes the listeners of the text boxes in the registrationView and calls for the
     * initialization method of the same view.
     * @see Utils.Views.TransitionView
     * @see RegistrationView
     */
    @Override
    public void init() {
       this.registrationView.attachFormStateListener(
           new TextChangedListener(
                   e -> this.considerFormState()
           )
       );
       this.registrationView.onTransitionTo();
    }

    /**
     * This method resets all input fields in the view to its initial default values.
     */
    @Override
    public void resetAndClear() {
        this.registrationView.setRegisterBtnState(false);
        this.registrationView.setUsername(RegistrationView.EMPTY_TEXT_FIELD);
        this.registrationView.setEmail(RegistrationView.EMPTY_TEXT_FIELD);
        this.registrationView.setPassword(RegistrationView.EMPTY_TEXT_FIELD);
        this.registrationView.setRepeatPassword(RegistrationView.EMPTY_TEXT_FIELD);
        this.registrationView.setAgeBoxByIndex(0);
        this.registrationView.setIsPremium(false);
    }

    /**
     * @param errorMsg Error message to be displayed.
     */
    @Override
    public void presentError(String errorMsg) {
        this.registrationView.displayErrorMsg(errorMsg);
    }

    @Override
    public void presentAuthenticationError(RegistrationResponse response) {
        this.registrationView.displayErrorMsg(
                "Registration information incorrect" + response.getErrorMsg()
        );
        resetAndClear();
    }

    @Override
    public void updatePresentingState(boolean formFilled) {
        this.registrationView.setRegisterBtnState(formFilled);
    }

    @Override
    public void end() { this.registrationView.onClose(); }

    /**
     * This method decides whether the state of the view fields is valid to allow the transaction button in the view
     * to be clickable.
     */
    private void considerFormState() {
       if(this.registrationView.getUsername().equals("") || this.registrationView.getPassword().equals("")
       || this.registrationView.getEmail().equals("") || this.registrationView.getRepeatPassword().equals("")) {
           this.registrationView.setRegisterBtnState(false);
       }
       else {
           this.registrationView.setRegisterBtnState(true);
       }
    }
}
