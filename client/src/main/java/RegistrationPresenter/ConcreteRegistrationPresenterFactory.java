package RegistrationPresenter;

import RegistrationController.RegistrationPresenterI;
import RegistrationView.ConcreteRegistrationViewFactory;

public class ConcreteRegistrationPresenterFactory extends AbstractRegistrationPresenterFactory {
    @Override
    public RegistrationPresenterI create() {
        return new RegistrationPresenter(
                new ConcreteRegistrationViewFactory()
        );
    }
}
