package RegistrationPresenter;

import RegistrationController.RegistrationPresenterI;

public abstract class AbstractRegistrationPresenterFactory {
    public abstract RegistrationPresenterI create();
}
