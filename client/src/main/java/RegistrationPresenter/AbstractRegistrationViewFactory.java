package RegistrationPresenter;

import RegistrationController.RegistrationPresenterI;

public abstract class AbstractRegistrationViewFactory {
    public abstract RegistrationView create(RegistrationPresenterI registrationPresenterI);
}
