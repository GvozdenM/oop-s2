package FeedController;

import FeedInteractor.FeedActionUserResponse;

/**
 * This interface provides a list of methods and functions to be implemented by the FeedScreenPresenter.
  */
public interface FeedPresenter {
    /**
     * Ininialize method for the Feed Presenter, assigns concrete FeedView and initializes items and attaches
     * listeners with views.
     */
    void init();

    /**
     * Change current user feed user to new user and sets like to preset.
     * @param user transitioning user
     * @param like
     */
    void goToNext(FeedActionUserResponse user, boolean like);

    /**
     * This method is called for a display of an error in the current View whenever there is no connection as a result
     * from a callback.
     */
    void presentNoConnectionError();

    /**
     * This method is called for the purpose of displaying that there were no users to match with found..
     */
    void presentFeedEmpty();
}
