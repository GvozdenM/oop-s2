package FeedController;

import FeedInteractor.FeedActionUserResponse;

/**
 * This interface provides the functions to be implemented by the MatchAlertScreenPresenter Class
 */
public interface MatchAlertPresenter {
    /**
     * Provides the info of the matched user and calls for its prompt in the assciated view.
     * @param match
     */
    void presentMatch(FeedActionUserResponse match);
}
