package FeedController;

/**
 * This interface provides the functions and methods to be implemented by the FeedController class
 * @see FeedController
 */
public interface FeedControllerI {
    /**
     * Provides initialization method for the controller, links the current user to the server side and gets a list of
     * people to match with in the case that the FeedActionUserResponse is correct
     */
    void onLoad();

    /**
     * This method makes a request to the server side to notify that the user has disliked a user and moves on to
     * the next one in the feed
     */
    void onDislike();

    /**
     * This method makes a request to the server side to notify that the user has liked a user and moves on to
     * the next one in the feed
     */
    void onLike();

    /**
     * This method refers to the opening of a profile of the user that is currently being viewed in the
     * matching panel.
     */
    void onViewProfile();

    /**
     * This method is called whenever the displayed Swing JPanel is closed or exited.
     */
    void onClose();

    /**
     * Provides an implementation of going into to the ChatView with current user
     */
    void onChatBtnClicked();

    /**
     * This method refers to the opening of the profile page of the current user using the application
     */
    void onProfileBtnClicked();

    /**
     * This method handles the feed update call triggered by the user
     */
    void onFeedUpdateBtnClicked();
}
