package FeedController;

import ClientMessagingEndpoint.ClientEndpoint;
import MatchAlertPresenter.MatchAlertScreenPresenterFactory;
import MatchedUsersPresenter.MatchedUsersPresenterFactory;
import UpdateProfilePresenter.UpdateProfilePresenterFactory;
import UserProfilePresenter.ConcreteUserProfilePresenterFactory;

public class FeedControllerFactory {
    private final FeedPresenter presenter;

    public FeedControllerFactory(FeedPresenter presenter) {
        this.presenter = presenter;
    }

    public FeedControllerI create() {
        return new FeedController(
                new MatchAlertScreenPresenterFactory(),
                this.presenter,
                new MatchedUsersPresenterFactory(),
                new UpdateProfilePresenterFactory(),
                new ConcreteUserProfilePresenterFactory(),
                ClientEndpoint.getInstance()
        );
    }
}
