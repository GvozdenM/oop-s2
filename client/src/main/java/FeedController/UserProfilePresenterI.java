package FeedController;

import FeedInteractor.FeedActionUserResponse;
import UpdateProfileInteractor.UserProfileResponse;

public interface UserProfilePresenterI {
    /**
     * Initialize method for the UserProfile Presenter, assigns concrete UserProfileView and initializes items and
     * attaches listeners with views.
     */
    void init();

    /**
     * Provides a data point to get all the user related information and moves it to the view,
     * @param profile
     */
    void presentProfile(FeedActionUserResponse profile);

    /**
     * This method is called for a display of an error in the current View whenever there is no connection as a result
     * from a callback.
     */
    void presentNoConnectionError();
}
