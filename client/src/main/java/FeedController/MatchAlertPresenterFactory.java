package FeedController;

public abstract class MatchAlertPresenterFactory {
    public abstract MatchAlertPresenter create();
}
