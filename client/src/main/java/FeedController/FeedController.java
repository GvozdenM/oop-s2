package FeedController;

import FeedInteractor.*;
import LoginController.LoginPresenter;
import LoginPresenter.LoginPresenterFactory;
import MatchedUsersController.AbstractMatchedUsersPresenterFactory;
import MatchedUsersController.MatchedUsersPresenter;
import UpdateProfileController.AbstractUpdateProfilePresenterFactory;
import UpdateProfileController.UpdateProfilePresenterI;
import UserProfilePresenter.AbstractUserProfilePresenterFactory;
import UserSession.UserSessionManager;
import Utils.Callbacks.CallbackAdapter;
import Utils.Swing.FrameManager.FrameManager;

import java.util.LinkedList;
import java.util.Optional;
import java.util.Queue;

/**
 * This Class implements all methods and functions related to the link between the presenter and the requester of the
 * Feed scenario.
 * @see FeedControllerI list of methods implemented.
 */
public class FeedController implements FeedControllerI {
    private final AbstractMatchedUsersPresenterFactory matchedUsersPresenterFactory;
    private final AbstractUpdateProfilePresenterFactory updateProfilePresenterFactory;
    private final AbstractUserProfilePresenterFactory userProfilePresenterFactory;

    private final FeedPresenter feedPresenter;
    private final MatchAlertPresenterFactory matchAlertPresenterFactory;
    private final Queue<FeedActionUserResponse> feed;

    private Optional<FeedActionUserResponse> currentUser = Optional.empty();

    private final FeedRequester<FeedActionRequest, FeedActionResponse, FeedActionUserResponse> feedRequester;

    private void updateFeedThenGoToNext(){

        this.feed.poll();

        if(!this.feed.isEmpty()) {
            this.feedPresenter.goToNext(
                    this.feed.peek(),
                    false
            );
        }
        else {
            this.feedRequester.getUserFeedBuffer(
                    UserSessionManager.getUserId(),
                    new CallbackAdapter<>(
                            feedActionUserResponses -> {
                                this.feed.addAll(feedActionUserResponses);
                                if (feedActionUserResponses.isEmpty()) {
                                    this.feedPresenter.presentFeedEmpty();
                                }
                                else {
                                    this.feedPresenter.goToNext(this.feed.peek(), false);
                                }
                            },
                            onFailResponse -> this.feedPresenter.presentNoConnectionError(),
                            onExceptionResponse -> this.feedPresenter.presentNoConnectionError()
                    )
            );
        }

    }

    public FeedController(MatchAlertPresenterFactory matchAlertPresenterFactory,
                          FeedPresenter feedPresenter,
                          AbstractMatchedUsersPresenterFactory matchedUsersPresenterFactory,
                          AbstractUpdateProfilePresenterFactory updateProfilePresenterFactory, AbstractUserProfilePresenterFactory userProfilePresenterFactory,
                          FeedRequesterFactory<FeedActionRequest, FeedActionResponse, FeedActionUserResponse> feedActionRequesterFactory) {
        this.feedPresenter = feedPresenter;
        this.matchAlertPresenterFactory = matchAlertPresenterFactory;
        this.matchedUsersPresenterFactory = matchedUsersPresenterFactory;
        this.updateProfilePresenterFactory = updateProfilePresenterFactory;
        this.userProfilePresenterFactory = userProfilePresenterFactory;
        this.feedRequester = feedActionRequesterFactory.createFeedRequester();
        this.feed = new LinkedList<>();
    }

    @Override
    public void onLoad() {
        this.feedRequester.getUserById(
                UserSessionManager.getUserId(), new CallbackAdapter<>(
                        onSuccessResponse -> {
                            this.currentUser = Optional.of(onSuccessResponse);
                            UserSessionManager.setUserProfileImgUrl(onSuccessResponse.getProfileImageUrl());
                            UserSessionManager.setUserUsername(onSuccessResponse.getUsername());

                            updateFeedThenGoToNext();
                        },
                        onFailResponse -> this.feedPresenter.presentNoConnectionError(),
                        onExceptionResponse -> this.feedPresenter.presentNoConnectionError()
                )
        );
    }

    @Override
    public void onDislike() {
        FeedActionRequest request = new FeedActionRequest();
        if (currentUser.isPresent()) {
            request.setInvokerId(
                    this.currentUser.get().getId()
            );
            if (this.feed.peek() != null) {
                request.setReceiverId(
                        this.feed.peek().getId()
                );
            }

            request.setLike(false);

            this.feedRequester.interact(
                    request,
                    new CallbackAdapter<>(
                            (response) -> {},
                            (onFailureResponse) -> this.feedPresenter.presentNoConnectionError(),
                            (onExceptionResponse) -> this.feedPresenter.presentNoConnectionError()
                    )
            );

            updateFeedThenGoToNext();

        }
    }

    @Override
    public void onLike() {
        if (this.currentUser.isPresent()) {
            FeedActionRequest request = new FeedActionRequest();
            request.setInvokerId(
                    this.currentUser.get().getId()
            );

            if(null != this.feed.peek()) {
                request.setReceiverId(
                        this.feed.peek().getId()
                );
            }


            request.setLike(true);

            this.feedRequester.interact(
                    request,
                    new CallbackAdapter<>(
                            (f) -> {},
                            (onFailureResponse) -> this.feedPresenter.presentNoConnectionError(),
                            (onExceptionResponse) -> this.feedPresenter.presentNoConnectionError()
                    )
            );

            updateFeedThenGoToNext();
        }
    }

    @Override
    public void onViewProfile() {
        if(this.feed.peek() != null) {
            this.feedRequester.getUserById(
                    this.feed.peek().getId(),
                    new CallbackAdapter<>(
                            (onSuccessResponse) -> {
                                UserProfilePresenterI presenterI  = this.userProfilePresenterFactory.create();
                                presenterI.init();
                                presenterI.presentProfile(this.feed.peek());
                            },
                            (onFailResponse) -> {

                            },
                            (onExceptionResponse) -> {

                            }
                    )
            );
        }
    }

    @Override
    public void onClose() {
        FrameManager.closeAll();

        UserSessionManager.logOut();
        LoginPresenterFactory loginPresenterFactory = new LoginPresenterFactory();
        LoginPresenter loginPresenter = loginPresenterFactory.create();
        loginPresenter.init();
    }

    @Override
    public void onChatBtnClicked() {
        MatchedUsersPresenter chatsPresenter = this.matchedUsersPresenterFactory.create();
        chatsPresenter.init();
    }

    @Override
    public void onProfileBtnClicked() {
        this.currentUser.ifPresent(feedActionUserResponse ->
                this.feedRequester.getUserById(
                feedActionUserResponse.getId(),
                new CallbackAdapter<>(
                        (onSuccessResponse) -> {
                            UpdateProfilePresenterI updateProfilePresenter = this.updateProfilePresenterFactory.create();
                            updateProfilePresenter.init();
                        },
                        (onFailResponse) -> {

                        },
                        (onExceptionResponse) -> {

                        }
                )
        ));
    }

    @Override
    public void onFeedUpdateBtnClicked() {
        updateFeedThenGoToNext();
    }
}
