package FeedController;

public abstract class FeedPresenterFactory {
    public abstract FeedPresenter create();
}
