package UserProfileView;

import UserProfileController.ConcreteUserProfileControllerFactory;
import FeedController.UserProfilePresenterI;
import UserProfilePresenter.AbstractUserProfileViewFactory;
import UserProfilePresenter.UserProfileView;

public class ConcreteUserProfileViewFactory extends AbstractUserProfileViewFactory {
    @Override
    public UserProfileView create(UserProfilePresenterI userProfilePresenterI) {
        return new SwingUserProfileView();
    }
}
