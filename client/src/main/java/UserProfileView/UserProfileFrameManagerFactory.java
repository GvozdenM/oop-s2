package UserProfileView;

import Utils.Swing.FrameManager.FrameManager;

public interface UserProfileFrameManagerFactory {
    FrameManager frameManager = FrameManager.newInstance();

    static FrameManager create() {
        return frameManager;
    }
}
