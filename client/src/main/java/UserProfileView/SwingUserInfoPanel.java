package UserProfileView;

import javax.swing.*;
import java.awt.*;

public class SwingUserInfoPanel extends JPanel {
    private JLabel jlUsername;
    private JLabel jlName;
    private JLabel jlAge;
    private JLabel jlInterest;
    private JLabel jlDescription;

    public SwingUserInfoPanel() {
        this.setLayout(new GridLayout(11,1));

        JLabel lUsername = new JLabel("Username:");
        lUsername.setHorizontalAlignment(JLabel.CENTER);
        jlUsername = new JLabel("-");
        jlUsername.setHorizontalAlignment(JLabel.CENTER);

        JLabel lName = new JLabel("Name:");
        lName.setHorizontalAlignment(JLabel.CENTER);
        jlName = new JLabel("-");
        jlName.setHorizontalAlignment(JLabel.CENTER);

        JLabel lAge = new JLabel("Age:");
        lAge.setHorizontalAlignment(JLabel.CENTER);
        jlAge = new JLabel("-");
        jlAge.setHorizontalAlignment(JLabel.CENTER);

        JLabel lInterest = new JLabel("Interest:");
        lInterest.setHorizontalAlignment(JLabel.CENTER);
        jlInterest = new JLabel("-");
        jlInterest.setHorizontalAlignment(JLabel.CENTER);

        JLabel lDescription = new JLabel("Description:");
        lDescription.setHorizontalAlignment(JLabel.CENTER);
        jlDescription = new JLabel("-");
        jlDescription.setHorizontalAlignment(JLabel.CENTER);

        JLabel jl = new JLabel("");

        this.add(lUsername);
        this.add(this.jlUsername);
        this.add(lName);
        this.add(this.jlName);
        this.add(lAge);
        this.add(this.jlAge);
        this.add(lInterest);
        this.add(this.jlInterest);
        this.add(lDescription);
        this.add(this.jlDescription);
        this.add(jl);
    }

    public void setUsername(String input) {
        this.jlUsername.setText(input);
    }

    public void setName(String input) {
        this.jlName.setText(input);
    }

    public void setAge(String input) {
        this.jlAge.setText(input);
    }

    public void setInterest(String input) {
        this.jlInterest.setText(input);
    }

    public void setDescription(String input) {
        this.jlDescription.setText(input);
    }
}