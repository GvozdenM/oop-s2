package UserProfileView;

import UserProfilePresenter.UserProfileView;
import UserProfilePresenter.UserProfileViewModel;
import Utils.Swing.FrameManager.FrameManager;
import Utils.Swing.Image.ImagePanel;
import Utils.Views.ResourceFetcher;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class SwingUserProfileView extends JPanel implements UserProfileView {

    private final ImagePanel imgUser;
    private final SwingUserInfoPanel infoPanel;

    public SwingUserProfileView() {
        this.setSize(600,760);

        this.setLayout(new BorderLayout());

        ResourceFetcher resourceFetcher = new ResourceFetcher("defaultuserimg.png");

        this.imgUser = new ImagePanel();
        try {
            this.imgUser.load(resourceFetcher.getResourceFile().getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.infoPanel = new SwingUserInfoPanel();

        this.add(this.imgUser, BorderLayout.PAGE_START);
        this.add(this.infoPanel, BorderLayout.CENTER);

    }


    @Override
    public void onTransitionTo() {
        FrameManager frameManager = UserProfileFrameManagerFactory.create();
        frameManager.configureFramePreferences("Profile", this, getWidth(), getHeight(), FrameManager.CENTER)
                .setResizable(false);
    }

    @Override
    public void displayMsgDialog(String msg) {
        SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(
                new JFrame(),
                msg,
                "Warning",
                JOptionPane.WARNING_MESSAGE
        ));
    }

    @Override
    public void displayErrorMsg(String msg) {
        displayMsgDialog("ERROR: " + msg);
    }

    @Override
    public void displayProfile(UserProfileViewModel vm) {
        this.infoPanel.setUsername(vm.getUsername());
        if (vm.getName() == null || vm.getName().length() == 0) {
            this.infoPanel.setName(VOID_TEXT_FIELD);
        } else this.infoPanel.setName(vm.getName());

        if (vm.getProfileImgUrl() != null && !vm.getProfileImgUrl().equals("")) {
            try {
                this.imgUser.setImgSize(200, 200);
                this.imgUser.load(
                        new URL(
                                vm.getProfileImgUrl()
                        )
                );
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        this.infoPanel.setAge(vm.getAge() + "");

        if (vm.getInterest() == null || vm.getInterest().length() == 0) {
            this.infoPanel.setInterest(VOID_TEXT_FIELD);
        } else this.infoPanel.setInterest(vm.getInterest());

        if (vm.getDescription() == null || vm.getDescription().length() == 0) {
            this.infoPanel.setDescription(VOID_TEXT_FIELD);
        } else this.infoPanel.setDescription(vm.getDescription());
    }
}
