package LoginPresenter;

import LoginController.LoginPresenter;

public abstract class AbstractLoginViewFactory {

    public abstract LoginView create(LoginPresenter presenterI);
}
