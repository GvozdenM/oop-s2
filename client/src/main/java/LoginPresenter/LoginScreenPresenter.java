package LoginPresenter;

import LoginController.LoginPresenter;
import Utils.listeners.interfaces.TextChangedListener;

/**
 * The Login Presenter is in charge of delegating requests from the attached LoginController in order to operate the
 * view externally.
 * This class implements the methods and functions of its respective interface.
 * @see LoginPresenter
 */
public class LoginScreenPresenter implements LoginPresenter {
    private LoginView loginView;

    public LoginScreenPresenter(AbstractLoginViewFactory loginViewFactory)  {
        this.loginView = loginViewFactory.create(this);
    }

    @Override
    public void init() {
        this.loginView.attachFormStateListener(
                new TextChangedListener(
                        e -> this.considerFormState()
                )
        );
        this.loginView.onTransitionTo();
    }

    @Override
    public void resetAndClear() {
        this.loginView.setLoginBtnState(false);
        this.loginView.setUsername(LoginView.EMPTY_TEXT_FIELD);
        this.loginView.setPassword(LoginView.EMPTY_TEXT_FIELD);
    }

    @Override
    public void presentError(String errorMsg) {
        this.loginView.displayErrorMsg(errorMsg);
    }

    @Override
    public void presentAuthenticationError() {
        this.loginView.displayErrorMsg(
                "Login information incorrect"
        );
        this.loginView.setPassword(LoginView.EMPTY_TEXT_FIELD);
    }

    @Override
    public void updatePresentingState(boolean formFilled) {
        this.loginView.setLoginBtnState(formFilled);
    }

    @Override
    public void end() {
        this.loginView.onCloseSuccessfully();
    }

    /**
     * This method observes the local view input fields and validates if they are adequate to allow the click of
     * the transactional button.
     */
    private void considerFormState() {
        if(this.loginView.getUsername().equals("") || this.loginView.getPassword().equals("")) {
            this.loginView.setLoginBtnState(false);
        }
        else {
            this.loginView.setLoginBtnState(true);
        }
    }
}
