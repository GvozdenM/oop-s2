package LoginPresenter;

import Utils.Views.TransitionView;
import Utils.listeners.interfaces.TextChangedListener;

/**
 * This interface provides the functions to be implemented by the LoginView Class
 */
public interface LoginView extends TransitionView {
    String EMPTY_TEXT_FIELD = "";

    /**
     * This method implements the listener of text boxes being changed.
     * @param listener TextChangedListener listener for the different fields (1 for multiple sections)
     */
    void attachFormStateListener(TextChangedListener listener);

    /**
     * This method is called for a display of an error in the current View
     * @param errorMsg Error message to be displayed.
     */
    void displayErrorMsg(String errorMsg);

    /**
     * This method is called whenever the displayed Swing JPanel is closed or exited.
     */
    void onCloseSuccessfully();

    //GETTERS AND SETTERS
    String getUsername();

    String getPassword();

    void setUsername(String input);

    void setPassword(String input);

    void setLoginBtnState(boolean clickable);

}
