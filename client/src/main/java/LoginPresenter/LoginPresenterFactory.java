package LoginPresenter;

import LoginController.AbstractLoginPresenterFactory;
import LoginController.LoginPresenter;
import LoginView.ConcreteLoginViewFactory;

public class LoginPresenterFactory extends AbstractLoginPresenterFactory {
    @Override
    public LoginPresenter create() {
        return new LoginScreenPresenter(
                new ConcreteLoginViewFactory()
        );
    }
}
