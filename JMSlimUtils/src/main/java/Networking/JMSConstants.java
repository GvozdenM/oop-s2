package Networking;

public class JMSConstants {
    public interface HOST_CONFIGURATION {
        int PORT = 13377;
        String IP_ADDRESS = "127.0.0.1";
    }
}
