package Networking.concurrent;

import Networking.SocketManager;
import Networking.SocketManagerFactory;

import java.net.Socket;


public class LowThroughPutConcurrentSocketManagerFactory extends SocketManagerFactory {
    private static final int LOW_THROUGHPUT_POOL_SIZE = 2;

    @Override
    public SocketManager create(Socket socket) {
        return new ConcurrentSocketManager(socket, LOW_THROUGHPUT_POOL_SIZE);
    }

    @Override
    public SocketManager createUnconnected() {
        return new ConcurrentSocketManager(LOW_THROUGHPUT_POOL_SIZE);
    }
}
