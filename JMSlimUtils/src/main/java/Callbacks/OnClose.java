package Callbacks;

@FunctionalInterface
public interface OnClose {
    void onClose();
}
