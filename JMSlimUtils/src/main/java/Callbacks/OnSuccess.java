package Callbacks;

@FunctionalInterface
public interface OnSuccess<T> {
    void onSuccess(T d);
}
