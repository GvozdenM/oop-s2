import Networking.JMSConstants;
import Networking.NetworkServiceFactory;
import ServerEndpoint.ServerEndpoint;
import ServiceImpl.NetworkExecutorServiceFactory;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MinderServer {
    private static final ExecutorService MESSAGING_SERVER_EXECUTOR = Executors.newSingleThreadExecutor();
    private static final ExecutorService MESSAGING_ENDPOINT_EXECUTOR = Executors.newSingleThreadExecutor();
    private static final ExecutorService MESSAGING_SERVER_UI_EXECUTOR = Executors.newSingleThreadExecutor();

    private static final int MESSAGING_SERVER_PORT = JMSConstants.HOST_CONFIGURATION.PORT;

    public static void main(String[] args) {

        startMessagingServer(new NetworkExecutorServiceFactory());
        MESSAGING_ENDPOINT_EXECUTOR.execute(ServerEndpoint.getInstance());
//        MESSAGING_SERVER_UI_EXECUTOR.execute(MinderServerUI.getInstance());
////
//        UserInteractionRepository userInteractionRepository = new UserInteractionHibernateRepository();
//        System.out.println(userInteractionRepository.getUserByLogin("Gvozden"));
//
//        HibernateRepository.closeEntityManager();
    }

    private static void startMessagingServer(@NotNull NetworkServiceFactory networkServiceFactory) {
        MESSAGING_SERVER_EXECUTOR.execute(networkServiceFactory.create(MESSAGING_SERVER_PORT));
    }

}
