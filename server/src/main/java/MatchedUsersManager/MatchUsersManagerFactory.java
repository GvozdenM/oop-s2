package MatchedUsersManager;

import MatchedUsersInteractor.*;
import Mappers.MatchedUsersMapper.MatchedUsersMapperFactory;
import Repositories.Factories.CrudRepositoryFactory;
import Repositories.Interfaces.MatchRepository;
import RepositoryImpl.Factories.MatchRepositoryFactory;
import org.hibernate.Session;

public class MatchUsersManagerFactory
        implements MatchedUsersRequesterFactory<MatchedUsersRequest, MatchedUsersUnmatchRequest, MatchedUsersResponse> {

    private final Session session;

    public MatchUsersManagerFactory(Session session) {
        this.session = session;
    }

    @Override
    public MatchedUsersRequester<MatchedUsersRequest, MatchedUsersUnmatchRequest, MatchedUsersResponse>
    createMatchedUsersRequester() {
        return new MatchedUsersManager(
            new MatchedUsersMapperFactory(new MatchRepositoryFactory(this.session))
        );
    }
}
