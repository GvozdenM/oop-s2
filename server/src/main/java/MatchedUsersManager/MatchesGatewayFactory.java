package MatchedUsersManager;

/**
 * Abstract factory for implementers of {@link MatchesGateway}
 */
public interface MatchesGatewayFactory {
    /**
     * Create the match gateway
     * @return Instance of implementor of {@link MatchesGateway}
     */
    MatchesGateway create();
}
