package MatchedUsersManager;

import MatchedUsersInteractor.MatchedUsersRequest;
import MatchedUsersInteractor.MatchedUsersRequester;
import MatchedUsersInteractor.MatchedUsersResponse;
import MatchedUsersInteractor.MatchedUsersUnmatchRequest;
import Model.User;
import Utils.Callbacks.CallbackAdapter;

import java.util.Collection;
import java.util.List;

public class MatchedUsersManager
        implements MatchedUsersRequester<MatchedUsersRequest, MatchedUsersUnmatchRequest, MatchedUsersResponse> {
    private final MatchesGatewayFactory matchesGatewayFactory;

    public MatchedUsersManager(MatchesGatewayFactory matchesGatewayFactory) {
        this.matchesGatewayFactory = matchesGatewayFactory;
    }

    @Override
    public void getMatchedUsers(MatchedUsersRequest request, CallbackAdapter<MatchedUsersResponse> callback) {
        MatchesGateway matchesGateway = this.matchesGatewayFactory.create();

        Collection<User> matchedUsers = matchesGateway.getMatchesByUserID(request.getUserID());

        MatchedUsersResponse response = new MatchedUsersResponse();

        for (User matchedUser : matchedUsers) {
            response.addUser(
                    response.createUser(
                            matchedUser.getId(),
                            matchedUser.getUsername(),
                            matchedUser.getProfileImageUrl()
                    )
            );
        }
        callback.onSuccess(response);
    }

    @Override
    public void unmatchUser(MatchedUsersUnmatchRequest request, CallbackAdapter<Class<Void>> callback) {
        MatchesGateway matchesGateway = this.matchesGatewayFactory.create();
        matchesGateway.unmatchUserByUserID(request.getCurrentUserID(), request.getUserToUnmatchID());
        callback.onSuccess(Void.class);
    }
}
