package MatchedUsersManager;

import Model.User;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

/**
 * Abstract gateway for retrieving match data by user id and
 * for deleting a match between two users
 */
public interface MatchesGateway {
    /**
     * Get all the users that the user identified by userId has matched with
     *
     * @param userId The user whos matches we are going to retrieve
     * @return A {@link Collection<User>} containing all the users that the user
     * identified by userId has matched with
     */
    Collection<User> getMatchesByUserID(@NotNull Long userId);

    /**
     * Un-match the two users identified by the two IDs passed
     *
     * @param firstUserId The first user's identifier
     * @param secondUserId The second user's identifier
     */
    void unmatchUserByUserID(@NotNull Long firstUserId, @NotNull Long secondUserId);
}
