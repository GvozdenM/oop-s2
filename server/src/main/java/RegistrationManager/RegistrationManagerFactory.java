package RegistrationManager;

import Mappers.UserMapper.RegistrationMapperFactory;
import RegistrationInteractor.RegistrationRequest;
import RegistrationInteractor.RegistrationRequester;
import RegistrationInteractor.RegistrationRequesterFactory;
import RegistrationInteractor.RegistrationResponse;
import RepositoryImpl.Factories.UserRepositoryFactory;
import org.hibernate.Session;

public class RegistrationManagerFactory
        implements RegistrationRequesterFactory<RegistrationRequest, RegistrationResponse> {
    private final Session session;

    public RegistrationManagerFactory(Session session) {
        this.session = session;
    }

    @Override
    public RegistrationRequester<RegistrationRequest, RegistrationResponse> createRegistrationRequester() {
        return new RegistrationManager(new RegistrationMapperFactory(new UserRepositoryFactory(this.session)));
    }
}
