package RegistrationManager;


import Model.User;
import RegistrationInteractor.RegistrationRequest;
import RegistrationInteractor.RegistrationRequester;
import RegistrationInteractor.RegistrationResponse;
import UserGateways.RegistrationGateway;
import UserGateways.RegistrationGatewayFactory;
import Utils.Callbacks.CallbackAdapter;
import org.hibernate.CustomEntityDirtinessStrategy;

import java.util.Optional;


public class RegistrationManager implements RegistrationRequester<RegistrationRequest, RegistrationResponse> {
    private final RegistrationGatewayFactory registrationGatewayFactory;

    public RegistrationManager(RegistrationGatewayFactory registrationGatewayFactory) {
        this.registrationGatewayFactory = registrationGatewayFactory;
    }

    @Override
    public void register(RegistrationRequest request, CallbackAdapter<RegistrationResponse> callback) {
        boolean registerUser = true;
        String errorMsg = "";

        RegistrationGateway gateway = this.registrationGatewayFactory.create();

        if (gateway.getUserByUsername(request.getUsername()).isPresent()) {
            registerUser = false;
            errorMsg = errorMsg + "\nUsername already taken";
        }

        if (gateway.getUserByUsernameOrEmail(request.getEmail()).isPresent()) {
            registerUser = false;
            errorMsg = errorMsg + "\nEmail already in use";
        }

        if (registerUser) {
            User newUser = new User(
                    request.getUsername(),
                    request.getPassword(),
                    request.getAge(),
                    request.getEmail(),
                    request.isPremium(),
                    request.getImgUrl()
            );

            newUser.setInterest(request.getInterest());

            Optional<User> optionalUser = gateway.registerUser(newUser);
            if (optionalUser.isPresent()) {
                User user = optionalUser.get();
                callback.onSuccess(new RegistrationResponse(user.getId()));
            }
            else {
                callback.onSuccess(new RegistrationResponse(false, "E_UNKNOWN"));
            }
        }
        else {
            callback.onSuccess(
                    new RegistrationResponse(false, errorMsg)
            );
        }
    }
}
