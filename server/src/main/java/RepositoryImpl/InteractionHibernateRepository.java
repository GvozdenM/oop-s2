package RepositoryImpl;

import Repositories.Entities.MatchEntity;
import Repositories.Entities.UserInteractionEntity;
import Repositories.Interfaces.CrudRepository;
import Repositories.Interfaces.InteractionRepository;
import org.hibernate.Session;
import org.jetbrains.annotations.NotNull;

import javax.persistence.NoResultException;
import java.util.Optional;

public class InteractionHibernateRepository extends HibernateCrudRepository<UserInteractionEntity> implements InteractionRepository {
    private final CrudRepository<MatchEntity> matchRepository;

    public InteractionHibernateRepository(@NotNull Session session,
                                          @NotNull CrudRepository<MatchEntity> matchRepository) {
        super(session);
        this.matchRepository = matchRepository;
    }

    @Override
    protected Class<UserInteractionEntity> getEntityClass() {
        return UserInteractionEntity.class;
    }

    /**
     * Persists the {@link UserInteractionEntity} via {@link Session} available through
     * {@link CrudRepository}. If the liker has previously been liked by the likee
     * creates a new {@link MatchEntity} and persists it using {@link CrudRepository<MatchEntity>}
     *
     * @param entity The entity to persist
     * @return {@link Optional} containing the persisted state of the entity or {@link Optional#empty()}
     * if persisting failed
     */
    @Override
    public Optional<UserInteractionEntity> create(@NotNull UserInteractionEntity entity) {
        Optional<UserInteractionEntity> userInteractionEntity = super.create(entity);
        boolean isReciprocal = false;

        try {
            //retrieve a previously create interaction between liker and likee in which likee
            //was the liker
            UserInteractionEntity reciprocalAction = this.session
                    .createNamedQuery(UserInteractionEntity.CHECK_LIKED, UserInteractionEntity.class)
                    .setParameter("liker", entity.getLikee())
                    .setParameter("likee", entity.getLiker())
                    .getSingleResult();

            isReciprocal = true;
        }
        catch (NoResultException ignored) {}

        //whould we create a match?
        boolean isLike = entity.isLike_not_dislike();
        boolean createdInteraction = userInteractionEntity.isPresent();

        //if all above conditions are met then we create and persist
        //a new match entity
        if(isReciprocal && isLike && createdInteraction) {
            MatchEntity matchEntity = new MatchEntity(
                    entity.getLikee(),
                    entity.getLiker(),
                    userInteractionEntity.get()
            );

            this.matchRepository.create(matchEntity);
        }

        return userInteractionEntity;
    }
}
