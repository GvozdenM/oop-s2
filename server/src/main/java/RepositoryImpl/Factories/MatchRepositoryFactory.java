package RepositoryImpl.Factories;

import Repositories.Factories.CrudRepositoryFactory;
import Repositories.Interfaces.MatchRepository;
import RepositoryImpl.ChatHibernateRepository;
import RepositoryImpl.MatchHibernateRepository;
import org.hibernate.Session;

/**
 * Implementation of {@link CrudRepositoryFactory<MatchRepository>}
 */
public class MatchRepositoryFactory extends CrudRepositoryFactory<MatchRepository> {
    public MatchRepositoryFactory(Session session) {
        super(session);
    }

    @Override
    public MatchRepository create() {
        return new MatchHibernateRepository(this.session, new ChatHibernateRepository(this.session));
    }
}
