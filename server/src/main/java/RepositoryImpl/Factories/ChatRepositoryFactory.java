package RepositoryImpl.Factories;

import Repositories.Factories.CrudRepositoryFactory;
import Repositories.Interfaces.ChatMessageRepository;
import RepositoryImpl.ChatHibernateRepository;
import org.hibernate.Session;

/**
 * Implementation of {@link Repositories.Factories.CrudRepositoryFactory<ChatMessageRepository>}
 */
public class ChatRepositoryFactory extends CrudRepositoryFactory<ChatMessageRepository> {
    public ChatRepositoryFactory(Session session) {
        super(session);
    }

    @Override
    public ChatMessageRepository create() {
        return new ChatHibernateRepository(this.session);
    }
}
