package RepositoryImpl.Factories;

import Repositories.Factories.CrudRepositoryFactory;
import Repositories.Interfaces.UserRepository;
import RepositoryImpl.UserHibernateRepository;
import org.hibernate.Session;

/**
 * Implementation of {@link CrudRepositoryFactory<UserRepository>}
 */
public class UserRepositoryFactory extends CrudRepositoryFactory<UserRepository> {
    public UserRepositoryFactory(Session session) {
        super(session);
    }

    @Override
    public UserRepository create() {
        return new UserHibernateRepository(this.session);
    }
}
