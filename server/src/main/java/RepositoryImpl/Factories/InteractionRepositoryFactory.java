package RepositoryImpl.Factories;

import Repositories.Factories.CrudRepositoryFactory;
import Repositories.Interfaces.InteractionRepository;
import RepositoryImpl.ChatHibernateRepository;
import RepositoryImpl.InteractionHibernateRepository;
import RepositoryImpl.MatchHibernateRepository;
import org.hibernate.Session;

/**
 * Implementation of {@link CrudRepositoryFactory<InteractionRepository>}
 */
public class InteractionRepositoryFactory extends CrudRepositoryFactory<InteractionRepository> {
    public InteractionRepositoryFactory(Session session) {
        super(session);
    }

    @Override
    public InteractionRepository create() {
        return new InteractionHibernateRepository(
                this.session,
                new MatchHibernateRepository(
                        this.session,
                    new ChatHibernateRepository(this.session)
                )
        );
    }
}
