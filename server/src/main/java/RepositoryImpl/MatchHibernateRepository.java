package RepositoryImpl;

import Model.Match;
import Repositories.Entities.MatchEntity;
import Repositories.Entities.UserEntity;
import Repositories.Entities.UserInteractionEntity;
import Repositories.Interfaces.ChatMessageRepository;
import Repositories.Interfaces.MatchRepository;
import org.hibernate.Session;
import org.jetbrains.annotations.NotNull;

import javax.persistence.NoResultException;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.Optional;

public class MatchHibernateRepository extends HibernateCrudRepository<MatchEntity> implements MatchRepository {
    private final ChatMessageRepository chatRepository;

    public MatchHibernateRepository(@NotNull Session session, ChatMessageRepository chatRepository) {
        super(session);
        this.chatRepository = chatRepository;
    }

    @Override
    public Collection<MatchEntity> getUserMatches(@NotNull Long userId) {
        try {
            return this.session
                    .createNamedQuery(MatchEntity.GET_USER_MATCHES, MatchEntity.class)
                    .setParameter("id", userId)
                    .getResultList();
        }
        catch (NoResultException e) {
            return new LinkedList<>();
        }
    }

    @Override
    public Optional<MatchEntity> match(@NotNull UserInteractionEntity like) {
        //try to load the users if either is not found ObjectNotFoundException is thrown
        UserEntity likee = this.session.load(UserEntity.class, like.getLikee());
        UserEntity liker = this.session.load(UserEntity.class, like.getLiker());

        return this.create(new MatchEntity(liker, likee, like));
    }

    @Override
    public void unmatch(@NotNull Long userA, @NotNull Long userB) {
        //try to load the users if either is not found ObjectNotFoundException is thrown
        UserEntity userEntityA = this.session.load(UserEntity.class, userA);
        UserEntity userEntityB = this.session.load(UserEntity.class, userB);

        //get the match
        Optional<MatchEntity> matchEntity = this.getMatch(userEntityA, userEntityB);

        if(matchEntity.isPresent()) {
            //delete the chat log between the users
            this.chatRepository.deleteChat(userA, userB);

            //delete the match
            this.delete(matchEntity.get());
        }
    }

    @Override
    public Optional<MatchEntity> getMatch(@NotNull UserEntity userA, @NotNull UserEntity userB) {
        try {
            return Optional.ofNullable(
                    this.session
                            .createNamedQuery(MatchEntity.GET_MATCHED_BETWEEN, MatchEntity.class)
                            .setParameter("userA", userA)
                            .setParameter("userB", userB)
                            .getSingleResult()
            );
        }
        catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<MatchEntity> getMatch(@NotNull Long userA, @NotNull Long userB) {
        //try to load the users if either is not found ObjectNotFoundException is thrown
        UserEntity userEntityA = this.session.load(UserEntity.class, userA);
        UserEntity userEntityB = this.session.load(UserEntity.class, userB);

        return this.getMatch(userEntityA, userEntityB);
    }

    @Override
    public Collection<MatchEntity> getMatchesBetweenTimestamps(Date timestamp, Date end) {
        try {
            return this.session
                    .createNamedQuery(MatchEntity.GET_MATCHES_IN_TIMESPAN, MatchEntity.class)
                    .setParameter("start", timestamp)
                    .setParameter("end", end)
                    .getResultList();
        }
        catch (NoResultException e) {
            return new LinkedList<>();
        }
    }

    @Override
    protected Class<MatchEntity> getEntityClass() {
        return MatchEntity.class;
    }
}
