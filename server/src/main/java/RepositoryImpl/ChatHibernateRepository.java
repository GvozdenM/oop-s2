package RepositoryImpl;

import Repositories.Entities.MatchEntity;
import Repositories.Entities.MessageEntity;
import Repositories.Entities.UserEntity;
import Repositories.Interfaces.ChatMessageRepository;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.jetbrains.annotations.NotNull;

import javax.persistence.NoResultException;
import java.util.Collection;
import java.util.LinkedList;

public class ChatHibernateRepository extends HibernateCrudRepository<MessageEntity> implements ChatMessageRepository {

    public ChatHibernateRepository(@NotNull Session session) {
        super(session);
    }

    @Override
    protected Class<MessageEntity> getEntityClass() {
        return MessageEntity.class;
    }

    @Override
    public Collection<MessageEntity> getChatLog(@NotNull Long userA, @NotNull Long userB) {
        //try to load the users if either is not found ObjectNotFoundException is thrown
        UserEntity userEntityA = this.session.load(UserEntity.class, userA);
        UserEntity userEntityB = this.session.load(UserEntity.class, userB);

        try {
            //get the match between the two users
            MatchEntity matchEntity = this.session
                    .createNamedQuery(MatchEntity.GET_MATCHED_BETWEEN, MatchEntity.class)
                    .setParameter("userA", userEntityA)
                    .setParameter("userB", userEntityB)
                    .getSingleResult();

            //get and return the chat log between the two users
            return this.session
                    .createNamedQuery(MessageEntity.GET_CHAT_LOG, MessageEntity.class)
                    .setParameter("match", matchEntity)
                    .getResultList();

        } catch (NoResultException e) {
            //if either the match was not found or the messages weren't found
            //there is not chat log between the users so we return an empty list
            return new LinkedList<>();
        }
    }

    @Override
    public void deleteChat(@NotNull Long userA, @NotNull Long userB) {
        //get the chat log and batch delete it
        Collection<MessageEntity> chatLog = this.getChatLog(userA, userB);
        this.batchDelete(chatLog);
    }
}
