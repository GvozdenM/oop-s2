package RepositoryImpl;

import Repositories.Entities.GenderEntity;
import Repositories.Entities.UserEntity;
import Repositories.Entities.UserInteractionEntity;
import Repositories.Interfaces.UserRepository;
import org.hibernate.Session;
import org.jetbrains.annotations.NotNull;

import javax.persistence.NoResultException;
import java.util.*;

public class UserHibernateRepository extends HibernateCrudRepository<UserEntity> implements UserRepository {
    public UserHibernateRepository(@NotNull Session session) {
        super(session);
    }

    @Override
    public Optional<UserEntity> getUserByLogin(@NotNull String login) {
        try {
            return Optional.ofNullable(this.session
                    .createNamedQuery(UserEntity.GET_USER_BY_LOGIN, UserEntity.class)
                    .setParameter("login", login)
                    .getSingleResult()
            );
        }
        catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<UserEntity> getUserByEmail(@NotNull String email) {
        try {
            return Optional.of(this.session
                    .createNamedQuery(UserEntity.GET_USER_BY_EMAIL, UserEntity.class)
                    .setParameter("email", email)
                    .getSingleResult()
            );
        }
        catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<UserEntity> getUserByEmailOrLogin(@NotNull String emailOrLogin) {
        Optional<UserEntity> u = this.getUserByLogin(emailOrLogin);

        return u.isPresent() ? u : this.getUserByEmail(emailOrLogin);
    }

    @Override
    public Collection<UserEntity> getTopUsers(int numUsers) {
        try {
            return this.session
                    .createNamedQuery(UserEntity.GET_TOP_USERS, UserEntity.class)
                    .setMaxResults(numUsers)
                    .getResultList();
        }
        catch (NoResultException e) {
            return new LinkedList<>();
        }
    }

    @Override
    public Collection<UserEntity> getLikedBy(@NotNull Long userId) {
        try {
            return this.session
                    .createNamedQuery(UserInteractionEntity.GET_LIKED_BY, UserEntity.class)
                    .setParameter("userId", userId)
                    .getResultList();
        }
        catch (NoResultException e) {
            return new LinkedList<>();
        }
    }

    @Override
    public Collection<UserEntity> getLiked(long userId) {
        try {
            return this.session
                    .createNamedQuery(UserInteractionEntity.GET_LIKED, UserEntity.class)
                    .setParameter("userId", userId)
                    .getResultList();
        }
        catch (NoResultException e) {
            return new LinkedList<>();
        }
    }

    @Override
    public Collection<UserEntity> getUserFeed(Long id) {
        //get the user
        UserEntity userEntity = this.session.get(UserEntity.class, id);

        //Checking for both Java and C lovers


        //if the user is premium we execute the premium feed query
        //else we use the normal query
        String feedQuery = userEntity.getPremium() ?
                UserInteractionEntity.GET_USER_PREMIUM_FEED :
                UserInteractionEntity.GET_USER_FEED;

        try {
            GenderEntity bothGender = this.session.get(GenderEntity.class, 10003L);

            //run the query
            List<UserEntity> feed = this.session
                    .createNamedQuery(feedQuery, UserEntity.class)
                    .setParameter("user", userEntity)
                    .setParameter("bothGender", bothGender)
                    .setParameter("userGender", userEntity.getGender())
                    .getResultList();
            Collections.shuffle(feed);


            if (userEntity.getPremium() && feed.size() < 10) {
                List<UserEntity> regFeed = this.session
                        .createNamedQuery(UserInteractionEntity.GET_USER_FEED, UserEntity.class)
                        .setParameter("user", userEntity)
                        .setParameter("userGender", userEntity.getGender())
                        .setParameter("bothGender", bothGender)
                        .getResultList();
                Collections.shuffle(regFeed);
                feed.addAll(regFeed);
            }

            return feed;
        } catch (NoResultException e) {
            return new LinkedList<>();
        }


    }

    @Override
    protected Class<UserEntity> getEntityClass() {
        return UserEntity.class;
    }
}
