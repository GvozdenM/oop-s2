package RepositoryImpl;

import Repositories.Interfaces.CrudRepository;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.Session;
import org.jetbrains.annotations.NotNull;

import javax.jms.ObjectMessage;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collection;
import java.util.Optional;

public abstract class HibernateCrudRepository<T> implements CrudRepository<T> {
    protected final Session session;

    public HibernateCrudRepository(@NotNull Session session) {
        this.session = session;
    }

    protected abstract Class<T> getEntityClass();

    @SuppressWarnings("unchecked")
    protected T createOrUpdate(@NotNull T entity) {
        try {
            this.session.saveOrUpdate(entity);
        }
        catch (NonUniqueObjectException e) {
            entity = (T) this.session.merge(entity);
            this.session.saveOrUpdate(entity);
        }

        return entity;
    }

    public Optional<T> get(@NotNull Long id) {
        return Optional.ofNullable(this.session.get(this.getEntityClass(), id));
    }

    public Collection<T> getAll() {
        CriteriaBuilder criteriaBuilder = this.session.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(this.getEntityClass());
        Root<T> rootEntity = criteriaQuery.from(this.getEntityClass());
        CriteriaQuery<T> allCriteria = criteriaQuery.select(rootEntity);

        return this.session.createQuery(allCriteria).getResultList();
    }

    public Optional<T> create(@NotNull T entity) {
        return Optional.ofNullable(this.createOrUpdate(entity));
    }

    public Optional<T> update(@NotNull T entity) {
        return Optional.ofNullable(this.createOrUpdate(entity));
    }

    @Override
    public void delete(@NotNull Long id) {
        T persistentInstance = session.load(this.getEntityClass(), id);

        if (persistentInstance != null) {
            this.delete(persistentInstance);
        }
    }

    public void delete(@NotNull T entity) {
        this.session.delete(entity);
    }

    public void batchDelete(@NotNull Collection<T> entities) {
        entities.forEach(this::delete);
    }
}
