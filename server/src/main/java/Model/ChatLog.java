package Model;

import java.io.Serializable;
import java.util.Collection;
import java.util.TreeSet;

/**
 * Domain model representing a chat log between two {@link User}s
 */
public class ChatLog implements Serializable {
    private User owner;
    private User partner;
    private Collection<ChatMessage> messages;

    /**
     * Default constructor
     * @param owner The owner of the chat log that is the user who requested it
     * @param partner The chatting partner of the owner
     * @param messages A collection of messages between the two users
     */
    public ChatLog(User owner, User partner, Collection<ChatMessage> messages) {
        this.owner = owner;
        this.partner = partner;
        this.messages = messages;
    }

    public ChatLog() {}

    public User getPartner() {
        return partner;
    }

    public void setPartner(User partner) {
        this.partner = partner;
    }

    public long getChatOwnerId() {
        return owner.getId();
    }

    public void setChatOwnerId(long chatOwnerId) {
        this.owner.setId(chatOwnerId);
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public User getOwner() {
        return owner;
    }

    public Collection<ChatMessage> getMessages() {
        return messages;
    }

    public void setMessages(TreeSet<ChatMessage> messages) {
        this.messages = messages;
    }
}
