package Model;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.io.Serializable;
import java.util.List;

/**
 * This class provides a basic shared data structure to englobe the definition of a user and its stored information.
 */
public class User implements Serializable {
    private Long id;
    private String username;
    private String name;
    private int age;
    private String email;
    private String password;
    private String interest;
    private boolean isPremium;
    private String profileImageUrl;
    private List<String> imageUrls;
    private String shortDescription;
    private String description;
    private int numLikes;

    public User() {
        this.imageUrls = new ArrayList<>();
    }
    public User(@NotNull Long id, String username, String name, int age, String email, String password, String interest, boolean isPremium, String profileImageUrl, List<String> imageUrls, String shortDescription, String description) {
        this.id = id;
        this.username = username;
        this.name = name;
        this.age = age;
        this.email = email;
        this.password = password;
        this.interest = interest;
        this.isPremium = isPremium;
        this.profileImageUrl = profileImageUrl;
        this.imageUrls = imageUrls;
        this.shortDescription = shortDescription;
        this.description = description;
        this.numLikes = 0;
    }

    public User(String username, String password, int age, String email, boolean premium) {
        this.username = username;
        this.password = password;
        this.age = age;
        this.email = email;
        this.isPremium = premium;
        this.imageUrls = new ArrayList<>();
    }

    public User(String username, String password, int age, String email, boolean premium, String profileImageUrl) {
        this.username = username;
        this.password = password;
        this.age = age;
        this.email = email;
        this.isPremium = premium;
        this.profileImageUrl = profileImageUrl;
        this.imageUrls = new ArrayList<>();
    }


    public User(@NotNull Long id, String username, String password, int age, String email, boolean premium) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.profileImageUrl = "";
        this.age = age;
        this.email = email;
        this.isPremium = premium;
        this.imageUrls = new ArrayList<>();
    }

    public Long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public boolean isPremium() {
        return isPremium;
    }

    public void setPremium(boolean premium) {
        isPremium = premium;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public List<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(List<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumLikes() {
        return numLikes;
    }

    public void setNumLikes(int numLikes) {
        this.numLikes = numLikes;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
