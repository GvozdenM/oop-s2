package Model;

import java.io.Serializable;
import java.util.Date;

public class Match implements Serializable {
    private User firstLiker;
    private User secondLiker;
    private Date matchTime;

    public Match() {
    }

    public Match(User firstLiker,
                 User secondLiker,
                 Date matchTime) {
        this.firstLiker = firstLiker;
        this.secondLiker = secondLiker;
        this.matchTime = matchTime;
    }

    public User getFirstLiker() {
        return firstLiker;
    }

    public void setFirstLiker(User firstLiker) {
        this.firstLiker = firstLiker;
    }

    public User getSecondLiker() {
        return secondLiker;
    }

    public void setSecondLiker(User secondLiker) {
        this.secondLiker = secondLiker;
    }

    public Date getMatchTime() {
        return matchTime;
    }

    public void setMatchTime(Date matchTime) {
        this.matchTime = matchTime;
    }
}
