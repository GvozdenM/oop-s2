package ChatManager;

import ChatInteractor.*;
import Mappers.ChatMapper.ChatMapperFactory;
import Repositories.Factories.CrudRepositoryFactory;
import Repositories.Interfaces.ChatMessageRepository;
import Repositories.Interfaces.MatchRepository;
import Repositories.Interfaces.UserRepository;
import RepositoryImpl.Factories.ChatRepositoryFactory;
import RepositoryImpl.Factories.MatchRepositoryFactory;
import RepositoryImpl.Factories.UserRepositoryFactory;
import org.hibernate.Session;
import org.jetbrains.annotations.NotNull;

public class ChatManagerFactory
        implements AbstractChatRequesterFactory<ChatLogRequest, OutgoingChatMessage, IncomingChatMessage, ChatLogResponse> {
    private final Session session;

    public ChatManagerFactory(Session session) {
        this.session = session;
    }

    @Override
    public ChatRequester<ChatLogRequest, OutgoingChatMessage, IncomingChatMessage, ChatLogResponse> createChatRequester() {
        ChatMapperFactory chatMapperFactory = new ChatMapperFactory(
                new UserRepositoryFactory(this.session),
                new ChatRepositoryFactory(this.session),
                new MatchRepositoryFactory(this.session)
        );

        return new ChatManager(
                chatMapperFactory,
                chatMapperFactory
        );
    }
}
