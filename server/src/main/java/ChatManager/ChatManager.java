package ChatManager;

import ChatGateways.ChatMessageGateway;
import ChatGateways.ChatMessageGatewayFactory;
import ChatGateways.MessageSendingGateway;
import ChatGateways.MessageSendingGatewayFactory;
import ChatInteractor.*;
import Model.ChatLog;
import Model.ChatMessage;
import Model.User;
import Utils.Callbacks.CallbackAdapter;
import Utils.Logger.Log;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;
import java.util.Optional;
import java.util.TreeSet;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class ChatManager implements ChatRequester<ChatLogRequest, OutgoingChatMessage, IncomingChatMessage, ChatLogResponse> {
    private static final String TAG = "ChatManager";

    private final ChatMessageGatewayFactory chatMessageGatewayFactory;
    private final MessageSendingGatewayFactory messageSendingGatewayFactory;

    public ChatManager(@NotNull ChatMessageGatewayFactory chatMessageGatewayFactory,
                       @NotNull MessageSendingGatewayFactory messageSendingGatewayFactory) {
        this.chatMessageGatewayFactory = chatMessageGatewayFactory;
        this.messageSendingGatewayFactory = messageSendingGatewayFactory;
    }

    @Override
    public void send(@NotNull OutgoingChatMessage message, @NotNull CallbackAdapter<IncomingChatMessage> callback) {
        MessageSendingGateway messageSendingGateway = this.messageSendingGatewayFactory.createMessageSendingGateway();
        Optional<ChatMessage> optionalChatMessage = messageSendingGateway.sendMessage(
                            new ChatMessage(
                                    message.getMessage(),
                                    message.getTimestamp(),
                                    message.getSenderId(),
                                    message.getReceiverId()
                            )
        );

        if(optionalChatMessage.isPresent()) {
            ChatMessage newMessage = optionalChatMessage.get();

            callback.onSuccess(
                    new IncomingChatMessage(
                            newMessage.getMessage(),
                            newMessage.getTimestamp(),
                            newMessage.getSenderId(),
                            newMessage.getReceiverId()
                    )
            );
        }
        else {
            callback.onFail(new Throwable("Could not persist chat message"));
        }
    }

    @Override
    public void setOnMessageReceived(@NotNull CallbackAdapter<IncomingChatMessage> callback) {
        Log.d(TAG,"This method is provided for the client implementation");
    }

    @Override
    public void getChatLog(@NotNull ChatLogRequest request, @NotNull CallbackAdapter<ChatLogResponse> callback) {
        ChatMessageGateway chatMessageGateway = this.chatMessageGatewayFactory.createChatMessageGateway();
        Optional<ChatLog> optionalChatLog = chatMessageGateway.getChatLog(request.getUserA(), request.getUserB());

        if(optionalChatLog.isPresent()) {
            ChatLog chatLog = optionalChatLog.get();

            //create response
            ChatLogResponse response = new ChatLogResponse(
                    getChatUser(chatLog.getOwner()),
                    getChatUser(chatLog.getPartner()),
                    toTreeSet(chatLog.getMessages())
            );

            callback.onSuccess(response);
        }
        else {
            callback.onFail(new IOException("Couldn't get chat log from persistence gateway!"));
        }
    }

    /**
     * Utility method to transform {@link User} domain model object to {@link ChatUser} response
     * object
     *
     * @param user The user to transform
     * @return New instatnce of {@link ChatUser} created based on {@link User} that was pased
     */
    private ChatUser getChatUser(User user) {
        return new ChatUser(
                user.getId(),
                user.getUsername(),
                user.getProfileImageUrl()
        );
    }

    /**
     * Utility method to conver {@link Collection<Model.ChatMessage>} to an auto-sorting {@link TreeSet}
     * based on {@link LoggedChatMessage#getTimestamp()}
     *
     * @param messages The collection of messages to transform
     * @return The newly instantiated {@link TreeSet} of messages sorted by timestamp
     */
    private TreeSet<LoggedChatMessage> toTreeSet(Collection<Model.ChatMessage> messages) {
        return messages
                .stream()
                .map(chatMessage -> new LoggedChatMessage(
                        chatMessage.getMessage(),
                        chatMessage.getTimestamp(),
                        chatMessage.getSenderId(),
                        chatMessage.getReceiverId()
                        )
                )
                .collect(Collectors.toCollection(
                        (Supplier<TreeSet<LoggedChatMessage>> & Serializable)
                                (new SerializableChatMessageTimestampComparator()::createTimestampComparator)
                ));
    }
}
