package ChatManager;

import ChatInteractor.LoggedChatMessage;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.TreeSet;
import java.util.function.Function;

/**
 * Serializable factory for creating a {@link TreeSet<LoggedChatMessage>} which auto
 * sorts {@link LoggedChatMessage} instances based on {@link LoggedChatMessage#getTimestamp()}
 */
public class SerializableChatMessageTimestampComparator implements Serializable {
    public TreeSet<LoggedChatMessage> createTimestampComparator() {
        return new TreeSet<>(Comparator.comparing(
                (Function<LoggedChatMessage, Date> & Serializable)
                        (LoggedChatMessage::getTimestamp)));
    }
}
