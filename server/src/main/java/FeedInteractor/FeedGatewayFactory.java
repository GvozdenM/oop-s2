package FeedInteractor;

/**
 * Abstract Factory interface for creating {@link FeedGateway} implementations
 */
public interface FeedGatewayFactory {
    /**
     * Create {@link FeedGateway}
     * @return {@link FeedGateway} implementer instance
     */
    FeedGateway create();
}
