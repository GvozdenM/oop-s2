package FeedInteractor;

import Model.User;
import UserGateways.UserDataGateway;
import UserGateways.UserDataGatewayFactory;
import Utils.Callbacks.CallbackAdapter;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Implementation of {@link FeedRequester} this class provides the central business rules for
 * processing user interactions and fetching the user feed.
 */
public class FeedManager implements FeedRequester<FeedActionRequest, FeedActionResponse, FeedActionUserResponse> {
    private final FeedGateway feedGateway;
    private final UserDataGateway userDataGateway;

    /**
     * Default constructor
     * @param feedGatewayFactory Factory for gateway to retrieve feed data from persistence store
     * @param userDataGatewayFactory Factory for gateway to retrieve user data from persistence store
     */
    public FeedManager(@NotNull FeedGatewayFactory feedGatewayFactory,
                       @NotNull UserDataGatewayFactory userDataGatewayFactory) {
        this.feedGateway = feedGatewayFactory.create();
        this.userDataGateway = userDataGatewayFactory.create();
    }

    /**
     * Create a like interaction between the two users
     * @param invoker The invoker of the like
     * @param receiver The receiver of the like
     *
     * @return Partially configured response object with {@link FeedActionResponse#isMatched()} returning true
     * if the the invoking user has already been liked by the receiving user or false otherwise.     */
    private FeedActionResponse like(@NotNull User invoker, @NotNull User receiver) {
        FeedActionResponse response = new FeedActionResponse();
        response.setMatched(false);

        this.feedGateway.like(receiver, invoker);

        if (this.feedGateway.areMatched(receiver.getId(), invoker.getId())) {
            response.setMatched(true);
        }

        return response;
    }

    /**
     * Create an interaction based on the request passed
     * @param interaction Request object for the interaction to create
     * @param callback Set of methods to call in the case of success or fail
     */
    @Override
    public void interact(@NotNull FeedActionRequest interaction,
                         @NotNull CallbackAdapter<FeedActionResponse> callback) {
        Optional<User> userOptional = this.userDataGateway.getUser(interaction.getReceiverId());
        Optional<User> matchMakerOptional = this.userDataGateway.getUser(interaction.getInvokerId());

        if(!(userOptional.isPresent() && matchMakerOptional.isPresent())) {
            callback.onFail(new Throwable("Users not found!"));
            return;
        }

        User invoker = matchMakerOptional.get();
        User receiver = userOptional.get();

        FeedActionResponse response = interaction.isLike() ?
                this.like(invoker, receiver) :
                this.dislike(invoker, receiver);

        response.setInvokerId(invoker.getId());
        response.setReceiverId(receiver.getId());
        response.setMatchedWith(this.userToFeedActionUserTransform(receiver));
        response.setWhoMatched(this.userToFeedActionUserTransform(invoker));

        callback.onSuccess(response);
    }

    /**
     * Create a dislike interaction between the two users
     * @param invoker The invoker of the like
     * @param receiver The receiver of the like
     *
     * @return Partially configured response object with {@link FeedActionResponse#isMatched()} returning false
     */
    private FeedActionResponse dislike(@NotNull User invoker,
                                       @NotNull User receiver) {
        FeedActionResponse response = new FeedActionResponse();
        response.setMatched(false);

        this.feedGateway.dislike(invoker, receiver);

        return response;
    }

    /**
     * Retrieves the user's feed from the gateway, transforms it to response models and returns it via the callback
     *
     * @param id The id of the user's who's feed we're requesting
     * @param callback Callback to execute when operation has completed
     */
    @Override
    public void getUserFeedBuffer(long id, @NotNull CallbackAdapter<List<FeedActionUserResponse>> callback) {
        Collection<User> feed = this.feedGateway.getUserFeed(id);

        //transform the feed
        List<FeedActionUserResponse> transformedFeed = feed
                .stream()
                .map(this::userToFeedActionUserTransform)
                .collect(Collectors.toList());

        callback.onSuccess(transformedFeed);
    }

    /**
     * Retrieves a specific user by id from the {@link UserDataGateway} and returns it via the callback
     * in the form of a {@link FeedActionUserResponse}.
     *
     * @param id The id of the user to retrieve
     * @param callback The call back to execute
     */
    @Override
    public void getUserById(long id, @NotNull CallbackAdapter<FeedActionUserResponse> callback) {
        Optional<User> user = this.userDataGateway.getUser(id);

        if(user.isPresent()) {
            callback.onSuccess(this.userToFeedActionUserTransform(user.get()));
        }
        else {
            callback.onFail(new Throwable("Could not find user"));
        }
    }

    /**
     * Utility function to transform {@link User} domain objects to {@link FeedActionUserResponse} objects
     * @param user The object to transform
     * @return {@link FeedActionUserResponse} instance representing the state of {@link User} passed
     */
    private FeedActionUserResponse userToFeedActionUserTransform(@NotNull User user) {
        FeedActionUserResponse feedActionUserResponse = new FeedActionUserResponse();
        feedActionUserResponse.setId(user.getId());
        feedActionUserResponse.setUsername(user.getUsername());
        feedActionUserResponse.setName(user.getName());
        feedActionUserResponse.setAge(user.getAge());
        feedActionUserResponse.setInterestedIn(user.getInterest());
        feedActionUserResponse.setProfileImageUrl(user.getProfileImageUrl());
        feedActionUserResponse.setShortDescription(user.getShortDescription());
        feedActionUserResponse.setDescription(user.getDescription());

        return feedActionUserResponse;
    }
}
