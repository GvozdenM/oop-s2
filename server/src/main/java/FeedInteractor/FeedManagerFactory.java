package FeedInteractor;

import Mappers.FeedMapper.FeedMapperFactory;
import Mappers.UserMapper.UserMapperFactory;
import RepositoryImpl.Factories.InteractionRepositoryFactory;
import RepositoryImpl.Factories.MatchRepositoryFactory;
import RepositoryImpl.Factories.UserRepositoryFactory;
import org.hibernate.Session;

public class FeedManagerFactory
        implements FeedRequesterFactory<FeedActionRequest, FeedActionResponse, FeedActionUserResponse> {
    private final Session session;

    public FeedManagerFactory(Session session) {
        this.session = session;
    }

    @Override
    public FeedRequester<FeedActionRequest, FeedActionResponse, FeedActionUserResponse> createFeedRequester() {
        UserRepositoryFactory userRepositoryFactory = new UserRepositoryFactory(this.session);
        InteractionRepositoryFactory interactionRepositoryFactory = new InteractionRepositoryFactory(this.session);
        MatchRepositoryFactory matchRepositoryFactory = new MatchRepositoryFactory(this.session);

        return new FeedManager(
                new FeedMapperFactory(userRepositoryFactory, interactionRepositoryFactory, matchRepositoryFactory),
                new UserMapperFactory(userRepositoryFactory)
        );
    }
}
