package FeedInteractor;

import Model.User;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Abstract gateway for manipulating user interactions
 */
public interface FeedGateway {
    /**
     * Register a 'like' interaction between the receiving and invoking user
     *
     * @param receiver The user that's been liked
     * @param invoker The user that performed the like
     */
    void like(@NotNull User receiver, @NotNull User invoker);

    /**
     * Register a 'dislike' interaction between the receiving and invoking user
     *
     * @param receiver The user that's been disliked
     * @param invoker The user that performed the dislike
     */
    void dislike(@NotNull User receiver, @NotNull User invoker);

    /**
     * Method to check whether the invoker and receiver are matched
     *
     * @param receiverId A user's id
     * @param invokerId A second user's id
     * @return True - if the users are matched or False - if the users are not matched
     */
    boolean areMatched(@NotNull Long receiverId, @NotNull Long invokerId);

    /**
     * Get the feed of users for the specified user id
     *
     * @param userId The user who's feed to fetch
     * @return A {@link Collection<User>} representing the feed of the user
     * passed
     */
    Collection<User> getUserFeed(@NotNull Long userId);
}
