package Mappers.MatchedUsersMapper;

import MatchedUsersManager.MatchesGateway;
import MatchedUsersManager.MatchesGatewayFactory;
import Repositories.Factories.CrudRepositoryFactory;
import Repositories.Interfaces.MatchRepository;
import org.hibernate.Session;
import org.jetbrains.annotations.NotNull;

/**
 * Factory for {@link MatchesGateway} implemented by {@link MatchUsersMapper}
 */
public class MatchedUsersMapperFactory implements MatchesGatewayFactory {
    private final CrudRepositoryFactory<MatchRepository> matchRepoFactory;
    /**
     * Default constructor

     * @param matchRepoFactory Factory for match repository
     */
    public MatchedUsersMapperFactory(@NotNull CrudRepositoryFactory<MatchRepository> matchRepoFactory) {
        this.matchRepoFactory = matchRepoFactory;
    }

    @Override
    public MatchesGateway create() {
        return new MatchUsersMapper(this.matchRepoFactory.create());
    }
}
