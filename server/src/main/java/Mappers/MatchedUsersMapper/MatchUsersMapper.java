package Mappers.MatchedUsersMapper;

import Mappers.Transformations.UserTransformation;
import MatchedUsersManager.MatchesGateway;
import Model.User;
import Repositories.Interfaces.MatchRepository;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Implementation of {@link MatchesGateway} relating to the retrieving {@link Model.Match} objects
 * by {@link User} id from the persistence store.
 */
public class MatchUsersMapper implements MatchesGateway {
    private final MatchRepository matchRepository;

    /**
     * Default constructor
     *
     * @param matchRepository The repository from which to get match data
     */
    public MatchUsersMapper(@NotNull MatchRepository matchRepository) {
        this.matchRepository = matchRepository;
    }

    /**
     * Retrieve from the repository a {@link Collection<User>} of user entities and map it
     * to a {@link Collection<Repositories.Entities.UserEntity>}
     *
     * @param userId The user whos matches we are going to retrieve
     * @return A {@link Collection<User>} representing the users the user passed has matched wtih
     */
    @Override
    public Collection<User> getMatchesByUserID(@NotNull Long userId) {
        //get the collection of matches for this user
        //then select the other user from the match object
        return this.matchRepository
                .getUserMatches(userId)
                .stream()
                .map((m) -> Objects.equals(m.getUserA().getId(), userId) ? m.getUserB() : m.getUserA())
                .map(UserTransformation::toModel)
                .collect(Collectors.toList());
    }

    /**
     * Un-match the two users
     *
     * @param firstUserId The first user's identifier
     * @param secondUserId The second user's identifier
     */
    @Override
    public void unmatchUserByUserID(@NotNull Long firstUserId, @NotNull Long secondUserId) {
        this.matchRepository.unmatch(firstUserId, secondUserId);
    }
}
