package Mappers;

public class MappersConfigurator {
    public static final String MODE_MOCK = "MOCK";
    public static final String MODE_DEFAULT = "DEFAULT";

    private static String mode = MODE_DEFAULT;

    public static String getMode() {
        return mode;
    }

    public static void setMode(String mode) {
        MappersConfigurator.mode = mode;
    }
}
