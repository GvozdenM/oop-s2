package Mappers.UserMapper;

import Repositories.Factories.CrudRepositoryFactory;
import Repositories.Interfaces.UserRepository;
import UserGateways.UserDataGateway;
import UserGateways.UserDataGatewayFactory;

/**
 * Factory for {@link UserDataGateway} as implemented by {@link UserMapper} implements
 * {@link UserDataGatewayFactory} to provide a concrete interface for creating instances
 * of {@link UserMapper}
 */
public class UserMapperFactory extends UserDataGatewayFactory {
    private final CrudRepositoryFactory<UserRepository> userRepositoryFactory;

    /**
     * Default Constructor
     * @param userRepositoryFactory Factory for creating a MatchRepository to provide to mappers
     *                        as a connection module to the persistence store
     */
    public UserMapperFactory(CrudRepositoryFactory<UserRepository> userRepositoryFactory) {
        this.userRepositoryFactory = userRepositoryFactory;
    }

    /**
     * Create an instance of {@link UserMapper}
     * @return instance of {@link UserMapper}
     */
    @Override
    public UserDataGateway create() {
        return new UserMapper(this.userRepositoryFactory.create());
    }
}
