package Mappers.UserMapper;

import Repositories.Factories.CrudRepositoryFactory;
import Repositories.Interfaces.UserRepository;
import UserGateways.RegistrationGateway;
import UserGateways.RegistrationGatewayFactory;
import UserGateways.UserDataGateway;
import UserGateways.UserDataGatewayFactory;

/**
 * Factory for {@link RegistrationGateway} as implemented by {@link UserMapper} implements
 * {@link RegistrationGatewayFactory} to provide a concrete interface for creating instances
 * of {@link UserMapper}
 */
public class RegistrationMapperFactory extends RegistrationGatewayFactory {
    private final CrudRepositoryFactory<UserRepository> userRepositoryFactory;

    /**
     * Default Constructor
     * @param userRepositoryFactory Factory for creating a MatchRepository to provide to mappers
     *                        as a connection module to the persistence store
     */
    public RegistrationMapperFactory(CrudRepositoryFactory<UserRepository> userRepositoryFactory) {
        this.userRepositoryFactory = userRepositoryFactory;
    }

    /**
     * Create an instance of implementor of {@link RegistrationGateway}
     * @return instance of {@link UserMapper} which implements {@link RegistrationGateway}
     */
    @Override
    public RegistrationGateway create() {
        return new UserMapper(this.userRepositoryFactory.create());
    }
}
