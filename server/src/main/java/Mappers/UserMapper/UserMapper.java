package Mappers.UserMapper;

import Mappers.Transformations.UserTransformation;
import Model.User;
import Repositories.Interfaces.UserRepository;
import UserGateways.RegistrationGateway;
import UserGateways.TopUsersGateway;
import UserGateways.UserDataGateway;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Implementation of various data gateways relating to the persistence of {@link Repositories.Entities.UserEntity}. Maps
 * primarily betweeen {@link Repositories.Entities.UserEntity} and {@link User}
 */
public class UserMapper implements UserDataGateway, RegistrationGateway, TopUsersGateway {
    private final UserRepository userRepository;

    /**
     * Default constructor
     * @param userRepository Repository that manages user data
     */
    public UserMapper(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<User> getUser(@NotNull Long id) {
        return this.userRepository
                .get(id)
                .map(UserTransformation::toModel);
    }

    @Override
    public Optional<User> getUserByUsername(@NotNull String username) {
        return this.userRepository
                .getUserByLogin(username)
                .map(UserTransformation::toModel);
    }

    @Override
    public Optional<User> getUserByUsernameOrEmail(@NotNull String userNameOrEmail) {
        return this.userRepository
                .getUserByEmailOrLogin(userNameOrEmail)
                .map(UserTransformation::toModel);
    }

    @Override
    public Optional<User> updateUserProfile(@NotNull User user) {
        return this.userRepository
                .update(UserTransformation.toEntity(user))
                .map(UserTransformation::toModel);
    }

    @Override
    public Optional<User> registerUser(@NotNull User newUser) {
        return this.userRepository
                .create(UserTransformation.toEntity(newUser))
                .map(UserTransformation::toModel);
    }

    @Override
    public Collection<User> getTopUsers(int numUsers) {
        return this.userRepository
                .getTopUsers(numUsers)
                .stream()
                .map(UserTransformation::toModel)
                .collect(Collectors.toList());
    }
}
