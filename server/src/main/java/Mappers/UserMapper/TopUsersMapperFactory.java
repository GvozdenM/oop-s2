package Mappers.UserMapper;

import Repositories.Factories.CrudRepositoryFactory;
import Repositories.Interfaces.UserRepository;
import UserGateways.TopUsersGateway;
import UserGateways.TopUsersGatewayFactory;

/**
 * Factory for {@link TopUsersGateway} as implemented by {@link UserMapper} implements
 * {@link TopUsersGatewayFactory} to provide a concrete interface for creating instances
 * of {@link TopUsersGateway}
 */
public class TopUsersMapperFactory extends TopUsersGatewayFactory {
    private final CrudRepositoryFactory<UserRepository> userRepositoryFactory;

    /**
     * Default Constructor
     * @param userRepositoryFactory Factory for creating a MatchRepository to provide to mappers
     *                        as a connection module to the persistence store
     */
    public TopUsersMapperFactory(CrudRepositoryFactory<UserRepository> userRepositoryFactory) {
        this.userRepositoryFactory = userRepositoryFactory;
    }

    /**
     * Create an instance of implementor of {@link TopUsersGateway}
     * @return instance of {@link UserMapper} which implements {@link TopUsersGateway}
     */
    @Override
    public TopUsersGateway create() {
        return new UserMapper(this.userRepositoryFactory.create());
    }
}
