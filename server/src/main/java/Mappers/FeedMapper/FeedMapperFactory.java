package Mappers.FeedMapper;

import FeedInteractor.FeedGateway;
import FeedInteractor.FeedGatewayFactory;
import Repositories.Factories.CrudRepositoryFactory;
import Repositories.Interfaces.InteractionRepository;
import Repositories.Interfaces.MatchRepository;
import Repositories.Interfaces.UserRepository;

/**
 * Factory for {@link FeedGateway} as implemented by {@link FeedMapper}. This class implements
 * {@link FeedGatewayFactory} to provide a concrete interface for creating instances
 * of {@link FeedMapper}
 */
public class FeedMapperFactory implements FeedGatewayFactory {
    private final CrudRepositoryFactory<UserRepository> userRepositoryFactory;
    private final CrudRepositoryFactory<InteractionRepository> interactionRepositoryFactory;
    private final CrudRepositoryFactory<MatchRepository> matchRepository;

    /**
     * Default constructor
     *
     * @param userRepositoryFactory Instance of factory for {@link UserRepository} to supply to mapper
     * @param interactionRepositoryFactory Instance of factory for {@link InteractionRepository} to supply to mapper
     * @param matchRepository Instance of factory for {@link MatchRepository} to supply to mapper
     */
    public FeedMapperFactory(CrudRepositoryFactory<UserRepository> userRepositoryFactory,
                             CrudRepositoryFactory<InteractionRepository> interactionRepositoryFactory,
                             CrudRepositoryFactory<MatchRepository> matchRepository) {
        this.userRepositoryFactory = userRepositoryFactory;
        this.interactionRepositoryFactory = interactionRepositoryFactory;
        this.matchRepository = matchRepository;
    }

    /**
     * Create FeedMapper
     * @return {@link FeedMapper} instance implements {@link FeedGateway}
     */
    @Override
    public FeedGateway create() {
        return new FeedMapper(
                this.userRepositoryFactory.create(),
                this.interactionRepositoryFactory.create(),
                this.matchRepository.create()
        );
    }
}
