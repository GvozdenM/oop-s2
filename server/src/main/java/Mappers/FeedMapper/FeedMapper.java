package Mappers.FeedMapper;

import FeedInteractor.FeedGateway;
import Mappers.Transformations.UserTransformation;
import Model.User;
import Repositories.Entities.UserEntity;
import Repositories.Entities.UserInteractionEntity;
import Repositories.Interfaces.InteractionRepository;
import Repositories.Interfaces.MatchRepository;
import Repositories.Interfaces.UserRepository;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Mapper between {@link UserEntity} and {@link User} and exposes
 * functionality for manipulating user interactions through various {@link Repositories.Interfaces.CrudRepository}
 */
public class FeedMapper implements FeedGateway {
    private final UserRepository userRepository;
    private final InteractionRepository interactionRepository;
    private final MatchRepository matchRepository;

    /**
     * Default constructor
     * @param userRepository Repository managing user data
     * @param interactionRepository Repository managing user interaction data
     * @param matchRepository Repository managing user match data
     */
    public FeedMapper(@NotNull UserRepository userRepository,
                      @NotNull InteractionRepository interactionRepository,
                      @NotNull MatchRepository matchRepository) {
        this.userRepository = userRepository;
        this.interactionRepository = interactionRepository;
        this.matchRepository = matchRepository;
    }

    @Override
    public void like(@NotNull User receiver, @NotNull User invoker) {
        Optional<UserEntity> receiverEntity = this.userRepository.get(receiver.getId());
        Optional<UserEntity> invokerEntity = this.userRepository.get(invoker.getId());

        if(receiverEntity.isPresent() && invokerEntity.isPresent()) {
            //create the entity
            UserInteractionEntity userInteractionEntity = new UserInteractionEntity(
                    true,
                    invokerEntity.get(),
                    receiverEntity.get()
            );

            //persist the entity
            this.interactionRepository.create(userInteractionEntity);
        }
    }

    @Override
    public void dislike(@NotNull User receiver, @NotNull User invoker) {
        Optional<UserEntity> receiverEntity = this.userRepository.get(receiver.getId());
        Optional<UserEntity> invokerEntity = this.userRepository.get(invoker.getId());

        if(receiverEntity.isPresent() && invokerEntity.isPresent()) {
            //create the entity
            UserInteractionEntity userInteractionEntity = new UserInteractionEntity(
                    false,
                    invokerEntity.get(),
                    receiverEntity.get()
            );

            //persist the entity
            this.interactionRepository.create(userInteractionEntity);
        }
    }

    @Override
    public boolean areMatched(@NotNull Long receiverId, @NotNull Long invokerId) {
        return this.matchRepository.getMatch(receiverId, invokerId).isPresent();
    }

    @Override
    public Collection<User> getUserFeed(@NotNull Long userId) {
        return this.userRepository
                .getUserFeed(userId)
                .stream()
                .map(UserTransformation::toModel)
                .collect(Collectors.toList());
    }
}
