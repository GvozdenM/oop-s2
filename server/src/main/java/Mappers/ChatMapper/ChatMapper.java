package Mappers.ChatMapper;

import ChatGateways.ChatMessageGateway;
import ChatGateways.MessageSendingGateway;
import Mappers.Transformations.ChatMessageTransformation;
import Mappers.Transformations.UserTransformation;
import Model.ChatLog;
import Model.ChatMessage;
import Model.User;
import Repositories.Entities.MatchEntity;
import Repositories.Entities.MessageEntity;
import Repositories.Entities.UserEntity;
import Repositories.Interfaces.ChatMessageRepository;
import Repositories.Interfaces.MatchRepository;
import Repositories.Interfaces.UserRepository;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Stateless mapper implements {@link ChatMessageGateway} and {@link MessageSendingGateway}
 * and maps between entities persisted via various repositories and the domain model
 * objects they represent.
 */
public class ChatMapper implements ChatMessageGateway, MessageSendingGateway {
    private final UserRepository userRepository;
    private final ChatMessageRepository chatRepository;
    private final MatchRepository matchRepository;

    /**
     * Default constructor
     * @param userRepository The repository from which to get user data
     * @param chatRepository The repository from which to get chat data
     * @param matchRepository The repository from which to get match data
     */
    public ChatMapper(UserRepository userRepository,
                      ChatMessageRepository chatRepository,
                      MatchRepository matchRepository) {
        this.userRepository = userRepository;
        this.chatRepository = chatRepository;
        this.matchRepository = matchRepository;
    }

    /**
     * Get the chat log between two users specified by their id
     *
     * @param userAId Identifier of the first user
     * @param userBId Identifier of the second user
     *
     * @return A {@link Optional} containing the chat log between the
     * two users or a {@link Optional#empty()} if one of the two users was
     * not found. {@link ChatLog#getMessages()} will return an empty Collection
     * if both users were found but there were no messages sent between them
     */
    @Override
    public Optional<ChatLog> getChatLog(Long userAId, Long userBId) {
        //retrieve the owner of the chat
        Optional<User> userA =
                this.userRepository.get(userAId).map(UserTransformation::toModel);

        //retrieve the partner
        Optional<User> userB =
                this.userRepository.get(userBId).map(UserTransformation::toModel);

        //retrieve the messages between them and map them 4
        Collection<ChatMessage> messages = this.chatRepository
                .getChatLog(userAId, userBId)
                .stream()
                .map(ChatMessageTransformation::toModel)
                .collect(Collectors.toList());

        if(userA.isPresent() && userB.isPresent()) {
            return Optional.of(new ChatLog(userA.get(), userB.get(), messages));
        }
        else {
            return Optional.empty();
        }
    }

    /**
     * Persist a new message between users
     * @param message The message domain model object to persist
     * @return A domain model object containing the message data as represented
     * in the persistence store
     */
    @Override
    public Optional<ChatMessage> sendMessage(ChatMessage message) {
        //retrieve the two users
        Optional<UserEntity> sender = this.userRepository.get(message.getSenderId());
        Optional<UserEntity> receiver = this.userRepository.get(message.getReceiverId());

        //retrieve the match between these two
        Optional<MatchEntity> matchEntity = this.matchRepository.getMatch(
                message.getSenderId(),
                message.getReceiverId()
        );

        //if both users exist and there is a match create the entity
        if(sender.isPresent() && receiver.isPresent() && matchEntity.isPresent()) {
            MessageEntity messageEntity = ChatMessageTransformation.toEntity(
                    message,
                    sender.get(),
                    receiver.get(),
                    matchEntity.get()
            );

            //persist the message entity
            return this.chatRepository
                    .create(messageEntity)
                    .map(ChatMessageTransformation::toModel);
        }
        else {
            //either one of the users or the match was not present so message could
            //not be persisted
            return Optional.empty();
        }
    }
}
