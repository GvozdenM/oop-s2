package Mappers.ChatMapper;

import ChatGateways.ChatMessageGateway;
import ChatGateways.ChatMessageGatewayFactory;
import ChatGateways.MessageSendingGateway;
import ChatGateways.MessageSendingGatewayFactory;
import FeedInteractor.FeedGatewayFactory;
import Mappers.FeedMapper.FeedMapper;
import Repositories.Factories.CrudRepositoryFactory;
import Repositories.Interfaces.ChatMessageRepository;
import Repositories.Interfaces.MatchRepository;
import Repositories.Interfaces.UserRepository;
import org.jetbrains.annotations.NotNull;

/**
 * Factory for {@link ChatMessageGateway} and {@link MessageSendingGateway}
 * as implemented by {@link FeedMapper}. This class implements {@link FeedGatewayFactory} to provide
 * a concrete interface for creating instances of {@link ChatMapper}
 */
public class ChatMapperFactory implements ChatMessageGatewayFactory, MessageSendingGatewayFactory {
    private final CrudRepositoryFactory<UserRepository> userRepositoryFactory;
    private final CrudRepositoryFactory<ChatMessageRepository> chatRepositoryFactory;
    private final CrudRepositoryFactory<MatchRepository> matchRepositoryFactory;

    /**
     * Default constructor
     *
     * @param userRepositoryFactory Instance of factory for {@link UserRepository} to supply to mapper
     * @param chatRepositoryFactory Instance of factory for {@link ChatMessageRepository} to supply to mapper
     * @param matchRepositoryFactory Instance of factory for {@link MatchRepository} to supply to mapper
     */
    public ChatMapperFactory(@NotNull CrudRepositoryFactory<UserRepository> userRepositoryFactory,
                             @NotNull CrudRepositoryFactory<ChatMessageRepository> chatRepositoryFactory,
                             @NotNull CrudRepositoryFactory<MatchRepository> matchRepositoryFactory) {
        this.userRepositoryFactory = userRepositoryFactory;
        this.chatRepositoryFactory = chatRepositoryFactory;
        this.matchRepositoryFactory = matchRepositoryFactory;
    }

    @Override
    public ChatMessageGateway createChatMessageGateway() {
        return this.create();

    }

    @Override
    public MessageSendingGateway createMessageSendingGateway() {
        return this.create();
    }

    private ChatMapper create() {
        return new ChatMapper(
                this.userRepositoryFactory.create(),
                this.chatRepositoryFactory.create(),
                this.matchRepositoryFactory.create()
        );
    }
}
