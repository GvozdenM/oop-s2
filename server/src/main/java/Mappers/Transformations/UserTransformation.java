package Mappers.Transformations;

import Model.User;
import Repositories.Entities.GenderEntity;
import Repositories.Entities.UserEntity;
import Repositories.Entities.UserImageEntity;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Utility functions for mapping between {@link User} domain model objects
 * and {@link UserEntity} persistence objects.
 */
public interface UserTransformation {
    /**
     * Map between persistence {@link UserEntity} object to a
     * {@link User} domain object.
     *
     * @param ue The object to map from
     * @return {@link User} that's been mapped to
     */
    static User toModel(@NotNull UserEntity ue) {
        User u = new User();

        //map base data
        u.setId(ue.getId());
        u.setUsername(ue.getLogin());
        u.setAge(ue.getAge());
        u.setEmail(ue.getEmail());
        u.setPassword(ue.getPassword());
        u.setInterest(ue.getGender().getTitle());
        u.setPremium(ue.getPremium());
        u.setShortDescription(ue.getBio());
        u.setDescription(ue.getDescription());
        u.setNumLikes(ue.getNumLikes());
        u.setProfileImageUrl(ue.getProfileImageUrl());

        return u;
    };

    /**
     * Map between domain {@link User} object to a
     * {@link UserEntity} domain object.
     *
     * @param u The {@link User} object to map from
     * @return {@link UserEntity} that's been mapped to
     */
    static UserEntity toEntity(@NotNull User u) {
        long genderId = 1L;

        switch (u.getInterest()) {
            case "C":
                genderId = 10001;
                break;
            case "Java":
                genderId = 10002;
                break;
            case "C and Java":
                genderId = 10003;
                break;
        }

        UserEntity userEntity = new UserEntity();

        userEntity.setId(u.getId());
        userEntity.setLogin(u.getUsername());
        userEntity.setEmail(u.getEmail());
        userEntity.setAge(u.getAge());
        userEntity.setPassword(u.getPassword());
        userEntity.setBio(u.getShortDescription());
        userEntity.setDescription(u.getDescription());
        userEntity.setProfileImageUrl(u.getProfileImageUrl());
        userEntity.setGender(new GenderEntity(genderId));
        userEntity.getGender().setTitle(u.getInterest());
        userEntity.setPremium(u.isPremium());

        return userEntity;
    };
}
