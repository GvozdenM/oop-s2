package Mappers.Transformations;

import Model.ChatLog;
import Model.ChatMessage;
import Model.Match;
import Repositories.Entities.MatchEntity;
import Repositories.Entities.MessageEntity;
import Repositories.Entities.UserEntity;
import org.jetbrains.annotations.NotNull;

import java.time.Instant;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.TreeSet;
import java.util.stream.Collectors;
/**
 * Utility functions for mapping between {@link ChatMessage} domain model objects
 * and {@link MessageEntity} persistence objects.
 */
public interface ChatMessageTransformation {
    static ChatMessage toModel(@NotNull MessageEntity me) {
        return new ChatMessage(
                me.getMessage(),
                me.getTimestamp(),
                me.getSender().getId(),
                me.getReceiver().getId()
        );
    }

    /**
     * Map between persistence {@link MessageEntity} object to a
     * {@link ChatMessage} domain object.
     *
     * @param cm The chat message to map from
     * @param sender The sending user {@link UserEntity} persistence entity
     * @param receiver The receiving user {@link UserEntity} persistence entity
     * @param match The {@link MatchEntity} between the sender and receiver associated with this message
     *
     * @return The {@link MessageEntity} persistence object
     */
    static MessageEntity toEntity(@NotNull ChatMessage cm,
                                  @NotNull UserEntity sender,
                                  @NotNull UserEntity receiver,
                                  @NotNull MatchEntity match) {
        return new MessageEntity(
                match,
                receiver,
                sender,
                cm.getMessage()
        );
    }

    /**
     * Maps from a {@link Collection<MessageEntity>} of persistence message entities
     * to a {@link ChatLog} domain model object
     *
     * @param messageEntities A collection of messages from the perspective of the user
     * @param user The user for whom the chat log is being retrieved
     * @param partner The user being chatted with
     *
     * @return A {@link ChatLog} domain object containing a {@link Collection<ChatMessage>}
     * mapped from the incoming {@link Collection<MessageEntity>}
     */
    static ChatLog toChatLogModel(@NotNull Collection<MessageEntity> messageEntities,
                                  @NotNull UserEntity user,
                                  @NotNull UserEntity partner) {
        Model.ChatLog chatLog = new Model.ChatLog();
        TreeSet<ChatMessage> messageList = messageEntities
                .stream()
                .map(ChatMessageTransformation::toModel)
                .collect(Collectors.toCollection(
                        () -> new TreeSet<>(Comparator.comparing(ChatMessage::getTimestamp))
                        )
                );

        chatLog.setMessages(messageList);
        chatLog.setOwner(UserTransformation.toModel(user));
        chatLog.setChatOwnerId(user.getId());
        chatLog.setPartner(UserTransformation.toModel(partner));

        return chatLog;
    }
}
