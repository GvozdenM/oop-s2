package Mappers.Transformations;

import Model.Match;
import Model.User;
import Repositories.Entities.MatchEntity;
import Repositories.Entities.UserEntity;
import Repositories.Entities.UserInteractionEntity;

/**
 * Utility functions for mapping between {@link Match} domain model objects
 * and {@link MatchEntity} persistence objects.
 */
public interface MatchTransform {
    /**
     * Map between persistence {@link MatchEntity} object to a
     * {@link Match} domain object.
     *
     * @param m The object to map from
     * @param ue The user interaction which spawned the match
     * @return {@link Match} that's been mapped to
     */
    static MatchEntity toEntity(Match m, UserInteractionEntity ue) {
        return new MatchEntity(
                UserTransformation.toEntity(m.getFirstLiker()),
                UserTransformation.toEntity(m.getSecondLiker()),
                ue
        );
    }

    /**
     * Map between domain {@link Match} object to a
     * {@link MatchEntity} domain object.
     *
     * @param me The {@link Match} object to map from
     * @return {@link MatchEntity} that's been mapped to
     */
    static Match toModel(MatchEntity me) {
        return new Match(
                UserTransformation.toModel(me.getUserA()),
                UserTransformation.toModel(me.getUserB()),
                me.getTimestamp()
        );
    }
}
