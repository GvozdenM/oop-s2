package ChatGateways;

import Model.ChatMessage;

import java.util.Optional;

public interface MessageSendingGateway {
    Optional<ChatMessage> sendMessage(ChatMessage message);
}
