package ChatGateways;

import Model.ChatLog;

import java.util.Optional;

public interface ChatMessageGateway {
    Optional<ChatLog> getChatLog(Long user1, Long user2);
}
