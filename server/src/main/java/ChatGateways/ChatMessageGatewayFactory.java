package ChatGateways;

/**
 * Abstract Factory interface for creating {@link ChatMessageGateway} implementer instances
 */
public interface ChatMessageGatewayFactory {
    ChatMessageGateway createChatMessageGateway();
}
