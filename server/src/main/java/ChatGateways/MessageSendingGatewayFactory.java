package ChatGateways;

/**
 * Abstract Factory interface for creating {@link MessageSendingGateway} implementer instances
 */
public interface    MessageSendingGatewayFactory {
    MessageSendingGateway createMessageSendingGateway();
}
