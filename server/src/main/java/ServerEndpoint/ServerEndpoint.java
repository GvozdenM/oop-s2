package ServerEndpoint;

import ChatInteractor.*;
import ChatManager.ChatManagerFactory;
import Destinations.JMSlimDestination;
import FeedInteractor.*;
import LoginInteractor.LoginRequest;
import LoginInteractor.LoginRequester;
import LoginInteractor.LoginResponse;
import LoginManager.LoginManagerFactory;
import MatchGraphInteractor.MatchGraphRequester;
import MatchGraphInteractor.MatchGraphRequesterFactory;
import MatchGraphInteractor.MatchGraphResponse;
import MatchGraphManager.*;
import MatchedUsersInteractor.MatchedUsersRequest;
import MatchedUsersInteractor.MatchedUsersRequester;
import MatchedUsersInteractor.MatchedUsersResponse;
import MatchedUsersInteractor.MatchedUsersUnmatchRequest;
import MatchedUsersManager.MatchUsersManagerFactory;
import RegistrationInteractor.RegistrationRequest;
import RegistrationInteractor.RegistrationRequester;
import RegistrationInteractor.RegistrationResponse;
import RegistrationManager.RegistrationManagerFactory;
import TopUsersInteractor.*;
import UpdateProfileInteractor.UpdateProfileRequest;
import UpdateProfileInteractor.UpdateProfileRequester;
import UpdateProfileInteractor.UserProfileResponse;
import UserProfileManager.ProfileUpdateManagerFactory;
import Utils.Callbacks.CallbackAdapter;
import Utils.JMSlim;
import Utils.Logger.Log;
import org.jetbrains.annotations.NotNull;

import javax.jms.*;
import java.io.Closeable;
import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

public class ServerEndpoint
        implements Runnable,
        Closeable {
    private static ServerEndpoint ENDPOINT = null;

    private final JMSlim jmSlim = JMSlim.getJMSlim();
    private final Map<Long, String> userSessions = new ConcurrentHashMap<>();
    private final ReentrantLock lock = new ReentrantLock(true);
    private final DatabaseSessionHandler databaseSessionHandler = new DatabaseSessionHandler();
    private Connection connection;

    public static ServerEndpoint getInstance() {
        if(null == ENDPOINT) {
            ENDPOINT = new ServerEndpoint();
        }

        return ENDPOINT;
    }

    /**
     * Default constructor gets connection instance but does not start it
     */
    private ServerEndpoint() {
        try {
            this.connection = ((ConnectionFactory) this.jmSlim.lookup("ConnectionFactory")).createConnection();
            this.connection.setClientID("MinderServer");
        } catch (JMSException ignored) {}
    }

    private void initIncomingQueues(Connection connection) throws JMSException {
        this.lock.lock();
        try {
            Destination loginRequests = this.jmSlim.createQueue("loginRequests");
            Destination registrationRequests = this.jmSlim.createQueue("registrationRequests");
            Destination updateProfileRequests = this.jmSlim.createQueue("updateProfileRequests");
            Destination userByIdRequests = this.jmSlim.createQueue("userByIdRequests");
            Destination feedActionRequests = this.jmSlim.createQueue("feedActionRequests");
            Destination feedBufferRequests = this.jmSlim.createQueue("feedBufferRequests");
            Destination feedUserByIdRequests = this.jmSlim.createQueue("feedUserByIdRequests");
            Destination matchedUsersRequests = this.jmSlim.createQueue("matchedUsersRequests");
            Destination unmatchUserRequests = this.jmSlim.createQueue("unmatchUserRequests");
            Destination chatMessageSendChannel = this.jmSlim.createQueue("chatMessageSend");
            Destination chatLogRequests = this.jmSlim.createQueue("chatLogRequests");
            Destination logOutRequests = this.jmSlim.createQueue("logOutRequests");
            Destination matchGraphRequests = this.jmSlim.createQueue("matchGraphRequests");
            Destination topUsersRequests = this.jmSlim.createQueue("topUsersRequests");

            Session session = connection.createSession();

            startListeningForLoginRequests(session, loginRequests);
            startListeningForRegistrationRequests(session, registrationRequests);
            startListeningForProfileUpdateRequests(session, updateProfileRequests);
            startListeningForUserByIdRequests(session, userByIdRequests);
            startListeningForFeedActionRequests(session, feedActionRequests);
            startListeningForFeedBufferRequests(session, feedBufferRequests);
            startListeningForFeedUserByIdRequests(session, feedUserByIdRequests);
            startListeningForChatMessageSends(session, chatMessageSendChannel);
            startListeningForMatchedUsersRequests(session, matchedUsersRequests);
            startListeningForUnmatchUserRequests(session, unmatchUserRequests);
            startListeningForChatLogRequests(session, chatLogRequests);
            startListeningForLogOutRequests(session, logOutRequests);
            startListeningForMatchGraphRequests(session, matchGraphRequests);
            startListeningForTopUsersRequests(session, topUsersRequests);
        }
        finally {
            this.lock.unlock();
        }
    }
    private void startListeningForTopUsersRequests(Session session, Destination topUserRequests) throws JMSException {
        session
                .createConsumer(topUserRequests)
                .setMessageListener(message -> {
                    //get database session
                    org.hibernate.Session databaseSession = this.databaseSessionHandler.getSession();

                    try {
                        //parse request
                        TopUsersRequest request = (TopUsersRequest) ((ObjectMessage) message).getObject();

                        TopUsersRequesterFactory<TopUsersRequest, TopUsersResponse> topUsersRequesterFactory =
                                new TopUserManagerFactory(databaseSession);

                        //create chat manager
                        TopUsersRequester<TopUsersRequest, TopUsersResponse> topUsersRequester =
                                topUsersRequesterFactory.create();

                        //start database transaction
                        databaseSession.beginTransaction();

                        //make request for chat log
                        topUsersRequester.getTopUsers(
                                request,
                                new CallbackAdapter<>(
                                        response -> {
                                            try {
                                                databaseSession.getTransaction().commit();

                                                respondViaMessage(
                                                        response,
                                                        ((JMSlimDestination) message.getJMSReplyTo()).getId(),
                                                        message.getStringProperty("originatorId"));

                                            } catch (JMSException e) {
                                                e.printStackTrace();
                                                databaseSession.getTransaction().rollback();
                                            }
                                        },
                                        throwable -> databaseSession.getTransaction().commit(),
                                        throwable -> databaseSession.getTransaction().rollback()
                                )
                        );

                    } catch (Exception e) {
                        //rollback transaction
                        databaseSession.getTransaction().rollback();
                        e.printStackTrace();
                    }
                });
    }

    private void startListeningForMatchGraphRequests(Session session, Destination matchGraphRequests) throws JMSException {
        session
                .createConsumer(matchGraphRequests)
                .setMessageListener(message -> {
                    //get database session
                    org.hibernate.Session databaseSession = this.databaseSessionHandler.getSession();

                    try {
                        MatchGraphRequesterFactory<MatchGraphResponse> matchGraphRequesterFactory =
                                new MatchGraphManagerFactory(databaseSession);

                        //create chat manager
                        MatchGraphRequester<MatchGraphResponse>
                                matchGraphRequester = matchGraphRequesterFactory.createMatchGraphRequester();

                        //start database transaction
                        databaseSession.beginTransaction();

                        //make request for chat log
                        matchGraphRequester.graph(
                                new CallbackAdapter<>(
                                        response -> {
                                            try {
                                                databaseSession.getTransaction().commit();

                                                respondViaMessage(
                                                        response,
                                                        ((JMSlimDestination) message.getJMSReplyTo()).getId(),
                                                        message.getStringProperty("originatorId"));

                                            } catch (JMSException e) {
                                                e.printStackTrace();
                                                databaseSession.getTransaction().rollback();
                                            }
                                        },
                                        throwable -> databaseSession.getTransaction().commit(),
                                        throwable -> databaseSession.getTransaction().rollback()
                                )
                        );

                    } catch (Exception e) {
                        //rollback transaction
                        databaseSession.getTransaction().rollback();
                        e.printStackTrace();
                    }
                });
    }

    private void startListeningForLogOutRequests(Session session, Destination logOutRequests) throws JMSException {
        session
                .createConsumer(logOutRequests)
                .setMessageListener(message -> {
                            try {
                                Long id = (Long) ((ObjectMessage) message).getObject();
                                this.userSessions.remove(id);
                                System.out.println("Logged out user " + id);
                            } catch (JMSException jmsException) {
                                jmsException.printStackTrace();
                            }
                        }
                );
    }


    private void startListeningForChatLogRequests(Session session, Destination chatLogRequests) throws JMSException {
        session
                .createConsumer(chatLogRequests)
                .setMessageListener(message -> {
                    //get database session
                    org.hibernate.Session databaseSession = this.databaseSessionHandler.getSession();

                    try {
                        //parse request
                        ChatLogRequest request = (ChatLogRequest) ((ObjectMessage) message).getObject();

                        ChatManagerFactory chatManagerFactory = new ChatManagerFactory(databaseSession);

                        //create chat manager
                        ChatRequester<ChatLogRequest, OutgoingChatMessage, IncomingChatMessage, ChatLogResponse>
                                chatRequester = chatManagerFactory.createChatRequester();

                        //start database transaction
                        databaseSession.beginTransaction();

                        //make request for chat log
                        chatRequester.getChatLog(
                                request,
                                new CallbackAdapter<>(
                                        response -> {
                                            try {
                                                databaseSession.getTransaction().commit();

                                                respondViaMessage(
                                                        response,
                                                        ((JMSlimDestination) message.getJMSReplyTo()).getId(),
                                                        message.getStringProperty("originatorId"));

                                            } catch (JMSException e) {
                                                e.printStackTrace();
                                                databaseSession.getTransaction().rollback();
                                            }
                                        },
                                        throwable -> databaseSession.getTransaction().commit(),
                                        throwable -> databaseSession.getTransaction().rollback()
                                )
                        );

                    } catch (Exception e) {
                        //rollback transaction
                        databaseSession.getTransaction().rollback();
                        e.printStackTrace();
                    }
                });
    }

    private void startListeningForUnmatchUserRequests(Session session, Destination unmatchUserRequests) throws JMSException {
        session
                .createConsumer(unmatchUserRequests)
                .setMessageListener( message -> {
                    //get database session
                    org.hibernate.Session databaseSession = this.databaseSessionHandler.getSession();

                    try {
                        //pares request
                        MatchedUsersUnmatchRequest request = (MatchedUsersUnmatchRequest) ((ObjectMessage) message).getObject();

                        //create manager factory
                        MatchUsersManagerFactory matchedUsersManagerFactory =
                                new MatchUsersManagerFactory(databaseSession);

                        //create match manager
                        MatchedUsersRequester<MatchedUsersRequest, MatchedUsersUnmatchRequest, MatchedUsersResponse>
                                matchedUsersRequester = matchedUsersManagerFactory.createMatchedUsersRequester();

                        //start database transaction
                        databaseSession.beginTransaction();

                        //un-match users
                        matchedUsersRequester.unmatchUser(
                                request,
                                new CallbackAdapter<>(
                                        response -> databaseSession.getTransaction().commit(),
                                        throwable -> databaseSession.getTransaction().commit(),
                                        throwable -> databaseSession.getTransaction().rollback()
                                )
                        );

                    } catch (Exception e) {
                        databaseSession.getTransaction().rollback();
                        e.printStackTrace();
                    }
                });
    }

    private void startListeningForMatchedUsersRequests(Session session, Destination matchedUsersRequests) throws JMSException {
        session
                .createConsumer(matchedUsersRequests)
                .setMessageListener( message -> {
                    //get database session
                    org.hibernate.Session databaseSession = this.databaseSessionHandler.getSession();

                    try {
                        MatchedUsersRequest request = (MatchedUsersRequest) ((ObjectMessage) message).getObject();

                        //create manager factory
                        MatchUsersManagerFactory matchedUsersManagerFactory =
                                new MatchUsersManagerFactory(databaseSession);

                        //create match manager
                        MatchedUsersRequester<MatchedUsersRequest, MatchedUsersUnmatchRequest, MatchedUsersResponse>
                                matchedUsersRequester = matchedUsersManagerFactory.createMatchedUsersRequester();

                        //start database transaction
                        databaseSession.beginTransaction();

                        //get matched users
                        matchedUsersRequester.getMatchedUsers(
                                request,
                                new CallbackAdapter<>(
                                        response -> {
                                            try {
                                                databaseSession.getTransaction().commit();

                                                respondViaMessage(
                                                        response,
                                                        ((JMSlimDestination) message.getJMSReplyTo()).getId(),
                                                        message.getStringProperty("originatorId"));
                                            } catch (JMSException ignored) {
                                                databaseSession.getTransaction().rollback();
                                            }
                                        },
                                        throwable -> databaseSession.getTransaction().commit(),
                                        throwable -> databaseSession.getTransaction().rollback()
                                )
                        );

                    } catch (Exception e) {
                        databaseSession.getTransaction().rollback();
                        e.printStackTrace();
                    }
                });
    }

    private void startListeningForChatMessageSends(Session session, Destination chatMessageSendChannel) throws JMSException {
        session
                .createConsumer(chatMessageSendChannel)
                .setMessageListener(message -> {
                    //get database session
                    org.hibernate.Session databaseSession = this.databaseSessionHandler.getSession();

                    try {
                        //parse message
                        OutgoingChatMessage chatMessage = (OutgoingChatMessage) ((ObjectMessage) message).getObject();

                        ChatManagerFactory chatManagerFactory = new ChatManagerFactory(databaseSession);

                        //create chat manager
                        ChatRequester<ChatLogRequest, OutgoingChatMessage, IncomingChatMessage, ChatLogResponse>
                                chatRequester = chatManagerFactory.createChatRequester();

                        //start database transaction
                        databaseSession.beginTransaction();

                        //send message
                        chatRequester.send(chatMessage, new CallbackAdapter<>(
                                (response) -> {
                                    try {
                                        databaseSession.getTransaction().commit();
                                        respondViaMessage(
                                                response,
                                                "queue/chatMessageReceive",
                                                this.userSessions.get(chatMessage.getReceiverId()));
                                    } catch (JMSException e) {
                                        databaseSession.getTransaction().rollback();
                                        Log.s(e.getMessage());
                                    }
                                },
                                throwable -> databaseSession.getTransaction().commit(),
                                throwable -> databaseSession.getTransaction().rollback()
                        ));
                    } catch (Exception e) {
                        databaseSession.getTransaction().rollback();
                        e.printStackTrace();
                    }
                });
    }

    private void startListeningForFeedUserByIdRequests(Session session, Destination feedActionRequests) throws JMSException {
        session
                .createConsumer(feedActionRequests)
                .setMessageListener(message -> {
                    //get database session
                    org.hibernate.Session databaseSession = this.databaseSessionHandler.getSession();

                    try {
                        //parse message
                        Long id = (Long) ((ObjectMessage) message).getObject();

                        //create requester
                        FeedRequester<FeedActionRequest, FeedActionResponse, FeedActionUserResponse> feedRequester =
                                new FeedManagerFactory(databaseSession).createFeedRequester();

                        //begin database transaction
                        databaseSession.beginTransaction();

                        //retrieve user by id
                        feedRequester.getUserById(id, new CallbackAdapter<>(
                                (response) -> {
                                    try {
                                        databaseSession.getTransaction().commit();
                                        respondViaMessage(
                                                response,
                                                ((JMSlimDestination) message.getJMSReplyTo()).getId(),
                                                message.getStringProperty("originatorId"));
                                    } catch (JMSException ignored) {
                                        databaseSession.getTransaction().rollback();
                                    }
                                },
                                throwable -> databaseSession.getTransaction().commit(),
                                throwable -> databaseSession.getTransaction().rollback()
                        ));
                    } catch (Exception e) {
                        databaseSession.getTransaction().rollback();
                        e.printStackTrace();
                    }
                });
    }

    private void startListeningForFeedBufferRequests(Session session, Destination feedBufferRequests) throws JMSException {
        session
                .createConsumer(feedBufferRequests)
                .setMessageListener(message -> {
                    //get database session
                    org.hibernate.Session databaseSession = this.databaseSessionHandler.getSession();

                    try {
                        Long id = (Long) ((ObjectMessage) message).getObject();

                        //create requester
                        FeedRequester<FeedActionRequest, FeedActionResponse, FeedActionUserResponse> feedRequester =
                                new FeedManagerFactory(databaseSession).createFeedRequester();

                        //begin database transaction
                        databaseSession.beginTransaction();

                        //get user's feed
                        feedRequester.getUserFeedBuffer(id, new CallbackAdapter<>(
                                (response) -> {
                                    try {
                                        databaseSession.getTransaction().commit();

                                        respondViaMessage(
                                                (Serializable) response,
                                                ((JMSlimDestination) message.getJMSReplyTo()).getId(),
                                                message.getStringProperty("originatorId"));
                                    } catch (JMSException ignored) {
                                        databaseSession.getTransaction().rollback();
                                    }
                                },
                                throwable -> databaseSession.getTransaction().commit(),
                                throwable -> databaseSession.getTransaction().rollback()
                        ));
                    } catch (Exception e) {
                        databaseSession.getTransaction().rollback();
                        e.printStackTrace();
                    }
                });
    }

    private void startListeningForFeedActionRequests(Session session, Destination feedActionRequests) throws JMSException {
        session
                .createConsumer(feedActionRequests)
                .setMessageListener((message -> {
                    //get database session
                    org.hibernate.Session databaseSession = this.databaseSessionHandler.getSession();

                    try {
                        FeedActionRequest request = (FeedActionRequest) ((ObjectMessage) message).getObject();

                        //create requester
                        FeedRequester<FeedActionRequest, FeedActionResponse, FeedActionUserResponse> feedRequester =
                                new FeedManagerFactory(databaseSession).createFeedRequester();

                        //begin database transaction
                        databaseSession.beginTransaction();

                        //commit intercation
                        feedRequester.interact(request, new CallbackAdapter<>(
                                (response) -> {
                                    try {
                                        databaseSession.getTransaction().commit();

                                        FeedActionResponse otherResponse = new FeedActionResponse();
                                        otherResponse.setWhoMatched(response.getMatchedWith());
                                        otherResponse.setMatchedWith(response.getWhoMatched());
                                        otherResponse.setReceiverId(response.getReceiverId());
                                        otherResponse.setInvokerId(response.getInvokerId());
                                        otherResponse.setMatched(response.isMatched());


                                        respondViaMessage(
                                                response,
                                                ((JMSlimDestination) message.getJMSReplyTo()).getId(),
                                                message.getStringProperty("originatorId"));

                                        if(response.isMatched()) {
                                            respondViaMessage(
                                                    otherResponse,
                                                    ((JMSlimDestination) message.getJMSReplyTo()).getId(),
                                                    userSessions.get(request.getReceiverId()));
                                        }
                                    } catch (JMSException ignored) {
                                        databaseSession.getTransaction().rollback();
                                    }
                                },
                                throwable -> databaseSession.getTransaction().commit(),
                                throwable -> databaseSession.getTransaction().rollback()
                        ));
                    } catch (Exception e) {
                        databaseSession.getTransaction().commit();
                        e.printStackTrace();
                    }
                }));
    }

    private void startListeningForLoginRequests(Session session, Destination loginRequests) throws JMSException {
        session
                .createConsumer(loginRequests)
                .setMessageListener((message) -> {
                    //get database session
                    org.hibernate.Session databaseSession = this.databaseSessionHandler.getSession();

                    try {
                        //parse request
                        Object body = ((ObjectMessage) message).getObject();

                        //create requester
                        LoginRequester<LoginRequest, LoginResponse> loginRequester =
                                new LoginManagerFactory(databaseSession).createLoginRequester();

                        databaseSession.beginTransaction();

                        //log user in
                        loginRequester.login((LoginRequest) body, new CallbackAdapter<>(
                                (response) -> {
                                    try {
                                        databaseSession.getTransaction().commit();

                                        this.userSessions.put(
                                                response.getUserId(),
                                                message.getStringProperty("originatorId")
                                        );

                                        respondViaMessage(
                                                response,
                                                ((JMSlimDestination) message.getJMSReplyTo()).getId(),
                                                message.getStringProperty("originatorId"));
                                    } catch (JMSException ignored) {
                                        databaseSession.getTransaction().rollback();
                                    }
                                },
                                throwable -> databaseSession.getTransaction().commit(),
                                throwable -> databaseSession.getTransaction().rollback()
                        ));
                    } catch (Exception e) {
                        databaseSession.getTransaction().rollback();
                        e.printStackTrace();
                    }
                });
    }

    private void startListeningForRegistrationRequests(Session session, Destination registrationRequests)
            throws JMSException {
        session
                .createConsumer(registrationRequests)
                .setMessageListener((message) -> {
                    //get database session
                    org.hibernate.Session databaseSession = this.databaseSessionHandler.getSession();

                    try {
                        RegistrationRequest body = (RegistrationRequest) ((ObjectMessage) message).getObject();

                        RegistrationRequester<RegistrationRequest, RegistrationResponse> registrationRequester
                                = new RegistrationManagerFactory(databaseSession).createRegistrationRequester();

                        databaseSession.beginTransaction();

                        registrationRequester.register(body, new CallbackAdapter<>(
                                (response) -> {
                                    try {
                                        databaseSession.getTransaction().commit();

                                        if(response.isAuthenticated()) {
                                            this.userSessions.put(
                                                    response.getId(),
                                                    message.getStringProperty("originatorId")
                                            );
                                        }

                                        respondViaMessage(
                                                response,
                                                ((JMSlimDestination) message.getJMSReplyTo()).getId(),
                                                message.getStringProperty("originatorId"));
                                    } catch (Exception ignored) {
                                        databaseSession.getTransaction().rollback();
                                        this.userSessions.remove(response.getId());
                                    }
                                },
                                throwable -> databaseSession.getTransaction().commit(),
                                throwable -> databaseSession.getTransaction().rollback()
                        ));
                    } catch (Exception e) {
                        e.printStackTrace();
                        databaseSession.getTransaction().rollback();
                    }
                });
    }

    private void startListeningForProfileUpdateRequests(Session session, Destination updateProfileRequests)
            throws JMSException {
        session
                .createConsumer(updateProfileRequests)
                .setMessageListener((message) -> {
                    //get database session
                    org.hibernate.Session databaseSession = this.databaseSessionHandler.getSession();

                    try {
                        UpdateProfileRequest body = (UpdateProfileRequest) ((ObjectMessage) message).getObject();

                        //create requester
                        UpdateProfileRequester<UpdateProfileRequest, UserProfileResponse> updateProfileRequester =
                                new ProfileUpdateManagerFactory(databaseSession).createUpdateProfileRequester();

                        //start db transaction
                        databaseSession.beginTransaction();

                        updateProfileRequester.updateUser(body, new CallbackAdapter<>(
                                (response) -> {
                                    try {
                                        databaseSession.getTransaction().commit();

                                        respondViaMessage(
                                                response,
                                                ((JMSlimDestination) message.getJMSReplyTo()).getId(),
                                                message.getStringProperty("originatorId"));
                                    } catch (JMSException ignored) {
                                        databaseSession.getTransaction().rollback();
                                    }
                                },
                                throwable -> databaseSession.getTransaction().commit(),
                                throwable -> databaseSession.getTransaction().rollback()
                        ));
                    } catch (Exception e) {
                        e.printStackTrace();
                        databaseSession.getTransaction().rollback();
                    }
                });
    }

    private void startListeningForUserByIdRequests(Session session, Destination userByIdRequests) throws JMSException {
        session
                .createConsumer(userByIdRequests)
                .setMessageListener((message) -> {
                    //get database session
                    org.hibernate.Session databaseSession = this.databaseSessionHandler.getSession();

                    try {
                        Long id = (Long) ((ObjectMessage) message).getObject();

                        //create requester
                        UpdateProfileRequester<UpdateProfileRequest, UserProfileResponse> updateProfileRequester =
                                new ProfileUpdateManagerFactory(databaseSession).createUpdateProfileRequester();

                        //start db transaction
                        databaseSession.beginTransaction();

                        updateProfileRequester.getUser(id, new CallbackAdapter<>(
                                (response) -> {
                                    try {
                                        databaseSession.getTransaction().commit();

                                        respondViaMessage(
                                                response,
                                                ((JMSlimDestination) message.getJMSReplyTo()).getId(),
                                                message.getStringProperty("originatorId"));
                                    } catch (JMSException ignored) {
                                        databaseSession.getTransaction().rollback();
                                    }
                                },
                                throwable -> databaseSession.getTransaction().commit(),
                                throwable -> databaseSession.getTransaction().rollback()
                        ));
                    } catch (Exception e) {
                        e.printStackTrace();
                        databaseSession.getTransaction().rollback();
                    }
                });
    }

    protected void respondViaMessage(Serializable body, @NotNull String destinationName, String clientId) throws JMSException {
        //create session and object message
        Session session = this.connection.createSession();
        ObjectMessage message = session.createObjectMessage();

        //target the resource server for this message and set the body
        message.setStringProperty("clientId", clientId);
        message.setObject(body);

        //initiate message producer for destination of the given name
        MessageProducer messageProducer =
                session.createProducer((Destination) JMSlim.getJMSlim().lookup(destinationName));

        //send the message and close the producer
        messageProducer.send(message);
        messageProducer.close();
    }

    private void initOutgoingQueues() {
        this.lock.lock();
        try {
           this.jmSlim.createQueue("loginResponses");
           this.jmSlim.createQueue("feedActionResponses");
           this.jmSlim.createQueue("matchedUsersResponses");
           this.jmSlim.createQueue("registrationResponses");
           this.jmSlim.createQueue("chatMessageReceive");
           this.jmSlim.createQueue("userByIdResponses");
           this.jmSlim.createQueue("feedBufferResponses");
           this.jmSlim.createQueue("updateProfileResponse");
           this.jmSlim.createQueue("chatLogResponses");
           this.jmSlim.createQueue("feedUserByIdResponses");
           this.jmSlim.createQueue("matchGraphResponses");
           this.jmSlim.createQueue("topUsersResponses");
        }
        finally {
            this.lock.unlock();
        }
    }

    @Override
    public void run() {
        this.lock.lock();
        try {
            this.initIncomingQueues(this.connection);
            this.initOutgoingQueues();
            this.connection.start();
        } catch (JMSException e) {
            e.printStackTrace();
        }
        finally {
            this.lock.unlock();
        }
    }
    @Override
    public void close() throws IOException {
        try {
            this.connection.close();
            this.databaseSessionHandler.close();
        } catch (JMSException e) {
            throw new IOException(e.getMessage());
        }
    }
}
