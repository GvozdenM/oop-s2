package ServerEndpoint;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DatabaseSessionHandler {
    private final EntityManagerFactory entityManagerFactory =
            Persistence.createEntityManagerFactory("MinderPersistence");
    private final SessionFactory sessionFactory = this.entityManagerFactory.unwrap(SessionFactory.class);

    public Session getSession() {
        return this.sessionFactory.getCurrentSession();
    }

    public void close() {
        this.sessionFactory.close();
    }
}
