package MatchGraphMapper;

import Mappers.Transformations.MatchTransform;
import MatchGraphManager.MatchGraphGateway;
import Model.Match;
import Repositories.Entities.MatchEntity;
import Repositories.Interfaces.MatchRepository;
import RepositoryImpl.Factories.MatchRepositoryFactory;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Stateless mapper maps Matches for the MatchGraphGateway
 * interface using {@link MatchTransform} for the conversion and
 * {@link MatchRepository} for management of
 * {@link Repositories.Entities.MatchEntity}
 */
public class MatchGraphMapper implements MatchGraphGateway {
    private final MatchRepository matchRepository;

    /**
     * Default constructor
     * @param matchRepository Repository used for managing match
     *                        entities
     */
    public MatchGraphMapper(MatchRepository matchRepository) {
        this.matchRepository = matchRepository;
    }

    /**
     * Retrieve a {@link Collection<Match>} from the persistence environment.
     * The function statelessly maps between the {@link Repositories.Entities.MatchEntity}
     * from the repository and {@link Match} domain model using {@link MatchTransform#toModel(MatchEntity)}.
     *
     * @param startTimestamp The starting bound for matches to include
     * @param endTimestamp The ending bound for matches to include
     *
     * @return A of {@link Collection<Match>} domain objects mapped
     * from entities retrieved from the {@link MatchRepository}
     */
    @Override
    public Collection<Match> getMatchForPeriod(long startTimestamp, long endTimestamp) {
        return this.matchRepository
                .getMatchesBetweenTimestamps(new Date(startTimestamp), new Date(endTimestamp))
                .stream()
                .map(MatchTransform::toModel)
                .collect(Collectors.toList());
    }
}
