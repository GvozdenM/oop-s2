package MatchGraphMapper;

import MatchGraphManager.MatchGraphGateway;
import MatchGraphManager.MatchGraphGatewayFactory;
import RepositoryImpl.Factories.MatchRepositoryFactory;
import org.hibernate.Session;

/**
 * Factory for {@link MatchGraphGateway} implemented by {@link MatchGraphMapper}
 */
public class MatchGraphMapperFactory implements MatchGraphGatewayFactory {
    private final MatchRepositoryFactory matchRepositoryFactory;

    /**
     * Default constructor
     * @param matchRepositoryFactory Factory for {@link Repositories.Interfaces.MatchRepository}
     */
    public MatchGraphMapperFactory(MatchRepositoryFactory matchRepositoryFactory) {
        this.matchRepositoryFactory = matchRepositoryFactory;
    }

    @Override
    public MatchGraphGateway create() {
        return new MatchGraphMapper(this.matchRepositoryFactory.create());
    }
}
