package TopUsersInteractor;

import Model.User;
import UserGateways.TopUsersGateway;
import Utils.Callbacks.CallbackAdapter;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * This class manages the process for generating a list of top users based on
 * their popularity.
 */
public class TopUsersManager implements TopUsersRequester<TopUsersRequest, TopUsersResponse> {
    private final TopUsersGateway gateway;

    /**
     * Default constructor
     * @param gateway The data gateway from which to retrieve top users
     */
    public TopUsersManager(TopUsersGateway gateway) {
        this.gateway = gateway;
    }

    /**
     * Retrieve {@link TopUsersRequest#getLength()} top users
     *
     * @param request The request object specifying how to retrieve the users
     * @param callback Callbacks available to call upon completion
     */
    @Override
    public void getTopUsers(TopUsersRequest request, CallbackAdapter<TopUsersResponse> callback) {
        TopUsersResponse response = new TopUsersResponse();

        //retrieve the collection of top users
        //stream it and then map it to TopUsers
        Collection<TopUser> topUsers = this.gateway
                .getTopUsers(request.getLength())
                .stream()
                .map(this::createTopUser)
                .collect(Collectors.toList());

        response.setUsers(topUsers);

        if (0 == topUsers.size()) {
            callback.onFail(new Throwable("Could not retrieve any users"));
        } else {
            callback.onSuccess(response);
        }
    }

    /**
     * Utility method maps {@link User} to {@link TopUser}
     * @param u The user to map
     * @return The TopUser mapped to
     */
    private TopUser createTopUser(User u) {
        return new TopUser(
                u.getId(),
                u.getProfileImageUrl(),
                u.getUsername(),
                u.getAge(),
                u.getNumLikes()
        );
    }
}
