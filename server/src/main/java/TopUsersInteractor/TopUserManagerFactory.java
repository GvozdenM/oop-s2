package TopUsersInteractor;

import Mappers.UserMapper.TopUsersMapperFactory;
import RepositoryImpl.Factories.UserRepositoryFactory;
import UserGateways.TopUsersGatewayFactory;
import org.hibernate.Session;
import org.jetbrains.annotations.NotNull;

/**
 * Factory for {@link TopUsersManager} which implements {@link TopUsersRequester}
 */
public class TopUserManagerFactory implements TopUsersRequesterFactory<TopUsersRequest, TopUsersResponse> {
    private final Session session;

    /**
     * Default constructor
     * @param session Hibernate sessions to use for transactions
     */
    public TopUserManagerFactory(@NotNull Session session) {
        this.session = session;
    }

    @Override
    public TopUsersRequester<TopUsersRequest, TopUsersResponse> create() {
        return new TopUsersManager(new TopUsersMapperFactory(new UserRepositoryFactory(this.session)).create());
    }
}
