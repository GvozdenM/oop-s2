package UserProfileManager;

import Model.User;
import UpdateProfileInteractor.UpdateProfileRequest;
import UpdateProfileInteractor.UpdateProfileRequester;
import UpdateProfileInteractor.UserProfileResponse;
import UserGateways.UserDataGateway;
import UserGateways.UserDataGatewayFactory;
import Utils.Callbacks.CallbackAdapter;

import java.util.LinkedList;
import java.util.Optional;

public class ProfileUpdateManager implements UpdateProfileRequester<UpdateProfileRequest, UserProfileResponse> {
    private final UserDataGatewayFactory userDataGatewayFactory;

    public ProfileUpdateManager(UserDataGatewayFactory userDataGatewayFactory) {
        this.userDataGatewayFactory = userDataGatewayFactory;
    }
    @Override
    public void updateUser(UpdateProfileRequest request, CallbackAdapter<UserProfileResponse> callback) {
        UserDataGateway gateway = this.userDataGatewayFactory.create();


        if(gateway.getUser(request.getId()).isPresent()) {
            User user = new User();

            user.setId(request.getId());
            user.setUsername(request.getUsername());
            user.setAge(request.getAge());
            user.setEmail(request.getEmail());
            user.setPassword(request.getPassword());
            user.setDescription(request.getDescription());
            user.setShortDescription(request.getDescription());
            user.setProfileImageUrl(request.getProfileImgUrl());
            user.getImageUrls().add(0, request.getProfileImgUrl());
            user.setInterest(request.getInterest());
            user.setPremium(request.isPremium());

            Optional<User> updatedUserOptional = gateway.updateUserProfile(user);

            if(updatedUserOptional.isPresent()) {
                User updatedUser = updatedUserOptional.get();
                callback.onSuccess(new UserProfileResponse(
                        updatedUser.getId(),
                        updatedUser.getUsername(),
                        "",
                        updatedUser.getAge(),
                        updatedUser.getEmail(),
                        updatedUser.getPassword(),
                        updatedUser.getInterest(),
                        updatedUser.isPremium(),
                        updatedUser.getProfileImageUrl(),
                        new LinkedList<>(),
                        updatedUser.getShortDescription(),
                        updatedUser.getDescription()
                ));
            }
            else {
                callback.onFail(new Throwable("Could not update profile"));
            }
        }
        else {
            callback.onFail(new Throwable("Can not find user by ID"));
        }
    }

    @Override
    public void getUser(long id, CallbackAdapter<UserProfileResponse> callback) {
        UserDataGateway gateway = this.userDataGatewayFactory.create();
        Optional<User> userOptional = gateway.getUser(id);

        userOptional.ifPresent((user) ->
                callback.onSuccess(new UserProfileResponse(
                        user.getId(),
                        user.getUsername(),
                        "",
                        user.getAge(),
                        user.getEmail(),
                        user.getPassword(),
                        user.getInterest(),
                        user.isPremium(),
                        user.getProfileImageUrl(),
                        new LinkedList<>(),
                        user.getShortDescription(),
                        user.getDescription()
                ))
        );
    }
}
