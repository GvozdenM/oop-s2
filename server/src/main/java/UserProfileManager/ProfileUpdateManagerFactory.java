package UserProfileManager;

import Mappers.UserMapper.UserMapperFactory;
import RepositoryImpl.Factories.UserRepositoryFactory;
import UpdateProfileInteractor.UpdateProfileRequest;
import UpdateProfileInteractor.UpdateProfileRequester;
import UpdateProfileInteractor.UpdateProfileRequesterFactory;
import UpdateProfileInteractor.UserProfileResponse;
import org.hibernate.Session;

public class ProfileUpdateManagerFactory
        implements UpdateProfileRequesterFactory<UpdateProfileRequest, UserProfileResponse> {

    private final Session session;

    public ProfileUpdateManagerFactory(Session session) {
        this.session = session;
    }

    @Override
    public UpdateProfileRequester<UpdateProfileRequest, UserProfileResponse> createUpdateProfileRequester() {
        return new ProfileUpdateManager(new UserMapperFactory(new UserRepositoryFactory(this.session)));
    }
}
