package MatchGraphManager;

import Model.Match;
import MatchGraphInteractor.MatchGraphRequester;
import MatchGraphInteractor.MatchGraphResponse;
import Utils.Callbacks.CallbackAdapter;
import org.h2.expression.Function;
import org.jetbrains.annotations.NotNull;

import java.time.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class MatchGraphManager implements MatchGraphRequester<MatchGraphResponse> {
    private static final int HOURS_IN_THIRTY_ONE_DAYS = 744;
    private static final double NUM_MILLISECONDS_IN_HOUR =  3.6e+6;

    private final MatchGraphGateway matchGraphGateway;

    public MatchGraphManager(MatchGraphGatewayFactory matchGraphGatewayFactory) {
        this.matchGraphGateway = matchGraphGatewayFactory.create();
    }

    @Override
    public void graph(CallbackAdapter<MatchGraphResponse> callback) {
        Date endDate = new Date();
        Date startDate = generateStartDate(endDate);
        Collection<Match> matches = this.matchGraphGateway.getMatchForPeriod(startDate.getTime(), endDate.getTime());



        List<Integer> graphVector = constructGraphVector(startDate, matches);

        callback.onSuccess(
                new MatchGraphResponse(graphVector, true, "E_NO_ERROR")
        );
    }

    private List<Integer> constructGraphVector(@NotNull final Date startDate, @NotNull Collection<Match> matches) {
        ArrayList<Integer> graphVector = Stream
                .generate(() -> 0)
                .limit(HOURS_IN_THIRTY_ONE_DAYS)
                .collect(Collectors.toCollection(ArrayList::new));

        int matchHour = 0;
        for(Match match : matches) {
            matchHour = this.getMatchHourBucket(match, startDate);

            graphVector.set(matchHour, graphVector.get(matchHour) + 1);
        }

        return graphVector;
    }

    /**
     * Utility function gets the array position, in an hours array for the last 31 days, into which the match passed
     * fits
     * @param match The match to find a bucket for
     * @param startDate The starting time from which we are bucketing matches
     * @return The array position to place the match in
     */
    private int getMatchHourBucket(@NotNull Match match, @NotNull Date startDate) {
        return (int) Math.round((match.getMatchTime().getTime() - startDate.getTime()) / NUM_MILLISECONDS_IN_HOUR) - 1;
    }

    @NotNull
    private Date generateStartDate(Date endDate) {
        Calendar c = Calendar.getInstance();
        c.setTime(endDate);
        c.add(Calendar.MONTH, -1);
        return c.getTime();
    }
}
