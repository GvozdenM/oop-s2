package MatchGraphManager;

import Model.Match;

import java.util.Collection;

/**
 * Abstract gateway for retrieving data about matches modeled by {@link Match}, which occurred
 * in a specific time period.
 */
public interface MatchGraphGateway {
    /**
     * Retrieve matches between the given period from the persistence environment
     *
     * @param startTimestamp The starting bound for matches to include
     * @param endTimestamp The ending bound for matches to include
     * @return A of {@link Collection<Match>} domain objects which occured between
     * the specified bounds
     */
    Collection<Match> getMatchForPeriod(long startTimestamp, long endTimestamp);
}
