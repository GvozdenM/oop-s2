package MatchGraphManager;

/**
 * Factory for {@link MatchGraphGateway}
 */
public interface MatchGraphGatewayFactory {
    /**
     * Create {@link MatchGraphGateway}
     * @return Instance of implementer of {@link MatchGraphGateway}
     */
    MatchGraphGateway create();
}
