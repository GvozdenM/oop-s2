package MatchGraphManager;


import MatchGraphMapper.MatchGraphMapperFactory;
import MatchGraphInteractor.MatchGraphRequesterFactory;
import MatchGraphInteractor.MatchGraphRequester;
import MatchGraphInteractor.MatchGraphResponse;
import RepositoryImpl.Factories.MatchRepositoryFactory;
import org.hibernate.Session;

/**
 * Factory for {@link MatchGraphRequester<MatchGraphResponse>} as implemented by
 * {@link MatchGraphManager}
 */
public class MatchGraphManagerFactory implements MatchGraphRequesterFactory<MatchGraphResponse> {
    private final Session session;

    /**
     * Default construction
     * @param session Session to handle database transactions with
     */
    public MatchGraphManagerFactory(Session session) {
        this.session = session;
    }

    @Override
    public MatchGraphRequester<MatchGraphResponse> createMatchGraphRequester() {
        return new MatchGraphManager(
                new MatchGraphMapperFactory(new MatchRepositoryFactory(this.session))
        );
    }
}
