package LoginManager;

import LoginInteractor.LoginRequest;
import LoginInteractor.LoginRequester;
import LoginInteractor.LoginResponse;
import Model.User;
import UserGateways.UserDataGateway;
import UserGateways.UserDataGatewayFactory;
import Utils.Callbacks.CallbackAdapter;

import java.util.Optional;

public class LoginManager implements LoginRequester<LoginRequest, LoginResponse> {
    private final UserDataGatewayFactory userDataGatewayFactory;

    public LoginManager(UserDataGatewayFactory userDataGatewayFactory) {
        this.userDataGatewayFactory = userDataGatewayFactory;
    }

    private boolean authenticate(User user, String password) {
        return user.getPassword().equals(password);
    }

    @Override
    public void login(LoginRequest request, CallbackAdapter<LoginResponse> callback) {
        UserDataGateway gateway = this.userDataGatewayFactory.create();
        Optional<User> userOptional = gateway.getUserByUsernameOrEmail(request.getUsername());

        if(userOptional.isPresent()) {
            User user = userOptional.get();
            if(authenticate(user, request.getPassword())) {
                callback.onSuccess(new LoginResponse(true, user.getId()));
            }
            else {
                callback.onSuccess(new LoginResponse(false));
            }
        }
        else {
            callback.onSuccess(new LoginResponse(false));
        }
    }
}
