package LoginManager;

import LoginInteractor.LoginRequest;
import LoginInteractor.LoginRequester;
import LoginInteractor.LoginRequesterFactory;
import LoginInteractor.LoginResponse;
import Mappers.UserMapper.UserMapperFactory;
import RepositoryImpl.Factories.UserRepositoryFactory;
import org.hibernate.Session;
import org.jetbrains.annotations.NotNull;

public class LoginManagerFactory implements LoginRequesterFactory<LoginRequest, LoginResponse> {
    private final Session session;

    public LoginManagerFactory(@NotNull Session session) {
        this.session = session;
    }

    @Override
    public LoginRequester<LoginRequest, LoginResponse> createLoginRequester() {
        return new LoginManager(new UserMapperFactory(new UserRepositoryFactory(this.session)));
    }
}
