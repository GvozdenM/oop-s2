import Repositories.Entities.GenderEntity;
import Repositories.Entities.UserEntity;
import RepositoryImpl.HibernateCrudRepository;
import RepositoryImpl.UserHibernateRepository;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class TestDbMain {
    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("MinderPersistence");

    public static void main(String[] args) {
        //ChatMapper chatMapper = new ChatMapper(new UserInteractionHibernateRepositoryFactory());
//        UserMapper userMapper = new UserMapper(new UserInteractionHibernateRepositoryFactory());
//        FeedMapper feedMapper = new FeedMapper(new UserInteractionHibernateRepositoryFactory(), new UserFeedHibernateRepositoryFactory());
//        MatchUsersMapper matchUsersMapper = new MatchUsersMapper(new UserInteractionHibernateRepositoryFactory());

        System.out.println(emf);
        Session session = (Session) emf.createEntityManager().getDelegate();
        System.out.println(session);

        Transaction tx = session.beginTransaction();;

       try {

           HibernateCrudRepository<UserEntity> repository = new UserHibernateRepository(session);

           UserEntity userEntity = new UserEntity();
           userEntity.setLogin("Fucker");
           userEntity.setEmail("fucker@gmail.com");
           userEntity.setAge(15);
           userEntity.setGender(session.get(GenderEntity.class, 10001L));
           userEntity.setPassword("yourmumsass123");
           userEntity.setPremium(false);

//           System.out.println("New user entity: ");
//           System.out.println(userEntity);
//
//           repository.create(userEntity);
//
//           System.out.println("Created user entity: ");
//           System.out.println(userEntity);
//
//           userEntity.setAge(22);

           UserEntity fucker = session.get(UserEntity.class, 4L);
//           fucker.setAge(99);


           System.out.println("Fetch user after updating obj: ");
           System.out.println(fucker);

//           repository.delete(userEntity);
//
//           System.out.println("User entity obj after delete: ");
//           System.out.println(userEntity);
//
//           System.out.println("Fetch after delete: ");
//           System.out.println(session.get(UserEntity.class, userEntity.getId()));

           tx.commit();

       }
       catch (Exception e) {
           e.printStackTrace();
           tx.rollback();
       }
       finally {

           session.close();

           emf.close();
       }
//        Collection<User> feed = feedMapper.getUserFeed(10001);
//        User ah = new User ("testboi", "testpassword", 69, "testy@boi.eu", true);
//        ah.setInterest("C");
//        userMapper.registerUser(ah);
//        Optional<User> user = userMapper.getUserByUsername("testboi");
//        if(user.isPresent()) {
//            feed = feedMapper.getUserFeed(user.get().getId());
//        }
//        /* Match Testing
//        boolean match = feedMapper.like(10001, 10002);
//        match = feedMapper.like(10002, 10001);
//        List<User> matches = matchUsersMapper.getMatchesByUserID(10001);
//        matchUsersMapper.unmatchUserByUserID(10001, 10002);
//        matches = matchUsersMapper.getMatchesByUserID(10001);
//         */
//
//        //boolean worked = feedMapper.isMatched(10001,10002);
//        HibernateRepository.closeEntityManager();
    }
}
