package UserGateways;

/**
 * Abstract factory for implementers {@link UserDataGateway}
 */
public abstract class UserDataGatewayFactory {
    public abstract UserDataGateway create();
}
