package UserGateways;

public abstract class RegistrationGatewayFactory {
    public abstract RegistrationGateway create();
}
