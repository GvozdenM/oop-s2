package UserGateways;

import Model.User;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

/**
 * Abstract gateway for registering users
 */
public interface RegistrationGateway {
    /**
     * Register a new user in the persistence store
     * @param newUser The user to register
     * @return The registered user's state as represented in the persistence store
     */
    Optional<User> registerUser(@NotNull User newUser);


    /**
     * Retrieve from the persistence mechanism a {@link User} identified
     * by the given username.
     *
     * @param username The username of the user to retrieve
     * @return An {@link Optional<User>} containing the user with the username specified
     *         or an {@link Optional#empty()
     */
    Optional<User> getUserByUsername(@NotNull String username);

    /**
     * Retrieve from the persistence mechanism a {@link User} identified
     * by the given username.
     *
     * @param userNameOrEmail The username or email of the user to retrieve
     * @return An {@link Optional<User>} containing the user with the username specified OR the
     *          email specified or an {@link Optional#empty() if no user could be found
     */
    Optional<User> getUserByUsernameOrEmail(@NotNull String userNameOrEmail);

}
