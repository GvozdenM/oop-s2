package UserGateways;

import Model.User;

import java.util.Collection;

/**
 * Abstract gateway for retrieving the most popular (top) users
 * from the persistence store
 */
public interface TopUsersGateway {
    /**
     * Retrieve numUsers users from the persistence store sorted by
     * whatever the definition of popularity is
     *
     * @param numUsers How many of the top users to retrieve
     * @return The top users sorted in descending order of popularity
     */
    Collection<User> getTopUsers(int numUsers);
}
