package UserGateways;

public abstract class TopUsersGatewayFactory {
    public abstract TopUsersGateway create();
}
