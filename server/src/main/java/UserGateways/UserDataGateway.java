package UserGateways;



import Model.User;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

/**
 * Abstract gateway for basic manipulation of {@link User} objects
 * persisted in some unknown format.
 */
public interface UserDataGateway {
    /**
     * Retrieve from the persistence mechanism a {@link User} identified
     * by the given id.
     *
     * @param id The id of the user to retrieve
     * @return An {@link Optional<User>} containing the user with the id specified
     * or an {@link Optional#empty()}
     */
    Optional<User> getUser(@NotNull Long id);

    /**
     * Retrieve from the persistence mechanism a {@link User} identified
     * by the given username.
     *
     * @param username The username of the user to retrieve
     * @return An {@link Optional<User>} containing the user with the username specified
     *         or an {@link Optional#empty()
     */
    Optional<User> getUserByUsername(@NotNull String username);

    /**
     * Retrieve from the persistence mechanism a {@link User} identified
     * by the given username.
     *
     * @param userNameOrEmail The username or email of the user to retrieve
     * @return An {@link Optional<User>} containing the user with the username specified OR the
     *          email specified or an {@link Optional#empty() if no user could be found
     */
    Optional<User> getUserByUsernameOrEmail(@NotNull String userNameOrEmail);

    /**
     * Persist the new state of the user's profile. The user is identified by setting the
     * user's id with {@link User#setId(Long)}. If no user id is set this is method is treated
     * as creating a new user in the persistence store. The user's id will be filled in the return
     * value.
     *
     * @param u The new state of the user to persist
     * @return The new state of the user as represented in the persistence store after being updated or
     * created.
     */
    Optional<User> updateUserProfile(@NotNull User u);
}
