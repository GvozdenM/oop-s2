package Repositories.Interfaces;

import Repositories.Entities.UserEntity;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

public interface UserRepository extends CrudRepository<UserEntity> {
    /**
     * Retrieve the a {@link UserEntity} identified by the given
     * login.
     *
     * @param login The login (username) of the user entity to fetch
     * @return An {@link Optional<UserEntity>} containing the user or {@link Optional#empty()} if no user is
     *         found with the given username
     */
    Optional<UserEntity> getUserByLogin(@NotNull String login);

    /**
     * Retrieve the a {@link UserEntity} identified by the given
     * email address.
     *
     * @param email The email address of the user entity to retrieve
     * @return An {@link Optional<UserEntity>} containing the user or {@link Optional#empty()} if no user is
     *         found with the given email address
     */
    Optional<UserEntity> getUserByEmail(@NotNull String email);

    /**
     * Retrieve the a {@link UserEntity} identified by the given
     * email address or by the given login. The simplest implementation would be:
     *
     * {@code
     *      Optional< UserEntity > user = this.getUserByLogin(emailOrLogin);
     *
     *       if(user.isPresent()) {
     *           return user;
     *       }
     *       else {
     *           return this.getUserByEmail(emailOrLogin);
     *       }
     * }
     *
     * @param emailOrLogin The email address or login (username) of the user entity to retrieve
     * @return An {@link Optional<UserEntity>} containing the user or {@link Optional#empty()} if no user is
     *         found with the given email address or username
     */
    Optional<UserEntity> getUserByEmailOrLogin(@NotNull String emailOrLogin);

    /**
     * Retrieve a set containing numUsers user sorted by the their popularity
     *
     * @return A set containing the most popular users
     */
    Collection<UserEntity> getTopUsers(int numUsers);

    /**
     * Retrieve those users who have liked the user identified by the id passed
     *
     * @param userId The id of the user who's likers we should retrieve
     * @return The users who liked the user passed
     */
    Collection<UserEntity> getLikedBy(@NotNull Long userId);

    /**
     * Retrieve those users whom have been liked by the user identified by the id passed.
     *
     * @param userId The id of the user who's likees we should retrieve
     * @return The users that were liked by the user passed
     */
    Collection<UserEntity> getLiked(long userId);

    /**
     * Get the feed, that is users to display, for the current user.
     *
     * @param id The id of the user who's feed we wish to retrieve
     * @return A {@link Collection<UserEntity>} of users that should be displayed to the user
     * identified by
     */
    Collection<UserEntity> getUserFeed(Long id);
}
