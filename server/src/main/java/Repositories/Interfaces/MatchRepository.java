package Repositories.Interfaces;

import Repositories.Entities.MatchEntity;
import Repositories.Entities.UserEntity;
import Repositories.Entities.UserInteractionEntity;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

/**
 * An extension to a basic Crud repository for managing the persistence of
 * {@link MatchEntity} instances
 */
public interface MatchRepository extends CrudRepository<MatchEntity> {
     /**
      * Get a {@link Collection<MatchEntity>} of matches for this user
      *
      * @param userId The identifier of the user for which to retrieve matches
      * @return {@link Collection<MatchEntity>} of matches
      */
     Collection<MatchEntity> getUserMatches(@NotNull Long userId);

     /**
      * Create a match between two users based on the {@link UserInteractionEntity} passed
      *
      * @param like The {@link UserInteractionEntity} to base the match on. From here the two users
      *             can be found
      * @return A {@link Optional<MatchEntity>} containing the match created or
      * an empty {@link Optional} if match was not created for whatever reason.
      */
     Optional<MatchEntity> match(@NotNull UserInteractionEntity like);

     /**
      * Remove the match entity representing the a match between the two users passed
      *
      * @param userA One of the two users matched
      * @param userB The other of the two users matched
      */
     void unmatch(@NotNull Long userA, @NotNull Long userB);


     /**
      * Retrieve a match between two users
      * @param userA A user
      * @param userB Another user
      * @return An {@link Optional<MatchEntity>} containing the match or {@link Optional#empty()} if no match is
      * found
      */
     Optional<MatchEntity> getMatch(@NotNull UserEntity userA, @NotNull UserEntity userB);

     /**
      * Retrieve a match between two users
      * @param userA A user id
      * @param userB Another user id
      * @return An {@link Optional<MatchEntity>} containing the match or {@link Optional#empty()} if no match is
      * found
      */
     Optional<MatchEntity> getMatch(@NotNull Long userA, @NotNull Long userB);

     /**
      * Retrieve all matches which happened between timestamp and end.
      *
      * @param timestamp The starting bound for matches to retrieve
      * @param end The ending bound for matches to retrive
      * @return A {@link Collection<MatchEntity>} of matches which occurred
      * between the two timestamps. If no matches happened within the specified
      * time an empty collection is returned.
      */
     Collection<MatchEntity> getMatchesBetweenTimestamps(Date timestamp, Date end);
}
