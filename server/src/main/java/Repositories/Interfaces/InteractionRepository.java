package Repositories.Interfaces;

import Repositories.Entities.UserInteractionEntity;

public interface InteractionRepository extends CrudRepository<UserInteractionEntity> {}
