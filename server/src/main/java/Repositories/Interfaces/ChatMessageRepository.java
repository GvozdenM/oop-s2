package Repositories.Interfaces;

import Repositories.Entities.MessageEntity;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

/**
 * An extension to a basic Crud repository for managing persisted {@link MessageEntity} instances
 */
public interface ChatMessageRepository extends CrudRepository<MessageEntity> {
    /**
     * Retrieve all the messages between two users, identified by
     * the identifiers passed, a chat log.
     *
     * @param userA Identifier of some user A
     * @param userB Identifier of some user B
     *
     * @return A {@link Collection<MessageEntity>} containing the chat messages
     * between the two users or an empty collection if the two have no messages
     * between them.
     */
    Collection<MessageEntity> getChatLog(@NotNull Long userA, @NotNull Long userB);

    /**
     * Clear the chat log between the two users. That is remove from the persistence mechanism
     * all chat messages associated with both users.
     *
     * @param userA A user
     * @param userB Another user
     */
    void deleteChat(@NotNull Long userA, @NotNull Long userB);
}
