package Repositories.Entities;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@org.hibernate.annotations.NamedQueries({
        @org.hibernate.annotations.NamedQuery(
                name = MessageEntity.GET_CHAT_LOG,
                query = "FROM MessageEntity AS m \n" +
                        "WHERE m.match = :match"
        )
})

@Entity
@Table(name = "message")
public class MessageEntity {
    public static final String GET_CHAT_LOG = "getChatLog";

    @Id
    @GeneratedValue
    @Column(name="id", unique = true)
    private Long messageId;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_match", nullable = false)
    private MatchEntity match;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_receiver", nullable = false)
    private UserEntity receiver;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_sender", nullable = false)
    private UserEntity sender;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "timestamp", nullable = false)
    private Date timestamp;

    @Basic
    @Column(name ="message", nullable = false)
    private String message;
    /**
     * Default constructor to allow hibernate to create entities with {@link java.lang.reflect}
     */
    public MessageEntity() {
    }

    /**
     * Constructor for inserts and updates (set id via setter method in case of update)
     * @param match The match associated with this chat message
     * @param receiver Id of user who is receiving message
     * @param message The actual message string
     */
    public MessageEntity(MatchEntity match, UserEntity receiver, UserEntity sender, String message) {
        this.messageId = null;
        this.match = match;
        this.receiver = receiver;
        this.sender = sender;
        this.message = message;
        this.timestamp = new Date();
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public MatchEntity getMatch() {
        return match;
    }

    public void setMatch(MatchEntity match) {
        this.match = match;
    }

    public UserEntity getReceiver() {
        return receiver;
    }

    public void setReceiver(UserEntity receiver) {
        this.receiver = receiver;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public UserEntity getSender() {
        return sender;
    }

    @Override
    public String toString() {
        return "MessageEntity{" +
                "messageId=" + messageId +
                ", match=" + match +
                ", receiver=" + receiver +
                ", sender=" + sender +
                ", timestamp=" + timestamp +
                ", message='" + message + '\'' +
                '}';
    }
}
