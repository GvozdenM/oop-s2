package Repositories.Entities;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@org.hibernate.annotations.NamedQueries({
        @org.hibernate.annotations.NamedQuery(
                name = MatchEntity.GET_USER_MATCHES,
                query = "FROM MatchEntity AS me " +
                        "WHERE me.userA.id = :id OR me.userB.id = :id"
        ),
        @org.hibernate.annotations.NamedQuery(
                name = MatchEntity.CHECK_LIKED,
                query = "FROM UserInteractionEntity AS uie " +
                        "WHERE uie.liker = :liker AND uie.likee = :likee"
        ),
        @org.hibernate.annotations.NamedQuery(
                name = MatchEntity.GET_MATCHED_BETWEEN,
                query = "FROM MatchEntity AS me " +
                        "WHERE (me.userA = :userA AND me.userB = :userB) " +
                        "OR (me.userB = :userA AND me.userA = :userB)"
        ),
        @org.hibernate.annotations.NamedQuery(
                name = MatchEntity.UNMATCH,
                query = "DELETE FROM MatchEntity AS me " +
                        "WHERE (me.userA = :userA AND me.userB = :userB) " +
                        "OR (me.userB = :userA AND me.userA = :userB)"
        ),
        @org.hibernate.annotations.NamedQuery(
                name = MatchEntity.GET_MATCHES_IN_TIMESPAN,
                query = "FROM MatchEntity AS me " +
                        "WHERE me.timestamp > :start AND me.timestamp < :end " +
                        "ORDER BY me.timestamp ASC"
        )
})

@Entity
@Table(name = "user_matches")
public class MatchEntity {

    public static final String GET_USER_MATCHES = "getMatches";
    public static final String CHECK_LIKED = "checkIfLikedBy";
    public static final String GET_MATCHED_BETWEEN = "getMatch";
    public static final String UNMATCH = "unmatch";
    public static final String GET_MATCHES_IN_TIMESPAN = "getMatchesTimestamp";

    @Id
    @GeneratedValue
    @Column(name="id_match", unique = true, nullable = false)
    private Long matchId;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_user_a", unique = false, nullable = false)
    private UserEntity userA;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_user_b", unique = false, nullable = false)
    private UserEntity userB;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_interaction", unique = false, nullable = false)
    private UserInteractionEntity interaction;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "timestamp", nullable = false)
    private Date timestamp;

    /**
     * Default constructor to allow hibernate to create entities with {@link java.lang.reflect}
     */
    public MatchEntity() {
    }

    /**
     * Constructor for users who are associated with this match
     * @param userA A user
     * @param userB Another user
     */
    public MatchEntity(UserEntity userA, UserEntity userB, UserInteractionEntity interaction) {
        this.userA = userA;
        this.userB = userB;
        this.interaction = interaction;
        this.timestamp = new Date(System.currentTimeMillis());
    }

    public Long getMatchId() {
        return matchId;
    }

    public void setMatchId(Long matchId) {
        this.matchId = matchId;
    }

    public UserEntity getUserA() {
        return userA;
    }

    public void setUserA(UserEntity userA) {
        this.userA = userA;
    }

    public UserEntity getUserB() {
        return userB;
    }

    public void setUserB(UserEntity userB) {
        this.userB = userB;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public UserInteractionEntity getInteraction() {
        return interaction;
    }

    public void setInteraction(UserInteractionEntity interaction) {
        this.interaction = interaction;
    }
}