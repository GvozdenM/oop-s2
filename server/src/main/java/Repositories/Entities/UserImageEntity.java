package Repositories.Entities;

import javax.persistence.*;

@Entity
@Table(name = "user_images")
public class UserImageEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_image", updatable = false, nullable = false, unique = true)
    private Long id;

    @Column(name = "url", nullable = false)
    private String url;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_user")
    private UserEntity user;

    /**
     * Default constructor to allow hibernate to create entities with {@link java.lang.reflect}
     */
    protected UserImageEntity() {
    }

    /**
     * Constructor for mapper objects to allow inserts
     * @param url Url of this user image
     * @param user User entity associated with this entity
     */
    public UserImageEntity(String url, UserEntity user) {
        this.id = null;
        this.url = url;
        this.user = user;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }
}
