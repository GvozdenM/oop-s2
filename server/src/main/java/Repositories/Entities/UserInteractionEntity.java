package Repositories.Entities;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@org.hibernate.annotations.NamedQueries({
        @org.hibernate.annotations.NamedQuery(
                name = UserInteractionEntity.GET_LIKED_BY,
                query = "FROM UserEntity as ue " +
                        "WHERE ue.id = " +
                        "(SELECT uie.likee.id FROM UserInteractionEntity as uie WHERE uie.liker.id = :userId)"
        ),
        @org.hibernate.annotations.NamedQuery(
                name = UserInteractionEntity.GET_LIKED,
                query = "FROM UserEntity as ue " +
                        "WHERE ue.id = " +
                        "(SELECT uie.liker.id FROM UserInteractionEntity as uie WHERE uie.likee.id = :userId)"
        ),

        @org.hibernate.annotations.NamedQuery(
                name = UserInteractionEntity.GET_USER_FEED,
                query = "FROM UserEntity AS ue " +
                        "WHERE ue NOT IN (SELECT i.likee FROM UserInteractionEntity as i WHERE i.liker = :user) " +
                        "AND (ue.gender = :userGender " +
                        "OR ue.gender = :bothGender " +
                        "OR :userGender = :bothGender) " +
                        "AND ue != :user"
        ),
        @org.hibernate.annotations.NamedQuery(
                name = UserInteractionEntity.GET_USER_PREMIUM_FEED,
                query = "FROM UserEntity AS ue " +
                        "WHERE ue NOT IN (SELECT i.likee FROM UserInteractionEntity as i WHERE i.liker = :user) " +
                        "AND ue IN (SELECT i.liker FROM UserInteractionEntity as i WHERE i.likee = :user) " +
                        "AND (ue.gender = :userGender " +
                        "OR ue.gender = :bothGender " +
                        "OR :userGender = :bothGender) " +
                        "AND ue != :user"
        ),
        @org.hibernate.annotations.NamedQuery(
                name = UserInteractionEntity.CHECK_LIKED,
                query = "FROM UserInteractionEntity AS ue " +
                        "WHERE liker = :liker " +
                        "AND likee = :likee " +
                        "AND like_not_dislike <> false"
        )
})

@Entity
@Table(name = "interactions")
public class UserInteractionEntity {

    public static final String GET_LIKED_BY = "getLikedBy";
    public static final String GET_LIKED = "getLiked";
    public static final String GET_USER_FEED = "getUserFeed";
    public static final String GET_USER_PREMIUM_FEED = "getUserPremiumFeed";
    public static final String GET_BOTH_USER_FEED = "getBothUserFeed";
    public static final String GET_BOTH_USER_PREMIUM_FEED = "getBothUserPremiumFeed";
    public static final String CHECK_LIKED = "isLikedBy";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_interaction", updatable = false, nullable = false, unique = true)
    private Long id;

    @Column(name = "like_not_dislike", updatable = false, nullable = false)
    private boolean like_not_dislike;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_liker", nullable = false)
    private UserEntity liker;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_likee", nullable = false)
    private UserEntity likee;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "timestamp", nullable = false)
    private Date timestamp;

    /**
     * Default constructor to allow hibernate to create entities with {@link java.lang.reflect}
     */
    protected UserInteractionEntity() {
    }

    /**
     * Constructor for mapper objects to allow inserts
     * @param like_not_dislike Whether interaction is like (true) or dislike (false)
     * @param liker The user who initiated the interaction
     * @param likee The user who has been interacted with
     */
    public UserInteractionEntity(boolean like_not_dislike, UserEntity liker, UserEntity likee) {
        this.id = null;
        this.like_not_dislike = like_not_dislike;
        this.liker = liker;
        this.likee = likee;
        this.timestamp = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isLike_not_dislike() {
        return like_not_dislike;
    }

    public void setLike_not_dislike(boolean like_not_dislike) {
        this.like_not_dislike = like_not_dislike;
    }

    public UserEntity getLiker() {
        return liker;
    }

    public void setLiker(UserEntity liker) {
        this.liker = liker;
    }

    public UserEntity getLikee() {
        return likee;
    }

    public void setLikee(UserEntity likee) {
        this.likee = likee;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return "UserInteractionEntity{" +
                "id=" + id +
                ", like_not_dislike=" + like_not_dislike +
                ", liker=" + liker +
                ", likee=" + likee +
                ", timestamp=" + timestamp +
                '}';
    }
}
