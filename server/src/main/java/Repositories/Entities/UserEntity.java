package Repositories.Entities;

import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@org.hibernate.annotations.NamedQueries({
    @org.hibernate.annotations.NamedQuery(
            name = UserEntity.GET_USER_BY_LOGIN,
            query = "FROM UserEntity AS u \n" +
                    "WHERE u.login = :login"
    ),
    @org.hibernate.annotations.NamedQuery(
            name = UserEntity.GET_USER_BY_EMAIL,
            query = "FROM UserEntity AS u \n" +
                    "WHERE u.email = :email"
    ),
    @org.hibernate.annotations.NamedQuery(
            name = UserEntity.GET_TOP_USERS,
            query = "FROM UserEntity AS u " +
                    "ORDER BY u.numLikes DESC"
    ),
})


@Entity
@Table(name = "users")
public class UserEntity implements Serializable {
    public static final String GET_USER_BY_EMAIL = "getUserByEmail";
    public static final String GET_USER_BY_LOGIN = "getUserByLogin";
    public static final String GET_TOP_USERS = "getTopUsers";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_user", updatable = false, nullable = false, unique = true)
    private Long id;

    @Column(name = "login", nullable = false, unique = true)
    private String login;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "age", nullable = false)
    private int age;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "is_premium", nullable = false, columnDefinition = "BIT(1) DEFAULT 0")
    private Boolean isPremium;

    @Column(name = "bio")
    private String bio;

    @Column(name = "description")
    private String description;

    @Column(name = "work")
    private String work;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_gender", nullable = false)
    private GenderEntity gender;

    @Column(name = "profileImageUrl")
    private String profileImageUrl;

    @Formula(value = "(SELECT COUNT(*) FROM interactions AS i WHERE i.id_likee = id_user AND i.like_not_dislike = 1)")
    private int numLikes;

    @Formula(value = "(SELECT COUNT(*) FROM user_matches AS m WHERE m.id_user_a = id_user OR m.id_user_b = id_user)")
    private int numMatches;

    /**
     * Default constructor to allow hibernate to create entities with {@link java.lang.reflect}
     */
    public UserEntity() {
    }

    /**
     * Constructor for mapper objects to allow inserts
     * @param login user login name
     * @param email user email
     * @param age user age
     * @param password user's password
     * @param isPremium user premium membership
     * @param bio user's short description (bio)
     * @param description user's full description
     * @param work user's work type / name
     * @param gender Gender object for this user
     */
    public UserEntity(String login,
                      String email,
                      int age,
                      String password,
                      Boolean isPremium,
                      String bio,
                      String description,
                      String work,
                      GenderEntity gender) {
        this.login = login;
        this.email = email;
        this.age = age;
        this.password = password;
        this.bio = bio;
        this.description = description;
        this.work = work;
        this.gender = gender;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBio() {
        return bio;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWork() {
        return work;
    }

    public GenderEntity getGender() {
        return gender;
    }

    public void setGender(GenderEntity gender) {
        this.gender = gender;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public int getNumLikes() {
        return numLikes;
    }

    public void setNumLikes(int numLikes) {
        this.numLikes = numLikes;
    }

    public int getNumMatches() {
        return numMatches;
    }

    public void setNumMatches(int numMatches) {
        this.numMatches = numMatches;
    }

    public Boolean getPremium() {
        return isPremium;
    }

    public void setPremium(Boolean premium) {
        isPremium = premium;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public void setWork(String work) {
        this.work = work;
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                ", password='" + password + '\'' +
                ", isPremium= '" + isPremium + '\'' +
                ", bio='" + bio + '\'' +
                ", description='" + description + '\'' +
                ", work='" + work + '\'' +
                ", gender=" + gender +
                ", numLikes=" + numLikes +
                ", numMatches=" + numMatches +
                '}';
    }
}
