package Repositories.Entities;

import javax.persistence.*;

@Entity
@Table(name = "genders")
public class GenderEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_gender", updatable = false, nullable = false, unique = true)
    private Long id;

    @Column(name = "title", updatable = false, nullable = false, unique = true)
    private String title;

    /**
     * Default constructor to allow hibernate to create entities with {@link java.lang.reflect}
     */
    public GenderEntity() {
    }

    public GenderEntity(Long id) {
        this.id = id;
    }

    /**
     * Constructor for create / update of gender entities
     * @param title The gender title
     */
    public GenderEntity(String title) {
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
