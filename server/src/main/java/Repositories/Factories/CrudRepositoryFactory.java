package Repositories.Factories;

import Repositories.Interfaces.CrudRepository;
import org.hibernate.Session;

/**
 * Abstract factory for implementations of {@link CrudRepository} for some type
 * @param <R> The type of repository being created
 */
public abstract class CrudRepositoryFactory<R extends CrudRepository<?>> {
    protected final Session session;

    /**
     * Default constructor
     *
     * @param session Thread context bound session to be used to handle
     *                all transactions by repositories created by this factory
     */
    protected CrudRepositoryFactory(Session session) {
        this.session = session;
    }

    /**
     * Create the repository R
     * @return An instance of R which extends CrudRepository for some type
     */
    public abstract R create();
}
