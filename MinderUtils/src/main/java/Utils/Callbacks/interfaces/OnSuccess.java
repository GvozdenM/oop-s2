package Utils.Callbacks.interfaces;

public interface OnSuccess<T> {
    void onSuccess(T d);
}
