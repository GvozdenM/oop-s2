package Utils.Callbacks.impl;

import Utils.Callbacks.interfaces.BaseCallback;
import Utils.Callbacks.interfaces.OnException;
import Utils.Callbacks.interfaces.OnSuccess;

public class ExceptionCallbackAdapter<S> extends BaseCallbackAdapter<S> implements BaseCallback<S> {
    private final OnException onExceptionCallback;

    public ExceptionCallbackAdapter(OnSuccess<S> onSuccessCallback,
                                    OnException onFailCallback) {
        super(onSuccessCallback);
        this.onExceptionCallback = null != onFailCallback ? onFailCallback : (t -> {});
    }

    @Override
    public void onException(Exception exception) {
        this.onExceptionCallback.onException(exception);
    }
}
