package Utils.Swing.Image;

import Utils.listeners.interfaces.SimpleOnClickListener;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

public class ImagePanel extends JPanel {
    private final int CACHE_MAP_SIZE = 15;


    private static final Map<URL, BufferedImage> imgCacheMap = new ConcurrentHashMap<>();

    private SimpleOnClickListener listener;

    private JLabel imgContainer;
    private Optional<Dimension> imgSize = Optional.empty();

    private synchronized void addLoadedImgToView(BufferedImage buffer) {
        ImageIcon imageIcon = new ImageIcon(buffer);
        Image scaledImg = imageIcon.getImage();

        if (this.imgSize.isPresent()) {
            scaledImg = imageIcon.getImage().getScaledInstance(this.imgSize.get().width, this.imgSize.get().height, Image.SCALE_DEFAULT);
            this.setPreferredSize(this.imgSize.get());
        }
        if (this.imgContainer != null) {
            this.remove(0);
        }
        this.imgContainer = new JLabel(new ImageIcon(scaledImg));
        this.imgSize.ifPresent(d -> this.imgContainer.setPreferredSize(d));
        if (this.listener != null) {
            this.imgContainer.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    super.mouseClicked(e);
                    listener.onClick();
                }
            });
        }
        this.add(this.imgContainer);
        this.revalidate();
    }

    public ImagePanel() {
    }

    public ImagePanel(String imgPath) {
        load(imgPath);
    }

    public ImagePanel(URL imgUrl) {
        load(imgUrl);
    }

    public synchronized void load(String imgPath) {
        try {
            addLoadedImgToView(ImageIO.read(new File(imgPath)));
        } catch (IOException e) {
            //File not found
            e.printStackTrace();
        }
    }

    public synchronized void load(URL imgUrl) {
        if (!imgCacheMap.containsKey(imgUrl)) {
            Thread loader = new Thread(
                    () -> {
                        try {
                            considerCacheSize();
                            imgCacheMap.put(imgUrl, ImageIO.read(imgUrl));
                        } catch (IOException e) {
                            //File not found
                            e.printStackTrace();
                        }
                        addLoadedImgToView(imgCacheMap.get(imgUrl));
                    }
            );
            loader.start();
        }
        else {
            addLoadedImgToView(imgCacheMap.get(imgUrl));
        }
    }

    private synchronized void considerCacheSize() {
        if(imgCacheMap.keySet().size() >= CACHE_MAP_SIZE) {
            imgCacheMap.clear();
        }
    }

    public synchronized void setImgSize(int width, int height) {
        this.imgSize = Optional.of(new Dimension(width, height));
    }

    public synchronized void setOnClickListener(SimpleOnClickListener listener) {
        if (this.imgContainer != null) {
            this.imgContainer.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    super.mouseClicked(e);
                    listener.onClick();
                }
            });
        }
        this.listener = listener;
    }
}
