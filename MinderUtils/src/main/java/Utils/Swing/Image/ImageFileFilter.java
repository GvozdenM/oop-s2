package Utils.Swing.Image;

import javax.swing.filechooser.FileFilter;
import java.io.File;

public class ImageFileFilter extends FileFilter {
    @Override
    public boolean accept(File file) {
        if (file.isDirectory()) {
            return true;
        }

        String extension = FileExtension.getExtension(file);
        if (extension != null) {
            if (extension.equals(FileExtension.tiff) ||
                    extension.equals(FileExtension.tif) ||
                    extension.equals(FileExtension.gif) ||
                    extension.equals(FileExtension.jpeg) ||
                    extension.equals(FileExtension.jpg) ||
                    extension.equals(FileExtension.png)) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    @Override
    public String getDescription() {
        return null;
    }
}
