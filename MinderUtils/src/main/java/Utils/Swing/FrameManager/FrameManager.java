package Utils.Swing.FrameManager;

import Utils.listeners.interfaces.WindowClosingListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;

public abstract class FrameManager implements FrameManagerInterface {
    private static final String TAG = "FrameManager";


    public static final Component CENTER = null;

    private final JFrame frame;

    private WindowClosingListener closingListener;

    public FrameManager() {
        this.frame = new JFrame();
    }

    public static FrameManager newInstance() {
        return new FrameManager() {};
    }

    @Override
    public FrameManagerInterface configureFramePreferences(String title) {
        this.frame.setTitle(title);
        this.frame.revalidate();
        return this;
    }

    @Override
    public FrameManagerInterface setSpawnRelativeTo(Component component) {
        this.frame.setLocationRelativeTo(component);
        return this;
    }

    @Override
    public FrameManagerInterface configureFramePreferences(int xSize, int ySize) {
        this.frame.setSize(xSize, ySize);
        this.frame.revalidate();
        return this;
    }

    @Override
    public FrameManagerInterface setExitOnClose(boolean doClose) {
        if(doClose) {
            this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }
        else {
            this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        }
        this.frame.revalidate();
        return this;
    }

    @Override
    public FrameManagerInterface configureFramePreferences(String title, int xSize, int ySize) {
        configureFramePreferences(title);
        configureFramePreferences(xSize, ySize);
        return this;
    }

    @Override
    public FrameManagerInterface configureFramePreferences(String title, JPanel startPanel, int xSize, int ySize) {
        configureFramePreferences(title, xSize, ySize);
        setContentPanel(startPanel);
        return this;
    }

    @Override
    public FrameManagerInterface configureFramePreferences(String title, JPanel startPanel, int xSize, int ySize, Component spawnRelativeTo) {
        configureFramePreferences(title, startPanel, xSize, ySize);
        setSpawnRelativeTo(spawnRelativeTo);
        return this;
    }

    @Override
    public void setContentPanel(JPanel nextPanel) {
        EventQueue.invokeLater(
                () -> {
                    this.frame.setContentPane(nextPanel);
                    this.frame.revalidate();
                    this.frame.setVisible(true);
                }
        );
    }

    @Override
    public void closeFrame() {
        EventQueue.invokeLater(
                () -> {
                    this.frame.dispatchEvent(new WindowEvent(this.frame, WindowEvent.WINDOW_CLOSING));
                }
        );
    }

    @Override
    public FrameManagerInterface setFavicon(String iconPath) {
        this.frame.setIconImage(
                new ImageIcon(iconPath).getImage()
        );
        return this;
    }

    @Override
    public FrameManagerInterface setResizable(boolean resizable) {
        this.frame.setResizable(resizable);
        return this;
    }

    @Override
    public FrameManagerInterface setCloseProcedure(boolean exitSystem) {
        setCloseProcedure(()-> {}, exitSystem);

        return this;
    }

    @Override
    public FrameManagerInterface setCloseProcedure(Runnable closeProcedure, boolean exitSystem) {
        this.frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        if (this.closingListener != null) {
            this.frame.removeWindowListener(this.closingListener);
        }

        this.closingListener = new WindowClosingListener(this.frame, closeProcedure, exitSystem);


        this.frame.addWindowListener(this.closingListener);

        return this;
    }

    public static void closeAll() {
        for (Window window : Window.getWindows()) {
            window.dispose();
        }
    }
}
