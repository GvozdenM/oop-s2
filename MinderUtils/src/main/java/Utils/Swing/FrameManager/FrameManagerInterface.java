package Utils.Swing.FrameManager;

import javax.swing.*;
import java.awt.*;

public interface FrameManagerInterface {
    FrameManagerInterface configureFramePreferences(String title, int xSize, int ySize);

    FrameManagerInterface configureFramePreferences(String title);

    FrameManagerInterface configureFramePreferences(int xSize, int ySize);

    FrameManagerInterface configureFramePreferences(String title, JPanel startPanel, int xSize, int ySize);

    FrameManagerInterface configureFramePreferences(String title,
                                                    JPanel startPanel,
                                                    int xSize,
                                                    int ySize,
                                                    Component spawnRelativeTo);

    FrameManagerInterface setSpawnRelativeTo(Component component);

    FrameManagerInterface setExitOnClose(boolean doClose);

    FrameManagerInterface setFavicon(String iconPath);

    void setContentPanel(JPanel nextPanel);

    void closeFrame();

    FrameManagerInterface setResizable(boolean resizable);

    FrameManagerInterface setCloseProcedure(Runnable closeProcedure, boolean exitSystem);

    FrameManagerInterface setCloseProcedure(boolean exitSystem);
}
