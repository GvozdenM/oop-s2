package Utils.Swing.FrameManager;

public interface FrameManagerFactory {
    FrameManager fm = FrameManager.newInstance();

    static FrameManager create() {
        return fm;
    }
}
