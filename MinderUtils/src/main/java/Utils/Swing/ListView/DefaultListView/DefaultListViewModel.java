package Utils.Swing.ListView.DefaultListView;

import Utils.Swing.ListView.Interface.ListViewModel;

import java.util.ArrayList;

public class DefaultListViewModel extends ListViewModel<DefaultListViewModel.DefaultListItem> {
    private final ArrayList<DefaultListItem> items;

    public DefaultListViewModel() {
        this.items = new ArrayList<>();
    }

    @Override
    public ArrayList<DefaultListItem> getItems() {
        return this.items;
    }

    @Override
    public DefaultListViewModel addItem(DefaultListItem item) {
        this.items.add(item);
        return this;
    }

    public DefaultListItem createItem(String title, String appendedText, String imgUrl) {
        return new DefaultListItem(title, appendedText, imgUrl);
    }

    static class DefaultListItem {
        private final String title;
        private final String appendedText;
        private final String imgUrl;

        public DefaultListItem(String title, String appendedText, String imgUrl) {
            this.title = title;
            this.appendedText = appendedText;
            this.imgUrl = imgUrl;
        }

        public String getAppendedText() {
            return appendedText;
        }

        public String getTitle() {
            return title;
        }

        public String getImgUrl() {
            return imgUrl;
        }
    }
}
