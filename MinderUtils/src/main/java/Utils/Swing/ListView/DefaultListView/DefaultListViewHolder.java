package Utils.Swing.ListView.DefaultListView;

import Utils.Swing.Image.ImagePanel;
import Utils.Swing.ListView.Interface.ListViewHolder;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

public class DefaultListViewHolder extends ListViewHolder {
    private ImagePanel image;
    private JLabel lTitle;
    private JLabel lAppendedText;

    private final Dimension imgSize = new Dimension(80, 80);

    @Override
    public void configurePanel() {
        int elementPadding = 30;
        this.setLayout(new GridLayout(1, 2));

        JPanel wrapperData = new JPanel();
        wrapperData.setLayout(new FlowLayout(FlowLayout.LEADING, elementPadding, elementPadding));

        this.image = new ImagePanel();
        this.lTitle = new JLabel();
        this.lAppendedText = new JLabel();

        this.add(this.image);
        JPanel wrapperCenter = new JPanel(new GridLayout(2, 1));
        wrapperCenter.add(this.lTitle);
        wrapperCenter.add(this.lAppendedText);
        wrapperData.add(wrapperCenter);
        this.add(wrapperData);
    }

    public void setImgUrl(URL imgUrl) {
        this.image.setImgSize(this.imgSize.width, this.imgSize.height);
        this.image.load(imgUrl);
        this.revalidate();
    }

    public void setImgUrl(String imgUrl) {
        this.image.setImgSize(this.imgSize.width, this.imgSize.height);
        this.image.load(imgUrl);
        this.revalidate();
    }

    public ImagePanel getImage() {
        return image;
    }

    public String  getTitle() {
        return lTitle.getText();
    }

    public void setTitle(String title) {
        this.lTitle.setText(title);
    }

    public String getAppendText() {
        return lAppendedText.getText();
    }

    public void setAppendText(String appendText) {
        this.lAppendedText.setText(appendText);
    }
}