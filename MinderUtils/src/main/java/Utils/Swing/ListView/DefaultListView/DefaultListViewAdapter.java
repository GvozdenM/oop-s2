package Utils.Swing.ListView.DefaultListView;

import Utils.Constants;
import Utils.Swing.ListView.Interface.ListViewAdapter;
import Utils.Swing.ListView.Interface.ListViewHolder;
import Utils.Views.ResourceFetcher;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class DefaultListViewAdapter extends ListViewAdapter {

    private final ArrayList<String> imgUrls;
    private final ArrayList<String> titles;
    private final ArrayList<String> appendedText;


    public DefaultListViewAdapter(DefaultListViewModel viewModel) {
        this.imgUrls = new ArrayList<>();
        this.titles = new ArrayList<>();
        this.appendedText = new ArrayList<>();

        for (DefaultListViewModel.DefaultListItem item : viewModel.getItems()) {
            this.imgUrls.add(item.getImgUrl());
            this.titles.add(item.getTitle());
            this.appendedText.add(item.getAppendedText());
        }
    }

    @Override
    public ListViewHolder onCreateViewHolder(int position) {
        DefaultListViewHolder holder = new DefaultListViewHolder();

        holder.getImage().setImgSize(150, 150);
        holder.setTitle(this.titles.get(position));
        holder.setAppendText(this.appendedText.get(position));
        try {
            holder.setImgUrl(
                    new URL(this.imgUrls.get(position))
            );
        } catch (MalformedURLException e) {
            e.printStackTrace();
            ResourceFetcher rf = new ResourceFetcher("defaultuserimg.png");
            try {
                holder.setImgUrl(rf.getResourceFile().getPath());
            } catch (IOException ioException) {
                ioException.printStackTrace();
                holder.setImgUrl(Constants.COSMETICS.DEFAULT_USER_IMG_URL);
            }
        }

        return holder;
    }

    @Override
    public int getNumItems() {
        return titles.size();
    }
}
