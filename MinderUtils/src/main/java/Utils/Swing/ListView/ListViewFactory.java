package Utils.Swing.ListView;

interface ListViewFactory {
    static ListView create(int length) {
        return new ListView(length) {};
    }

    static ListView create() {
        return new ListView() {};
    }
}
