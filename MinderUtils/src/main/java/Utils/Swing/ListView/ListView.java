package Utils.Swing.ListView;


import javax.swing.*;
import java.awt.*;

public abstract class ListView extends JPanel {
    private JScrollPane scrollPane;
    private final int length;

    ListView(int length) {
        this.length = length;
        //this.setLayout(new GridLayout(length,1));
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
    }

    public ListView() {
        this.length = 0;
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
    }

    public int length() {
        return length;
    }

    public static ListViewBuilder builder() {
        return ListViewBuilderFactory.create();
    }

    void setScrollPane(JScrollPane scrollPane) {
        this.scrollPane = scrollPane;
        this.add(scrollPane);
    }

    public void scrollToBottom() {
        JScrollBar scrollBar = scrollPane.getVerticalScrollBar();
        scrollBar.setValue(scrollBar.getMaximum());
    }
}
