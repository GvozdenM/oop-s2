package Utils.Swing.ListView.Interface;


public abstract class ListViewAdapter {
    public abstract ListViewHolder onCreateViewHolder(int position);

    /**
     * @return number of items that is going to be displayed.
     */
    public abstract int getNumItems();

}
