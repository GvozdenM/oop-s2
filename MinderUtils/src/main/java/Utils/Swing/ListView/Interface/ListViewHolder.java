package Utils.Swing.ListView.Interface;

import javax.swing.*;

/**
 *      This class is supposed to represent one panel cell in the Utils.Swing.ListView
 */
public abstract class ListViewHolder extends JPanel {

    public ListViewHolder() {
        configurePanel();
    }

    /**
     *      This method is intended for the client to provide the implementation of one list panel slot.
     *      This is done by configuring the ViewHolder class directly by referencing to the panel by "this" reference.
     */
    public abstract void configurePanel();
}
