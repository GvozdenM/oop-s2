package Utils.Swing.ListView.Interface;

import java.util.ArrayList;

public abstract class ListViewModel<ListItem> {
    protected abstract ArrayList<ListItem> getItems();

    protected abstract ListViewModel addItem(ListItem item);
}
