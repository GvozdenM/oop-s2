package Utils.Swing.ListView;

import Utils.Swing.ListView.Interface.ListViewHolder;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CachedListViewBuilder extends ListViewBuilder {
    private final Map<Integer, ListViewHolder> cachedViewHolderMap;

    public CachedListViewBuilder() {
        cachedViewHolderMap = new ConcurrentHashMap<>();
    }

    @Override
    protected ListViewHolder procureViewHolder(int position) {
        if (!this.cachedViewHolderMap.containsKey(position)) {
            this.cachedViewHolderMap.put(
                    position,
                    super.procureViewHolder(position)
            );
        }
        return this.cachedViewHolderMap.get(position);
    }
}
