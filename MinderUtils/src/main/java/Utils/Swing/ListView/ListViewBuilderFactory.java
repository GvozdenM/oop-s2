package Utils.Swing.ListView;

import Utils.Swing.ListView.Interface.ListViewAdapter;

public interface ListViewBuilderFactory {
    static ListViewBuilder create() {
        return new ListViewBuilder() {};
    }

    static ListViewBuilder create(ListViewAdapter listViewHolderListViewAdapter) {
        return new ListViewBuilder(listViewHolderListViewAdapter) {};
    }
}
