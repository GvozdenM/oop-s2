package Utils.Swing.ListView;

import Utils.Swing.ListView.DefaultListView.DefaultListViewAdapter;
import Utils.Swing.ListView.DefaultListView.DefaultListViewModel;
import Utils.Swing.ListView.Interface.ListViewAdapter;
import Utils.Swing.ListView.Interface.ListViewHolder;

import javax.swing.*;

/**
 *      This abstract implementation provides a way to build a Utils.Swing.ListView using a ListViewAdapter.
 *      If default list view is desired, the need of specifying a list adapter is no longer applicable.
 *      What the builder needs in this case is a DefaultListViewModel instead.
 *      If neither is provided the Builder will build a empty Utils.Swing.ListView.
 *      The director implementation is left to the client to implement.
 *
 *      If the ViewHolder user for the ListView contains a image loaded from a URL, it is recommended to carry
 *      out the process of both building the ListView and setting it to the desired JFrame asynchronously.
 */
public abstract class ListViewBuilder {
    protected ListViewAdapter listViewAdapter;

    protected DefaultListViewModel defaultListViewModel;

    ListViewBuilder() {
    }

    ListViewBuilder(ListViewAdapter listViewAdapter) {
        this.listViewAdapter = listViewAdapter;
    }

    public ListViewBuilder setListViewAdapter(ListViewAdapter listViewAdapter) {
        this.listViewAdapter = listViewAdapter;
        return this;
    }

    public ListViewBuilder setDefaultViewModel(DefaultListViewModel defaultViewModel) {
        this.defaultListViewModel = defaultViewModel;
        return this;
    }

    public ListView build() {

        if(!adapterConfigured()) {
            if (this.defaultListViewModel != null) {
                setListViewAdapter(new DefaultListViewAdapter(this.defaultListViewModel));
            }
            else {
                //Can't resolve specifications, returns empty listView
                return ListViewFactory.create(0);
            }
        }

        ListView listView = ListViewFactory.create();
        ListView listViewWrapper = ListViewFactory.create(this.listViewAdapter.getNumItems());

        for (int i = 0; i < listViewAdapter.getNumItems(); i++) {
            listViewWrapper.add(procureViewHolder(i));
        }

        listViewWrapper.setVisible(true);

        JScrollPane scrollPane = new JScrollPane(
                listViewWrapper,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
        );

        scrollPane.getVerticalScrollBar().setMaximum(200*listViewAdapter.getNumItems());

        listView.setScrollPane(scrollPane);

        return listView;
    }

    protected ListViewHolder procureViewHolder(int position) {
        ListViewHolder viewHolder = this.listViewAdapter.onCreateViewHolder(position);
        viewHolder.revalidate();

        return viewHolder;
    }

    private boolean adapterConfigured() {
        return this.listViewAdapter != null;
    }

}
