package Utils.listeners.interfaces;

import java.util.function.Consumer;

public class TextChangedListener {
    protected Consumer<String> onTextChangedConsumer;

    public TextChangedListener(Consumer<String> onTextChanged) {
        this.onTextChangedConsumer = onTextChanged;
    }

    public TextChangedListener(TextChangedListener textChangedListener) {
        this.onTextChangedConsumer = textChangedListener.getOnTextChangedConsumer();
    }

    protected void onTextChange(String t) {
        this.onTextChangedConsumer.accept(t);
    }

    protected Consumer<String>getOnTextChangedConsumer() {
        return onTextChangedConsumer;
    }

    protected void setOnTextChangedConsumer(Consumer<String> onTextChangedConsumer) {
        this.onTextChangedConsumer = onTextChangedConsumer;
    }
}
