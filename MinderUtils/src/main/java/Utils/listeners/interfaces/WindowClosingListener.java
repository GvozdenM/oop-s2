package Utils.listeners.interfaces;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class WindowClosingListener extends WindowAdapter {
    private final JFrame parentFrame;
    private final Runnable closeProcedure;
    private final boolean exitTrigger;


    public WindowClosingListener(JFrame parentFrame, Runnable closeProcedure, boolean exitTrigger) {
        this.parentFrame = parentFrame;
        this.closeProcedure = closeProcedure;
        this.exitTrigger = exitTrigger;
    }

    @Override
    public void windowClosing(WindowEvent e) {
        super.windowClosing(e);
        closeProcedure.run();
        parentFrame.dispose();
        if (this.exitTrigger) System.exit(0);
    }
}
