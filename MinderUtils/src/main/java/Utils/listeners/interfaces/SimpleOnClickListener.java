package Utils.listeners.interfaces;

/**
 *  This interface provides the most simple OnClick listener that a client might want to register
 */
public interface SimpleOnClickListener {
    void onClick();
}
