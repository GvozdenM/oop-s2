package Utils.Views;

public interface TransitionView {

    //Enforces every view to implement how to set up itself when transitioned to
    void onTransitionTo();
}
