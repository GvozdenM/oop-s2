package Utils.Logger;

public class Log {
    /**
     * Log simple string msg
     */
    public static void s(String msg) {
        System.out.println(msg);
    }

    /**
     * Log for debugging
     *
     * @param TAG Debug tag
     * @param msg appended debug message
     */
    public static void d(String TAG, String msg) {
        System.out.println(TAG + " :: " + msg);
    }
}
