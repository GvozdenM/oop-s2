package Utils.Adapters;

import Utils.listeners.interfaces.TextChangedListener;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import java.util.function.Consumer;

public class TextChangeListenerSwingAdapter extends TextChangedListener implements DocumentListener {
    public TextChangeListenerSwingAdapter(Consumer<String> onTextChanged) {
        super(onTextChanged);
    }

    //copy constructor
    public TextChangeListenerSwingAdapter(TextChangedListener textChangedListener) {
        super(textChangedListener);
    }

    @Override
    public void insertUpdate(DocumentEvent documentEvent) {
        try {
            this.onTextChange(documentEvent.getDocument().getText(0, documentEvent.getDocument().getLength()));
        } catch (BadLocationException e) {
            this.onTextChange("");
            e.printStackTrace();
        }
    }

    @Override
    public void removeUpdate(DocumentEvent documentEvent) {
        try {
            this.onTextChange(documentEvent.getDocument().getText(0, documentEvent.getDocument().getLength()));
        } catch (BadLocationException e) {
            this.onTextChange("");
            e.printStackTrace();
        }
    }

    @Override
    public void changedUpdate(DocumentEvent documentEvent) {
        try {
            this.onTextChange(documentEvent.getDocument().getText(0, documentEvent.getDocument().getLength()));
        } catch (BadLocationException e) {
            this.onTextChange("");
            e.printStackTrace();
        }
    }
}
