package LoginInteractor;

public interface LoginRequesterFactory<Req, Res> {
    LoginRequester<Req, Res> createLoginRequester();
}
