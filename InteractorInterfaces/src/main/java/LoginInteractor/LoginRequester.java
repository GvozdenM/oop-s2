package LoginInteractor;

import Utils.Callbacks.CallbackAdapter;

/**
 * This interface forces implementers to provide a functionality for the methods and functions required in order
 * to perform calls to the server side regarding the Login of the user.
 * @param <Req> Needs a LoginRequest
 * @param <Res> Needs a LoginResponse
 */
public interface LoginRequester<Req, Res> {

    /**
     * This method will be called when a call to the server side regarding a login request is performed.
     * @param request  A LoginRequest to be passed with the data from the user.
     * @param callback A LoginResponse to be assigned in a callback to perform different actions.
     */
    void login(Req request, CallbackAdapter<Res> callback);
}
