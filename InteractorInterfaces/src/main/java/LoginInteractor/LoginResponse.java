package LoginInteractor;

import java.io.Serializable;

/**
 * This class provides a data type to identify and understand login transaction results.
 */
public class LoginResponse implements Serializable {
    private final boolean authenticated;
    private String errorMsg;
    private long userId;

    public LoginResponse(boolean authenticated) {
        this.authenticated = authenticated;
    }

    public LoginResponse(boolean authenticated, long userId) {
        this.authenticated = authenticated;
        this.userId = userId;
    }

    public LoginResponse(boolean authenticated, String errorMsg, long userId) {
        this.authenticated = authenticated;
        this.errorMsg = errorMsg;
        this.userId = userId;
    }

    //GETTERS AND SETTERS
    public boolean isAuthenticated() {
        return authenticated;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public long getUserId() {
        return userId;
    }
}
