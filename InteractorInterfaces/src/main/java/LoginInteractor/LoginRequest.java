package LoginInteractor;

import java.io.Serializable;

/**
 * This class provides a data type to logically implement and generate login transaction petitions for the server side.
 */
public class LoginRequest implements Serializable {
    private final String username;
    private final String password;

    public LoginRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    //GETTERS
    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
