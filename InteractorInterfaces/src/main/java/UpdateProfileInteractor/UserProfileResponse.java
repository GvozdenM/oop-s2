package UpdateProfileInteractor;

import java.io.Serializable;
import java.util.List;

public class UserProfileResponse implements Serializable {
    private long id;
    private String username;
    private String name;
    private int age;
    private String email;
    private String password;
    private String interest;
    private boolean isPremium;
    private String profileImgUrl;
    private List<String> imgUrls;
    private String shortDescription;
    private String description;

    public UserProfileResponse() {
    }

    public UserProfileResponse(long id, String username, String name, int age, String email, String password,
                               String interest, boolean isPremium, String profileImgUrl,
                               List<String> imgUrls, String shortDescription, String description) {
        this.id = id;
        this.username = username;
        this.name = name;
        this.age = age;
        this.email = email;
        this.password = password;
        this.interest = interest;
        this.isPremium = isPremium;
        this.profileImgUrl = profileImgUrl;
        this.imgUrls = imgUrls;
        this.shortDescription = shortDescription;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getInterest() {
        return interest;
    }

    public boolean isPremium() {
        return isPremium;
    }

    public String getProfileImgUrl() {
        return profileImgUrl;
    }

    public List<String> getImgUrls() {
        return imgUrls;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public String getDescription() {
        return description;
    }
}
