package UpdateProfileInteractor;

import Utils.Callbacks.CallbackAdapter;

public interface UpdateProfileRequester<Req, Res> {
    void updateUser(Req request, CallbackAdapter<Res> callback);
    void getUser(long id, CallbackAdapter<Res> callback);
}