package UpdateProfileInteractor;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

/**
 * This class provides a data structure to englobe a request to the server side in a transaction of user profile
 * updating.
 */
public class UpdateProfileRequest implements Serializable {
    private final Long id;
    private final String username;
    private final String name;
    private final int age;
    private final String email;
    private final String password;
    private final String interest;
    private final boolean isPremium;
    private String profileImgUrl;
    private String description;

    public UpdateProfileRequest(Long id, String username, String name, int age, String email, String password, String interest, boolean isPremium, String profileImgUrl, String description) {
        this.id = id;
        this.username = username;
        this.name = name;
        this.age = age;
        this.email = email;
        this.password = password;
        this.interest = interest;
        this.isPremium = isPremium;
        this.profileImgUrl = profileImgUrl;
        this.description = description;
    }

    public String getProfileImgUrl() {
        return profileImgUrl;
    }

    public void setProfileImgUrl(String profileImgUrl) {
        this.profileImgUrl = profileImgUrl;
    }

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getInterest() {
        return interest;
    }

    public boolean isPremium() {
        return isPremium;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
