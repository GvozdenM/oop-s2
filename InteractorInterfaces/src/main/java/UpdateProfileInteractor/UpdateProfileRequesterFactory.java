package UpdateProfileInteractor;

public interface UpdateProfileRequesterFactory<Req,Res>{
    UpdateProfileRequester<Req,Res> createUpdateProfileRequester();
}
