package Utils;

import Utils.Callbacks.CallbackAdapter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jms.*;
import java.io.Serializable;

public abstract class MessageRequester {
    private final Connection connection;

    /**
     * Default constructor
     * @param connection JMS Connection object created via {@link ConnectionFactory}
     */
    public MessageRequester(Connection connection) {
        this.connection = connection;
    }

    /**
     * Configures a message with the given body and sends it to the destination of the given name
     * @param body Serializable body, the request object
     * @param destinationName The name of the destination to send to
     * @throws JMSException Rethrow all JMS Exceptions
     */
    protected void makeRequestViaMessage(@NotNull Serializable body,
                                         @NotNull String destinationName,
                                         @Nullable String replyToDestination) throws JMSException {
        //create session and object message
        Session session = this.connection.createSession();
        ObjectMessage message = session.createObjectMessage();

        //target the resource server for this message and set the body
        message.setStringProperty("clientId", "MinderServer");
        message.setObject(body);

        if(null != replyToDestination) {
            message.setJMSReplyTo((Destination) JMSlim.getJMSlim().lookup(replyToDestination));
        }

        //initiate message producer for destination of the given name
        MessageProducer messageProducer =
                session.createProducer((Destination) JMSlim.getJMSlim().lookup(destinationName));

        //send the message and close the producer
        messageProducer.send(message);
        messageProducer.close();
    }

    /**
     * Assigns the given {@link CallbackAdapter} as the message listener for messages on the given destination
     * @param destinationName The name of the destination on which to listen to messages on
     * @param messageListener The callback adapter used to trigger callbacks upon message reception
     * @param <T> The type of object that the callback adapter accept / The data type of the destination with the given
     *           name
     * @throws JMSException Rethrow all JMS Exceptions
     */
    @SuppressWarnings("unchecked")
    protected <T> void listenForMessagesOn(String destinationName, CallbackAdapter<T> messageListener) throws JMSException {
        //initiate session and create consumer for destination with the given name
        Session session = connection.createSession();

        MessageConsumer messageConsumer =
                session.createConsumer((Destination) JMSlim.getJMSlim().lookup(destinationName));

        //set message listener for this consumer
        messageConsumer.setMessageListener((message) -> {

            //all messages in JMSlim are object messages so cast is safe
            ObjectMessage objectMessage = (ObjectMessage) message;
            try {
                //all messages on this channel must be of the specified type as all channels are
                //data type channels so cast is safe
                messageListener.onSuccess((T) objectMessage.getObject());
            } catch (JMSException e) {

                //the server responded but we failed to get the body
                messageListener.onFail(e);
            }
        });
    }
}
