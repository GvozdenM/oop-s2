package MatchedUsersInteractor;

import java.io.Serializable;

public class MatchedUsersUnmatchRequest implements Serializable {
    private final Long currentUserID;
    private final Long userToUnmatchID;

    public MatchedUsersUnmatchRequest(Long currentUserID, Long userToUnmatchID) {
        this.currentUserID = currentUserID;
        this.userToUnmatchID = userToUnmatchID;
    }

    public Long getCurrentUserID() {
        return currentUserID;
    }

    public Long getUserToUnmatchID() {
        return userToUnmatchID;
    }
}
