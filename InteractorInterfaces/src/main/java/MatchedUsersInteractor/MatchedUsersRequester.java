package MatchedUsersInteractor;

import Utils.Callbacks.CallbackAdapter;

/**
 * @param <T> Users List Request Object
 * @param <K>   Unmatch user Request Object
 * @param <Res> Response Object
 */
public interface MatchedUsersRequester<T, K, Res> {
    /**
     * @param request the request encapsulated in MatchedUsersRequest object
     * @param callback specifies in it's consturctor the three different types of response callbacks
     */
    void getMatchedUsers(T request, CallbackAdapter<Res> callback);

    /**
     * @param request MatchedUsersUnmatchRequest
     * @param callback specifies the responses whether the user was unmatched or not
     */
    void unmatchUser(K request, CallbackAdapter<Class<Void>> callback);
}
