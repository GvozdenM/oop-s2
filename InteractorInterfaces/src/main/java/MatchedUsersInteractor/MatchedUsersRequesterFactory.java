package MatchedUsersInteractor;

/**
 * @param <T> Users List Request Object
 * @param <K>   Unmatch user Request Object
 * @param <Res> Response Object
 */
public interface MatchedUsersRequesterFactory<T, K, Res> {
    MatchedUsersRequester<T, K, Res> createMatchedUsersRequester();
}
