package MatchedUsersInteractor;

import java.io.Serializable;

/**
 * This request object specifies which user it is desired to acquire the matched users list of
 */
public class MatchedUsersRequest implements Serializable {
    private final Long userID;

    public MatchedUsersRequest(Long userID) {
        this.userID = userID;
    }

    public Long getUserID() {
        return userID;
    }
}
