package MatchedUsersInteractor;

import java.io.Serializable;
import java.util.ArrayList;

public class MatchedUsersResponse implements Serializable {

    private final ArrayList<MatchedUser> matchedUsers;

    public MatchedUsersResponse() {
        this.matchedUsers = new ArrayList<>();
    }

    public void addUser(MatchedUser matchedUser) {
        matchedUsers.add(matchedUser);
    }

    public ArrayList<MatchedUser> getMatchedUsers() {
        return matchedUsers;
    }

    public MatchedUser createUser(Long userID, String userLogin, String userImgUrl) {
        return new MatchedUser(userID, userLogin, userImgUrl);
    }

    public static class MatchedUser implements Serializable {
        public final Long userID;
        public final String userLogin;
        public final String userImgUrl;

        MatchedUser(Long userID, String userLogin, String userImgUrl) {
            this.userID = userID;
            this.userLogin = userLogin;
            this.userImgUrl = userImgUrl;
        }
    }
}
