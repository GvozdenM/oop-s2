package RegistrationInteractor;

import java.io.Serializable;

/**
 * This class provides a data structure to represent a Registration response, indicating whether a Registration
 * transaction was successful or not.
 */
public class RegistrationResponse implements Serializable {
    private Long id;
    private final boolean authenticated;
    private final String errorMsg;

    public RegistrationResponse(boolean authenticated, String errorMsg) {
        this.authenticated = authenticated;
        this.errorMsg = errorMsg;
    }

    public RegistrationResponse(Long id) {
        this.id = id;
        this.authenticated = true;
        this.errorMsg = "E_NO_ERROR";
    }

    public RegistrationResponse(Long id, boolean authenticated, String errorMsg) {
        this.id = id;
        this.authenticated = authenticated;
        this.errorMsg = errorMsg;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
