package RegistrationInteractor;

import Utils.Callbacks.CallbackAdapter;

/**
 * This interface forces implementers to provide a functionality for the methods and functions required in order
 * to perform calls to the server side regarding the Registration of the user.
 * @param <Req> Needs a RegistrationRequest
 * @param <Res> Needs a RegistrationResponse
 */
public interface RegistrationRequester<Req, Res> {

    /**
     * This method will be called when a call to the server side regarding a registration request is performed.
     * @param request  A RegistrationRequest to be passed with the data from the user.
     * @param callback A RegistrationResponse to be assigned in a callback to perform different actions.
     */
    void register(Req request, CallbackAdapter<Res> callback);
}
