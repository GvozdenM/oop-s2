package RegistrationInteractor;

public interface RegistrationRequesterFactory<Req, Res> {
    RegistrationRequester<Req, Res> createRegistrationRequester();
}
