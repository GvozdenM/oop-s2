package RegistrationInteractor;

import java.io.Serializable;

/**
 * This class provides a data structure to generate a request for a registration transaction with the server side
 * and contains the basic information to validate in server side its info.
 */
public class RegistrationRequest implements Serializable {
    private final String username;
    private final int age;
    private final boolean isPremium;
    private final String interest;
    private final String email;
    private final String password;
    private final String imgUrl;

    public RegistrationRequest(String username, int age, boolean isPremium, String password, String email, String interest, String imgUrl) {
        this.username = username;
        this.age = age;
        this.isPremium = isPremium;
        this.email = email;
        this.password = password;
        this.interest = interest;
        this.imgUrl = imgUrl;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public int getAge() {
        return age;
    }

    public boolean isPremium() {
        return isPremium;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getInterest() {
        return interest;
    }
}
