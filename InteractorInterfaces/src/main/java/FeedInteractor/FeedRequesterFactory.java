package FeedInteractor;

public interface FeedRequesterFactory<T, M, U> {
    FeedRequester<T, M, U> createFeedRequester();
}
