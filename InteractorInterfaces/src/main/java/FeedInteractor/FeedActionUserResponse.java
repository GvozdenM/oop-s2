package FeedInteractor;

import java.io.Serializable;

public class FeedActionUserResponse implements Serializable {
    private long id;
    private int age;
    private String username;
    private String name;
    private String interestedIn;
    private String profileImageUrl;
    private String shortDescription;
    private String description;

    public FeedActionUserResponse() {}

    public FeedActionUserResponse(long id,
                                  int age,
                                  String username,
                                  String name,
                                  String interestedIn,
                                  String profileImageUrl,
                                  String shortDescription,
                                  String description) {
        this.id = id;
        this.age = age;
        this.username = username;
        this.name = name;
        this.interestedIn = interestedIn;
        this.profileImageUrl = profileImageUrl;
        this.shortDescription = shortDescription;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public String getInterestedIn() {
        return interestedIn;
    }

    public void setInterestedIn(String interestedIn) {
        this.interestedIn = interestedIn;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
