package FeedInteractor;

import java.io.Serializable;

public class FeedActionRequest implements Serializable {
    private long invokerId;
    private long receiverId;
    private boolean like;

    public boolean isLike() {
        return like;
    }

    public void setLike(boolean like) {
        this.like = like;
    }

    public long getInvokerId() {
        return invokerId;
    }

    public void setInvokerId(long invokerId) {
        this.invokerId = invokerId;
    }

    public long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(long receiverId) {
        this.receiverId = receiverId;
    }
}
