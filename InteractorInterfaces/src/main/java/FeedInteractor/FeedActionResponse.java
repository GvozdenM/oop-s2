package FeedInteractor;

import java.io.Serializable;

public class FeedActionResponse implements Serializable {
    private long invokerId;
    private long receiverId;
    private boolean isMatched;
    private FeedActionUserResponse matchedWith;
    private FeedActionUserResponse whoMatched;

    public FeedActionResponse() {
    }

    public FeedActionUserResponse getWhoMatched() {
        return whoMatched;
    }

    public void setWhoMatched(FeedActionUserResponse whoMatched) {
        this.whoMatched = whoMatched;
    }

    public long getInvokerId() {
        return invokerId;
    }

    public void setInvokerId(long invokerId) {
        this.invokerId = invokerId;
    }

    public long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(long receiverId) {
        this.receiverId = receiverId;
    }

    public boolean isMatched() {
        return isMatched;
    }

    public void setMatched(boolean matched) {
        isMatched = matched;
    }

    public FeedActionUserResponse getMatchedWith() {
        return matchedWith;
    }

    public void setMatchedWith(FeedActionUserResponse matchedWith) {
        this.matchedWith = matchedWith;
    }
}
