package FeedInteractor;

import Utils.Callbacks.CallbackAdapter;

import java.util.List;

/**
 * @param <Req> FeedActionRequest
 * @param <Res> FeedActionResponse
 * @param <U> FeedActionUserResponse
 */
public interface FeedRequester<Req, Res, U> {
    /**
     * Retrieve the user's feed as a {@link List<U>}
     * @param id The id of the user's who's feed we're requesting
     * @param callback Callback to execute when operation has completed
     */
    void getUserFeedBuffer(long id, CallbackAdapter<List<U>> callback);

    /**
     * Create a user interaction
     *
     * @param interaction Interaction request model
     * @param callback Callback to execute when operation has completed
     */
    void interact(Req interaction, CallbackAdapter<Res> callback);

    /**
     * Retrieve a user modeled as {@link U} by id
     * @param id The id of the user to retrieve
     * @param callback The call back to execute
     */
    void getUserById(long id, CallbackAdapter<U> callback);
}
