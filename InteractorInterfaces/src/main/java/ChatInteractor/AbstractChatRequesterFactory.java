package ChatInteractor;


/**
 * @param <T> Outgoing Message
 * @param <K> Incoming Message
 * @param <F> Chat Log
 * @param <Req> Chat log request type
 */
public interface AbstractChatRequesterFactory<Req, T, K, F> {
     ChatRequester<Req, T, K, F> createChatRequester();
}
