package ChatInteractor;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.TreeSet;

public class ChatLogResponse implements Serializable {
    private ChatUser owner;
    private ChatUser partner;
    private LinkedList<LoggedChatMessage> messagesSortedByTimestamp;

    public ChatLogResponse(
            ChatUser chatOwner,
            ChatUser chatPartner,
            TreeSet<LoggedChatMessage> messagesSortedByTimestamp) {
        this.owner = chatOwner;
        this.partner = chatPartner;
        this.messagesSortedByTimestamp = new LinkedList<>(messagesSortedByTimestamp);
    }

    public ChatUser getPartner() {
        return partner;
    }

    public void setPartner(ChatUser partner) {
        this.partner = partner;
    }

    public long getChatOwnerId() {
        return owner.getId();
    }

    public void setChatOwnerId(long chatOwnerId) {
        this.owner.setId(chatOwnerId);
    }

    public void setOwner(ChatUser owner) {
        this.owner = owner;
    }

    public ChatUser getOwner() {
        return owner;
    }

    public Collection<LoggedChatMessage> getMessagesSortedByTimestamp() {
        return messagesSortedByTimestamp;
    }

    public void setMessagesSortedByTimestamp(TreeSet<LoggedChatMessage> messagesSortedByTimestamp) {
        this.messagesSortedByTimestamp = new LinkedList<>(messagesSortedByTimestamp);
    }
}
