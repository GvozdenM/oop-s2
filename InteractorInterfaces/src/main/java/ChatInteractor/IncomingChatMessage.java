package ChatInteractor;

import java.io.Serializable;
import java.util.Date;

public class IncomingChatMessage implements Serializable {
    private final String message;
    private final Date timestamp;
    private final long senderID;
    private final long receiverID;

    public IncomingChatMessage(String message, Date timestamp, long senderID, long receiverID) {
        this.message = message;
        this.timestamp = timestamp;
        this.senderID = senderID;
        this.receiverID = receiverID;
    }

    public String getMessage() {
        return message;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public long getReceiverID() {
        return receiverID;
    }

    public long getSenderID() {
        return this.senderID;
    }
}
