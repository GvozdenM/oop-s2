package ChatInteractor;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

/**
 * Request object for the chat log between two users
 */
public class ChatLogRequest implements Serializable {
    private final Long userA;
    private final Long userB;

    public ChatLogRequest(@NotNull Long userA, @NotNull Long userB) {
        this.userA = userA;
        this.userB = userB;
    }

    public Long getUserA() {
        return userA;
    }

    public Long getUserB() {
        return userB;
    }
}
