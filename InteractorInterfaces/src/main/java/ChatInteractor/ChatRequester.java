package ChatInteractor;

import Utils.Callbacks.CallbackAdapter;
import org.jetbrains.annotations.NotNull;

/**
 * Abstract interface for sending and receiving messages as well as requesting
 * a chat log of messages based on some abstract criteria defined in the {@link Req} model
 *
 * @param <Req> Chat Log Request
 * @param <T> Outgoing Message
 * @param <K> Incoming Message
 * @param <F> Chat Log
 */
public interface ChatRequester<Req, T, K, F> {
    /**
     * Send a chat message and persist it on the server
     * @param message The message to send
     * @param callback Callback to call upon successfully sending a message or upon some error occurring
     */
    void send(@NotNull T message, @NotNull CallbackAdapter<K> callback);

    /**
     * Set a listener for when messages are received
     * @param callback The callback to call when a message is received
     */
    void setOnMessageReceived(@NotNull CallbackAdapter<K> callback);

    /**
     * Retrieve a chat log based on the criteria defined by {@link Req} passed.
     * @param request The request object defining how to select the chat log
     * @param callback A set of callbacks to call in case of success failure or exception.
     */
    void getChatLog(@NotNull Req request, @NotNull CallbackAdapter<F> callback);
}
