package MatchGraphInteractor;

import java.io.Serializable;
import java.util.List;

public class MatchGraphResponse implements Serializable  {
    private final List<Integer> matches; //30 days match information
    private final boolean success;
    private final String errorMsg;

    public MatchGraphResponse(List<Integer> matches, boolean success, String errorMsg) {
        this.matches = matches;
        this.success = success;
        this.errorMsg = errorMsg;
    }

    public List<Integer> getMatches() {
        return matches;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}
