package MatchGraphInteractor;

import Utils.Callbacks.CallbackAdapter;

public interface MatchGraphRequester<Res> {
    /**
     * Performs a request to obtain the data corresponding to the match evolution of the system.
     * @param callback callback when the graph is ready
     */
    void graph(CallbackAdapter<Res> callback);
}
