package MatchGraphInteractor;

public interface MatchGraphRequesterFactory<Res> {
    MatchGraphRequester<Res> createMatchGraphRequester();
}
