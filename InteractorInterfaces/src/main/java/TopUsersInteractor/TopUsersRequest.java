package TopUsersInteractor;

import java.io.Serializable;

/**
 * Basic request model for retrieving the top users
 */
public class TopUsersRequest implements Serializable {
    private final int length;

    /**
     * Default constructor
     * @param length The number of top users to retrieve
     */
    public TopUsersRequest(int length) {
        this.length = length;
    }

    public int getLength() {
        return length;
    }
}
