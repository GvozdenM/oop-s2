package TopUsersInteractor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Basic response object meant as a reply to a {@link TopUsersRequest} made to
 * {@link TopUsersRequester}
 */
public class TopUsersResponse implements Serializable {
    private Collection<TopUser> users;

    public TopUsersResponse() {
        this.users = new ArrayList<>();
    }

    public Collection<TopUser> getUsers() {
        return users;
    }

    public void setUsers(Collection<TopUser> users) {
        this.users = users;
    }
}
