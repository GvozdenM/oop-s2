package TopUsersInteractor;

import java.io.Serializable;

public class TopUser implements Serializable {
    public final Long id;
    public final String imgUrl;
    public final String username;
    public final int age;
    public final int likes;

    TopUser(Long id, String imgUrl, String username, int age, int likes) {
        this.id = id;
        this.imgUrl = imgUrl;
        this.username = username;
        this.age = age;
        this.likes = likes;
    }
}
