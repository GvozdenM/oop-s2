package TopUsersInteractor;

import Utils.Callbacks.CallbackAdapter;

/**
 * @param <Req> Request type
 * @param <Res> Response type
 */
public interface TopUsersRequester<Req, Res> {
    /**
     * Get the top users registered in the system, sorted by popularity
     * @param request The request object specifiying how to retrieve the users
     * @param callback Callbacks available to call upon completion
     */
    void getTopUsers(Req request, CallbackAdapter<Res> callback);
}
