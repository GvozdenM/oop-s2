package TopUsersInteractor;

public interface TopUsersRequesterFactory<Req, Res> {
    TopUsersRequester<Req, Res> create();
}
