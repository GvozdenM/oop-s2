package Routing;

import Handlers.ConnectionHandler;
import Handlers.ConnectionHandlerFactory;

import java.net.Socket;

public class ConcurrentMessageRouterFactory extends ConnectionHandlerFactory {
    @Override
    public ConnectionHandler create(Socket socket) {
        return new ConcurrentMessageRouter(socket);
    }
}
