package Routing;

import Handlers.ConnectionHandler;
import Messages.JMSlimObjectMessage;
import Networking.SocketManager;
import Networking.concurrent.ConcurrentSocketManagerFactory;
import ServiceImpl.NetworkExecutorService;

import javax.jms.Message;
import java.net.Socket;

public class ConcurrentMessageRouter extends ConnectionHandler {
    public ConcurrentMessageRouter(Socket socket) {
        super(socket, new ConcurrentSocketManagerFactory());
    }

    @Override
    public void run() {
        this.lock.lock();
        try {
            this.socketManager.start();
            this.lock.unlock();

            this.lock.lock();
            this.socketManager.setOnReceived((message) -> {
                if(message instanceof Message) {
                    JMSlimObjectMessage objectMessage = (JMSlimObjectMessage) message;
                    String clientConnectionId = objectMessage.getStringProperty("clientId");

                    if(null != clientConnectionId) {
                        SocketManager clientSocketManager = NetworkExecutorService.getConnectionById(clientConnectionId);
                        if(null != clientSocketManager) {
                            clientSocketManager.send(message);
                        }
                    }

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            this.lock.unlock();
        }
    }
}
