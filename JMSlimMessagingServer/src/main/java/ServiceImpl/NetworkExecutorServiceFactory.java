package ServiceImpl;

import Networking.NetworkService;
import Networking.NetworkServiceFactory;
import Routing.ConcurrentMessageRouterFactory;

public class NetworkExecutorServiceFactory implements NetworkServiceFactory {
    @Override
    public NetworkService create(int port) {
        return new NetworkExecutorService(
                new ConcurrentMessageRouterFactory(),
                0 >= port ? 3337 : port,
                5
        );
    }
}
