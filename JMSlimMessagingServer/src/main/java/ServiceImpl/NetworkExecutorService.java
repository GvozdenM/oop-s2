package ServiceImpl;

import Handlers.ConnectionHandlerFactory;
import Networking.NetworkService;
import Networking.SocketManager;
import Networking.SocketManagerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class NetworkExecutorService implements NetworkService {
    private static final ConcurrentHashMap<String, SocketManager> clientConnections =
            new ConcurrentHashMap<>();

    private final ExecutorService pool;
    private final ConnectionHandlerFactory connectionHandlerFactory;
    private final int port;

    public NetworkExecutorService(ConnectionHandlerFactory connectionHandlerFactory,
                                  int port) {
        this.pool = Executors.newFixedThreadPool(10);
        this.port = port;
        this.connectionHandlerFactory = connectionHandlerFactory;
    }

    public NetworkExecutorService(ConnectionHandlerFactory connectionHandlerFactory,
                                  int port,
                                  int poolSize) {
        if(0 >= poolSize) {
            this.pool = Executors.newFixedThreadPool(poolSize);
        }
        else {
            this.pool = Executors.newSingleThreadExecutor();
        }

        this.port = port;
        this.connectionHandlerFactory = connectionHandlerFactory;
    }

    public static void addClient(String clientId, SocketManager socketManager) {
        clientConnections.put(clientId, socketManager);
    }

    public static void removeClient(String clientId) {
        clientConnections.remove(clientId);
    }

    public static Collection<String> getClientIds() {
        return clientConnections.keySet();
    }

    public static SocketManager getConnectionById(String id) {
        return clientConnections.get(id);
    }

    public static void stopClients() {
        for(String managerId : clientConnections.keySet()) {
            try {
                SocketManager socketManager = clientConnections.get(managerId);
                clientConnections.remove(managerId);
                socketManager.close();
            } catch (IOException e) {

                e.printStackTrace();
            }
        }
    }

    @Override
    public void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(this.port);
            while(!Thread.currentThread().isInterrupted()) {
                pool.execute(this.connectionHandlerFactory.create(serverSocket.accept()));
            }
        } catch (IOException e) {
            System.out.println("Network service caught exception. Stopping");
        }
    }

    @Override
    public void close() {
        try {
            this.pool.shutdown();

            if(!this.pool.awaitTermination(600, TimeUnit.MILLISECONDS)) {
                this.pool.shutdownNow();
                NetworkExecutorService.stopClients();
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        catch (Exception ignored) {}
    }
}
