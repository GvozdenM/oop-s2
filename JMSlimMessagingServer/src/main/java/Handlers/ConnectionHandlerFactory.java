package Handlers;


import java.net.Socket;

public abstract class ConnectionHandlerFactory {
    public abstract ConnectionHandler create(Socket socketManager);
}
