package Handlers;

import Networking.SocketManager;
import Networking.SocketManagerFactory;
import Networking.concurrent.ConcurrentSocketManager;
import ServiceImpl.NetworkExecutorService;

import java.net.Socket;
import java.util.concurrent.locks.ReentrantLock;

public abstract class ConnectionHandler implements Runnable {
    protected final SocketManager socketManager;
    protected final ReentrantLock lock = new ReentrantLock(true);
    protected String connectionId;

    public ConnectionHandler(Socket socket, SocketManagerFactory socketManagerFactory) {
        this.socketManager = socketManagerFactory.create(socket);
        this.socketManager.setInterceptNextListener((o) -> {
            if(o instanceof String) {
                this.lock.lock();
                this.connectionId = (String) o;

                this.socketManager.setOnClose(
                        () -> NetworkExecutorService.removeClient(this.connectionId)
                );

                NetworkExecutorService.addClient(
                        this.connectionId,
                        this.socketManager
                );

                this.lock.unlock();
            }
        });
    }
}
