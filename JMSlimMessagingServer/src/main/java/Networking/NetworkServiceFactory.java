package Networking;

public interface NetworkServiceFactory {
    NetworkService create(int port);
}
