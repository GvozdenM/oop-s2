package Messages;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import java.io.Serializable;
import java.util.Enumeration;

/**
 * Implementation of {@link ObjectMessage} extends uponJMSlimMessage by
 * defining the body type. All other methods are unimplemented and throw {@link RuntimeException}
 */
public class JMSlimObjectMessage extends JMSlimMessage implements ObjectMessage {
    private Serializable body;

    @Override
    public <T> T getBody(Class<T> aClass) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public boolean isBodyAssignableTo(Class aClass) throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public void setObject(Serializable serializable) {
        this.body = serializable;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Serializable getObject() {
        return this.body;
    }

    @Override
    public long getJMSDeliveryTime() {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public void setJMSDeliveryTime(long l) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public boolean propertyExists(String s) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public boolean getBooleanProperty(String s) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public byte getByteProperty(String s) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public short getShortProperty(String s) {
        throw new RuntimeException("Unimplemente1d");
    }

    @Override
    public int getIntProperty(String s) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public long getLongProperty(String s) {
        return 0;
    }

    @Override
    public float getFloatProperty(String s) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public double getDoubleProperty(String s) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public Object getObjectProperty(String s) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public Enumeration<String> getPropertyNames() {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public void setBooleanProperty(String s, boolean b) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public void setByteProperty(String s, byte b) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public void setShortProperty(String s, short i) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public void setIntProperty(String s, int i) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public void setLongProperty(String s, long l) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public void setFloatProperty(String s, float v) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public void setDoubleProperty(String s, double v) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public void setObjectProperty(String s, Object o) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public void acknowledge() {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public void clearBody() throws JMSException {
        this.body = null;
    }
}
