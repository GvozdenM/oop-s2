package Messages;

import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Implements java Message abstraction
 */
public abstract class JMSlimMessage implements Message, Serializable {
    protected String id;
    protected String transactionId;
    protected long timestamp;
    protected Destination replyTo;
    protected Destination destination;
    protected int deliveryMode = DeliveryMode.NON_PERSISTENT;
    protected boolean redelivered;
    protected String type;
    protected long expiration;
    protected int JMSPriority;
    protected String destinationName;
    protected Map<String, String> stringProperties = new HashMap<>();

    @Override
    public String getStringProperty(String s) {
        return this.stringProperties.getOrDefault(s, "");
    }

    @Override
    public void setStringProperty(String s, String s1) {
        this.stringProperties.put(s, s1);
    }
    /**
     * Get the name of the message destination
     */
    public String getDestinationName() {
        return destinationName;
    }

    /**
     * Set the name of the destination
     * @param destinationName The name of the destination
     */
    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getJMSPriority() {
        return JMSPriority;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setJMSPriority(int JMSPriority) {
        this.JMSPriority = JMSPriority;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJMSMessageID() {
        return this.id;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setJMSMessageID(String s) {
        this.id = s;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getJMSTimestamp() {
        return timestamp;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setJMSTimestamp(long l) {
        this.timestamp = l;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] getJMSCorrelationIDAsBytes() {
        if(null != this.transactionId) {
            return this.transactionId.getBytes();
        }
        else {
            return new byte[0];
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setJMSCorrelationIDAsBytes(byte[] bytes) {
        this.transactionId = Arrays.toString(bytes);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setJMSCorrelationID(String s) {
        this.transactionId = s;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJMSCorrelationID() {
        return this.transactionId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Destination getJMSReplyTo() {
        return this.replyTo;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setJMSReplyTo(Destination destination) {
        this.replyTo = destination;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Destination getJMSDestination() {
        return this.destination;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setJMSDestination(Destination destination) {
        this.destination = destination;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getJMSDeliveryMode() {
        return this.deliveryMode;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setJMSDeliveryMode(int i) {
        this.deliveryMode = i;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean getJMSRedelivered() {
        return this.redelivered;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setJMSRedelivered(boolean b) {
        this.redelivered = b;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJMSType() {
        return this.type;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setJMSType(String s) {
        this.type = s;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getJMSExpiration() {
        return this.expiration;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setJMSExpiration(long expiration) {
        this.expiration = expiration;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clearProperties() {
        this.id = null;
        this.transactionId = null;
        this.timestamp = 0;
        this.replyTo = null;
        this.destination = null;
        this.deliveryMode = DeliveryMode.NON_PERSISTENT;
        this.redelivered = false;
        this.type = null;
        this.expiration = 0;
    }

    @Override
    public String toString() {
        return "JMSlimMessage{" +
                "id='" + id + '\'' +
                ", transactionId='" + transactionId + '\'' +
                ", timestamp=" + timestamp +
                ", replyTo=" + replyTo +
                ", destination=" + destination +
                ", deliveryMode=" + deliveryMode +
                ", redelivered=" + redelivered +
                ", type='" + type + '\'' +
                ", expiration=" + expiration +
                ", JMSPriority=" + JMSPriority +
                ", destinationName='" + destinationName + '\'' +
                ", stringProperties=" + stringProperties +
                '}';
    }
}
