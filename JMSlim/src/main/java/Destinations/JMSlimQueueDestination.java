package Destinations;

import Consumers.JMSlimMessageConsumer;
import org.jetbrains.annotations.NotNull;

import javax.jms.Message;
import javax.jms.Queue;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Implementation of {@link Queue} extends upon {@link JMSlimDestination}
 * and implements {@link Notifier} for running locally received messages.
 */
public class JMSlimQueueDestination extends JMSlimDestination implements Queue {
    private final transient  ConcurrentLinkedQueue<Message> messageQueue;
    private final transient Notifier notifierThread;
    private transient int consumerToUse = 0;

    /**
     * Default constructor
     * @param name The name of this queue
     */
    public JMSlimQueueDestination(@NotNull String name) {
        super(name, TYPE_QUEUE);
        this.messageQueue = new ConcurrentLinkedQueue<>();
        this.notifierThread = new Notifier();
        this.notifierThread.start();
    }

    @Override
    public String getQueueName() {
        return this.getName();
    }

    /**
     * Delivers the message via {@link Notifier} to a local listener
     * @param message The message to be posted to this destination
     */
    public void put(@NotNull Message message) {
        Message msgConfigured = this.configMessage(message);

        if(null == msgConfigured) {
            LOGGER.log("Cannot send null message. Aborting.");
            return;
        }
        //enqueue message for sending
        this.messageQueue.add(message);

        //awaken notifier thread so it reads
        //the message out of the queue
        this.notifierThread.deliver();
    }

    /**
     * Notifies the next consumer in the cyclic consumer queue waiting on messages
     * in this documentation. This is a managed implementation of Competing Consumer
     * pattern.
     * @param message The message to be delivered to a consumer
     * @return Has the consumer been notified successfully
     */
    private synchronized boolean notifyNextConsumer(@NotNull Message message) {
        if(!this.noConsumers()) {
            ArrayList<ConsumerNode> consumerNodes = this.consumers;

            if(this.consumerToUse >= consumerNodes.size()) {
                this.consumerToUse = 0;
            }

            ConsumerNode consumerNode = consumerNodes.get(this.consumerToUse);
            this.consumerToUse++;

            JMSlimMessageConsumer messageConsumer = (JMSlimMessageConsumer) consumerNode.getConsumer();

            try {
                messageConsumer.onMessage(message);
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                LOGGER.log("Failed to deliver message to consumer on " + this.getName());
            }
        }
        return false;
    }

    /**
     * Check whether any consumers are registered
     * @return whether consumers are registered or not
     */
    private synchronized boolean noConsumers() {
        return 0 == consumers.size();
    }

    /**
     * Waits for a timeout and then interrupts the notifier thread
     */
    public synchronized void close() {
        try {
            wait(100);
            this.notifierThread.interrupt();

        } catch (InterruptedException interruptedException) {
            interruptedException.printStackTrace();
        }
        finally {
            this.killed = true;
        }
    }

    /**
     * Class handles notification of local message delivery
     */
    class Notifier extends Thread {

        /**
         * Unblocks client threads and delivers messages synchronously
         */
        @Override
        public void run() {
            super.run();

            try {
                LOGGER.log("Starting notifier thread for " + getQueueName());
                do {
                    boolean fQueueMessages = true;

                    while (fQueueMessages) {
                        //deliver first message from queue if available
                        //do not check with size() since this is not
                        //a constant time operation on ConcurrentLinkedQueue
                        //simply get it

                        Message message = messageQueue.peek();

                        if (null != message) {
                            //we have a message so try to deliver it
                            if (notifyNextConsumer(message)) {
                                //message has been delivered so remove it from queue
                                messageQueue.remove(message);
                            }
                            else {
                                if(message.getJMSRedelivered()) {
                                    LOGGER.error("Message failed to re-deliver. Removing it from queue");
                                    messageQueue.remove(message);
                                }
                                else {
                                    message.setJMSRedelivered(true);
                                }
                            }

                            //remove any expired messages left in the queue
                            this.removeExpiredMessages();

                            //wait and relinquish control to allow
                            //other threads to perform work before
                            //continuing to deliver next message in queue
                            sleep(10);
                        } else {
                            //we don't have a message so we can
                            //break out of the loop and wait to be
                            //asked to deliver another message
                            fQueueMessages = false;
                        }
                    }

                    //wait to be notified of new message
                    this.waitForNotification();
                } while (true);
            }
            catch (InterruptedException interruptedException) {
                Thread.currentThread().interrupt();
                LOGGER.log("Notifier thread " + this.getName() + " stopping!");
            }
            catch (Exception e) {
                LOGGER.error("Caught exception while managing destination notifications");
                e.printStackTrace();
            }
        }

        /**
         * Removes expired messages from this queue
         */
        private void removeExpiredMessages() {

            //cycle through all messages in queue
            //iterator on concurrent queue is not guaranteed to see
            //all the elements but it will at least see those that
            //were presented when it is called
            for (Message message : messageQueue) {
                //can the message expire?
                try {
                    if (0 != message.getJMSExpiration()) {

                        //message can expire so check if it has expired
                        if (message.getJMSTimestamp() + message.getJMSExpiration() >
                                (System.currentTimeMillis() / 1000)) {

                            //remove expired message
                            messageQueue.remove(message);
                        }
                    }
                }
                //failed to get some data from the message
                catch (Exception e) {
                    LOGGER.log("Failed to remove expired message");
                    LOGGER.log(e.getMessage());
                }
            }
        }

        /**
         * Wait until this thread is notified to wake up and deliver more messages
         * @throws InterruptedException Throws exception if interrupted while waiting
         */
        private synchronized void waitForNotification() throws InterruptedException {
            LOGGER.log("Notifier thread waiting");
            wait();
        }

        /**
         * Deliver all messages in queue to synchronous consumers
         */
        public synchronized void deliver() {
            try {
                //wake up notifier thread to clear input queue
                super.notify();
            }
            catch (Exception e) {
                LOGGER.error("Could not awaken notifier thread!");
                LOGGER.error(e.getMessage());
            }
        }
    }
}
