package Routing.Internals;

import Logging.JMSlimLogger;
import Destinations.JMSlimDestination;
import Destinations.JMSlimQueueDestination;
import Routing.MessageRouter;
import Utils.JMSlim;

import javax.jms.JMSException;
import javax.jms.Message;

/**
 * Internal message router implements a basic {@link MessageRouter} by
 * reading the incoming message's destination variable's id and looking-up
 * a local destination by that name
 */
public class JMSlimInternalMessageRouter extends MessageRouter {
    private final JMSlimLogger logger = JMSlimLogger.createLogger();

    /**
     * Route the message to the appropriate local destination
     * @param message The message to be routed
     */
    @Override
    public void routeMessageToDestination(Message message) {
        try {
            this.logger.log("Router got " + message.getJMSMessageID());
            JMSlim jmslim = new JMSlim();

            //get local destinationq
            JMSlimDestination messageDestination  = (JMSlimDestination) message.getJMSDestination();
            JMSlimQueueDestination destination
                    = (JMSlimQueueDestination) jmslim.lookup(messageDestination.getId());

            if(null != destination) {
                //write to local destination
                destination.put(message);
            }
            else {
                logger.log("No destination with id " + ((JMSlimDestination) message.getJMSDestination()).getId());
            }
        } catch (JMSException e) {
            this.logger.error("Could not route message internal exception thrown");
            this.logger.log(e.getMessage());
        }
    }
}

