package Routing.Internals;

import Routing.MessageRouter;
import Routing.MessageRouterFactory;

/**
 * Implements {@link MessageRouterFactory} in a generic way
 */
public class JMSlimInternalMessageRouterFactory extends MessageRouterFactory {
    /**
     * Instantiates new internal router
     * @return instance of {@link JMSlimInternalMessageRouter}
     */
    @Override
    public MessageRouter create() {
        return new JMSlimInternalMessageRouter();
    }
}
