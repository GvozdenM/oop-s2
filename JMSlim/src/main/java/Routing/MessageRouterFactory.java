package Routing;

/**
 * Abstract
 */
public abstract class MessageRouterFactory {
    public abstract MessageRouter create();
}
