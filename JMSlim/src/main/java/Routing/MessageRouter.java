package Routing;

import javax.jms.Message;

/**
 * Abstraction for a message router. Message routers are
 * used internally to route messages to the correct destination
 */
public abstract class MessageRouter {
    /**
     * Route a message to its given destination
     * @param message The message to route
     */
    public abstract void routeMessageToDestination(Message message);
}
