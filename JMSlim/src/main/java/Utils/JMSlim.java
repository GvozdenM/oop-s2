package Utils;

import Connection.JMSlimConnectionFactory;
import Logging.JMSlimLogger;
import Destinations.JMSlimDestination;
import Destinations.JMSlimQueueDestination;

import javax.jms.Destination;
import java.io.Closeable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

/**
 * JMSlim singleton class servers as the entry point to
 * JMSlim JMS provider. It simulates the functionality of
 * a JDNI server with little overhead in order to simplify
 * the provider implementation.
 */
public class JMSlim implements Closeable {
    private static JMSlim INSTANCE = null;
    private static final JMSlimLogger LOGGER = JMSlimLogger.createLogger();
    private static final Map<String, JMSlimQueueDestination> queues = new ConcurrentHashMap<>();


    /**
     * Basic constructor instantiates concurrent hashmap to allow
     * multiple threads access to destinations
     */
    public JMSlim() { }
    /**
     * Lazy load the JMSlim singleton instance
     * @return Instance of JMSlim either newly or previously created
     */
    public static JMSlim getJMSlim() {
        if(null == INSTANCE) {
            INSTANCE = new JMSlim();
            LOGGER.log("Create JMSlim singleton instance");
        }

        return INSTANCE;
    }

    /**
     * Generates a message ID for a given destination
     * @param destination Destination for which to generate an ID
     * @return The generated ID
     */
    public static String generateMessageId(JMSlimDestination destination) {
        return "<" +
                destination.getType() +
                "/" +
                destination.getName() +
                "@" +
                System.currentTimeMillis() +
                ">";
    }

    /**
     * Lookup method simulates {@link javax.naming.Context}
     * without the need to implement a JDNI provider
     * @param name Name of object to be retrieved
     * @return A connection factory or destination matching the given name or null if none is found
     */
    public Object lookup(String name) {
        LOGGER.log("Looking up JMSlim object " + name);

        if(name.equals("ConnectionFactory")) {
            LOGGER.log("Successfully looked up connection factory");
            return new JMSlimConnectionFactory();
        }

        if(queues.containsKey(name)) {
            LOGGER.log("Successfully looked up queue " + name);
            return queues.get(name);
        }

        return null;
    }

    /**
     * Factory method returns instance of an existing
     * queue with the name given or creates a new queue
     * and registers it as a JMSlim destination
     * @param name Name of the queue to create
     * @return Destination registered as a JMSlim queue
     */
    public Destination createQueue(String name) {

        LOGGER.log("");
        if(queues.containsKey(name)) {
            return queues.get(name);
        }

        LOGGER.log("Creating queue " + name);
        JMSlimQueueDestination queue = new JMSlimQueueDestination(name);
        queues.put(queue.getId(), queue);
        return queue;
    }

    /**
     * Closes all registered destinations
     */
    @Override
    public void close() {
        LOGGER.log("Stopping JMSlim messaging destinations");
        for(JMSlimQueueDestination queue : queues.values()) {
            queue.close();
        }
    }
}
