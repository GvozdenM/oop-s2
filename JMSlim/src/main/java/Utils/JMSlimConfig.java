package Utils;

import Utils.Views.ResourceFetcher;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class JMSlimConfig {
    private static final String CONFIG_FILE = "JMSlimConfig.json";
    private static final String LOCAL_PORT = "13377";
    private static final String LOCAL_ADDRESS = "127.0.0.1";
    private static final String ADDRESS_KEY = "address";
    private static final String PORT_KEY = "port";

    private final Gson gson = new GsonBuilder().setPrettyPrinting().create();;
    private Map<String, String> configuration;

    public JMSlimConfig() {
        try {
            load();
        }
        catch (IOException e) {
            this.configuration = new HashMap<>();
            this.configuration.put(ADDRESS_KEY, LOCAL_ADDRESS);
            this.configuration.put(PORT_KEY, LOCAL_PORT);
        }
    }

    public int getServerPort() {
        return Integer.parseInt(this.configuration.getOrDefault(PORT_KEY, LOCAL_PORT));
    }

    public String getServerAddress() {
        return this.configuration.getOrDefault(ADDRESS_KEY, LOCAL_ADDRESS);
    }

    private void load() throws IOException {
        ResourceFetcher resourceFetcher = new ResourceFetcher(CONFIG_FILE);

        try(FileReader reader = new FileReader(resourceFetcher.getResourceFile())) {
            Type type = new TypeToken<Map<String, String>>(){}.getType();
            this.configuration = this.gson.fromJson(reader, type);
        }
    }
}
