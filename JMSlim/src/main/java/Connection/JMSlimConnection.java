package Connection;

import Logging.JMSlimLogger;
import Messages.JMSlimMessage;
import Networking.SocketManager;
import Networking.SocketManagerFactory;
import Routing.MessageRouter;
import Routing.MessageRouterFactory;
import Utils.JMSlimConfig;

import javax.jms.*;
import java.io.IOException;
import java.net.Socket;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Implementation of Java Messaging System {@link Connection}
 * provides a multi-threaded context for sending and receiving messages.
 */
public class JMSlimConnection implements Connection {
    protected final JMSlimLogger logger = JMSlimLogger.createLogger();
    protected final ReentrantLock lock = new ReentrantLock(true);
    protected final MessageRouter messageRouter;
    protected final JMSlimConfig jmSlimConfig;
    protected final SocketManagerFactory socketManagerFactory;
    protected final Collection<Session> sessions;
    protected String clientId;
    protected boolean initiated = false;
    protected boolean started = false;
    protected SocketManager socketManager;

    /**
     * Basic constructor for internal use by JMSlim provides the connection
     * with an interface for instantiating the internal {@link MessageRouter}
     * and a {@link SocketManager}
     * @param socketManagerFactory Interface for creating a {@link SocketManager}
     * @param messageRouterFactory Interface for creating a {@link MessageRouter}
     * @param jmSlimConfig Interface for retrieving config data for the location of
     *                     the messaging server
     */
    public JMSlimConnection(SocketManagerFactory socketManagerFactory,
                            MessageRouterFactory messageRouterFactory,
                            JMSlimConfig jmSlimConfig) {
        this.sessions = new ConcurrentLinkedQueue<>();
        this.jmSlimConfig = jmSlimConfig;
        this.socketManagerFactory = socketManagerFactory;
        this.messageRouter = messageRouterFactory.create();
    }

    /**
     * Creates a session bound to this connection
     * @param transact Property unimplemented
     * @param ackMode Acknowledge mode
     * @return New instance of {@link Session} bound to this
     * connection.
     */
    @Override
    public Session createSession(boolean transact, int ackMode) {
        Session session;

        this.lock.lock();
        try {
            session = new JMSlimSession(this, transact, ackMode);
            this.sessions.add(session);
            this.logger.log("Started new session");
        }
        finally {
            this.lock.unlock();
        }

        return session;
    }

    /**
     * Return this client id
     * @return The client id
     **/
    @Override
    public String getClientID() {
        this.lock.lock();
        try {
            if(null == this.clientId) {
                this.logger.log("Generating clientId");
                this.clientId = this.generateClientId();
                return this.clientId;
            }

            return this.clientId;
        }
        finally {
            this.lock.unlock();
        }
    }

    /**
     * Setter for overriding default clientId. Must be used before connection
     * is started.
     * @param clientId The custom client id
     */
    @Override
    public void setClientID(String clientId) {
        this.lock.lock();
        this.clientId = clientId;
        this.lock.unlock();
    }

    /**
     * Start the connection
     * @throws JMSException If starting connection failed on JMSlim side
     */
    @Override
    public void start() throws JMSException {
        this.lock.lock();

        try {

            //has been previously started
            //and temporarily suspended using stop()
            //so we simply re-start
            if(this.initiated) {
                this.started = true;
                return;
            }

            //try connecting ot messaging server
            this.connectToMessagingServer();
            this.started = true;

        } catch (IOException e) {
            this.logger.log("Caught IOException while starting JMSlimConnection. Retrying...");

            //retry connecting to server
            try {
                this.connectToMessagingServer();
            } catch (IOException ioException) {
                this.logger.error("Cannot start JMSlimConnection to messaging server. Caught IOException.");
                throw new JMSException("Couldn't connect to messaging server");
            }

            //succeeded
            this.started = true;
            this.initiated = true;

        } finally {
            this.lock.unlock();
        }
    }

    /**
     * Tries to create socket connection to messaging server and wraps it inside
     * a socket manager.
     * @return SocketManager instance connected to the messaging server
     * @throws IOException Rethrown if socket fails to connect
     */
    private SocketManager tryConfigureSocketManager() throws IOException {
        this.socketManager = this.socketManagerFactory.create(
                new Socket(jmSlimConfig.getServerAddress(), jmSlimConfig.getServerPort())
        );

        this.logger.log("Instantiated socket manager");

        //set internal message router as listener for incoming objects
        //on socket manager
        this.socketManager.setOnReceived((o) -> {
            if(o instanceof Message) {
                this.messageRouter.routeMessageToDestination((Message) o);
            }
        });

        return this.socketManager;
    }

    /**
     * Connects socket to messaging server and starts the socket manager
     * @throws IOException Thrown if socket internally fails to connect
     * @throws JMSException Thrown if clientId can not be retrieved for whatever reason
     */
    private void connectToMessagingServer() throws IOException, JMSException {
        if(null == this.socketManager) {
            this.socketManager = this.tryConfigureSocketManager();
        }
        this.socketManager.start();
        this.socketManager.send(this.getClientID());
        this.logger.log("Connected to messaging server");
    }

    public void deliver(Message configuredMessage) {
        this.lock.lock();
        if(this.started) {
            ((JMSlimMessage) configuredMessage).setStringProperty("originatorId", this.getClientID());
            this.socketManager.send(configuredMessage);
        }
        else {
            this.logger.error("Cannot deliver message! Connection is not open. Ignoring you!");
        }
        this.lock.unlock();
    }

    /**
     * Temporarily stops the connection but does not free
     * allocated resources.
     */
    @Override
    public void stop() {
        this.lock.lock();
        this.started = false;
        this.lock.unlock();
    }

    /**
     * Stops the connection and releases the resources allocated to it including
     * closing the underlying socket connection.
     */
    @Override
    public void close() {
        this.lock.lock();

        this.logger.log("Starting to close connection");
        this.stop();

        try {

            //closing all active session
            this.logger.log("Closing sessions");
            for(Session session : this.sessions) {
                try {
                    session.close();
                }
                catch (JMSException e) {
                    this.logger.error("Failed to close a session!");
                }
            }

            this.socketManager.close();
        }
        catch (Exception ignored) {}
        finally {
            this.started = false;
            this.lock.unlock();
        }
    }

    /**
     * Generates client id if SocketManager has been initiated
     * @return Empty string if SocketManager is not initiated or clientId
     */
    private String generateClientId() {
        if(null != this.socketManager) {
            return "<" +
                    new Timestamp(System.currentTimeMillis()).toString() +
                    "@" +
                    this.socketManager.getLocalAddress() +
                    ":" +
                    this.socketManager.getLocalPort() +
                    ">";
        }

        return "";
    }

    @Override
    public ConnectionConsumer createConnectionConsumer(Destination destination,
                                                       String s,
                                                       ServerSessionPool serverSessionPool,
                                                       int i) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public Session createSession(int i) throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public Session createSession() throws JMSException {
        Session session;

        this.lock.lock();
        try {
            session = new JMSlimSession(this, false, Session.AUTO_ACKNOWLEDGE);
            this.sessions.add(session);
            this.logger.log("Started new session");
        }
        finally {
            this.lock.unlock();
        }

        return session;
    }

    @Override
    public ConnectionConsumer createSharedConnectionConsumer(Topic topic, String s, String s1, ServerSessionPool serverSessionPool, int i) throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public ConnectionMetaData getMetaData() {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public ExceptionListener getExceptionListener() {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public void setExceptionListener(ExceptionListener exceptionListener) {
        throw new RuntimeException("Unimplemented");
    }


    @Override
    public ConnectionConsumer createDurableConnectionConsumer(Topic topic,
                                                              String s,
                                                              String s1,
                                                              ServerSessionPool serverSessionPool,
                                                              int i) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public ConnectionConsumer createSharedDurableConnectionConsumer(Topic topic, String s, String s1, ServerSessionPool serverSessionPool, int i) throws JMSException {
        throw new JMSException("Unimplemented");
    }
}
