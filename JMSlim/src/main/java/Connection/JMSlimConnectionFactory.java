package Connection;

import Networking.concurrent.LowThroughPutConcurrentSocketManagerFactory;
import Routing.Internals.JMSlimInternalMessageRouterFactory;
import Utils.JMSlimConfig;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;
import java.io.IOException;

/**
 * Implementation of {@link ConnectionFactory} for JMSlim.
 * Methods create un-connected instance of JMSlimConnection.
 */
public class JMSlimConnectionFactory implements ConnectionFactory {

    /**
     * Create new connection with low throughput socket manager
     * @return Connection implemented by {@link JMSlimConnection}
     */
    @Override
    public Connection createConnection() {
        return new JMSlimConnection(
                new LowThroughPutConcurrentSocketManagerFactory(),
                new JMSlimInternalMessageRouterFactory(),
                new JMSlimConfig()
        );
    }

    @Override
    public Connection createConnection(String s, String s1) {
        return this.createConnection();
    }

    @Override
    public JMSContext createContext() {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public JMSContext createContext(String s, String s1) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public JMSContext createContext(String s, String s1, int i) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public JMSContext createContext(int i) {
        throw new RuntimeException("Unimplemented");
    }
}
