package Connection;

import Logging.JMSlimLogger;
import Consumers.JMSlimMessageConsumer;
import Destinations.JMSlimDestination;
import Messages.JMSlimObjectMessage;
import Producers.JMSlimMessageProducer;
import org.jetbrains.annotations.NotNull;

import javax.jms.*;
import java.io.Serializable;

/**
 * JMSlim implementation of {@link Session} provides a context for creating
 * messages, consumers and producers.
 */
public class JMSlimSession implements Session {
    protected final JMSlimLogger logger = JMSlimLogger.createLogger();
    protected boolean active = true;
    protected int ackMode;
    protected Connection connection;

    /**
     * Default constructor
     * @param jmSlimConnection JMSlim connection this session uses
     * @param transact unimplemented
     * @param ackMode unimplemented
     */
    public JMSlimSession(@NotNull JMSlimConnection jmSlimConnection, boolean transact, int ackMode) {
        this.connection = jmSlimConnection;
        this.ackMode = ackMode;
    }

    /**
     * Create a message consumer for the given destination using this
     * session's internal connection.
     * @param destination The destination to create teh consumer for
     * @return A message consumer
     * @throws JMSException Thrown if connection or session are closed
     */
    @Override
    public MessageConsumer createConsumer(@NotNull Destination destination) throws JMSException {
        return createConsumer(destination, null, false);
    }

    /**
     * Create a connection consumer for the given destination and message
     * selector
     * @param destination The destination to create a consumer for
     * @param selector Message selector to be given to the consumer
     * @return A configured message consumer
     * @throws JMSException Thrown if connection or session are closed
     */
    @Override
    public MessageConsumer createConsumer(@NotNull Destination destination, String selector) throws JMSException {
        return createConsumer(destination, selector, false);
    }

    /**
     * Create a connection consumer for the given destination and message
     * selector and sets whether to accept local messages (currently ignored)
     * @param destination The destination to create a consumer for
     * @param selector Message selector to be given to the consumer
     * @return A configured message consumer
     * @throws JMSException Thrown if connection or session are closed
     */
    @Override
    public MessageConsumer createConsumer(@NotNull Destination destination, String selector, boolean noLocal) throws JMSException {
        this.logger.log("Session is creating message consumer");


        if(!active) {
            logger.error("Session is closed cannot create consumer");
            throw new JMSException("Session is closed cannot create consumer");
        }
        else {
            if(destination instanceof Queue) {
                MessageConsumer consumer = new JMSlimMessageConsumer(destination, selector);
                ((JMSlimDestination) destination).addConsumer(consumer, this.connection);
                return consumer;
            }
        }
        logger.error("Destination cannot be null");
        throw new JMSException("Destination is not queue");
    }

    /**
     * Create a producer for the given destination using the
     * sessions internal connection
         * @param destination Destination to create consumer for
     * @return A MessageProducer
     * @throws JMSException Thrown if we connection is closed
     */
    @Override
    public MessageProducer createProducer(@NotNull Destination destination) throws JMSException {
        MessageProducer producer = new JMSlimMessageProducer(destination);
        ((JMSlimDestination) destination).addProducer(producer, this.connection);
        return producer;
    }

    @Override
    public ObjectMessage createObjectMessage() {
        return new JMSlimObjectMessage();
    }

    @Override
    public BytesMessage createBytesMessage() throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public MapMessage createMapMessage() throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public Message createMessage() throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public ObjectMessage createObjectMessage(Serializable serializable) throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public StreamMessage createStreamMessage() throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public TextMessage createTextMessage() throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public TextMessage createTextMessage(String s) throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public boolean getTransacted() throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public int getAcknowledgeMode() throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public void commit() throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public void rollback() throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public void close() throws JMSException {
        this.active = false;
    }

    @Override
    public void recover() throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public MessageListener getMessageListener() throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public void setMessageListener(MessageListener messageListener) throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public void run() {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public Queue createQueue(String s) throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public Topic createTopic(String s) throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public TopicSubscriber createDurableSubscriber(Topic topic, String s) throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public TopicSubscriber createDurableSubscriber(Topic topic, String s, String s1, boolean b) throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public MessageConsumer createDurableConsumer(Topic topic, String s) throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public MessageConsumer createDurableConsumer(Topic topic, String s, String s1, boolean b) throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public MessageConsumer createSharedDurableConsumer(Topic topic, String s) throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public MessageConsumer createSharedDurableConsumer(Topic topic, String s, String s1) throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public QueueBrowser createBrowser(Queue queue) throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public QueueBrowser createBrowser(Queue queue, String s) throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public TemporaryQueue createTemporaryQueue() throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public TemporaryTopic createTemporaryTopic() throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public MessageConsumer createSharedConsumer(Topic topic, String s) throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public MessageConsumer createSharedConsumer(Topic topic, String s, String s1) throws JMSException {
        throw new JMSException("Unimplemented");
    }

    @Override
    public void unsubscribe(String s) throws JMSException {
        throw new JMSException("Unimplemented");
    }


}
