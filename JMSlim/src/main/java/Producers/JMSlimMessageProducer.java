package Producers;

import Destinations.JMSlimDestination;
import Logging.JMSlimLogger;
import Destinations.JMSlimDestination;
import Destinations.JMSlimQueueDestination;
import Messages.JMSlimMessage;
import Utils.JMSlim;
import org.jetbrains.annotations.NotNull;

import javax.jms.*;

/**
 * JMSlim implementation of {@link MessageProducer}
 */
public class JMSlimMessageProducer implements MessageProducer {
    protected final JMSlimLogger logger = JMSlimLogger.createLogger();
    protected int deliveryMode = DeliveryMode.NON_PERSISTENT;
    protected int priority = Message.DEFAULT_PRIORITY;
    protected long timeToLive = Message.DEFAULT_TIME_TO_LIVE;
    protected boolean disableMessageID = false;
    protected boolean disableMessageTimestamp = false;
    protected Destination destination;

    /**
     * Default constructor
     * @param destination The remote destination this producer will send messages to
     */
    public JMSlimMessageProducer(@NotNull Destination destination) {
        this.destination = destination;
    }

    /**
     * Method used internally to configure a message before it is sent
     * @param message Message to be configured
     * @param deliveryMode Delivery mode
     * @param priority Message priority
     * @param timeToLive Time between now and when the message expires
     * @return The input message, but configured
     * @throws JMSException Rethrown from {@link JMSlimMessage}
     */
    private Message configureMessage(@NotNull Message message,
                                     int deliveryMode,
                                     int priority,
                                     long timeToLive) throws JMSException {
        //set header fields
        message.setJMSExpiration(timeToLive);
        message.setJMSPriority(priority);
        message.setJMSDeliveryMode(deliveryMode);

        if(!this.disableMessageTimestamp) {
            message.setJMSTimestamp(System.currentTimeMillis() / 1000);
        }

        if(!this.disableMessageID) {
            message.setJMSMessageID(JMSlim.generateMessageId((JMSlimDestination) destination));
        }

        return message;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void send(@NotNull Message message) {
        this.send(this.destination, message, this.deliveryMode, this.priority, this.timeToLive);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void send(@NotNull Message message, int deliveryMode, int priority, long timeToLive) {
        this.send(destination, message, deliveryMode, priority, timeToLive);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void send(@NotNull Destination destination, @NotNull Message message) {
        this.send(destination, message, this.deliveryMode, this.priority, this.timeToLive);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void send(@NotNull Destination destination,
                     @NotNull Message message,
                     int deliveryMode,
                     int priority,
                     long timeToLive) {
        try {
            Message configuredMessage = configureMessage(
                    message,
                    deliveryMode,
                    priority,
                    timeToLive
            );

            if (destination instanceof Queue) {
                JMSlimQueueDestination queue = (JMSlimQueueDestination) destination;
                queue.putToRemote(configuredMessage, this);
            } else {
                logger.error("Topics are not implemented!");
                throw new JMSException("Not queue fuck you");
            }
        }
        catch (Exception e) {
            logger.error("Couldn't send message");
            logger.error(e.getMessage());
        }
    }

    @Override
    public void send(Message message, CompletionListener completionListener) throws JMSException {

    }

    @Override
    public void send(Message message, int i, int i1, long l, CompletionListener completionListener) throws JMSException {

    }

    @Override
    public void send(Destination destination, Message message, CompletionListener completionListener) throws JMSException {

    }

    @Override
    public void send(Destination destination, Message message, int i, int i1, long l, CompletionListener completionListener) throws JMSException {

    }

    public Destination getDestination()
    {
        return destination;
    }

    public int getDeliveryMode()
    {
        // Gets the producer's default delivery mode.
        return deliveryMode;
    }

    public boolean getDisableMessageID()
    {
        // Gets an indication of whether message IDs are disabled.
        return disableMessageID;
    }

    public boolean getDisableMessageTimestamp()
    {
        // Gets an indication of whether message timestamps are disabled.
        return disableMessageTimestamp;
    }

    public int getPriority()
    {
        // Gets the producer's default priority.
        return priority;
    }

    public long getTimeToLive()
    {
        // Gets the default length of time in milliseconds from its dispatch
        // time that a produced message should be retained by the message system
        return timeToLive;
    }

    @Override
    public void setDeliveryDelay(long l) throws JMSException {

    }

    @Override
    public long getDeliveryDelay() throws JMSException {
        return 0;
    }

    public void setDeliveryMode(int deliveryMode)
    {
        // Sets the producer's default delivery mode.
        this.deliveryMode = deliveryMode;
    }

    public void setDisableMessageID(boolean value)
    {
        // Sets whether message IDs are disabled.
        this.disableMessageID = value;
    }

    public void setDisableMessageTimestamp(boolean value)
    {
        // Sets whether message timestamps are disabled.
        this.disableMessageTimestamp = value;
    }

    public void setPriority(int priority)
    {
        // Sets the producer's default priority.
        this.priority = priority;
    }

    public void setTimeToLive(long timeToLive)
    {
        // Sets the default length of time in milliseconds from its dispatch
        // time that a produced message should be retained by the message system.
        this.timeToLive = timeToLive;
    }

    /**
     * {@inheritDoc}
     */
    public void close()  throws JMSException
    {
        // Closes the message producer.
        logger.log("JMSLightMessageProducer - closing MessageProducer");
        ((JMSlimDestination)destination).removeProducer(this);
        destination = null;
    }
}
