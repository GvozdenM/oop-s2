package TopUsersPresenter;

import TopUsersController.TopUsersPresenter;

public abstract class AbstractTopUsersViewFactory {
    public abstract TopUsersView create(TopUsersPresenter presenter);
}
