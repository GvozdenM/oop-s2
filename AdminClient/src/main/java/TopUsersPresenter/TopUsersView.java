package TopUsersPresenter;

import Utils.Swing.ListView.DefaultListView.DefaultListViewModel;
import Utils.Views.TransitionView;

/**
 * This interface provides the functions and methods to be implemented by the Swing TopUsersView.
 */
public interface TopUsersView extends TransitionView {
    /**
     * The method is called when there is a user list to be inserted in the view, corresponding to the list of
     * most liked users
     * @param defaultListViewModel
     */
    void displayUsersList(DefaultListViewModel defaultListViewModel);

    /**
     * This method is called whenever there is an warning message to be displayed
     * @param msg
     */
    void displayDialogMsg(String msg);
}
