package TopUsersPresenter;

import TopUsersController.AbstractTopUsersPresenterFactory;
import TopUsersController.TopUsersPresenter;
import TopUsersView.TopUsersViewFactory;

public class TopUsersPresenterFactory extends AbstractTopUsersPresenterFactory {
    @Override
    public TopUsersPresenter create() {
        return new TopUsersScreenPresenter(
                new TopUsersViewFactory()
        );
    }
}
