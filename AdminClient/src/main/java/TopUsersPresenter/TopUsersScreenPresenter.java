package TopUsersPresenter;

import TopUsersInteractor.TopUser;
import TopUsersInteractor.TopUsersResponse;
import TopUsersController.TopUsersPresenter;
import Utils.Swing.ListView.DefaultListView.DefaultListViewModel;

/**
 * The Top Users Presenter is in charge of delegating requests from the attached TopUsersController
 * in order to operate the view externally.
 * This class implements the methods and functions of its respective interface.
 * @see TopUsersPresenter
 */
public class TopUsersScreenPresenter implements TopUsersPresenter {
    private final TopUsersView view;

    public TopUsersScreenPresenter(AbstractTopUsersViewFactory viewFactory) {
        this.view = viewFactory.create(this);
    }

    @Override
    public void init() {
        this.view.onTransitionTo();
    }

    @Override
    public void presentTopUsersList(TopUsersResponse response) {
        DefaultListViewModel defaultListViewModel = new DefaultListViewModel();
        for (TopUser user : response.getUsers()) {
            defaultListViewModel.addItem(
                    defaultListViewModel.createItem(
                            user.username,
                            "Age: " + user.age + "    Likes: " + user.likes,
                            user.imgUrl
                    )
            );
        }

        this.view.displayUsersList(defaultListViewModel);
    }

    @Override
    public void presentNoConnection() {
        this.view.displayDialogMsg("ERROR: Could not contact server successfully");
    }
}
