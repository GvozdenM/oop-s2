package TopUsersView;

import Utils.Swing.FrameManager.FrameManager;

public interface TopUsersFrameManagerFactory {
    FrameManager frameManager = FrameManager.newInstance();

    static FrameManager create() {
        return frameManager;
    }
}
