package TopUsersView;

import TopUsersController.TopUsersControllerFactory;
import TopUsersController.TopUsersControllerI;
import TopUsersPresenter.TopUsersView;
import Utils.Swing.FrameManager.FrameManager;
import Utils.Swing.ListView.DefaultListView.DefaultListViewModel;
import Utils.Swing.ListView.ListView;
import Utils.Views.ResourceFetcher;

import javax.swing.*;
import java.io.IOException;

/**
 * This class provides the main JPanel in TopUsers View as well as attachers of parts like listeners and different
 * methods and functions for the Top Users Use Case
 */
public class SwingTopUsersView extends JPanel implements TopUsersView {
    private final TopUsersControllerI topUsersControllerI;

    private JPanel header;

    public SwingTopUsersView(TopUsersControllerFactory controllerFactory) {
        this.topUsersControllerI = controllerFactory.create();
    }

    private void configureHeader() {
        this.header = new JPanel();
        JLabel headerTitle = new JLabel("Top 5 matched users:");
        this.header.add(headerTitle);
    }

    @Override
    public void onTransitionTo() {

        configureHeader();

        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        ResourceFetcher rf = new ResourceFetcher("favicon.jpg");

        FrameManager frameManager = TopUsersFrameManagerFactory.create();
        frameManager
                .configureFramePreferences("Top 5 Users", this, 400, 800)
                .setResizable(false)
                .setSpawnRelativeTo(FrameManager.CENTER)
                .setExitOnClose(false);

        try {
            frameManager.setFavicon(rf.getResourceFile().getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.topUsersControllerI.onLoad();
    }

    @Override
    public void displayUsersList(DefaultListViewModel defaultListViewModel) {
        this.removeAll();

        this.add(this.header);

        ListView topUsersList = ListView.builder()
                .setDefaultViewModel(defaultListViewModel)
                .build();

        this.add(topUsersList);

        this.revalidate();
    }

    @Override
    public void displayDialogMsg(String msg) {
        SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(
                new JFrame(),
                msg,
                "Warning",
                JOptionPane.WARNING_MESSAGE
        ));
    }
}
