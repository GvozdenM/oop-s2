package TopUsersView;

import TopUsersController.TopUsersControllerFactory;
import TopUsersController.TopUsersPresenter;
import TopUsersPresenter.AbstractTopUsersViewFactory;
import TopUsersPresenter.TopUsersView;

public class TopUsersViewFactory extends AbstractTopUsersViewFactory {
    @Override
    public TopUsersView create(TopUsersPresenter presenter) {
        return new SwingTopUsersView(
                new TopUsersControllerFactory(presenter)
        );
    }
}
