package MessagingEndpoint;

import MatchGraphInteractor.MatchGraphRequester;
import MatchGraphInteractor.MatchGraphRequesterFactory;
import MatchGraphInteractor.MatchGraphResponse;
import MessagingEndpoint.MessageRequesters.MatchGraphMessageRequester;
import MessagingEndpoint.MessageRequesters.TopUsersMessageRequester;
import TopUsersInteractor.TopUsersRequest;
import TopUsersInteractor.TopUsersRequester;
import TopUsersInteractor.TopUsersRequesterFactory;
import TopUsersInteractor.TopUsersResponse;
import Utils.JMSlim;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

public class AdminClientEndpoint implements
        MatchGraphRequesterFactory<MatchGraphResponse>,
        TopUsersRequesterFactory<TopUsersRequest, TopUsersResponse> {
    private static AdminClientEndpoint ENDPOINT = null;
    private Connection connection;

    private AdminClientEndpoint() {
        try {
            JMSlim JMSLIM = JMSlim.getJMSlim();
            this.connection = ((ConnectionFactory) JMSLIM.lookup("ConnectionFactory")).createConnection();

            this.initIncomingQueues(this.connection);
            this.initOutgoingQueues();
            this.connection.start();
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    private void initIncomingQueues(Connection connection) {
        JMSlim jmSlim = JMSlim.getJMSlim();

        jmSlim.createQueue("matchGraphResponses");
        jmSlim.createQueue("topUsersResponses");
    }

    private void  initOutgoingQueues() {
        JMSlim jmSlim = JMSlim.getJMSlim();

        jmSlim.createQueue("matchGraphRequests");
        jmSlim.createQueue("topUsersRequests");
    }

    public static AdminClientEndpoint getInstance() {
        if(null == ENDPOINT) {
            ENDPOINT = new AdminClientEndpoint();
        }

        return ENDPOINT;
    }

    @Override
    public MatchGraphRequester<MatchGraphResponse> createMatchGraphRequester() {
        return new MatchGraphMessageRequester(this.connection);
    }

    @Override
    public TopUsersRequester<TopUsersRequest, TopUsersResponse> create() {
        return new TopUsersMessageRequester(this.connection);
    }
}
