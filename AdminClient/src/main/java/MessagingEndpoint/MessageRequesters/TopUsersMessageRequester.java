package MessagingEndpoint.MessageRequesters;

import TopUsersInteractor.TopUsersRequest;
import TopUsersInteractor.TopUsersRequester;
import TopUsersInteractor.TopUsersResponse;
import Utils.Callbacks.CallbackAdapter;
import Utils.MessageRequester;

import javax.jms.Connection;
import javax.jms.JMSException;

public class TopUsersMessageRequester
        extends MessageRequester
        implements TopUsersRequester<TopUsersRequest, TopUsersResponse> {
    public TopUsersMessageRequester(Connection connection) {
        super(connection);
    }

    @Override
    public void getTopUsers(TopUsersRequest request, CallbackAdapter<TopUsersResponse> callback) {
        try {
            //start listening for responses
            this.listenForMessagesOn("queue/topUsersResponses", callback);

            //send request
            this.makeRequestViaMessage(
                    request,
                    "queue/topUsersRequests",
                    "queue/topUsersResponses"
            );
        } catch (JMSException e) {
            callback.onException(e);
        }
    }
}
