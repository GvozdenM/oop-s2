package MessagingEndpoint.MessageRequesters;

import MatchGraphInteractor.MatchGraphRequester;
import MatchGraphInteractor.MatchGraphResponse;
import Utils.Callbacks.CallbackAdapter;
import Utils.MessageRequester;

import javax.jms.Connection;
import javax.jms.JMSException;

public class MatchGraphMessageRequester extends MessageRequester implements MatchGraphRequester<MatchGraphResponse> {
    public MatchGraphMessageRequester(Connection connection) {
        super(connection);
    }

    @Override
    public void graph(CallbackAdapter<MatchGraphResponse> callback) {
        try {
            //start listening for responses
            this.listenForMessagesOn("queue/matchGraphResponses", callback);

            //send request
            this.makeRequestViaMessage(
                    "request",
                    "queue/matchGraphRequests",
                    "queue/matchGraphResponses"
            );

        } catch (JMSException e) {
            callback.onException(e);
        }
    }
}
