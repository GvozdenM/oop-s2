package ControlPanelPresenter;

import ControlPanelController.AbstractControlPanelPresenterFactory;
import ControlPanelController.ControlPanelPresenter;
import ControlPanelView.ControlPanelViewFactory;

public class ControlPanelPresenterFactory extends AbstractControlPanelPresenterFactory {

    @Override
    public ControlPanelPresenter create() {
        return new ControlPanelScreenPresenter(
                new ControlPanelViewFactory()
        );
    }
}
