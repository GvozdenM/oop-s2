package ControlPanelPresenter;

import Utils.Views.TransitionView;

/**
 * This interface provides the functions and methods to be implemented by the Swing ControlPanelView.
 */
public interface ControlPanelView extends TransitionView {
}
