package ControlPanelPresenter;

import ControlPanelController.ControlPanelPresenter;

public abstract class AbstractControlPanelViewFactory {
    public abstract ControlPanelView create(ControlPanelPresenter presenter);
}
