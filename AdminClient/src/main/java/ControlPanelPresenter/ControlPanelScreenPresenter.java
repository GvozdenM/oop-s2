package ControlPanelPresenter;

import ControlPanelController.ControlPanelPresenter;

/**
 * The ControlPanel Presenter is in charge of delegating requests from the attached ControlPanelController
 * in order to operate the view externally.
 * This class implements the methods and functions of its respective interface.
 * @see ControlPanelPresenter
 */
public class ControlPanelScreenPresenter implements ControlPanelPresenter {
    private final ControlPanelView view;

    public ControlPanelScreenPresenter(AbstractControlPanelViewFactory viewFactory) {
        this.view = viewFactory.create(this);
    }

    @Override
    public void init() {
        this.view.onTransitionTo();
    }
}
