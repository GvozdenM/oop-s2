package ControlPanelView;

import ControlPanelController.ControlPanelControllerFactory;
import ControlPanelController.ControlPanelPresenter;
import ControlPanelPresenter.AbstractControlPanelViewFactory;
import ControlPanelPresenter.ControlPanelView;

public class ControlPanelViewFactory extends AbstractControlPanelViewFactory {

    @Override
    public ControlPanelView create(ControlPanelPresenter presenter) {
        return new SwingControlPanelView(
                new ControlPanelControllerFactory(presenter)
        );
    }
}
