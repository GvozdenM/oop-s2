package ControlPanelView;

import Utils.Swing.FrameManager.FrameManager;

public interface ControlPanelFrameManagerFactory {
    FrameManager frameManager = FrameManager.newInstance();

    static FrameManager create() {
        return frameManager;
    }
}
