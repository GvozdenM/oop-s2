package ControlPanelView;

import ControlPanelController.ControlPanelControllerI;
import ControlPanelPresenter.ControlPanelView;
import Utils.Swing.FrameManager.FrameManager;
import Utils.Views.ResourceFetcher;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

/**
 * This class provides the main JPanel in ControlPanel View as well as attachers of parts like listeners and different
 * methods and functions for the ControlPanel
 */
public class SwingControlPanelView extends JPanel implements ControlPanelView {
    private final ControlPanelControllerI listenerI;

    private JButton matchGraphBtn;
    private JButton topUsersBtn;

    public SwingControlPanelView(AbstractControlPanelControllerFactory controllerFactory) {
        this.listenerI = controllerFactory.create();
        configurePanel();
        attachListeners();
    }

    private void attachListeners() {
        this.matchGraphBtn.addActionListener(l -> listenerI.onMatchGraphClicked());
        this.topUsersBtn.addActionListener(l -> listenerI.onTopUsersClicked());
    }

    private void configurePanel() {
        this.setLayout(new FlowLayout(FlowLayout.CENTER, 50, 50));

        JPanel content = new JPanel();
        content.setLayout(new GridLayout(2,1));

        this.matchGraphBtn = new JButton("Matches evolution");
        this.topUsersBtn = new JButton("Most liked users");

        content.add(this.matchGraphBtn);
        content.add(this.topUsersBtn);

        this.add(content);

        this.setVisible(true);

    }

    @Override
    public void onTransitionTo() {
        ResourceFetcher rf = new ResourceFetcher("favicon.jpg");

        FrameManager frameManager = ControlPanelFrameManagerFactory.create();
        frameManager
                .configureFramePreferences(
                        "Control Panel",
                        this,
                        300,
                        300,
                        FrameManager.CENTER
                )
                .setResizable(false)
                .setCloseProcedure(listenerI::end, true);

        try {
            frameManager.setFavicon(rf.getResourceFile().getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
