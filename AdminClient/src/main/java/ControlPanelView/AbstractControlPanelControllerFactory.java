package ControlPanelView;

import ControlPanelController.ControlPanelControllerI;

public abstract class AbstractControlPanelControllerFactory {
    public abstract ControlPanelControllerI create();
}
