import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AdminPanel {
    private static final ExecutorService MESSAGING_SERVER_UI_EXECUTOR = Executors.newSingleThreadExecutor();

    public static void main(String[] args) {
        MESSAGING_SERVER_UI_EXECUTOR.execute(MinderServerUI.getInstance());
    }
}
