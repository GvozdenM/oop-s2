package MatchGraphPresenter;

import MatchGraphController.MatchGraphPresenterI;

public abstract class AbstractMatchGraphViewFactory {
    public abstract MatchGraphView create(MatchGraphPresenterI presenterI);
}
