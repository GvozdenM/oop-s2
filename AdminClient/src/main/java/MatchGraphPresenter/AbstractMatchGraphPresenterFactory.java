package MatchGraphPresenter;

import MatchGraphController.MatchGraphPresenterI;

public abstract class AbstractMatchGraphPresenterFactory {
    public abstract MatchGraphPresenterI create();
}
