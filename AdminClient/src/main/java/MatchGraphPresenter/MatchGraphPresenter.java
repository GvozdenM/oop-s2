package MatchGraphPresenter;

import MatchGraphController.MatchGraphPresenterI;
import MatchGraphInteractor.MatchGraphResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * The Match Graph Presenter is in charge of delegating requests from the attached MatchGraphController
 * in order to operate the view externally.
 * This class implements the methods and functions of its respective interface.
 * @see MatchGraphPresenterI
 */
public class MatchGraphPresenter implements MatchGraphPresenterI {
    private final MatchGraphView view;

    private List<Integer> vector;
    private String period;

    public MatchGraphPresenter(AbstractMatchGraphViewFactory matchGraphViewFactory){
        this.view = matchGraphViewFactory.create(this);
        this.period = this.view.getPeriod();
    }

    @Override
    public void init() {
        this.view.onTransitionTo();
    }

    @Override
    public void presentError(String errorMsg) {
        this.view.displayErrorMsg(errorMsg);
    }

    @Override
    public void changeGraphPeriod(String period) {
        this.period = period;
        presentGraph();
    }

    @Override
    public void presentGraphFromVector(MatchGraphResponse response) {
        this.vector = response.getMatches();

        presentGraph();
    }

    private void presentGraph() {
        if (this.vector == null) return;

        List<Integer> matches = new ArrayList<>();
        switch (this.period){
            case MatchGraphView.INTERVAL_DAY:
                matches = this.vector.subList(this.vector.size() - 25, this.vector.size());
                break;
            case MatchGraphView.INTERVAL_WEEK:
                matches = this.vector.subList(this.vector.size() - 169, this.vector.size());
                break;
            case MatchGraphView.INTERVAL_MONTH:
                matches = this.vector;
                break;
        }

        this.view.displayGraph(matches);
    }
}
