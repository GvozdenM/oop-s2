package MatchGraphPresenter;

import MatchGraphController.MatchGraphPresenterI;
import MatchGraphView.ConcreteMatchGraphViewFactory;

public class ConcreteMatchGraphPresenterFactory extends AbstractMatchGraphPresenterFactory {
    @Override
    public MatchGraphPresenterI create() {
        return new MatchGraphPresenter(
                new ConcreteMatchGraphViewFactory()
        );
    }
}
