package ControlPanelController;

public abstract class AbstractControlPanelPresenterFactory {
    public abstract ControlPanelPresenter create();
}
