package ControlPanelController;

/**
 * This interface provides the necessary functions and methods to implement in the ControlPanelPresenter
 */
public interface ControlPanelPresenter {
    /**
     * Initializes the associated view with the presenter.
     */
    void init();
}
