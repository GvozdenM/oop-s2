package ControlPanelController;

/**
 * This interface provides a map of methods and functions to be implemented by the ControlPanelController
 */
public interface ControlPanelControllerI {
    /**
     * This method calls for an implementation to transition to the Match Graph View and use case
     */
    void onMatchGraphClicked();

    /**
     * This method calls for an implementation to transition to the Top Users View and use case
     */
    void onTopUsersClicked();

    /**
     * This method is called whenever the displayed Swing JPanel is closed or exited.
     */
    void end();
}
