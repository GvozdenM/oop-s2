package ControlPanelController;

import MatchGraphController.MatchGraphPresenterI;
import MatchGraphPresenter.AbstractMatchGraphPresenterFactory;
import TopUsersController.AbstractTopUsersPresenterFactory;
import TopUsersController.TopUsersPresenter;

/**
 * This Class implements all methods and functions related to the link between the presenter and the requester of the
 * ControlPanel use case.
 * @see ControlPanelControllerI
 */
public class ControlPanelController implements ControlPanelControllerI {
    private final ControlPanelPresenter presenter;
    private final AbstractMatchGraphPresenterFactory matchGraphPresenterFactory;
    private final AbstractTopUsersPresenterFactory topUsersPresenterFactory;

    public ControlPanelController(ControlPanelPresenter presenter,
                                  AbstractMatchGraphPresenterFactory matchGraphPresenterFactory,
                                  AbstractTopUsersPresenterFactory topUsersPresenterFactory) {
        this.presenter = presenter;
        this.matchGraphPresenterFactory = matchGraphPresenterFactory;
        this.topUsersPresenterFactory = topUsersPresenterFactory;
    }

    @Override
    public void onMatchGraphClicked() {
        MatchGraphPresenterI matchGraphPresenter = this.matchGraphPresenterFactory.create();
        matchGraphPresenter.init();
    }

    @Override
    public void onTopUsersClicked() {
        TopUsersPresenter topUsersPresenter = this.topUsersPresenterFactory.create();
        topUsersPresenter.init();
    }

    @Override
    public void end() {

    }
}
