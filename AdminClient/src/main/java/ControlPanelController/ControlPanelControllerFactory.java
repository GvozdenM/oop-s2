package ControlPanelController;

import ControlPanelView.AbstractControlPanelControllerFactory;
import MatchGraphPresenter.ConcreteMatchGraphPresenterFactory;
import TopUsersPresenter.TopUsersPresenterFactory;

public class ControlPanelControllerFactory extends AbstractControlPanelControllerFactory {
    private final ControlPanelPresenter presenter;

    public ControlPanelControllerFactory(ControlPanelPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public ControlPanelControllerI create() {
        return new ControlPanelController(
                this.presenter,
                new ConcreteMatchGraphPresenterFactory(),
                new TopUsersPresenterFactory()
        );
    }
}
