package MatchGraphController;

public abstract class AbstractMatchGraphControllerFactory {
    public abstract MatchGraphControllerI create();
}
