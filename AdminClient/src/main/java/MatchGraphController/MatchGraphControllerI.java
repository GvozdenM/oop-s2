package MatchGraphController;

/**
 * This interface provides a map of methods and functions to be implemented by the MatchGraphController
 */
public interface MatchGraphControllerI {
    /**
     * This method initializes the presenter for the MatchGraph view and requests the info on previous matches
     * to the dedicated database.
     */
    void onLoad();

    /**
     * This method calls for a change in the view with the new period of evolution in the match graph view.
     * @param period period indicating the new graphical display setting
     */
    void onPeriodChanged(String period);
}
