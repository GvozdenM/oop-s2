package MatchGraphController;

import MessagingEndpoint.AdminClientEndpoint;

public class ConcreteMatchGraphControllerFactory extends AbstractMatchGraphControllerFactory {
    private final MatchGraphPresenterI matchGraphPresenter;

    public ConcreteMatchGraphControllerFactory(MatchGraphPresenterI matchGraphPresenter){
        this.matchGraphPresenter = matchGraphPresenter;
    }

    @Override
    public MatchGraphControllerI create() {
        return new MatchGraphController(
                this.matchGraphPresenter,
                AdminClientEndpoint.getInstance()
        );
    }
}
