package MatchGraphController;

import MatchGraphInteractor.MatchGraphResponse;

/**
 * This interface provides the necessary functions and methods to implement in the MatchGraphPresenter
 */
public interface MatchGraphPresenterI {
    /**
     * Initializes the associated view with the presenter.
     */
    void init();

    /**
     * This method is called for a display of an error in the current View
     * @param errorMsg Error message to be displayed.
     */
    void presentError(String errorMsg);

    /**
     * method to make view change graph data to selected period, obtaining the data frm the previously local stored
     * information on match evolution.
     * @param period The period the graph sould take
     */
    void changeGraphPeriod(String period);

    /**
     * Dumps data to match graph view and assigns it to local data container.
     * @param response callback response containing match data
     */
    void presentGraphFromVector(MatchGraphResponse response);
}
