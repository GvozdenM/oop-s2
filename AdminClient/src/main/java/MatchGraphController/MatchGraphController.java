package MatchGraphController;

import MatchGraphInteractor.MatchGraphRequesterFactory;
import MatchGraphInteractor.MatchGraphRequester;
import MatchGraphInteractor.MatchGraphResponse;
import Utils.Callbacks.CallbackAdapter;

/**
 * This Class implements all methods and functions related to the link between the presenter and the requester of the
 * Match Graph use case.
 * @see MatchGraphControllerI list of methods implemented.
 */
public class MatchGraphController implements MatchGraphControllerI {
    private final MatchGraphPresenterI presenterI;
    private final MatchGraphRequester<MatchGraphResponse> requester;

    public MatchGraphController(MatchGraphPresenterI presenterI,
                                MatchGraphRequesterFactory<MatchGraphResponse> requester){
        this.presenterI = presenterI;
        this.requester = requester.createMatchGraphRequester();
    }


    @Override
    public void onLoad() {
        this.requester.graph(
                new CallbackAdapter<>(
                        (onSuccessResponse) -> {
                            if(onSuccessResponse.isSuccess()) {
                                this.presenterI.presentGraphFromVector(onSuccessResponse);
                            }
                            else {
                                this.presenterI.presentError(onSuccessResponse.getErrorMsg());
                            }
                        },
                        (onFailResponse) -> presenterI.presentError(onFailResponse.getMessage()),
                        (onExceptionResponse) -> presenterI.presentError(onExceptionResponse.getMessage())
                )
        );
    }

    @Override
    public void onPeriodChanged(String period) {
        this.presenterI.changeGraphPeriod(period);
    }
}
