package MatchGraphView;

import Utils.Swing.FrameManager.FrameManager;

import MatchGraphController.AbstractMatchGraphControllerFactory;
import MatchGraphController.MatchGraphControllerI;
import MatchGraphPresenter.MatchGraphView;
import Utils.Views.ResourceFetcher;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.List;

/**
 * This class provides the main JPanel in MatchGraph View as well as attachers of parts like listeners and different
 * methods and functions for the Match Graph Use Case
 */
public class SwingMatchGraphView extends JPanel implements MatchGraphView {
    private final int PANEL_PADDING = 15;

    private final MatchGraphControllerI controllerI;

    private final SwingMatchGraphPanel graphPanel;

    private final JComboBox<String> cbPeriod;

    public SwingMatchGraphView(AbstractMatchGraphControllerFactory controllerFactory) {
        this.controllerI = controllerFactory.create();

        GridBagConstraints c = new GridBagConstraints();
        setPanelConstraints(c);

        JPanel jpNorthWrapper = new JPanel();
        setLayout(new BorderLayout());

        JPanel jpNorth = new JPanel(new GridLayout(1,1));

        JLabel jlTitle = new JLabel("Match Evolution      ");
        jpNorth.add(jlTitle);

        cbPeriod = new JComboBox<>();
        String periodItemPrefix = "Last ";
        cbPeriod.addItem(periodItemPrefix + INTERVAL_DAY);
        cbPeriod.addItem(periodItemPrefix + INTERVAL_WEEK);
        cbPeriod.addItem(periodItemPrefix + INTERVAL_MONTH);

        jpNorth.add(cbPeriod);
        jpNorthWrapper.add(jpNorth, c);

        this.add(jpNorthWrapper, BorderLayout.NORTH);

        graphPanel = new SwingMatchGraphPanel();

        this.add(graphPanel, BorderLayout.CENTER);

        JLabel xAxisTitle = new JLabel("Hours");
        xAxisTitle.setHorizontalAlignment(JLabel.CENTER);
        this.add(xAxisTitle, BorderLayout.SOUTH);

        attachListeners();
    }

    private void setPanelConstraints(GridBagConstraints c) {
        c.insets = new Insets(
                PANEL_PADDING,
                PANEL_PADDING,
                PANEL_PADDING,
                PANEL_PADDING);
    }

    private void attachListeners(){
        this.cbPeriod.addActionListener(
                actionEvent -> controllerI.onPeriodChanged(
                        getPeriod()
                )
        );
    }

    @Override
    public void onTransitionTo() {
        ResourceFetcher resourceFetcher = new ResourceFetcher("favicon.jpg");

        FrameManager frameManager = MatchGraphFrameManagerFactory.create();
        try {
            frameManager
                    .configureFramePreferences("Match Evolution", this,600, 600, FrameManager.CENTER)
                    .setResizable(false)
                    .setFavicon(resourceFetcher.getResourceFile().getPath());
            this.controllerI.onLoad();
        } catch (IOException ignore) {}
    }

    @Override
    public String getPeriod() {
        String aux = null;
        switch (cbPeriod.getSelectedIndex()){
            case 0:
                aux = INTERVAL_DAY;
                break;
            case 1:
                aux = INTERVAL_WEEK;
                break;
            case 2:
                aux = INTERVAL_MONTH;
                break;
        }
        return aux;
    }

    @Override
    public void onCloseSuccessfully() {
        FrameManager frameManager = MatchGraphFrameManagerFactory.create();
        frameManager
                .closeFrame();
    }

    @Override
    public void displayErrorMsg(String errorMsg) {
        SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(
                new JFrame(),
                "ERROR: " + errorMsg,
                "Warning",
                JOptionPane.WARNING_MESSAGE
        ));
    }

    @Override
    public void displayGraph(List<Integer> graphVector) {
        this.graphPanel.setValues(graphVector);
    }
}


