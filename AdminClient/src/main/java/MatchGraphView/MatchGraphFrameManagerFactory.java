package MatchGraphView;

import Utils.Swing.FrameManager.FrameManager;

public interface MatchGraphFrameManagerFactory {
    FrameManager frameManager = FrameManager.newInstance();

    static FrameManager create() {
        return frameManager;
    }
}
