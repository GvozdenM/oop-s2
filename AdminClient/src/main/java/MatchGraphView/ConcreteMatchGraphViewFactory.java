package MatchGraphView;

import MatchGraphController.ConcreteMatchGraphControllerFactory;
import MatchGraphController.MatchGraphPresenterI;
import MatchGraphPresenter.AbstractMatchGraphViewFactory;
import MatchGraphPresenter.MatchGraphView;

public class ConcreteMatchGraphViewFactory extends AbstractMatchGraphViewFactory {
    @Override
    public MatchGraphView create(MatchGraphPresenterI presenterI) {
        return new SwingMatchGraphView(
               new ConcreteMatchGraphControllerFactory(presenterI)
        );
    }
}
