package TopUsersController;

/**
 * This interface provides a map of methods and functions to be implemented by the TopUsersController
 */
public interface TopUsersControllerI {
    void onUserClicked();

    /**
     * This method initializes the presenter for the TopUsers view and requests the users info to the dedicated
     * database request.
     */
    void onLoad();
}
