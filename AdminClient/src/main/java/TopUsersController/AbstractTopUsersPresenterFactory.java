package TopUsersController;

public abstract class AbstractTopUsersPresenterFactory {
    public abstract TopUsersPresenter create();
}
