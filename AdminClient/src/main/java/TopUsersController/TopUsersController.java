package TopUsersController;


import TopUsersInteractor.TopUsersRequest;

import TopUsersInteractor.TopUsersRequesterFactory;
import TopUsersInteractor.TopUsersResponse;
import Utils.Callbacks.CallbackAdapter;
import org.jetbrains.annotations.NotNull;

/**
 * This Class implements all methods and functions related to the link between the presenter and the requester of the
 * Top Users use case.
 * @see TopUsersControllerI list of methods implemented.
 */
public class TopUsersController implements TopUsersControllerI {
    private final int TOP_USERS_LIST_LENGTH = 5;
    private final TopUsersRequesterFactory<TopUsersRequest, TopUsersResponse> topUsersRequesterFactory;
    private final TopUsersPresenter presenter;

    public TopUsersController(@NotNull TopUsersPresenter topUsersPresenter,
                              @NotNull TopUsersRequesterFactory<TopUsersRequest, TopUsersResponse> topUsersRequesterFactory) {
        this.topUsersRequesterFactory = topUsersRequesterFactory;
        this.presenter = topUsersPresenter;
    }

    @Override
    public void onUserClicked() {

    }

    @Override
    public void onLoad() {
        this.topUsersRequesterFactory.create().getTopUsers(
                new TopUsersRequest(TOP_USERS_LIST_LENGTH),
                new CallbackAdapter<>(
                        this.presenter::presentTopUsersList,
                        (onFailureResponse) -> this.presenter.presentNoConnection(),
                        (onExceptionResponse) -> this.presenter.presentNoConnection()
                )
        );
    }
}
