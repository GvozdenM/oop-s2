package TopUsersController;

import TopUsersInteractor.TopUsersResponse;

/**
 * This interface provides the necessary functions and methods to implement in the TopUsersPresenter
 */
public interface TopUsersPresenter {
    /**
     * Initializes the associated view with the presenter.
     */
    void init();

    /**
     * This method provides a user list to be passed and implemented in the view to show in order.
     * @param response user list
     */
    void presentTopUsersList(TopUsersResponse response);

    /**
     * This method is called for a display of an error in the current View whenever there is no connection as a result
     * from a callback.
     */
    void presentNoConnection();
}
