package TopUsersController;

import MessagingEndpoint.AdminClientEndpoint;

public class TopUsersControllerFactory {
    private final TopUsersPresenter presenter;

    public TopUsersControllerFactory(TopUsersPresenter presenter) {
        this.presenter = presenter;
    }

    public TopUsersControllerI create() {
        return new TopUsersController(this.presenter, AdminClientEndpoint.getInstance());
    }
}
