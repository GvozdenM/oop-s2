import ControlPanelController.ControlPanelPresenter;
import ControlPanelPresenter.ControlPanelPresenterFactory;

public class MinderServerUI implements Runnable {
    private static MinderServerUI UI = null;

    public static MinderServerUI getInstance() {
        if (null == UI) {
            UI = new MinderServerUI();
        }
        return UI;
    }

    @Override
    public void run() {
        ControlPanelPresenterFactory controlPanelPresenterFactory = new ControlPanelPresenterFactory();

        ControlPanelPresenter controlPanelPresenter = controlPanelPresenterFactory.create();

        controlPanelPresenter.init();
    }
}
